<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160118080146 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels_pro CHANGE destination destination VARCHAR(50) DEFAULT NULL, CHANGE country country VARCHAR(127) DEFAULT NULL, CHANGE star_rating star_rating SMALLINT DEFAULT NULL, CHANGE hotel_postal_code hotel_postal_code VARCHAR(30) DEFAULT NULL, CHANGE latitude latitude VARCHAR(20) DEFAULT NULL, CHANGE longitude longitude VARCHAR(20) DEFAULT NULL, CHANGE rooms_number rooms_number SMALLINT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels_pro CHANGE destination destination LONGTEXT DEFAULT NULL, CHANGE country country LONGTEXT DEFAULT NULL, CHANGE star_rating star_rating INT DEFAULT NULL, CHANGE hotel_postal_code hotel_postal_code LONGTEXT DEFAULT NULL, CHANGE latitude latitude LONGTEXT DEFAULT NULL, CHANGE longitude longitude LONGTEXT DEFAULT NULL, CHANGE rooms_number rooms_number INT DEFAULT NULL');
    }
}

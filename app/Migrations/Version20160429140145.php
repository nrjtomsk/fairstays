<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160429140145 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `common_destinations` (`id` INT AUTO_INCREMENT NOT NULL, `country_name` VARCHAR(127) NOT NULL, `country_code` VARCHAR(3) NOT NULL, `state` VARCHAR(127) DEFAULT NULL, `city_name` VARCHAR(127) NOT NULL, `created_at` DATETIME NOT NULL, `updated_at` DATETIME NOT NULL, `deleted_at` DATETIME DEFAULT NULL, INDEX idx_common_destinations_city_name (city_name), UNIQUE INDEX uk_common_destinations_country_name (country_name, state, city_name), UNIQUE INDEX uk_common_destinations_country_code (country_code, state, city_name), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `common_hotels` (`id` INT AUTO_INCREMENT NOT NULL, `hotel_name` VARCHAR(255) NOT NULL, `hotels_pro_hotel_code` VARCHAR(6) DEFAULT NULL, `travel_guru_hotel_code` VARCHAR(8) DEFAULT NULL, `created_at` DATETIME NOT NULL, `updated_at` DATETIME NOT NULL, `deleted_at` DATETIME DEFAULT NULL, `common_destination_id` INT DEFAULT NULL, UNIQUE INDEX UNIQ_9F3465B07CD6F512 (`hotels_pro_hotel_code`), UNIQUE INDEX UNIQ_9F3465B0D71919EB (`travel_guru_hotel_code`), INDEX IDX_9F3465B0BF10FA21 (`common_destination_id`), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `common_hotels` ADD CONSTRAINT FK_9F3465B0BF10FA21 FOREIGN KEY (`common_destination_id`) REFERENCES `common_destinations` (`id`) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE common_hotels DROP FOREIGN KEY FK_9F3465B0BF10FA21');
        $this->addSql('DROP TABLE `common_destinations`');
        $this->addSql('DROP TABLE `common_hotels`');
    }
}

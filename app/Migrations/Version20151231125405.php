<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151231125405 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE access_tokens (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, token VARCHAR(64) NOT NULL, expires_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_58D184BCA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking_requests (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, hotel_id VARCHAR(255) DEFAULT NULL, correlation_id VARCHAR(64) NOT NULL, transaction_identifier VARCHAR(64) NOT NULL, hotel_code VARCHAR(255) NOT NULL, requested_currency VARCHAR(4) NOT NULL, time_span_start_date DATE NOT NULL, time_span_end_date DATE NOT NULL, room_type_number_of_units SMALLINT DEFAULT NULL, room_name LONGTEXT NOT NULL, room_type_code VARCHAR(127) DEFAULT NULL, rate_plan_code VARCHAR(127) DEFAULT NULL, total_amount_before_tax DOUBLE PRECISION DEFAULT \'0\' NOT NULL, total_taxes_amount DOUBLE PRECISION DEFAULT \'0\' NOT NULL, room_price_per_night DOUBLE PRECISION DEFAULT \'0\' NOT NULL, guest_charges DOUBLE PRECISION DEFAULT \'0\' NOT NULL, price_before VARCHAR(255) DEFAULT NULL, tax_before VARCHAR(255) DEFAULT NULL, price VARCHAR(255) DEFAULT NULL, tax VARCHAR(255) DEFAULT NULL, comment_text LONGTEXT DEFAULT NULL, profile_name_prefix VARCHAR(4) NOT NULL, profile_given_name VARCHAR(127) NOT NULL, profile_middle_name VARCHAR(127) DEFAULT NULL, profile_last_name VARCHAR(127) NOT NULL, profile_phone_area_city_code VARCHAR(4) NOT NULL, profile_phone_country_access_code VARCHAR(4) NOT NULL, profile_phone_extension VARCHAR(15) NOT NULL, profile_phone_number VARCHAR(20) NOT NULL, profile_phone_tech_type VARCHAR(20) NOT NULL, profile_email VARCHAR(127) NOT NULL, profile_address_line1 VARCHAR(255) NOT NULL, profile_address_line2 VARCHAR(255) NOT NULL, profile_city_name VARCHAR(127) NOT NULL, profile_postal_code VARCHAR(15) NOT NULL, profile_address_state_prov VARCHAR(127) NOT NULL, profile_address_state_prov_code VARCHAR(127) NOT NULL, profile_country_name VARCHAR(127) NOT NULL, profile_country_name_code VARCHAR(127) NOT NULL, guarantee_type VARCHAR(16) NOT NULL, guest_counts LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', booking_id VARCHAR(32) DEFAULT NULL, travel_guru_booking_id VARCHAR(32) DEFAULT NULL, amount_after_tax DOUBLE PRECISION DEFAULT NULL, payu_amount DOUBLE PRECISION DEFAULT NULL, payu_transaction_id LONGTEXT DEFAULT NULL, payu_mih_pay_id LONGTEXT DEFAULT NULL, total_amount_before_tax_external DOUBLE PRECISION DEFAULT NULL, total_taxes_amount_external DOUBLE PRECISION DEFAULT NULL, status VARCHAR(255) NOT NULL, public_booking_id VARCHAR(12) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, tracking_id VARCHAR(32) DEFAULT NULL, hotels_pro_cancel_response_id INT DEFAULT NULL, hotels_pro_agency_reference_number VARCHAR(32) DEFAULT NULL, hotels_pro_booking_status VARCHAR(255) DEFAULT NULL, hotels_pro_cancellation_note VARCHAR(255) DEFAULT NULL, meta_data_dto LONGTEXT DEFAULT NULL, INDEX IDX_40B148DCA76ED395 (user_id), INDEX IDX_40B148DC3243BB18 (hotel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_phone_numbers (id INT AUTO_INCREMENT NOT NULL, booking_request_id INT NOT NULL, area_city_code VARCHAR(8) DEFAULT NULL, country_access_code VARCHAR(8) DEFAULT NULL, phone_number VARCHAR(25) DEFAULT NULL, phone_tech_type VARCHAR(4) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_C644054CDBE4DBD4 (booking_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency_convertor (id INT AUTO_INCREMENT NOT NULL, currency_code VARCHAR(15) NOT NULL, currency_name VARCHAR(150) NOT NULL, created_date DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE files (id INT AUTO_INCREMENT NOT NULL, path VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, mime_type VARCHAR(255) NOT NULL, size NUMERIC(10, 0) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotels (id VARCHAR(255) NOT NULL, trip_advisor_location_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, class VARCHAR(255) DEFAULT NULL, location VARCHAR(1023) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, address_line1 VARCHAR(511) DEFAULT NULL, address_line2 VARCHAR(511) DEFAULT NULL, area VARCHAR(255) DEFAULT NULL, formatted_address VARCHAR(1023) DEFAULT NULL, overview LONGTEXT DEFAULT NULL, review_rating DOUBLE PRECISION DEFAULT NULL, review_count INT DEFAULT NULL, latitude VARCHAR(20) DEFAULT NULL, longitude VARCHAR(20) DEFAULT NULL, default_check_in_time VARCHAR(255) DEFAULT NULL, default_check_out_time VARCHAR(255) DEFAULT NULL, star VARCHAR(255) DEFAULT NULL, group_id INT DEFAULT NULL, group_name VARCHAR(255) DEFAULT NULL, image_path VARCHAR(511) DEFAULT NULL, search_key VARCHAR(255) DEFAULT NULL, area_seo_id VARCHAR(255) DEFAULT NULL, secondary_area_ids VARCHAR(1023) DEFAULT NULL, secondary_area_name VARCHAR(255) DEFAULT NULL, number_of_floors INT DEFAULT NULL, number_of_rooms INT DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, pin_code VARCHAR(15) DEFAULT NULL, theme_list VARCHAR(1023) DEFAULT NULL, category_list VARCHAR(1023) DEFAULT NULL, city_zone VARCHAR(255) DEFAULT NULL, week_day_rank INT DEFAULT NULL, week_end_rank INT DEFAULT NULL, UNIQUE INDEX UNIQ_E402F625A8E97559 (trip_advisor_location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotel_image (id INT AUTO_INCREMENT NOT NULL, image_path VARCHAR(1023) NOT NULL, title LONGTEXT NOT NULL, VendorID VARCHAR(255) NOT NULL, INDEX IDX_26E9CA9B1593D363 (VendorID), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotels_pro (id INT AUTO_INCREMENT NOT NULL, hotel_code LONGTEXT DEFAULT NULL, destination LONGTEXT DEFAULT NULL, country LONGTEXT DEFAULT NULL, hotel_name LONGTEXT DEFAULT NULL, star_rating INT DEFAULT NULL, hotel_address LONGTEXT DEFAULT NULL, hotel_postal_code LONGTEXT DEFAULT NULL, hotel_phone_number LONGTEXT DEFAULT NULL, hotel_area LONGTEXT DEFAULT NULL, latitude LONGTEXT DEFAULT NULL, longitude LONGTEXT DEFAULT NULL, images_urls LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', rooms_number INT DEFAULT NULL, property_amenities LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', room_amenities LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotels_city (city_id INT AUTO_INCREMENT NOT NULL, country_name VARCHAR(200) NOT NULL, city_name VARCHAR(200) NOT NULL, state VARCHAR(200) NOT NULL, country_code VARCHAR(50) NOT NULL, hotels_pro_id VARCHAR(255) DEFAULT NULL, status INT NOT NULL, updt INT NOT NULL, PRIMARY KEY(city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hotels_destinations (city_id INT AUTO_INCREMENT NOT NULL, hotels_pro_id VARCHAR(255) DEFAULT NULL, country_name VARCHAR(200) NOT NULL, city_name VARCHAR(200) NOT NULL, state VARCHAR(200) DEFAULT NULL, country_code VARCHAR(50) DEFAULT NULL, PRIMARY KEY(city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mobile_login_validation (id INT AUTO_INCREMENT NOT NULL, otp VARCHAR(30) NOT NULL, phone_number VARCHAR(255) NOT NULL, status VARCHAR(20) NOT NULL, expiry_date DATETIME NOT NULL, created_date DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip_advisor_locations (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, rating DOUBLE PRECISION DEFAULT NULL, rating_image_url LONGTEXT DEFAULT NULL, num_reviews INT DEFAULT NULL, ranking INT DEFAULT NULL, ranking_out_of INT DEFAULT NULL, price_level LONGTEXT DEFAULT NULL, latitude LONGTEXT DEFAULT NULL, longitude LONGTEXT DEFAULT NULL, address_string LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_43D83E7864D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip_advisor_temp (id INT AUTO_INCREMENT NOT NULL, hotel_id VARCHAR(255) NOT NULL, hotel_name VARCHAR(255) DEFAULT NULL, rating VARCHAR(255) DEFAULT NULL, INDEX IDX_A7BE8E723243BB18 (hotel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, avatar_id INT DEFAULT NULL, currency_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, full_name VARCHAR(255) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(255) NOT NULL, roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_1483A5E986383B10 (avatar_id), INDEX IDX_1483A5E938248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbbc_money_doctrine_storage_ratios (id INT AUTO_INCREMENT NOT NULL, currency_code VARCHAR(3) NOT NULL, ratio DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_1168A609FDA273EC (currency_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tbbc_money_ratio_history (id INT AUTO_INCREMENT NOT NULL, currency_code VARCHAR(3) NOT NULL, reference_currency_code VARCHAR(3) NOT NULL, ratio DOUBLE PRECISION NOT NULL, saved_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE access_tokens ADD CONSTRAINT FK_58D184BCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking_requests ADD CONSTRAINT FK_40B148DCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking_requests ADD CONSTRAINT FK_40B148DC3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotels (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_phone_numbers ADD CONSTRAINT FK_C644054CDBE4DBD4 FOREIGN KEY (booking_request_id) REFERENCES booking_requests (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE hotels ADD CONSTRAINT FK_E402F625A8E97559 FOREIGN KEY (trip_advisor_location_id) REFERENCES trip_advisor_locations (id)');
        $this->addSql('ALTER TABLE hotel_image ADD CONSTRAINT FK_26E9CA9B1593D363 FOREIGN KEY (VendorID) REFERENCES hotels (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip_advisor_temp ADD CONSTRAINT FK_A7BE8E723243BB18 FOREIGN KEY (hotel_id) REFERENCES hotels (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E986383B10 FOREIGN KEY (avatar_id) REFERENCES files (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E938248176 FOREIGN KEY (currency_id) REFERENCES currency_convertor (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_phone_numbers DROP FOREIGN KEY FK_C644054CDBE4DBD4');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E938248176');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E986383B10');
        $this->addSql('ALTER TABLE booking_requests DROP FOREIGN KEY FK_40B148DC3243BB18');
        $this->addSql('ALTER TABLE hotel_image DROP FOREIGN KEY FK_26E9CA9B1593D363');
        $this->addSql('ALTER TABLE trip_advisor_temp DROP FOREIGN KEY FK_A7BE8E723243BB18');
        $this->addSql('ALTER TABLE hotels DROP FOREIGN KEY FK_E402F625A8E97559');
        $this->addSql('ALTER TABLE access_tokens DROP FOREIGN KEY FK_58D184BCA76ED395');
        $this->addSql('ALTER TABLE booking_requests DROP FOREIGN KEY FK_40B148DCA76ED395');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE access_tokens');
        $this->addSql('DROP TABLE booking_requests');
        $this->addSql('DROP TABLE contact_phone_numbers');
        $this->addSql('DROP TABLE currency_convertor');
        $this->addSql('DROP TABLE files');
        $this->addSql('DROP TABLE hotels');
        $this->addSql('DROP TABLE hotel_image');
        $this->addSql('DROP TABLE hotels_pro');
        $this->addSql('DROP TABLE hotels_city');
        $this->addSql('DROP TABLE hotels_destinations');
        $this->addSql('DROP TABLE mobile_login_validation');
        $this->addSql('DROP TABLE trip_advisor_locations');
        $this->addSql('DROP TABLE trip_advisor_temp');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE tbbc_money_doctrine_storage_ratios');
        $this->addSql('DROP TABLE tbbc_money_ratio_history');
    }
}

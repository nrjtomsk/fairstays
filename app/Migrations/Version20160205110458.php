<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160205110458 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE hotels_city');
        $this->addSql('ALTER TABLE booking_requests DROP FOREIGN KEY FK_40B148DC3243BB18');
        $this->addSql('DROP INDEX IDX_40B148DC3243BB18 ON booking_requests');
        $this->addSql('ALTER TABLE booking_requests DROP hotel_id, CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hotels_city (city_id INT AUTO_INCREMENT NOT NULL, country_name VARCHAR(200) NOT NULL, city_name VARCHAR(200) NOT NULL, state VARCHAR(200) NOT NULL, country_code VARCHAR(50) NOT NULL, hotels_pro_id VARCHAR(255) DEFAULT NULL, status INT NOT NULL, updt INT NOT NULL, PRIMARY KEY(city_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking_requests ADD hotel_id VARCHAR(255) DEFAULT NULL, CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE booking_requests ADD CONSTRAINT FK_40B148DC3243BB18 FOREIGN KEY (hotel_id) REFERENCES hotels (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_40B148DC3243BB18 ON booking_requests (hotel_id)');
    }
}

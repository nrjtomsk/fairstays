<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160317175812 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests ADD stripe_token VARCHAR(255) DEFAULT NULL, ADD stripe_charge_id VARCHAR(255) DEFAULT NULL, ADD stripe_transaction_id VARCHAR(255) DEFAULT NULL, ADD stripe_response LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', DROP service_provider_currency, DROP currency_rate');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests ADD service_provider_currency VARCHAR(4) NOT NULL, ADD currency_rate DOUBLE PRECISION DEFAULT \'0\' NOT NULL, DROP stripe_token, DROP stripe_charge_id, DROP stripe_transaction_id, DROP stripe_response');
    }
}

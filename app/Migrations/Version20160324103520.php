<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160324103520 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE price_selected_hotels CHANGE check_in_date check_in_date VARCHAR(10) NOT NULL, CHANGE city_code city_code VARCHAR(10) NOT NULL, CHANGE selected_hotels selected_hotels LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', CHANGE insert_time insert_time DATE NOT NULL'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE price_selected_hotels CHANGE check_in_date check_in_date VARCHAR(10) DEFAULT NULL, CHANGE city_code city_code VARCHAR(10) DEFAULT NULL, CHANGE selected_hotels selected_hotels LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', CHANGE insert_time insert_time VARCHAR(10) DEFAULT NULL'
        );
    }
}

<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160301154157 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hotels DROP FOREIGN KEY FK_E402F625A8E97559');
        $this->addSql('CREATE TABLE `trip_advisor_rating` (id INT AUTO_INCREMENT NOT NULL, trip_advisor_id VARCHAR(12) NOT NULL, city_name VARCHAR(127) DEFAULT NULL, state_name VARCHAR(127) DEFAULT NULL, country_name VARCHAR(127) DEFAULT NULL, rating DOUBLE PRECISION DEFAULT NULL, multiplied_rating DOUBLE PRECISION DEFAULT NULL, response_body LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE trip_advisor_locations');
        $this->addSql('DROP TABLE trip_advisor_temp');
        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_E402F625A8E97559 ON hotels');
        $this->addSql('ALTER TABLE hotels CHANGE trip_advisor_location_id trip_advisor_rating_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels ADD CONSTRAINT FK_E402F6255E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES `trip_advisor_rating` (id)');
        $this->addSql('CREATE INDEX IDX_E402F6255E0C9B04 ON hotels (trip_advisor_rating_id)');
        $this->addSql('ALTER TABLE hotels_pro ADD trip_advisor_rating_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels_pro ADD CONSTRAINT FK_952BDB885E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES `trip_advisor_rating` (id)');
        $this->addSql('CREATE INDEX IDX_952BDB885E0C9B04 ON hotels_pro (trip_advisor_rating_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hotels DROP FOREIGN KEY FK_E402F6255E0C9B04');
        $this->addSql('ALTER TABLE hotels_pro DROP FOREIGN KEY FK_952BDB885E0C9B04');
        $this->addSql('CREATE TABLE trip_advisor_locations (id INT AUTO_INCREMENT NOT NULL, location_id INT NOT NULL, rating DOUBLE PRECISION DEFAULT NULL, rating_image_url LONGTEXT DEFAULT NULL, num_reviews INT DEFAULT NULL, ranking INT DEFAULT NULL, ranking_out_of INT DEFAULT NULL, price_level LONGTEXT DEFAULT NULL, latitude LONGTEXT DEFAULT NULL, longitude LONGTEXT DEFAULT NULL, address_string LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_43D83E7864D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip_advisor_temp (id INT AUTO_INCREMENT NOT NULL, hotel_id VARCHAR(255) NOT NULL, hotel_name VARCHAR(255) DEFAULT NULL, rating VARCHAR(255) DEFAULT NULL, INDEX IDX_A7BE8E723243BB18 (hotel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE trip_advisor_temp ADD CONSTRAINT FK_A7BE8E723243BB18 FOREIGN KEY (hotel_id) REFERENCES hotels (id)');
        $this->addSql('DROP TABLE `trip_advisor_rating`');
        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_E402F6255E0C9B04 ON hotels');
        $this->addSql('ALTER TABLE hotels CHANGE trip_advisor_rating_id trip_advisor_location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels ADD CONSTRAINT FK_E402F625A8E97559 FOREIGN KEY (trip_advisor_location_id) REFERENCES trip_advisor_locations (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E402F625A8E97559 ON hotels (trip_advisor_location_id)');
        $this->addSql('DROP INDEX IDX_952BDB885E0C9B04 ON hotels_pro');
        $this->addSql('ALTER TABLE hotels_pro DROP trip_advisor_rating_id');
    }
}

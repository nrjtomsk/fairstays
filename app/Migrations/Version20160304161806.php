<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160304161806 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels DROP FOREIGN KEY FK_E402F6255E0C9B04');
        $this->addSql('ALTER TABLE hotels ADD CONSTRAINT FK_E402F6255E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES `trip_advisor_rating` (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE hotels_pro DROP FOREIGN KEY FK_952BDB885E0C9B04');
        $this->addSql('ALTER TABLE hotels_pro ADD CONSTRAINT FK_952BDB885E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES `trip_advisor_rating` (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_634777F793C17661 ON trip_advisor_rating (trip_advisor_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests CHANGE price_before price_before VARCHAR(255) DEFAULT NULL, CHANGE tax_before tax_before VARCHAR(255) DEFAULT NULL, CHANGE price price VARCHAR(255) DEFAULT NULL, CHANGE tax tax VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE hotels DROP FOREIGN KEY FK_E402F6255E0C9B04');
        $this->addSql('ALTER TABLE hotels ADD CONSTRAINT FK_E402F6255E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES trip_advisor_rating (id)');
        $this->addSql('ALTER TABLE hotels_pro DROP FOREIGN KEY FK_952BDB885E0C9B04');
        $this->addSql('ALTER TABLE hotels_pro ADD CONSTRAINT FK_952BDB885E0C9B04 FOREIGN KEY (trip_advisor_rating_id) REFERENCES trip_advisor_rating (id)');
        $this->addSql('DROP INDEX UNIQ_634777F793C17661 ON trip_advisor_rating');
    }
}

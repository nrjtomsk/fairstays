<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160504155842 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `common_destination_mapping` (`id` INT AUTO_INCREMENT NOT NULL, `service_name` VARCHAR(12) NOT NULL, `service_destination_identifier` VARCHAR(32) NOT NULL, `created_at` DATETIME NOT NULL, `updated_at` DATETIME NOT NULL, `deleted_at` DATETIME DEFAULT NULL, `common_destination_id` INT NOT NULL, INDEX IDX_FBAA8CF0BF10FA21 (`common_destination_id`), INDEX common_destination_mapping_service_destination (service_name, service_destination_identifier), PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `common_destination_mapping` ADD CONSTRAINT FK_FBAA8CF0BF10FA21 FOREIGN KEY (`common_destination_id`) REFERENCES `common_destinations` (`id`) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `common_destination_mapping`');
    }
}

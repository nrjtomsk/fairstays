<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160316153436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests ADD pure_price_amount_service_provider DOUBLE PRECISION DEFAULT NULL, ADD pure_price_currency_service_provider VARCHAR(3) DEFAULT NULL, ADD pure_tax_amount_service_provider DOUBLE PRECISION DEFAULT NULL, ADD pure_tax_currency_service_provider VARCHAR(3) DEFAULT NULL, ADD pure_price_amount_internal DOUBLE PRECISION DEFAULT NULL, ADD pure_price_currency_internal VARCHAR(3) DEFAULT NULL, ADD pure_tax_amount_internal DOUBLE PRECISION DEFAULT NULL, ADD pure_tax_currency_internal VARCHAR(3) DEFAULT NULL, ADD sale_price_amount_service_provider DOUBLE PRECISION DEFAULT NULL, ADD sale_price_currency_service_provider VARCHAR(3) DEFAULT NULL, ADD sale_tax_amount_service_provider DOUBLE PRECISION DEFAULT NULL, ADD sale_tax_currency_service_provider VARCHAR(3) DEFAULT NULL, ADD sale_price_amount_internal DOUBLE PRECISION DEFAULT NULL, ADD sale_price_currency_internal VARCHAR(3) DEFAULT NULL, ADD sale_tax_amount_internal DOUBLE PRECISION DEFAULT NULL, ADD sale_tax_currency_internal VARCHAR(3) DEFAULT NULL, ADD pay_price_amount_payment_system DOUBLE PRECISION DEFAULT NULL, ADD pay_price_currency_payment_system VARCHAR(3) DEFAULT NULL, ADD pay_price_amount_internal DOUBLE PRECISION DEFAULT NULL, ADD pay_price_currency_internal VARCHAR(3) DEFAULT NULL, DROP price_before, DROP tax_before, DROP price, DROP tax');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE booking_requests ADD price_before VARCHAR(255) DEFAULT NULL, ADD tax_before VARCHAR(255) DEFAULT NULL, ADD price VARCHAR(255) DEFAULT NULL, ADD tax VARCHAR(255) DEFAULT NULL, DROP pure_price_amount_service_provider, DROP pure_price_currency_service_provider, DROP pure_tax_amount_service_provider, DROP pure_tax_currency_service_provider, DROP pure_price_amount_internal, DROP pure_price_currency_internal, DROP pure_tax_amount_internal, DROP pure_tax_currency_internal, DROP sale_price_amount_service_provider, DROP sale_price_currency_service_provider, DROP sale_tax_amount_service_provider, DROP sale_tax_currency_service_provider, DROP sale_price_amount_internal, DROP sale_price_currency_internal, DROP sale_tax_amount_internal, DROP sale_tax_currency_internal, DROP pay_price_amount_payment_system, DROP pay_price_currency_payment_system, DROP pay_price_amount_internal, DROP pay_price_currency_internal');
    }
}

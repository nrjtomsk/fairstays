-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 11 2015 г., 12:39
-- Версия сервера: 5.6.24
-- Версия PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `costclub_fairstay`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_login`
--

DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `emailid` varchar(200) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `permissions` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `admin_login_detail`
--

DROP TABLE IF EXISTS `admin_login_detail`;
CREATE TABLE IF NOT EXISTS `admin_login_detail` (
  `id` int(10) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_email` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `loginDate` datetime NOT NULL,
  `logoutDate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `attractions`
--

DROP TABLE IF EXISTS `attractions`;
CREATE TABLE IF NOT EXISTS `attractions` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `supplier_id` varchar(25) NOT NULL,
  `landmark` varchar(50) NOT NULL,
  `distance` varchar(25) NOT NULL,
  `hours` varchar(10) NOT NULL,
  `mins` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `bank_details`
--

DROP TABLE IF EXISTS `bank_details`;
CREATE TABLE IF NOT EXISTS `bank_details` (
  `id` int(11) NOT NULL,
  `supplier_id` varchar(25) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_address1` varchar(200) NOT NULL,
  `bank_address2` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip_code` varchar(20) NOT NULL,
  `ifsc_code` varchar(20) NOT NULL,
  `swift_code` varchar(50) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `bookingdetials`
--

DROP TABLE IF EXISTS `bookingdetials`;
CREATE TABLE IF NOT EXISTS `bookingdetials` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bookingId` varchar(50) NOT NULL,
  `api_bookingid` varchar(50) NOT NULL,
  `ConfirmationNo` varchar(250) NOT NULL,
  `ReferenceNo` varchar(250) NOT NULL,
  `TripId` varchar(250) NOT NULL,
  `tbo_status` text NOT NULL,
  `bookingDate` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `cancelStatus` varchar(50) NOT NULL,
  `cancelId` varchar(50) NOT NULL,
  `cancelDate` datetime NOT NULL,
  `cancel_charges` varchar(150) NOT NULL,
  `hotelId` varchar(50) NOT NULL,
  `hotel_id` varchar(15) NOT NULL,
  `hotelName` varchar(200) NOT NULL,
  `hotelAddress` varchar(500) NOT NULL,
  `hotelCity` varchar(100) NOT NULL,
  `hotelPhone` varchar(100) NOT NULL,
  `guestName` varchar(100) NOT NULL,
  `guestLastName` varchar(100) NOT NULL,
  `guestEmail` varchar(200) NOT NULL,
  `guestPhone` varchar(20) NOT NULL,
  `checkIn` date NOT NULL,
  `checkOut` date NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `roomType` varchar(300) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) NOT NULL,
  `baseFare` double NOT NULL,
  `serviceTax` double NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `xml_price` float NOT NULL,
  `markup` varchar(50) NOT NULL,
  `refund_amt` text NOT NULL,
  `pg_charges` varchar(150) NOT NULL,
  `cc_service_charge` varchar(150) NOT NULL,
  `extraGuest` double NOT NULL,
  `manual_refund_amt` varchar(150) NOT NULL,
  `total_refund_amt` varchar(150) NOT NULL,
  `cancellationPolicy` text NOT NULL,
  `hotelPolicy` text NOT NULL,
  `termsAndCondition` text NOT NULL,
  `API_TYPE` varchar(50) NOT NULL,
  `promo_code` varchar(50) NOT NULL,
  `promo_value` varchar(25) NOT NULL,
  `Inclusions` text NOT NULL,
  `negotiation_id` varchar(25) NOT NULL,
  `pay_later` char(1) NOT NULL,
  `first_cancel_date` datetime NOT NULL,
  `amount_remain` bigint(10) NOT NULL,
  `amount_paid` bigint(10) NOT NULL,
  `pay_later_status` varchar(50) NOT NULL,
  `booked_device` varchar(50) NOT NULL DEFAULT 'web'
) ENGINE=InnoDB AUTO_INCREMENT=296 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `bookingdetials_backup`
--

DROP TABLE IF EXISTS `bookingdetials_backup`;
CREATE TABLE IF NOT EXISTS `bookingdetials_backup` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bookingId` varchar(50) NOT NULL,
  `api_bookingid` varchar(50) NOT NULL,
  `ConfirmationNo` varchar(250) NOT NULL,
  `ReferenceNo` varchar(250) NOT NULL,
  `TripId` varchar(250) NOT NULL,
  `tbo_status` text NOT NULL,
  `bookingDate` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `cancelStatus` varchar(50) NOT NULL,
  `cancelId` varchar(50) NOT NULL,
  `cancelDate` datetime NOT NULL,
  `cancel_charges` varchar(150) NOT NULL,
  `hotelId` varchar(50) NOT NULL,
  `hotel_id` varchar(15) NOT NULL,
  `hotelName` varchar(200) NOT NULL,
  `hotelAddress` varchar(500) NOT NULL,
  `hotelCity` varchar(100) NOT NULL,
  `hotelPhone` varchar(100) NOT NULL,
  `guestName` varchar(100) NOT NULL,
  `guestLastName` varchar(100) NOT NULL,
  `guestEmail` varchar(200) NOT NULL,
  `guestPhone` varchar(20) NOT NULL,
  `checkIn` date NOT NULL,
  `checkOut` date NOT NULL,
  `noOfRooms` int(11) NOT NULL,
  `roomType` varchar(300) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) NOT NULL,
  `baseFare` double NOT NULL,
  `serviceTax` double NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `xml_price` float NOT NULL,
  `markup` varchar(50) NOT NULL,
  `refund_amt` text NOT NULL,
  `pg_charges` varchar(150) NOT NULL,
  `cc_service_charge` varchar(150) NOT NULL,
  `extraGuest` double NOT NULL,
  `manual_refund_amt` varchar(150) NOT NULL,
  `total_refund_amt` varchar(150) NOT NULL,
  `cancellationPolicy` text NOT NULL,
  `hotelPolicy` text NOT NULL,
  `termsAndCondition` text NOT NULL,
  `API_TYPE` varchar(50) NOT NULL,
  `promo_code` varchar(50) NOT NULL,
  `promo_value` varchar(25) NOT NULL,
  `Inclusions` text NOT NULL,
  `negotiation_id` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `book_now_paylater`
--

DROP TABLE IF EXISTS `book_now_paylater`;
CREATE TABLE IF NOT EXISTS `book_now_paylater` (
  `PRI_KEY` varchar(50) NOT NULL,
  `discount` int(10) NOT NULL,
  `hour` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cancel_tickets`
--

DROP TABLE IF EXISTS `cancel_tickets`;
CREATE TABLE IF NOT EXISTS `cancel_tickets` (
  `id` int(11) NOT NULL,
  `booking_id` varchar(255) NOT NULL,
  `source` varchar(50) NOT NULL,
  `status_code` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `description` varchar(56) NOT NULL,
  `flight_type` varchar(45) NOT NULL,
  `pax_email` varchar(50) NOT NULL,
  `date_cancel` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `city_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `city_code` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `city_int`
--

DROP TABLE IF EXISTS `city_int`;
CREATE TABLE IF NOT EXISTS `city_int` (
  `id` int(11) NOT NULL,
  `city` varchar(70) NOT NULL,
  `city_code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `country_state_city`
--

DROP TABLE IF EXISTS `country_state_city`;
CREATE TABLE IF NOT EXISTS `country_state_city` (
  `id` int(20) NOT NULL,
  `country` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `city` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1453 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_booking_detail`
--

DROP TABLE IF EXISTS `crs_booking_detail`;
CREATE TABLE IF NOT EXISTS `crs_booking_detail` (
  `id` int(11) NOT NULL,
  `confirm_no` text NOT NULL,
  `promotion_id` int(200) NOT NULL,
  `bookingref` varchar(100) NOT NULL,
  `booking_of` varchar(50) NOT NULL,
  `booking_type` varchar(50) NOT NULL,
  `supp_id` text NOT NULL,
  `agent_id` text NOT NULL,
  `staff_id` int(200) NOT NULL,
  `h_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(1000) NOT NULL,
  `guests` text NOT NULL,
  `address_line1` varchar(1000) NOT NULL,
  `address_line2` varchar(1000) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pin_code` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `other_no` int(20) NOT NULL,
  `room_type` varchar(50) NOT NULL,
  `room_name` text NOT NULL,
  `booking_date` date NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `no_rooms` int(20) NOT NULL,
  `people` text NOT NULL,
  `break_f` varchar(50) NOT NULL,
  `lunch` varchar(50) NOT NULL,
  `dinner` varchar(50) NOT NULL,
  `min_occupancy` text NOT NULL,
  `max_occupancy` text NOT NULL,
  `no_nights` text NOT NULL,
  `agent_price` varchar(200) NOT NULL,
  `room_price` text NOT NULL,
  `addon_id` varchar(100) NOT NULL,
  `addon_price` varchar(100) NOT NULL,
  `tax` varchar(100) NOT NULL,
  `total_price` varchar(200) NOT NULL,
  `promo_discount` int(200) NOT NULL,
  `currency_type` varchar(50) NOT NULL,
  `booking_status` text NOT NULL,
  `supplier_view` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_book_hotel_tickets`
--

DROP TABLE IF EXISTS `crs_book_hotel_tickets`;
CREATE TABLE IF NOT EXISTS `crs_book_hotel_tickets` (
  `id` int(25) NOT NULL,
  `hotel_id` varchar(100) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `single_qty` int(25) NOT NULL,
  `double_qty` int(25) NOT NULL,
  `child_bed` int(25) NOT NULL,
  `alloc_id` varchar(300) NOT NULL,
  `bookingref` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `lead_paxname` varchar(400) NOT NULL,
  `pax_addr` varchar(2000) NOT NULL,
  `pax_city` varchar(300) NOT NULL,
  `pax_state` varchar(300) NOT NULL,
  `pax_pincode` varchar(30) NOT NULL,
  `cost_pernight` float NOT NULL,
  `total_amount` float NOT NULL,
  `trans_date` date NOT NULL,
  `booking_status` varchar(200) NOT NULL,
  `cancel_amount` float NOT NULL,
  `cancel_date` date NOT NULL,
  `room_type` varchar(200) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `PRN` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_cities`
--

DROP TABLE IF EXISTS `crs_cities`;
CREATE TABLE IF NOT EXISTS `crs_cities` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city` varchar(200) CHARACTER SET utf8 NOT NULL,
  `country_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `city_name` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18723 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_countries`
--

DROP TABLE IF EXISTS `crs_countries`;
CREATE TABLE IF NOT EXISTS `crs_countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(200) NOT NULL,
  `phonecode` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_hotel_image`
--

DROP TABLE IF EXISTS `crs_hotel_image`;
CREATE TABLE IF NOT EXISTS `crs_hotel_image` (
  `image_id` int(16) NOT NULL,
  `hotel_id` int(16) NOT NULL,
  `image_link` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_hotel_supplier_details`
--

DROP TABLE IF EXISTS `crs_hotel_supplier_details`;
CREATE TABLE IF NOT EXISTS `crs_hotel_supplier_details` (
  `hotel_id` bigint(20) NOT NULL,
  `supplier_id` varchar(30) NOT NULL,
  `property_type` varchar(100) NOT NULL,
  `markup` int(20) NOT NULL,
  `admin_commision` int(200) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `city_id` int(15) NOT NULL,
  `salutation` varchar(20) NOT NULL,
  `agent_name` varchar(200) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `agent_address` text NOT NULL,
  `hotel_name` varchar(100) NOT NULL,
  `hotel_address` varchar(200) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `goacity` varchar(20) NOT NULL,
  `room_quantity` bigint(20) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `office_no` bigint(50) NOT NULL,
  `mobile_no` bigint(50) NOT NULL,
  `fax_no` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `hotel_desc` text NOT NULL,
  `loc_info` text NOT NULL,
  `city_info` text NOT NULL,
  `card_acceptance` varchar(255) NOT NULL,
  `amenities` varchar(1000) NOT NULL,
  `map` varchar(10000) NOT NULL,
  `star_rate` varchar(50) NOT NULL,
  `hotel_services` text NOT NULL,
  `room_services` text NOT NULL,
  `picture1` varchar(255) NOT NULL,
  `picture2` varchar(255) NOT NULL,
  `picture3` varchar(255) NOT NULL,
  `picture4` varchar(255) NOT NULL,
  `picture5` varchar(255) NOT NULL,
  `suppl_type` int(20) NOT NULL,
  `contact_email` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `popular_rating` int(11) NOT NULL,
  `agent_contact_phone` varchar(255) NOT NULL,
  `reserv_sal` varchar(20) NOT NULL,
  `reserv_firstname` varchar(100) NOT NULL,
  `reserv_lastname` varchar(100) NOT NULL,
  `reserv_phone` bigint(50) NOT NULL,
  `reserv_mobileno` bigint(50) NOT NULL,
  `reserv_email` varchar(100) NOT NULL,
  `hotel_style` varchar(100) NOT NULL,
  `single_or_chain` varchar(50) NOT NULL,
  `countrycode` varchar(50) NOT NULL,
  `countrycodeR` varchar(50) NOT NULL,
  `country_code_m` varchar(15) NOT NULL,
  `country_code_Rm` varchar(15) NOT NULL,
  `info_type` varchar(25) NOT NULL,
  `term` varchar(500) NOT NULL,
  `video_link` text NOT NULL,
  `cancel` varchar(500) NOT NULL,
  `api` varchar(150) NOT NULL,
  `rank` varchar(150) NOT NULL,
  `VendorID` varchar(15) NOT NULL,
  `negotiate_status` varchar(15) NOT NULL,
  `min_days` int(11) NOT NULL,
  `max_days` int(11) NOT NULL,
  `min_hours` int(11) NOT NULL,
  `max_hours` int(11) NOT NULL,
  `negotiate_percent` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_map`
--

DROP TABLE IF EXISTS `crs_map`;
CREATE TABLE IF NOT EXISTS `crs_map` (
  `hotel_id` int(36) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_meta_admin_login`
--

DROP TABLE IF EXISTS `crs_meta_admin_login`;
CREATE TABLE IF NOT EXISTS `crs_meta_admin_login` (
  `id` int(11) NOT NULL,
  `admin_user` varchar(50) NOT NULL,
  `admin_pass` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_meta_columns`
--

DROP TABLE IF EXISTS `crs_meta_columns`;
CREATE TABLE IF NOT EXISTS `crs_meta_columns` (
  `index_tag` varchar(50) NOT NULL,
  `hotel_tag` varchar(50) NOT NULL,
  `hotel_detail_tag` varchar(50) NOT NULL,
  `hotel_before_pre_book_tag` varchar(50) NOT NULL,
  `hotel_pre_book_tag` varchar(50) NOT NULL,
  `footer_hotel_tag` varchar(100) NOT NULL,
  `footer_travel_guides_tag` varchar(100) NOT NULL,
  `footer_about_us_tag` varchar(100) NOT NULL,
  `footer_contact_us_tag` varchar(100) NOT NULL,
  `footer_jobs_tag` varchar(100) NOT NULL,
  `footer_faqs_tag` varchar(100) NOT NULL,
  `footer_privacy_tag` varchar(100) NOT NULL,
  `footer_cookie_policy_tag` varchar(100) NOT NULL,
  `footer_site_back_tag` varchar(100) NOT NULL,
  `footer_terms_conditions_tag` varchar(100) NOT NULL,
  `meta_id` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_rate_manager`
--

DROP TABLE IF EXISTS `crs_rate_manager`;
CREATE TABLE IF NOT EXISTS `crs_rate_manager` (
  `id` int(255) NOT NULL,
  `hotel_id` int(255) NOT NULL,
  `room_type` int(100) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `room_name` text NOT NULL,
  `date` date NOT NULL,
  `min_occ` int(20) NOT NULL,
  `max_occ` int(20) NOT NULL,
  `single_price` int(30) NOT NULL,
  `double_price` int(30) NOT NULL,
  `triple_price` int(30) NOT NULL,
  `quad_price` int(30) NOT NULL,
  `b2c_rate` int(30) NOT NULL,
  `extra_bed` int(30) NOT NULL,
  `child_with_bed` int(30) NOT NULL,
  `child_without_bed` int(30) NOT NULL,
  `status` int(2) NOT NULL,
  `blocked_room` int(100) NOT NULL,
  `negotiate_status` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_room_details`
--

DROP TABLE IF EXISTS `crs_room_details`;
CREATE TABLE IF NOT EXISTS `crs_room_details` (
  `ht_rm_id` int(11) NOT NULL,
  `room_pernit` varchar(100) NOT NULL,
  `room_id` varchar(255) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `occupancy_type` varchar(255) NOT NULL,
  `child_occupancy` int(10) NOT NULL,
  `price` varchar(255) NOT NULL,
  `child_rate` varchar(255) NOT NULL,
  `no_of_rooms_alloted` varchar(255) NOT NULL,
  `startdate` date NOT NULL,
  `todate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_room_image`
--

DROP TABLE IF EXISTS `crs_room_image`;
CREATE TABLE IF NOT EXISTS `crs_room_image` (
  `image_id` int(100) NOT NULL,
  `hotel_id` int(100) NOT NULL,
  `room_type` int(50) NOT NULL,
  `room_name` text NOT NULL,
  `image_link` text NOT NULL,
  `main` int(2) NOT NULL,
  `des` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_room_type`
--

DROP TABLE IF EXISTS `crs_room_type`;
CREATE TABLE IF NOT EXISTS `crs_room_type` (
  `room_type_id` int(10) NOT NULL,
  `room_type_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_room_type_detail`
--

DROP TABLE IF EXISTS `crs_room_type_detail`;
CREATE TABLE IF NOT EXISTS `crs_room_type_detail` (
  `room_type_id` int(20) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_type` int(11) NOT NULL,
  `room_name` text NOT NULL,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `date` date NOT NULL,
  `total_no_of_unit` int(30) NOT NULL,
  `no_of_unit` int(30) NOT NULL,
  `booked_unit` int(100) NOT NULL,
  `city_hotel_id` int(11) NOT NULL,
  `no_of_accomodate` int(100) NOT NULL,
  `max_no_of_accomodate` int(100) NOT NULL,
  `min_occ` int(20) NOT NULL,
  `max_occ` int(20) NOT NULL,
  `no_extra_bed` int(20) NOT NULL,
  `commissionable_rate` float NOT NULL DEFAULT '0',
  `c_single_price` float NOT NULL DEFAULT '0',
  `c_double_price` float NOT NULL DEFAULT '0',
  `c_triple_price` float NOT NULL DEFAULT '0',
  `single_price` float NOT NULL,
  `double_price` float NOT NULL,
  `triple_price` int(11) NOT NULL,
  `quad_price` int(11) NOT NULL,
  `b2c_rate` int(11) NOT NULL,
  `child_withoutbed` int(11) NOT NULL,
  `cancel_policy` varchar(500) NOT NULL,
  `extra_bed_price` float NOT NULL,
  `child_withbed` float NOT NULL,
  `break_fast_p` int(11) NOT NULL,
  `lunch_p` int(11) NOT NULL,
  `dinner_p` int(11) NOT NULL,
  `description` text NOT NULL,
  `checkin_time` text NOT NULL,
  `cin` varchar(2) NOT NULL,
  `checkout_time` text NOT NULL,
  `cout` varchar(2) NOT NULL,
  `amenities` varchar(1000) NOT NULL,
  `complement` text NOT NULL,
  `total_images` int(11) NOT NULL,
  `image_link` text NOT NULL,
  `min_stay` int(20) NOT NULL,
  `property_size` int(50) NOT NULL,
  `no_bed` int(20) NOT NULL,
  `day_rate` int(50) NOT NULL,
  `week_rate` int(50) NOT NULL,
  `month_rate` int(100) NOT NULL,
  `extra_person` int(200) NOT NULL,
  `property_detail` text NOT NULL,
  `location_detail` text NOT NULL,
  `terms_cond` text NOT NULL,
  `breakf_inclusive` int(2) NOT NULL,
  `lunch_inclusive` int(2) NOT NULL,
  `dinner_inclusive` int(2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `active_status` varchar(20) NOT NULL,
  `feature_status` int(2) NOT NULL,
  `info_type` text NOT NULL,
  `rate_type` text NOT NULL,
  `currency` varchar(5) NOT NULL,
  `negotiate_status` varchar(15) NOT NULL,
  `room_markup` float NOT NULL,
  `markup_type` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_supplier_login`
--

DROP TABLE IF EXISTS `crs_supplier_login`;
CREATE TABLE IF NOT EXISTS `crs_supplier_login` (
  `sup_id` bigint(20) NOT NULL,
  `supplier_id` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `supplier_type` int(11) NOT NULL,
  `salutation` text NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `mobile_no` int(20) NOT NULL,
  `office_no` int(20) NOT NULL,
  `contact_email` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `singleorchain` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_supplier_notice`
--

DROP TABLE IF EXISTS `crs_supplier_notice`;
CREATE TABLE IF NOT EXISTS `crs_supplier_notice` (
  `notice_id` int(200) NOT NULL,
  `notice1` text NOT NULL,
  `notice2` text NOT NULL,
  `notice3` text NOT NULL,
  `notice4` text NOT NULL,
  `image1` text NOT NULL,
  `image2` text NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `crs_upimage`
--

DROP TABLE IF EXISTS `crs_upimage`;
CREATE TABLE IF NOT EXISTS `crs_upimage` (
  `id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `hotel_id` int(30) NOT NULL,
  `des` varchar(100) NOT NULL,
  `main` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `currency_convertor`
--

DROP TABLE IF EXISTS `currency_convertor`;
CREATE TABLE IF NOT EXISTS `currency_convertor` (
  `curr_id` int(15) NOT NULL,
  `currency_code` varchar(15) NOT NULL,
  `currency_name` varchar(150) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `discount_coupon`
--

DROP TABLE IF EXISTS `discount_coupon`;
CREATE TABLE IF NOT EXISTS `discount_coupon` (
  `id` int(11) NOT NULL,
  `coupon_name` varchar(100) NOT NULL,
  `coupon_code` varchar(50) NOT NULL,
  `coupon_value` varchar(50) NOT NULL,
  `coupon_type` varchar(50) NOT NULL,
  `coupon_status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `feed_back`
--

DROP TABLE IF EXISTS `feed_back`;
CREATE TABLE IF NOT EXISTS `feed_back` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(55) NOT NULL,
  `mobile` int(11) NOT NULL,
  `category` varchar(45) NOT NULL,
  `message` text NOT NULL,
  `date_added` date NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `holiday_list`
--

DROP TABLE IF EXISTS `holiday_list`;
CREATE TABLE IF NOT EXISTS `holiday_list` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL DEFAULT '0',
  `h_type` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `depart_from` varchar(255) NOT NULL,
  `duration` int(11) NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `des` text NOT NULL,
  `highlight` text NOT NULL,
  `iternery` text NOT NULL,
  `rule` text NOT NULL,
  `Inclusions` text NOT NULL,
  `Categories` varchar(255) NOT NULL,
  `S_H_Description` text NOT NULL,
  `S_Price` varchar(255) NOT NULL,
  `D_H_Description` text NOT NULL,
  `D_Price` varchar(255) NOT NULL,
  `P_H_Description` text NOT NULL,
  `H_Price` varchar(255) NOT NULL,
  `P2_H_Description` text NOT NULL,
  `H2_Price` varchar(500) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `thumb_image` varchar(255) NOT NULL,
  `large_image` varchar(255) NOT NULL,
  `gallary_image` text NOT NULL,
  `offer` varchar(30) NOT NULL,
  `created_date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  `wl_label` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `home_page_banners`
--

DROP TABLE IF EXISTS `home_page_banners`;
CREATE TABLE IF NOT EXISTS `home_page_banners` (
  `id` int(11) NOT NULL,
  `banner_title` varchar(100) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_category` varchar(45) NOT NULL,
  `is_active` char(1) NOT NULL DEFAULT 'N',
  `date_added` date NOT NULL,
  `banner_type` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels_city`
--

DROP TABLE IF EXISTS `hotels_city`;
CREATE TABLE IF NOT EXISTS `hotels_city` (
  `city_id` int(11) NOT NULL,
  `country_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `city_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `state` varchar(200) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `updt` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotels_city-10april`
--

DROP TABLE IF EXISTS `hotels_city-10april`;
CREATE TABLE IF NOT EXISTS `hotels_city-10april` (
  `city_id` int(11) NOT NULL,
  `country_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `city_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `state` varchar(200) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `updt` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40799 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_amenities`
--

DROP TABLE IF EXISTS `hotel_amenities`;
CREATE TABLE IF NOT EXISTS `hotel_amenities` (
  `id` int(11) NOT NULL,
  `amenities_name` varchar(150) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_customer_detail`
--

DROP TABLE IF EXISTS `hotel_customer_detail`;
CREATE TABLE IF NOT EXISTS `hotel_customer_detail` (
  `cus_id` int(11) NOT NULL,
  `marchent_ref_no` varchar(200) NOT NULL,
  `adult_salutation` varchar(200) NOT NULL,
  `adult_f_name` varchar(500) NOT NULL,
  `adult_l_name` varchar(500) NOT NULL,
  `adult_age` varchar(200) NOT NULL,
  `child_salutation` varchar(300) NOT NULL,
  `child_f_name` varchar(300) NOT NULL,
  `child_l_name` varchar(300) NOT NULL,
  `child_age` varchar(300) NOT NULL,
  `chkin_date` varchar(200) NOT NULL,
  `chkout_date` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `bookingdate` date NOT NULL,
  `bookingtime` time NOT NULL,
  `noofrooms` varchar(50) NOT NULL,
  `no_of_nights` varchar(50) NOT NULL,
  `hotel_name` varchar(200) NOT NULL,
  `hotel_rating` varchar(50) NOT NULL,
  `hotel_city` varchar(150) NOT NULL,
  `hotel_address` varchar(300) NOT NULL,
  `hotel_website_url` varchar(200) NOT NULL,
  `hotel_email` varchar(200) NOT NULL,
  `hotel_fax_number` varchar(200) NOT NULL,
  `hotel_phone_number` varchar(200) NOT NULL,
  `hotel_code` varchar(200) NOT NULL,
  `user_ip` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=388 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_details`
--

DROP TABLE IF EXISTS `hotel_details`;
CREATE TABLE IF NOT EXISTS `hotel_details` (
  `id` int(10) NOT NULL,
  `VendorName` varchar(125) NOT NULL,
  `VendorID` varchar(15) NOT NULL,
  `HotelClass` varchar(15) NOT NULL,
  `Location` varchar(30) NOT NULL,
  `City` varchar(25) NOT NULL,
  `Country` varchar(20) NOT NULL,
  `Address1` varchar(100) NOT NULL,
  `Address2` varchar(100) NOT NULL,
  `Area` varchar(25) NOT NULL,
  `Address` text NOT NULL,
  `HotelOverview` text NOT NULL,
  `ReviewRating` varchar(15) NOT NULL,
  `ReviewCount` varchar(15) NOT NULL,
  `Latitude` varchar(15) NOT NULL,
  `Longitude` varchar(15) NOT NULL,
  `DefaultCheckInTime` varchar(15) NOT NULL,
  `DefaultCheckOutTime` varchar(15) NOT NULL,
  `Hotel_Star` int(10) NOT NULL,
  `HotelGroupID` varchar(25) NOT NULL,
  `HotelGroupName` varchar(25) NOT NULL,
  `ImagePath` varchar(240) NOT NULL,
  `HotelSearchKey` varchar(25) NOT NULL,
  `Area_Seo_Id` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `request_status` varchar(50) NOT NULL,
  `confirm_status` varchar(50) NOT NULL,
  `fairstayid` varchar(55) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17339 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_details-blk`
--

DROP TABLE IF EXISTS `hotel_details-blk`;
CREATE TABLE IF NOT EXISTS `hotel_details-blk` (
  `id` int(10) NOT NULL,
  `VendorName` varchar(125) NOT NULL,
  `VendorID` varchar(15) NOT NULL,
  `HotelClass` varchar(15) NOT NULL,
  `Location` varchar(30) NOT NULL,
  `City` varchar(25) NOT NULL,
  `Country` varchar(20) NOT NULL,
  `Address1` varchar(100) NOT NULL,
  `Address2` varchar(100) NOT NULL,
  `Area` varchar(25) NOT NULL,
  `Address` text NOT NULL,
  `HotelOverview` text NOT NULL,
  `ReviewRating` varchar(15) NOT NULL,
  `ReviewCount` varchar(15) NOT NULL,
  `Latitude` varchar(15) NOT NULL,
  `Longitude` varchar(15) NOT NULL,
  `DefaultCheckInTime` varchar(15) NOT NULL,
  `DefaultCheckOutTime` varchar(15) NOT NULL,
  `Hotel_Star` int(10) NOT NULL,
  `HotelGroupID` varchar(25) NOT NULL,
  `HotelGroupName` varchar(25) NOT NULL,
  `ImagePath` varchar(240) NOT NULL,
  `HotelSearchKey` varchar(25) NOT NULL,
  `Area_Seo_Id` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `request_status` varchar(50) NOT NULL,
  `confirm_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_details-old`
--

DROP TABLE IF EXISTS `hotel_details-old`;
CREATE TABLE IF NOT EXISTS `hotel_details-old` (
  `id` int(10) NOT NULL,
  `VendorName` varchar(125) NOT NULL,
  `VendorID` varchar(15) NOT NULL,
  `HotelClass` varchar(15) NOT NULL,
  `Location` varchar(30) NOT NULL,
  `City` varchar(25) NOT NULL,
  `Country` varchar(20) NOT NULL,
  `Address1` varchar(100) NOT NULL,
  `Address2` varchar(100) NOT NULL,
  `Area` varchar(25) NOT NULL,
  `Address` text NOT NULL,
  `HotelOverview` text NOT NULL,
  `ReviewRating` varchar(15) NOT NULL,
  `ReviewCount` varchar(15) NOT NULL,
  `Latitude` varchar(15) NOT NULL,
  `Longitude` varchar(15) NOT NULL,
  `DefaultCheckInTime` varchar(15) NOT NULL,
  `DefaultCheckOutTime` varchar(15) NOT NULL,
  `Hotel_Star` int(10) NOT NULL,
  `HotelGroupID` varchar(25) NOT NULL,
  `HotelGroupName` varchar(25) NOT NULL,
  `ImagePath` varchar(240) NOT NULL,
  `HotelSearchKey` varchar(25) NOT NULL,
  `Area_Seo_Id` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `request_status` varchar(50) NOT NULL,
  `confirm_status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17339 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_details_delhi`
--

DROP TABLE IF EXISTS `hotel_details_delhi`;
CREATE TABLE IF NOT EXISTS `hotel_details_delhi` (
  `id` int(10) NOT NULL,
  `VendorName` varchar(125) NOT NULL,
  `VendorID` varchar(15) NOT NULL,
  `HotelClass` varchar(15) NOT NULL,
  `Location` varchar(30) NOT NULL,
  `City` varchar(25) NOT NULL,
  `Country` varchar(20) NOT NULL,
  `Address1` varchar(100) NOT NULL,
  `Address2` varchar(100) NOT NULL,
  `Area` varchar(25) NOT NULL,
  `Address` text NOT NULL,
  `HotelOverview` text NOT NULL,
  `ReviewRating` varchar(15) NOT NULL,
  `ReviewCount` varchar(15) NOT NULL,
  `Latitude` varchar(15) NOT NULL,
  `Longitude` varchar(15) NOT NULL,
  `DefaultCheckInTime` varchar(15) NOT NULL,
  `DefaultCheckOutTime` varchar(15) NOT NULL,
  `Hotel_Star` int(10) NOT NULL,
  `HotelGroupID` varchar(25) NOT NULL,
  `HotelGroupName` varchar(25) NOT NULL,
  `ImagePath` varchar(240) NOT NULL,
  `HotelSearchKey` varchar(25) NOT NULL,
  `Area_Seo_Id` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `request_status` varchar(50) NOT NULL,
  `confirm_status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=637 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_facility`
--

DROP TABLE IF EXISTS `hotel_facility`;
CREATE TABLE IF NOT EXISTS `hotel_facility` (
  `VendorID` varchar(200) NOT NULL,
  `Amenity_id` varchar(25) NOT NULL,
  `AmenityType` varchar(200) NOT NULL,
  `Description` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_image`
--

DROP TABLE IF EXISTS `hotel_image`;
CREATE TABLE IF NOT EXISTS `hotel_image` (
  `VendorID` int(16) NOT NULL,
  `ImageUrl` text NOT NULL,
  `Content-Title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_payment_report`
--

DROP TABLE IF EXISTS `hotel_payment_report`;
CREATE TABLE IF NOT EXISTS `hotel_payment_report` (
  `id` int(11) NOT NULL,
  `h_payment_status` varchar(200) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(200) NOT NULL,
  `entered_date` date NOT NULL,
  `entered_time` time NOT NULL,
  `user_ip` varchar(200) NOT NULL,
  `marchent_ref_no` varchar(200) NOT NULL,
  `mode` varchar(200) NOT NULL,
  `ResponseMessage` varchar(200) NOT NULL,
  `payment_id` varchar(200) NOT NULL,
  `TransactionID` varchar(200) NOT NULL,
  `agent_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_reviews`
--

DROP TABLE IF EXISTS `hotel_reviews`;
CREATE TABLE IF NOT EXISTS `hotel_reviews` (
  `id` int(11) NOT NULL,
  `VendorID` text NOT NULL,
  `VendorName` text NOT NULL,
  `title` text NOT NULL,
  `AvgGuestRating` text NOT NULL,
  `OverallRating` text NOT NULL,
  `Comments` text NOT NULL,
  `RoomComfort` text NOT NULL,
  `Cleanliness` text NOT NULL,
  `Location` text NOT NULL,
  `Service_staff` text NOT NULL,
  `SleepQuality` text NOT NULL,
  `ValueforPrice` text NOT NULL,
  `recomend` text NOT NULL,
  `Post_Date` date NOT NULL,
  `customer_id` varchar(25) NOT NULL,
  `Consumer_city` text NOT NULL,
  `Consumer_Country` text NOT NULL,
  `Customer_name` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_reviews_desia`
--

DROP TABLE IF EXISTS `hotel_reviews_desia`;
CREATE TABLE IF NOT EXISTS `hotel_reviews_desia` (
  `VendorID` text NOT NULL,
  `AvgGuestRating` text NOT NULL,
  `OverallRating` text NOT NULL,
  `Comments` text NOT NULL,
  `RoomQuality` text NOT NULL,
  `ServiceQuality` text NOT NULL,
  `DiningQuality` text NOT NULL,
  `Cleanliness` text NOT NULL,
  `Post_Date` text NOT NULL,
  `Consumer_city` text NOT NULL,
  `Consumer_Country` text NOT NULL,
  `Customer_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_room_blocking`
--

DROP TABLE IF EXISTS `hotel_room_blocking`;
CREATE TABLE IF NOT EXISTS `hotel_room_blocking` (
  `pk` int(10) NOT NULL,
  `room_array` text NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33519 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_style`
--

DROP TABLE IF EXISTS `hotel_style`;
CREATE TABLE IF NOT EXISTS `hotel_style` (
  `id` int(11) NOT NULL,
  `style_name` varchar(150) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_voucher`
--

DROP TABLE IF EXISTS `hotel_voucher`;
CREATE TABLE IF NOT EXISTS `hotel_voucher` (
  `id` int(11) NOT NULL,
  `xml_amount` varchar(200) NOT NULL,
  `admin_markup` varchar(200) NOT NULL,
  `admin_markup_price` int(11) NOT NULL,
  `gateway_markup` varchar(200) NOT NULL,
  `gateway_amount` int(11) NOT NULL,
  `BookingId` varchar(200) NOT NULL,
  `ConfirmationNo` varchar(300) NOT NULL,
  `ReferenceNo` varchar(300) NOT NULL,
  `TripId` varchar(300) NOT NULL,
  `marchent_ref_no` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `module_type` varchar(50) NOT NULL,
  `booking_date` date NOT NULL,
  `booking_time` time NOT NULL,
  `cancel_status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `inandaround`
--

DROP TABLE IF EXISTS `inandaround`;
CREATE TABLE IF NOT EXISTS `inandaround` (
  `VendorID` varchar(25) NOT NULL,
  `NameOfAttraction` varchar(250) NOT NULL,
  `DistanceInKm` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `locators`
--

DROP TABLE IF EXISTS `locators`;
CREATE TABLE IF NOT EXISTS `locators` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `supplier_id` varchar(25) NOT NULL,
  `locator_type` varchar(30) NOT NULL,
  `locator_name` varchar(200) NOT NULL,
  `locator_city` varchar(50) NOT NULL,
  `locator_distance` varchar(10) NOT NULL,
  `locator_hours` varchar(10) NOT NULL,
  `locator_mins` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `locator_type`
--

DROP TABLE IF EXISTS `locator_type`;
CREATE TABLE IF NOT EXISTS `locator_type` (
  `id` int(11) NOT NULL,
  `locator_name` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `markup_comm_hotel`
--

DROP TABLE IF EXISTS `markup_comm_hotel`;
CREATE TABLE IF NOT EXISTS `markup_comm_hotel` (
  `id` int(10) NOT NULL,
  `type` varchar(300) NOT NULL,
  `carrier` varchar(300) NOT NULL,
  `code` varchar(300) NOT NULL,
  `cal_type` varchar(10) NOT NULL,
  `value` float NOT NULL,
  `markup_type` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `max_num_rooms`
--

DROP TABLE IF EXISTS `max_num_rooms`;
CREATE TABLE IF NOT EXISTS `max_num_rooms` (
  `id` int(11) NOT NULL,
  `max_rooms` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `max_validaty_negotiation`
--

DROP TABLE IF EXISTS `max_validaty_negotiation`;
CREATE TABLE IF NOT EXISTS `max_validaty_negotiation` (
  `id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `membership_plan`
--

DROP TABLE IF EXISTS `membership_plan`;
CREATE TABLE IF NOT EXISTS `membership_plan` (
  `plan_id` int(11) NOT NULL,
  `plan_name` varchar(250) NOT NULL,
  `plan_period` int(11) NOT NULL,
  `period_type` varchar(150) NOT NULL,
  `plan_value` int(11) NOT NULL,
  `plan_description` text NOT NULL,
  `plan_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mobile_login_validation`
--

DROP TABLE IF EXISTS `mobile_login_validation`;
CREATE TABLE IF NOT EXISTS `mobile_login_validation` (
  `id` int(11) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `otp` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `negotiate_prices`
--

DROP TABLE IF EXISTS `negotiate_prices`;
CREATE TABLE IF NOT EXISTS `negotiate_prices` (
  `id` int(11) NOT NULL,
  `request_id` varchar(25) NOT NULL,
  `actual_price` float(10,2) NOT NULL,
  `negotiate_price` float(10,2) NOT NULL,
  `counter_price` float(10,2) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `negotiate_request`
--

DROP TABLE IF EXISTS `negotiate_request`;
CREATE TABLE IF NOT EXISTS `negotiate_request` (
  `request_id` varchar(25) NOT NULL,
  `supplier_id` varchar(25) NOT NULL,
  `hotel_id` varchar(20) NOT NULL,
  `room_type_id` varchar(15) NOT NULL,
  `hotel_name` varchar(50) NOT NULL,
  `hotel_email` varchar(150) NOT NULL,
  `hotel_city` varchar(25) NOT NULL,
  `actual_price` float(10,2) NOT NULL,
  `negotiate_price` float(10,2) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `status` varchar(25) NOT NULL,
  `view_status` varchar(15) NOT NULL,
  `booking_status` varchar(25) NOT NULL,
  `requested_date` datetime NOT NULL,
  `validity_date` datetime NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `no_rooms` int(11) NOT NULL,
  `no_adults` int(11) NOT NULL,
  `no_childs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `new_payment`
--

DROP TABLE IF EXISTS `new_payment`;
CREATE TABLE IF NOT EXISTS `new_payment` (
  `id` int(200) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_contact_no` bigint(200) NOT NULL,
  `name` varchar(50) NOT NULL,
  `userid` int(11) NOT NULL,
  `user_type` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_no` bigint(200) NOT NULL,
  `reason` varchar(800) NOT NULL,
  `amount` int(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `payment_date` date NOT NULL,
  `status_edit_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_details_app`
--

DROP TABLE IF EXISTS `payment_details_app`;
CREATE TABLE IF NOT EXISTS `payment_details_app` (
  `id` int(11) NOT NULL,
  `hotel_blocking_id` int(11) NOT NULL,
  `pay_mrchet_refno` varchar(25) NOT NULL,
  `rooms` int(8) NOT NULL,
  `total_days` int(8) NOT NULL,
  `PaymentG_Id` int(11) NOT NULL,
  `htl_chk_in` varchar(30) NOT NULL,
  `htl_chk_out` varchar(30) NOT NULL,
  `total_pax_form` text NOT NULL,
  `total_trans_details` text NOT NULL,
  `total_amt` varchar(20) NOT NULL,
  `DOM_INT` varchar(30) NOT NULL,
  `hotel_type` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `payment_report`
--

DROP TABLE IF EXISTS `payment_report`;
CREATE TABLE IF NOT EXISTS `payment_report` (
  `id` int(11) NOT NULL,
  `payment_id` varchar(50) NOT NULL,
  `TransactionID` varchar(250) NOT NULL,
  `ResponseMessage` varchar(250) NOT NULL,
  `booking_id` varchar(50) NOT NULL,
  `booking_type` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  `amount` varchar(40) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `user_type` varchar(15) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `userContact` varchar(50) NOT NULL,
  `entered_date` datetime NOT NULL,
  `user_ip` varchar(100) NOT NULL,
  `browzer_type` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1347 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `poi_data`
--

DROP TABLE IF EXISTS `poi_data`;
CREATE TABLE IF NOT EXISTS `poi_data` (
  `Poi_Id` varchar(120) NOT NULL,
  `Poi_Seo_Id` text NOT NULL,
  `Poi_Name` text NOT NULL,
  `latitude` varchar(15) NOT NULL,
  `longitude` varchar(15) NOT NULL,
  `City_Id` varchar(15) NOT NULL,
  `City_Name` varchar(50) NOT NULL,
  `Seo_City_Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `policies`
--

DROP TABLE IF EXISTS `policies`;
CREATE TABLE IF NOT EXISTS `policies` (
  `id` int(11) NOT NULL,
  `supplier_id` varchar(25) NOT NULL,
  `sequence` int(11) NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  `room_name` varchar(150) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `policy` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `popular_hotels`
--

DROP TABLE IF EXISTS `popular_hotels`;
CREATE TABLE IF NOT EXISTS `popular_hotels` (
  `id` int(11) NOT NULL,
  `hotel_loc` varchar(50) NOT NULL,
  `hotel_name` varchar(50) NOT NULL,
  `hotel_image` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `star_rating` int(11) NOT NULL,
  `main_price` decimal(8,2) NOT NULL,
  `discount_price` decimal(8,2) NOT NULL,
  `is_active` char(1) NOT NULL DEFAULT 'N',
  `date_added` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `room_description`
--

DROP TABLE IF EXISTS `room_description`;
CREATE TABLE IF NOT EXISTS `room_description` (
  `VendorID` varchar(25) NOT NULL,
  `RoomType` text NOT NULL,
  `RoomTypeID` text NOT NULL,
  `RoomDescription` text NOT NULL,
  `Max_Adult_Occupancy` text NOT NULL,
  `Max_Child_Occupancy` text NOT NULL,
  `Max_Infant_Occupancy` text NOT NULL,
  `Max_Guest_Occupancy` text NOT NULL,
  `ImagePath` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `service_charge`
--

DROP TABLE IF EXISTS `service_charge`;
CREATE TABLE IF NOT EXISTS `service_charge` (
  `id` int(10) NOT NULL,
  `type` varchar(300) NOT NULL,
  `cal_type` varchar(10) NOT NULL,
  `value` float NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `subscribe`
--

DROP TABLE IF EXISTS `subscribe`;
CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` int(11) NOT NULL,
  `email` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `supplier_tax`
--

DROP TABLE IF EXISTS `supplier_tax`;
CREATE TABLE IF NOT EXISTS `supplier_tax` (
  `tax_id` bigint(20) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `supp_id` varchar(100) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `tax_type` varchar(100) NOT NULL,
  `charge_type` varchar(100) NOT NULL,
  `tax_value` varchar(100) NOT NULL,
  `created_date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `supplier_tax_type`
--

DROP TABLE IF EXISTS `supplier_tax_type`;
CREATE TABLE IF NOT EXISTS `supplier_tax_type` (
  `id` int(11) NOT NULL,
  `tax_name` varchar(30) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo`
--

DROP TABLE IF EXISTS `tbo_hotelinfo`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `hotel_city` varchar(125) NOT NULL,
  `hotel_country_id` varchar(25) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `Hotel_fairid` varchar(255) NOT NULL,
  `utpt` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15825 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo-new`
--

DROP TABLE IF EXISTS `tbo_hotelinfo-new`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo-new` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `Hotel_fairid` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14804 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo-old`
--

DROP TABLE IF EXISTS `tbo_hotelinfo-old`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo-old` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `Hotel_fairid` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14764 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo_delhi`
--

DROP TABLE IF EXISTS `tbo_hotelinfo_delhi`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo_delhi` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo_int`
--

DROP TABLE IF EXISTS `tbo_hotelinfo_int`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo_int` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `hotel_city` varchar(125) NOT NULL,
  `hotel_country_id` varchar(25) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `Hotel_fairid` varchar(255) NOT NULL,
  `updt` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=88441 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tbo_hotelinfo_paris`
--

DROP TABLE IF EXISTS `tbo_hotelinfo_paris`;
CREATE TABLE IF NOT EXISTS `tbo_hotelinfo_paris` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `hotel_name` varchar(250) NOT NULL,
  `hotel_address` varchar(500) NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_image` varchar(500) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4962 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `temp_hotel`
--

DROP TABLE IF EXISTS `temp_hotel`;
CREATE TABLE IF NOT EXISTS `temp_hotel` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `star` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `discount` float NOT NULL,
  `hotel_type` varchar(25) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `free_cancel` varchar(25) NOT NULL,
  `fairstayid` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1042296 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `temp_random`
--

DROP TABLE IF EXISTS `temp_random`;
CREATE TABLE IF NOT EXISTS `temp_random` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `discount` float NOT NULL,
  `star` int(11) NOT NULL,
  `hotel_type` varchar(25) NOT NULL,
  `free_cancel` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45748 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `temp_special_hotel`
--

DROP TABLE IF EXISTS `temp_special_hotel`;
CREATE TABLE IF NOT EXISTS `temp_special_hotel` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `HotelIndex` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `star` int(11) NOT NULL,
  `review` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `is_active` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=51757 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `trend_des`
--

DROP TABLE IF EXISTS `trend_des`;
CREATE TABLE IF NOT EXISTS `trend_des` (
  `id` int(11) NOT NULL,
  `HotelCode` varchar(50) NOT NULL,
  `HotelIndex` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `star` int(11) NOT NULL,
  `review` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `is_active` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=1147 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_table`
--

DROP TABLE IF EXISTS `user_table`;
CREATE TABLE IF NOT EXISTS `user_table` (
  `id` int(11) NOT NULL,
  `fb_id` varchar(50) NOT NULL,
  `google_id` varchar(150) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `salutation` varchar(10) NOT NULL,
  `fname` varchar(300) NOT NULL,
  `lname` varchar(300) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `alt_phone` decimal(11,0) NOT NULL,
  `alt_email` varchar(300) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(300) NOT NULL,
  `state` varchar(300) NOT NULL,
  `country` varchar(300) NOT NULL,
  `pincode` int(20) NOT NULL,
  `confirm_code` varchar(255) NOT NULL,
  `is_confirm` char(1) NOT NULL DEFAULT 'N',
  `remarks` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_status` varchar(10) NOT NULL,
  `login_date` datetime NOT NULL,
  `otp_number` varchar(50) NOT NULL,
  `membership_status` varchar(150) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_name` varchar(150) NOT NULL,
  `plan_amount` varchar(50) NOT NULL,
  `memberplan_active_status` varchar(150) NOT NULL,
  `plan_active_date` datetime NOT NULL,
  `plan_expire_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=489 DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_login_detail`
--
ALTER TABLE `admin_login_detail`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `attractions`
--
ALTER TABLE `attractions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bookingdetials`
--
ALTER TABLE `bookingdetials`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `bookingdetials_backup`
--
ALTER TABLE `bookingdetials_backup`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `book_now_paylater`
--
ALTER TABLE `book_now_paylater`
  ADD PRIMARY KEY (`PRI_KEY`);

--
-- Индексы таблицы `cancel_tickets`
--
ALTER TABLE `cancel_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `city_int`
--
ALTER TABLE `city_int`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country_state_city`
--
ALTER TABLE `country_state_city`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `crs_booking_detail`
--
ALTER TABLE `crs_booking_detail`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `crs_book_hotel_tickets`
--
ALTER TABLE `crs_book_hotel_tickets`
  ADD PRIMARY KEY (`id`), ADD KEY `hotel_id` (`hotel_id`), ADD KEY `bookingref` (`bookingref`), ADD KEY `user_email` (`user_email`), ADD KEY `PRN` (`PRN`);

--
-- Индексы таблицы `crs_cities`
--
ALTER TABLE `crs_cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Индексы таблицы `crs_countries`
--
ALTER TABLE `crs_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Индексы таблицы `crs_hotel_image`
--
ALTER TABLE `crs_hotel_image`
  ADD PRIMARY KEY (`image_id`);

--
-- Индексы таблицы `crs_hotel_supplier_details`
--
ALTER TABLE `crs_hotel_supplier_details`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Индексы таблицы `crs_map`
--
ALTER TABLE `crs_map`
  ADD UNIQUE KEY `hotel_id` (`hotel_id`);

--
-- Индексы таблицы `crs_meta_admin_login`
--
ALTER TABLE `crs_meta_admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `crs_meta_columns`
--
ALTER TABLE `crs_meta_columns`
  ADD PRIMARY KEY (`meta_id`);

--
-- Индексы таблицы `crs_rate_manager`
--
ALTER TABLE `crs_rate_manager`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `crs_room_details`
--
ALTER TABLE `crs_room_details`
  ADD PRIMARY KEY (`ht_rm_id`);

--
-- Индексы таблицы `crs_room_image`
--
ALTER TABLE `crs_room_image`
  ADD PRIMARY KEY (`image_id`);

--
-- Индексы таблицы `crs_room_type`
--
ALTER TABLE `crs_room_type`
  ADD PRIMARY KEY (`room_type_id`);

--
-- Индексы таблицы `crs_room_type_detail`
--
ALTER TABLE `crs_room_type_detail`
  ADD PRIMARY KEY (`room_type_id`);

--
-- Индексы таблицы `crs_supplier_login`
--
ALTER TABLE `crs_supplier_login`
  ADD PRIMARY KEY (`sup_id`);

--
-- Индексы таблицы `crs_supplier_notice`
--
ALTER TABLE `crs_supplier_notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Индексы таблицы `crs_upimage`
--
ALTER TABLE `crs_upimage`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency_convertor`
--
ALTER TABLE `currency_convertor`
  ADD PRIMARY KEY (`curr_id`);

--
-- Индексы таблицы `discount_coupon`
--
ALTER TABLE `discount_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `feed_back`
--
ALTER TABLE `feed_back`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `holiday_list`
--
ALTER TABLE `holiday_list`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `home_page_banners`
--
ALTER TABLE `home_page_banners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotels_city`
--
ALTER TABLE `hotels_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Индексы таблицы `hotels_city-10april`
--
ALTER TABLE `hotels_city-10april`
  ADD PRIMARY KEY (`city_id`);

--
-- Индексы таблицы `hotel_amenities`
--
ALTER TABLE `hotel_amenities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotel_customer_detail`
--
ALTER TABLE `hotel_customer_detail`
  ADD PRIMARY KEY (`cus_id`);

--
-- Индексы таблицы `hotel_details`
--
ALTER TABLE `hotel_details`
  ADD KEY `id` (`id`), ADD KEY `VendorID` (`VendorID`), ADD KEY `VendorID_2` (`VendorID`);

--
-- Индексы таблицы `hotel_details-blk`
--
ALTER TABLE `hotel_details-blk`
  ADD KEY `id` (`id`), ADD KEY `VendorID` (`VendorID`);

--
-- Индексы таблицы `hotel_details-old`
--
ALTER TABLE `hotel_details-old`
  ADD KEY `id` (`id`), ADD KEY `VendorID` (`VendorID`);

--
-- Индексы таблицы `hotel_details_delhi`
--
ALTER TABLE `hotel_details_delhi`
  ADD KEY `id` (`id`), ADD KEY `VendorID` (`VendorID`), ADD KEY `VendorID_2` (`VendorID`);

--
-- Индексы таблицы `hotel_facility`
--
ALTER TABLE `hotel_facility`
  ADD KEY `VendorID` (`VendorID`), ADD KEY `VendorID_2` (`VendorID`), ADD KEY `Description` (`Description`(1000));

--
-- Индексы таблицы `hotel_image`
--
ALTER TABLE `hotel_image`
  ADD KEY `VendorID` (`VendorID`);

--
-- Индексы таблицы `hotel_payment_report`
--
ALTER TABLE `hotel_payment_report`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotel_reviews`
--
ALTER TABLE `hotel_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotel_room_blocking`
--
ALTER TABLE `hotel_room_blocking`
  ADD PRIMARY KEY (`pk`);

--
-- Индексы таблицы `hotel_style`
--
ALTER TABLE `hotel_style`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hotel_voucher`
--
ALTER TABLE `hotel_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `locators`
--
ALTER TABLE `locators`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `locator_type`
--
ALTER TABLE `locator_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `markup_comm_hotel`
--
ALTER TABLE `markup_comm_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `max_num_rooms`
--
ALTER TABLE `max_num_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `max_validaty_negotiation`
--
ALTER TABLE `max_validaty_negotiation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `membership_plan`
--
ALTER TABLE `membership_plan`
  ADD PRIMARY KEY (`plan_id`);

--
-- Индексы таблицы `mobile_login_validation`
--
ALTER TABLE `mobile_login_validation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `negotiate_prices`
--
ALTER TABLE `negotiate_prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `negotiate_request`
--
ALTER TABLE `negotiate_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Индексы таблицы `new_payment`
--
ALTER TABLE `new_payment`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `payment_details_app`
--
ALTER TABLE `payment_details_app`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payment_report`
--
ALTER TABLE `payment_report`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `popular_hotels`
--
ALTER TABLE `popular_hotels`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `service_charge`
--
ALTER TABLE `service_charge`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `supplier_tax`
--
ALTER TABLE `supplier_tax`
  ADD PRIMARY KEY (`tax_id`);

--
-- Индексы таблицы `supplier_tax_type`
--
ALTER TABLE `supplier_tax_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo`
--
ALTER TABLE `tbo_hotelinfo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo-new`
--
ALTER TABLE `tbo_hotelinfo-new`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo-old`
--
ALTER TABLE `tbo_hotelinfo-old`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo_delhi`
--
ALTER TABLE `tbo_hotelinfo_delhi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo_int`
--
ALTER TABLE `tbo_hotelinfo_int`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tbo_hotelinfo_paris`
--
ALTER TABLE `tbo_hotelinfo_paris`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `temp_hotel`
--
ALTER TABLE `temp_hotel`
  ADD PRIMARY KEY (`id`), ADD KEY `sessionId` (`sessionId`), ADD KEY `sessionId_2` (`sessionId`), ADD KEY `sessionId_3` (`sessionId`);

--
-- Индексы таблицы `temp_random`
--
ALTER TABLE `temp_random`
  ADD PRIMARY KEY (`id`), ADD KEY `sessionId` (`sessionId`);

--
-- Индексы таблицы `temp_special_hotel`
--
ALTER TABLE `temp_special_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `trend_des`
--
ALTER TABLE `trend_des`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `admin_login_detail`
--
ALTER TABLE `admin_login_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблицы `attractions`
--
ALTER TABLE `attractions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `bookingdetials`
--
ALTER TABLE `bookingdetials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=296;
--
-- AUTO_INCREMENT для таблицы `bookingdetials_backup`
--
ALTER TABLE `bookingdetials_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT для таблицы `cancel_tickets`
--
ALTER TABLE `cancel_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT для таблицы `city_int`
--
ALTER TABLE `city_int`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT для таблицы `country_state_city`
--
ALTER TABLE `country_state_city`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1453;
--
-- AUTO_INCREMENT для таблицы `crs_booking_detail`
--
ALTER TABLE `crs_booking_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `crs_book_hotel_tickets`
--
ALTER TABLE `crs_book_hotel_tickets`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `crs_cities`
--
ALTER TABLE `crs_cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18723;
--
-- AUTO_INCREMENT для таблицы `crs_countries`
--
ALTER TABLE `crs_countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=194;
--
-- AUTO_INCREMENT для таблицы `crs_hotel_image`
--
ALTER TABLE `crs_hotel_image`
  MODIFY `image_id` int(16) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `crs_hotel_supplier_details`
--
ALTER TABLE `crs_hotel_supplier_details`
  MODIFY `hotel_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `crs_meta_admin_login`
--
ALTER TABLE `crs_meta_admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `crs_meta_columns`
--
ALTER TABLE `crs_meta_columns`
  MODIFY `meta_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `crs_rate_manager`
--
ALTER TABLE `crs_rate_manager`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=333;
--
-- AUTO_INCREMENT для таблицы `crs_room_details`
--
ALTER TABLE `crs_room_details`
  MODIFY `ht_rm_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `crs_room_image`
--
ALTER TABLE `crs_room_image`
  MODIFY `image_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `crs_room_type`
--
ALTER TABLE `crs_room_type`
  MODIFY `room_type_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT для таблицы `crs_room_type_detail`
--
ALTER TABLE `crs_room_type_detail`
  MODIFY `room_type_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `crs_supplier_login`
--
ALTER TABLE `crs_supplier_login`
  MODIFY `sup_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `crs_supplier_notice`
--
ALTER TABLE `crs_supplier_notice`
  MODIFY `notice_id` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `crs_upimage`
--
ALTER TABLE `crs_upimage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=202;
--
-- AUTO_INCREMENT для таблицы `currency_convertor`
--
ALTER TABLE `currency_convertor`
  MODIFY `curr_id` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `discount_coupon`
--
ALTER TABLE `discount_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT для таблицы `feed_back`
--
ALTER TABLE `feed_back`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `holiday_list`
--
ALTER TABLE `holiday_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT для таблицы `home_page_banners`
--
ALTER TABLE `home_page_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `hotels_city-10april`
--
ALTER TABLE `hotels_city-10april`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40799;
--
-- AUTO_INCREMENT для таблицы `hotel_amenities`
--
ALTER TABLE `hotel_amenities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT для таблицы `hotel_customer_detail`
--
ALTER TABLE `hotel_customer_detail`
  MODIFY `cus_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=388;
--
-- AUTO_INCREMENT для таблицы `hotel_details`
--
ALTER TABLE `hotel_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17339;
--
-- AUTO_INCREMENT для таблицы `hotel_details-blk`
--
ALTER TABLE `hotel_details-blk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `hotel_details-old`
--
ALTER TABLE `hotel_details-old`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17339;
--
-- AUTO_INCREMENT для таблицы `hotel_details_delhi`
--
ALTER TABLE `hotel_details_delhi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=637;
--
-- AUTO_INCREMENT для таблицы `hotel_payment_report`
--
ALTER TABLE `hotel_payment_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT для таблицы `hotel_reviews`
--
ALTER TABLE `hotel_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `hotel_room_blocking`
--
ALTER TABLE `hotel_room_blocking`
  MODIFY `pk` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33519;
--
-- AUTO_INCREMENT для таблицы `hotel_style`
--
ALTER TABLE `hotel_style`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `hotel_voucher`
--
ALTER TABLE `hotel_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `locators`
--
ALTER TABLE `locators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `locator_type`
--
ALTER TABLE `locator_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `markup_comm_hotel`
--
ALTER TABLE `markup_comm_hotel`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `max_validaty_negotiation`
--
ALTER TABLE `max_validaty_negotiation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `membership_plan`
--
ALTER TABLE `membership_plan`
  MODIFY `plan_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mobile_login_validation`
--
ALTER TABLE `mobile_login_validation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT для таблицы `negotiate_prices`
--
ALTER TABLE `negotiate_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT для таблицы `new_payment`
--
ALTER TABLE `new_payment`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `payment_details_app`
--
ALTER TABLE `payment_details_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT для таблицы `payment_report`
--
ALTER TABLE `payment_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1347;
--
-- AUTO_INCREMENT для таблицы `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `popular_hotels`
--
ALTER TABLE `popular_hotels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `service_charge`
--
ALTER TABLE `service_charge`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT для таблицы `supplier_tax`
--
ALTER TABLE `supplier_tax`
  MODIFY `tax_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `supplier_tax_type`
--
ALTER TABLE `supplier_tax_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo`
--
ALTER TABLE `tbo_hotelinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15825;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo-new`
--
ALTER TABLE `tbo_hotelinfo-new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14804;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo-old`
--
ALTER TABLE `tbo_hotelinfo-old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14764;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo_delhi`
--
ALTER TABLE `tbo_hotelinfo_delhi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo_int`
--
ALTER TABLE `tbo_hotelinfo_int`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88441;
--
-- AUTO_INCREMENT для таблицы `tbo_hotelinfo_paris`
--
ALTER TABLE `tbo_hotelinfo_paris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4962;
--
-- AUTO_INCREMENT для таблицы `temp_hotel`
--
ALTER TABLE `temp_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1042296;
--
-- AUTO_INCREMENT для таблицы `temp_random`
--
ALTER TABLE `temp_random`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45748;
--
-- AUTO_INCREMENT для таблицы `temp_special_hotel`
--
ALTER TABLE `temp_special_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51757;
--
-- AUTO_INCREMENT для таблицы `trend_des`
--
ALTER TABLE `trend_des`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1147;
--
-- AUTO_INCREMENT для таблицы `user_table`
--
ALTER TABLE `user_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=489;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssp.amazons3importer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssp.amazons3importer.service.ImportService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author KustovVA
 */
public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("You must pass 1 parameter - number of threads");
            System.exit(-1);
        }
        int threadCount = Integer.parseInt(args[0]);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.ssp_amazonS3Importer_jar_1.0PU");
        EntityManager entityManager = emf.createEntityManager();

        ImportService service = new ImportService(threadCount, entityManager, new ObjectMapper());
        service.run();
        service.shutdownAndAwaitTermination();

    }
}

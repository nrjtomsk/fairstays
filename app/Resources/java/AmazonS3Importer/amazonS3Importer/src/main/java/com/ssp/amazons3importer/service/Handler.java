/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssp.amazons3importer.service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.util.json.JSONObject;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssp.amazons3importer.entity.HotelsPro;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author KustovVA
 */
public class Handler implements Runnable {

    final public static String BUCKET_NAME = "hotel-images-selectrooms";
    final public static String BUCKET_REGION = "eu-west-1";

    private ImportService service;

    private HotelsPro hotel;

    private ObjectMapper mapper;

    private List<String> imageUrls;

    AmazonS3Client amazonS3Client;

    public Handler(HotelsPro hotel, ObjectMapper mapper, ImportService service, AmazonS3Client amazonS3Client) {
        this.hotel = hotel;
        this.mapper = mapper;
        imageUrls = new CopyOnWriteArrayList<>();
        this.service = service;
        this.amazonS3Client = amazonS3Client;

    }

    @Override
    public void run() {
        try {
            Logger.getLogger(Handler.class.getName()).log(Level.INFO, "Processing hotel {0} started " + hotel.getId(), hotel.getHotelCode());

            fillImageUrls();
            replaceImages();

            Logger.getLogger(Handler.class.getName()).log(Level.INFO, "Processing hotel {0} complete " + hotel.getId(), hotel.getHotelCode());
        } catch (Exception ex) {
            service.incrementError();
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        service.logInfo();
    }

    private void fillImageUrls() throws IOException {
        JsonNode readTree = mapper.readTree(hotel.getImagesUrls());
        for (JsonNode jsonNode : readTree) {
            String imageUrl = jsonNode.asText().trim();
            if (!"".equals(imageUrl)) {
                imageUrls.add(imageUrl);
            }
        }
    }

    private void replaceImages() throws JsonProcessingException, UnsupportedEncodingException, IOException, NoSuchAlgorithmException {
        List<String> newImageUrls = new ArrayList<>();
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        for (String imageUrl : imageUrls) {
            try {
                int indexOf = imageUrl.indexOf(BUCKET_NAME);
                if (indexOf != -1) {
                    service.incrementSkip();
                    return;
                }
                HttpClient client = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(imageUrl);
                HttpResponse response = client.execute(request);

                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentType("image/jpg");

                String fileName = hotel.getHotelCode().trim() + "_" + UUID.randomUUID().toString() + ".jpg";

                PutObjectRequest putObjectRequest = new PutObjectRequest(
                        BUCKET_NAME,
                        fileName,
                        response.getEntity().getContent(),
                        metadata
                );
                PutObjectResult result = amazonS3Client.putObject(putObjectRequest
                        .withBucketName(BUCKET_NAME)
                        .withCannedAcl(CannedAccessControlList.PublicRead)
                        .withSdkRequestTimeout(5 * 60 * 1000) // 5 minutes
                );

                newImageUrls.add(amazonS3Client.getUrl(BUCKET_NAME, fileName).toString());
            } catch (Throwable e) {
                service.incrementError();
                return;
            }

        }

        if (imageUrls.size() != newImageUrls.size()) {
            service.incrementError();
            return;
        }

        try {
            String oldImages = hotel.getImagesUrls();
            hotel.setImagesUrls(mapper.writeValueAsString(newImageUrls));
            if (!oldImages.equals(hotel.getImagesUrls())) {
                service.saveHotel(hotel);
                service.incrementSuccess();
            } else {
                service.incrementSkip();
            }
        } catch (Exception e) {
            service.incrementError();
            return;
        }

    }

}

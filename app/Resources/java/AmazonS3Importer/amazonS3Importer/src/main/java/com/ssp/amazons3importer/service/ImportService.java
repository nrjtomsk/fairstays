/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssp.amazons3importer.service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssp.amazons3importer.entity.HotelsPro;
import static com.ssp.amazons3importer.service.Handler.BUCKET_NAME;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author KustovVA
 */
public class ImportService implements Runnable {

    private ExecutorService pool;

    private EntityManager em;
    
    private ObjectMapper mapper;
    
    private AtomicInteger skippedCount = new AtomicInteger();
    private AtomicInteger successCount = new AtomicInteger();
    private AtomicInteger errorCount = new AtomicInteger();
    
    private int poolSize;

    public ImportService(int poolSize, EntityManager em, ObjectMapper mapper) {
        this.pool = Executors.newFixedThreadPool(poolSize);
        this.em = em;
        this.mapper = mapper;
        this.poolSize = poolSize;
    }

    public synchronized void saveHotel(HotelsPro hotel) {
        EntityTransaction tr = em.getTransaction();
        if (!tr.isActive()) {
            tr.begin();
        }
        em.persist(hotel);
        em.flush();
        tr.commit();
    }
    
    

    @Override
    public void run() {

        int offset = 0;
        int batchSize = poolSize;

        while (em.createNamedQuery("HotelsPro.findAll", HotelsPro.class)
                .setFirstResult(offset)
                .setMaxResults(batchSize)
                .getResultList().size() > 0) {

            ListIterator<HotelsPro> listIterator = em.createNamedQuery("HotelsPro.findAll", HotelsPro.class)
                    .setFirstResult(offset)
                    .setMaxResults(batchSize)
                    .getResultList()
                    .listIterator();
            
            List<Future<?>> submits = new ArrayList<>();
            AmazonS3Client amazonClient = getAmazonClient();
            while (listIterator.hasNext()) {
                HotelsPro hotel = listIterator.next();
                pool.execute(new Handler(hotel, mapper, this, amazonClient));
            }
            offset += batchSize;
            
            
        }

    }
    
    public void logInfo() {
        Logger.getLogger(ImportService.class.getName()).log(Level.INFO, 
                    "\r\n---\r\n---\r\n---\r\n" 
                            + "SUCCESS COUNT: {0}" + "\r\n"
                            + "ERROR COUNT: {1}" + "\r\n" 
                            + "SKIPPED COUNT: {2}\r\n---\r\n---\r\n", new Object[]{successCount, errorCount, skippedCount});
    }
    
    public void incrementSuccess() {
        successCount.incrementAndGet();
    }
    
    public void incrementError() {
        errorCount.incrementAndGet();
    }
    
    public void incrementSkip() {
        skippedCount.incrementAndGet();
    }

    public void shutdownAndAwaitTermination() {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    private AmazonS3Client getAmazonClient() {
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setConnectionTimeout(5 * 60 * 1000);
        configuration.setClientExecutionTimeout(5 * 60 * 1000);
        configuration.setMaxConnections(poolSize * 10);
        configuration.setMaxErrorRetry(5);
        configuration.setRequestTimeout(5 * 60 * 1000);
        configuration.setSocketTimeout(5 * 60 * 1000);
        configuration.setUseGzip(true);
        configuration.setUseTcpKeepAlive(true);
        AmazonS3Client amazonS3Client = new AmazonS3Client(new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return "AKIAIDGNEHCSQFKX2G5Q";
            }

            @Override
            public String getAWSSecretKey() {
                return "W9CqokDefWDVA3ADbX/Qc+9jemuSqHPzIgCmjSt7";
            }
        }, configuration);
        amazonS3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));
        amazonS3Client.setBucketAcl(BUCKET_NAME, CannedAccessControlList.PublicRead);
        
        return amazonS3Client;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssp.amazons3importer.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author KustovVA
 */
@Entity
@Table(name = "hotels_pro", catalog = "fairstays", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HotelsPro.findAll", query = "SELECT h FROM HotelsPro h ORDER BY h.id ASC"),
    @NamedQuery(name = "HotelsPro.findById", query = "SELECT h FROM HotelsPro h WHERE h.id = :id"),
    @NamedQuery(name = "HotelsPro.findByHotelCode", query = "SELECT h FROM HotelsPro h WHERE h.hotelCode = :hotelCode"),
    @NamedQuery(name = "HotelsPro.findByDestination", query = "SELECT h FROM HotelsPro h WHERE h.destination = :destination"),
    @NamedQuery(name = "HotelsPro.findByCountry", query = "SELECT h FROM HotelsPro h WHERE h.country = :country"),
    @NamedQuery(name = "HotelsPro.findByStarRating", query = "SELECT h FROM HotelsPro h WHERE h.starRating = :starRating"),
    @NamedQuery(name = "HotelsPro.findByHotelPostalCode", query = "SELECT h FROM HotelsPro h WHERE h.hotelPostalCode = :hotelPostalCode"),
    @NamedQuery(name = "HotelsPro.findByLatitude", query = "SELECT h FROM HotelsPro h WHERE h.latitude = :latitude"),
    @NamedQuery(name = "HotelsPro.findByLongitude", query = "SELECT h FROM HotelsPro h WHERE h.longitude = :longitude"),
    @NamedQuery(name = "HotelsPro.findByRoomsNumber", query = "SELECT h FROM HotelsPro h WHERE h.roomsNumber = :roomsNumber"),
    @NamedQuery(name = "HotelsPro.findByTripAdvisorRatingId", query = "SELECT h FROM HotelsPro h WHERE h.tripAdvisorRatingId = :tripAdvisorRatingId")})
public class HotelsPro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;
    @Column(name = "hotel_code", length = 10)
    private String hotelCode;
    @Column(length = 50)
    private String destination;
    @Column(length = 127)
    private String country;
    @Lob
    @Column(name = "hotel_name", length = 2147483647)
    private String hotelName;
    @Column(name = "star_rating")
    private Short starRating;
    @Lob
    @Column(name = "hotel_address", length = 2147483647)
    private String hotelAddress;
    @Column(name = "hotel_postal_code", length = 30)
    private String hotelPostalCode;
    @Lob
    @Column(name = "hotel_phone_number", length = 2147483647)
    private String hotelPhoneNumber;
    @Lob
    @Column(name = "hotel_area", length = 2147483647)
    private String hotelArea;
    @Column(length = 20)
    private String latitude;
    @Column(length = 20)
    private String longitude;
    @Lob
    @Column(name = "images_urls", length = 2147483647)
    private String imagesUrls;
    @Column(name = "rooms_number")
    private Short roomsNumber;
    @Lob
    @Column(name = "property_amenities", length = 2147483647)
    private String propertyAmenities;
    @Lob
    @Column(name = "room_amenities", length = 2147483647)
    private String roomAmenities;
    @Lob
    @Column(length = 2147483647)
    private String description;
    @Column(name = "trip_advisor_rating_id")
    private Integer tripAdvisorRatingId;

    public HotelsPro() {
    }

    public HotelsPro(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Short getStarRating() {
        return starRating;
    }

    public void setStarRating(Short starRating) {
        this.starRating = starRating;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getHotelPostalCode() {
        return hotelPostalCode;
    }

    public void setHotelPostalCode(String hotelPostalCode) {
        this.hotelPostalCode = hotelPostalCode;
    }

    public String getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    public void setHotelPhoneNumber(String hotelPhoneNumber) {
        this.hotelPhoneNumber = hotelPhoneNumber;
    }

    public String getHotelArea() {
        return hotelArea;
    }

    public void setHotelArea(String hotelArea) {
        this.hotelArea = hotelArea;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImagesUrls() {
        return imagesUrls;
    }

    public void setImagesUrls(String imagesUrls) {
        this.imagesUrls = imagesUrls;
    }

    public Short getRoomsNumber() {
        return roomsNumber;
    }

    public void setRoomsNumber(Short roomsNumber) {
        this.roomsNumber = roomsNumber;
    }

    public String getPropertyAmenities() {
        return propertyAmenities;
    }

    public void setPropertyAmenities(String propertyAmenities) {
        this.propertyAmenities = propertyAmenities;
    }

    public String getRoomAmenities() {
        return roomAmenities;
    }

    public void setRoomAmenities(String roomAmenities) {
        this.roomAmenities = roomAmenities;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTripAdvisorRatingId() {
        return tripAdvisorRatingId;
    }

    public void setTripAdvisorRatingId(Integer tripAdvisorRatingId) {
        this.tripAdvisorRatingId = tripAdvisorRatingId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HotelsPro)) {
            return false;
        }
        HotelsPro other = (HotelsPro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ssp.amazons3importer.entity.HotelsPro[ id=" + id + " ]";
    }
    
}

Fairstays
=========

Requirements for Running
------------------------

* PHP 5.6
* MySQL 5.6


Install
-------

* `git clone git@github.com:Fairstays/fairstays.git <project-name>`
* install [composer][1]
* run in root project directory `php composer.phar install -o` or `composer install -o`
* Answer on console questions


Build with capistrano
---------------------
* `cap production deploy`


Windows Local virtual hosting example
-------------------------------------

    <VirtualHost fairstays.loc:80>
        ServerAdmin admin@fairstays.loc
        DocumentRoot "/path/to/project/<project-name>/web"
        ServerName fairstays.loc
        ErrorLog "logs/fairstays.loc-error.log"
        CustomLog "logs/fairstays.loc-access.log" common
         
        <Directory "/path/to/project/<project-name>/web">
            AllowOverride All
        </Directory>
    </VirtualHost>


Add new host in your hosts file
-------------------------------

    `127.0.0.1 fairstays.loc`


Symfony2 commands after install
-------------------------------
* `php app/console doctrine:database:drop --force`
* `php app/console doctrine:database:create`
* `php app/console doctrine:migrations:migrate`
* `php app/console hotels-pro:fill-destinations`  - fill cities
* `php app/console app:import-hotels:hotels-pro`  - fill hotelPro hotels
* `php app/console app:import-hotels:travel-guru` - fill the travelGuru hotels
* `php app/console hotels-pro:fill-amenities`     - fill the hotelPro amenities
* `php app/console hotels-pro:fill-descriptions`  - fill the hotelPro descriptions
* `php app/console tbbc:money:ratio-fetch`        - fill the currencies ratio

Additional commands
-------------------
* `php app/console documentation:create - generate php doc

Project Environments
---------------

* `dev` - for develops
* `stage` - for testing on test server
* `test` - for unit-tests
* `prod` - for production server


Project Bundles
---------------

* [symfony/FrameworkBundle][11] - Symfony framework bundle
* [symfony/SecurityBundle][12] - SecurityBundle
* [symfony/TwigBundle][13] - TwigBundle - include twig template engine
* [symfony/MonologBundle][14] - MonologBundle - include logger functions
* [symfony/SwiftmailerBundle][15] - SwiftmailerBundle - include send mails functions
* [symfony/AsseticBundle][16] - AsseticBundle - js/css/fonts management functions
* [doctrine/DoctrineBundle][17] - DoctrineBundle - data access layer
* [doctrine/DoctrineMigrationsBundle][19] - DoctrineMigrationsBundle - adds DB migrations functions
* [doctrine/DoctrineFixturesBundle][20] - DoctrineFixturesBundle - adds data fixtures functions
* [sensiolabs/SensioFrameworkExtraBundle][18] - Adds annotation configuration for Controller classes
* [FOSUserBundle][9] - User bundle 
* [StofDoctrineExtensionsBundle][8] - Doctrine extensions bundle
* [FOSJsRoutingBundle][6] - Symfony2 routes for js
* [FriendsOfSymfony/FOSRestBundle][21] - This Bundle provides various tools to rapidly develop RESTful API's with Symfony2
* [JMSSerializerBundle][22] - JMSSerializerBundle allows you to serialize your data into a requested output format such as JSON, XML, or YAML
* [nelmio/NelmioApiDocBundle][23] - Generates documentation for your REST API from annotations
* [DebugBundle][25] - DebugBundle - adds `dump()` function
* [WebProfilerBundle][32] - WebProfilerBundle - part of Symfony2 (provides profile panel)
* [SensioDistributionBundle][29] - SensioDistributionBundle - provides composer hooks, web configurator, security checker features
* [SensioGeneratorBundle][30] - SensioGeneratorBundle - generates Symfony2 bundles, entities, forms, CRUD, and more...


[1]:  https://getcomposer.org/download/
[2]:  https://github.com/craue/CraueGeoBundle
[3]:  https://github.com/jcroll/foursquare-api-bundle
[5]:  https://github.com/genemu/GenemuFormBundle
[6]:  https://github.com/FriendsOfSymfony/FOSJsRoutingBundle
[8]:  https://github.com/stof/StofDoctrineExtensionsBundle
[9]:  https://github.com/FriendsOfSymfony/FOSUserBundle
[11]: https://github.com/symfony/FrameworkBundle
[12]: https://github.com/symfony/SecurityBundle
[13]: https://github.com/symfony/TwigBundle
[14]: https://github.com/symfony/MonologBundle
[15]: https://github.com/symfony/SwiftmailerBundle
[16]: https://github.com/symfony/AsseticBundle
[17]: https://github.com/doctrine/DoctrineBundle
[18]: https://github.com/sensiolabs/SensioFrameworkExtraBundle
[19]: https://github.com/doctrine/DoctrineMigrationsBundle
[20]: https://github.com/doctrine/DoctrineFixturesBundle
[21]: https://github.com/FriendsOfSymfony/FOSRestBundle
[22]: http://jmsyst.com/bundles/JMSSerializerBundle
[23]: https://github.com/nelmio/NelmioApiDocBundle
[25]: https://github.com/symfony/debug-bundle
[29]: https://github.com/sensiolabs/SensioDistributionBundle
[30]: https://github.com/sensiolabs/SensioGeneratorBundle
[32]: https://github.com/symfony/WebProfilerBundle


# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

server '52.74.201.149', user: 'ubuntu', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}



# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any  hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

role :app, %w{ubuntu@52.74.201.149}, my_property: :my_value
# role :web, %w{user1@primary.com user2@additional.com}, other_property: :other_value
# role :db,  %w{deploy@example.com}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(app/Resources/keys/fairstays.pem),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
 server '52.74.201.149',
   user: 'ubuntu',
   roles: %w{web app},
   ssh_options: {
     user: 'ubuntu', # overrides user setting above
     keys: %w(app/Resources/keys/fairstays.pem),
     forward_agent: false,
     auth_methods: %w(publickey)
     # password: 'please use keys'
   }

set :application, 'fairstays'

set :repo_url, 'git@github.com:Fairstays/fairstays.git'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/fairstays'

# Default value for keep_releases is 5
set :keep_releases,         5

# Symfony environment
set :symfony_env,           "prod"

# Symfony application path
set :app_path,              "app"

# Symfony web path
set :web_path,              "web"

# Symfony log path
set :log_path,              fetch(:app_path) + "/logs"

# Symfony cache path
set :cache_path,            fetch(:app_path) + "/cache"

# Symfony config file path
set :app_config_path,       fetch(:app_path) + "/config"

# Controllers to clear
set :controllers_to_clear, ["app_*.php"]

# Files that need to remain the same between deploys
set :linked_files,          []

# Dirs that need to remain the same between deploys (shared dirs)
set :linked_dirs,           [fetch(:log_path), fetch(:web_path) + "/uploads"]

# Dirs that need to be writable by the HTTP Server (i.e. cache, log dirs)
set :file_permissions_paths,         [fetch(:log_path), fetch(:cache_path)]

# Name used by the Web Server (i.e. www-data for Apache)
set :file_permissions_users, ['www-data']

# Name used by the Web Server (i.e. www-data for Apache)
set :webserver_user,        "www-data"

# Method used to set permissions (:chmod, :acl, or :chgrp)
set :permission_method,     false

# Execute set permissions
set :use_set_permissions,   false

# Symfony console path
set :symfony_console_path, fetch(:app_path) + "/console"

# Symfony console flags
set :symfony_console_flags, "--no-debug"

# Assets install path
set :assets_install_path,   fetch(:web_path)

# Assets install flags
set :assets_install_flags,  '--symlink'

# Assetic dump flags
set :assetic_dump_flags,  '-e=prod'

set :use_composer, true
set :update_vendors, true
set :composer_install_flags, '--quiet --no-interaction --optimize-autoloader'

fetch(:default_env).merge!(symfony_env: fetch(:symfony_env))

namespace :deploy do

  desc "Fix permission"
  task :fix_permissions do
    on roles  (:app)  do
      execute "#{:sudo} chown ubuntu:www-data -R #{current_path}"
      execute "#{:sudo} chmod 777 -R #{current_path}/app/cache/"
      execute "#{:sudo} chmod 777 -R #{current_path}/app/Resources/TempForHotelsProFilter"
      execute "#{:sudo} chmod 777 -R #{current_path}/app/logs/"
      execute "#{:sudo} chmod 777 -R #{current_path}/web/uploads/"
      execute "#{:sudo} rm -rf #{current_path}/app/cache/*"
    end
  end

end

after 'deploy:finished', 'deploy:fix_permissions'

namespace :params do

 desc "Upload parameters file"
 task :upload_params do
   on roles (:app) do
     execute "mkdir -p #{shared_path}/app/config"
     upload! StringIO.new(File.read("app/config/parameters_build.yml")),
     "#{shared_path}/app/config/parameters.yml"
     end
   end

 desc "Copy parameters file to current dir"
 task :copy_params do
   on roles (:app) do
    execute "cp -fp #{shared_path}/app/config/parameters.yml #{current_path}/app/config/"
    execute "#{:sudo} chmod 664 #{current_path}/app/config/parameters.yml"
   end
 end

end

namespace :deploy do
  task :migrate do
    invoke 'symfony:console', 'assets:install', '-e=prod --symlink'
    invoke 'symfony:console', 'assetic:dump', '-e=prod'
  end
end

after 'composer:install', "params:upload_params"
after 'deploy:finished', "params:copy_params"
after 'composer:install',   'deploy:migrate'

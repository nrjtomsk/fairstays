# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

server 'linux-test7', user: 'teamcity', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}



# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any  hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

role :app, %w{teamcity@linux-test7}, my_property: :my_value
# role :web, %w{user1@primary.com user2@additional.com}, other_property: :other_value
# role :db,  %w{deploy@example.com}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
  set :ssh_options, {
    keys: %w(/home/teamcity/.ssh/id_rsa),
    forward_agent: false,
    auth_methods: %w(password)
  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
 server 'linux-test7',
   user: 'teamcity',
   roles: %w{web app},
   ssh_options: {
     user: 'teamcity', # overrides user setting above
     keys: %w(/home/teamcity/.ssh/id_rsa),
     forward_agent: false,
     auth_methods: %w(publickey password)
     # password: 'please use keys'
   }

set :application, 'fairstays-capistrano'

set :repo_url, 'git@git.sibir.local:web/fairstays.git'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/fairstays-capistrano'

# Default value for keep_releases is 5
set :keep_releases, 5

# Symfony environment
set :symfony_env,  "stage"

# Symfony application path
set :app_path,              "app"

# Symfony web path
set :web_path,              "web"

# Symfony log path
set :log_path,              fetch(:app_path) + "/logs"

# Symfony cache path
set :cache_path,            fetch(:app_path) + "/cache"

# Symfony config file path
set :app_config_path,       fetch(:app_path) + "/config"

# Controllers to clear
set :controllers_to_clear, ["app_*.php"]

# Files that need to remain the same between deploys
set :linked_files,          []

# Dirs that need to remain the same between deploys (shared dirs)
set :linked_dirs,           [fetch(:log_path), fetch(:web_path) + "/uploads"]

# Dirs that need to be writable by the HTTP Server (i.e. cache, log dirs)
set :file_permissions_paths,         [fetch(:log_path), fetch(:cache_path)]

# Name used by the Web Server (i.e. www-data for Apache)
set :file_permissions_users, ['teamcity']

# Name used by the Web Server (i.e. www-data for Apache)
set :webserver_user,        "teamcity"

# Method used to set permissions (:chmod, :acl, or :chgrp)
set :permission_method,     false

# Execute set permissions
set :use_set_permissions,   false

# Symfony console path
set :symfony_console_path, fetch(:app_path) + "/console"

# Symfony console flags
set :symfony_console_flags, "--no-debug"

# Assets install path
set :assets_install_path,   fetch(:web_path)

# Assets install flags
set :assets_install_flags,  '--symlink'

# Assetic dump flags
set :assetic_dump_flags,  ''

set :use_composer, true
set :update_vendors, true
set :composer_install_flags, '--no-interaction --quiet --optimize-autoloader'

fetch(:default_env).merge!(symfony_env: fetch(:symfony_env))

namespace :deploy do
  task :migrate do
    invoke 'symfony:console', 'doctrine:migrations:migrate', '--no-interaction -e=stage'
  end
end


after 'composer:install',   'deploy:migrate'


<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 15.12.2015
 * Time: 16:23
 */

namespace AppBundle\Command;


use AppBundle\Entity\HotelsDestination;
use Prewk\XmlStringStreamer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Imports a codes cities
 *
 * Class HotelsProImportCitiesCommand
 * @package AppBundle\Command
 */
class HotelsProImportCitiesCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('hotels-pro:fill-destinations')
            ->setDescription('Import cities codes');
    }

    /**
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    /**
     * Fills hotels destinations from destinations.xml
     *
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->getConnection()
            ->query('DELETE FROM hotels_destinations WHERE 1')
            ->execute();

        $cityRepo = $this->getContainer()->get('app.repo.hotels_destination');

        $fileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            'HotelsPro',
            'destinations.xml',
        ];

        $fileName = implode(DIRECTORY_SEPARATOR, $fileNameParts);

        if (!file_exists($fileName)) {
            $output->writeln('Expected file: '.$fileName);

            return 1;
        }

        $streamer = XmlStringStreamer::createStringWalkerParser(
            $fileName,
            [
                'captureDepth' => 3,
            ]
        );

        $counter = 0;
        while ($node = $streamer->getNode()) {

            $data = simplexml_load_string($node);
            if (isset($data->DestinationId, $data->Country, $data->City)) {
                $destination = HotelsDestination::fromSimpleXml($data, $cityRepo->findOneBy(['hotelsProId' => $data->DestinationId]));
                $em->persist($destination);
            }

            if ($counter > 0 && $counter % 1000 === 0) {
                $em->flush();
                $em->clear();
            }
            $counter++;
        }

        $em->flush();
        $em->clear();

        return 0;
    }
}
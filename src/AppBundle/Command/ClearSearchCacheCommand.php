<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.02.2016
 * Time: 12:21
 */

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ClearSearchCacheCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('clear:search-cache')
            ->setDescription('Clear search results cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cacheRepository = $this->getContainer()->get('app.repo.cache_value');

        $cacheRepository->markExpiredCacheValues();
        $fileName = $this->dumpFile();
        $cacheRepository->dumpExpiredDataInFile($fileName);
        $cacheRepository->clearExpiredData();
        $output->writeln('Cache data cleared successfully');
    }

    private function dumpFile()
    {
        $fileName = implode('/', [
                str_replace('\\', '/', $this->getContainer()->getParameter('kernel.root_dir')),
                'Resources',
                'dumps',
                'cacheArchive'
            ]) . '/' . (new \DateTime())->format('YmdHis') . '_cache_table_archive.csv';
        return $fileName;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.11.2015
 * Time: 15:45
 */

namespace AppBundle\Command;


use AppBundle\Entity\HotelPro;
use Prewk\XmlStringStreamer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Imports a hotels
 *
 * Class HotelsProImportCommand
 * @package AppBundle\Command
 */
class HotelsProImportCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('app:import-hotels:hotels-pro')
            ->setDescription('Import hotels pro data');
    }

    /**
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    /**
     * Fills lists of hotels from hotellist.xml
     *
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $hotelsRepo = $this->getContainer()->get('app.repo.hotels_pro');

        $fileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            'HotelsPro',
            'hotellist.xml',
        ];

        $fileName = implode(DIRECTORY_SEPARATOR, $fileNameParts);
        if (!file_exists($fileName)) {
            $output->writeln('Expected file: '.$fileName);

            return 1;
        }

        $streamer = XmlStringStreamer::createStringWalkerParser(
            $fileName,
            [
                'captureDepth' => 3,
            ]
        );

        $counter = 0;
        while ($node = $streamer->getNode()) {

            $data = simplexml_load_string($node);
            if (isset($data->HotelCode, $data->Country)) {
                $em->persist(
                    HotelPro::fromSimpleXml(
                        $data,
                        $hotelsRepo->findHotelByHotelCode($data->HotelCode)
                    )
                );
            }

            if ($counter % 1000 === 0 && $counter > 0) {
                $output->writeln(sprintf('%d hotels imported', $counter));
                $em->flush();
                $em->clear();
            }
            $counter++;
        }

        $em->flush();
        $em->clear();
        $output->writeln(sprintf('%d hotels imported', $counter));

        return 0;
    }
}
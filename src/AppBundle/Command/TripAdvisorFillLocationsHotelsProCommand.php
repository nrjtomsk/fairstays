<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 01.03.2016
 * Time: 11:39
 */

namespace AppBundle\Command;


use AppBundle\Entity\HotelPro;
use AppBundle\Entity\TripAdvisorRating;
use AppBundle\Utils\FileUtils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TripAdvisorFillLocationsHotelsProCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('trip-advisor:fill-locations:hotels-pro')
            ->setDescription('Fill hotels with rating data from csv');
    }

    /**
     * @param OutputInterface $output
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $hotelsRepository = $this->getContainer()->get('app.repo.hotels_pro');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $ratingRepository = $em->getRepository('AppBundle:TripAdvisorRating');
        $locationIds = [];

        $rootDir = $this->getContainer()->get('kernel')->getRootDir();
        $path = $rootDir.'/Resources/tripadvisor_hotels_pro.csv';
        if (file_exists($path)) {
            $i = 0;
            foreach (FileUtils::getFileLines($path) as $line) {
                list($hotelCode, $tripAdvisorId) = explode(',', $line);
                $hotelCode = trim($hotelCode);
                $tripAdvisorId = trim($tripAdvisorId);
                if ($hotelCode && $tripAdvisorId) {
                    if (!in_array($tripAdvisorId, $locationIds, true)) {
                        $hotel = $hotelsRepository->findHotelByHotelCode($hotelCode);
                        if ($hotel instanceof HotelPro) {
                            $rating = $ratingRepository->findOneBy(['tripAdvisorId' => $tripAdvisorId]);
                            if (!$rating) {
                                $rating = new TripAdvisorRating();
                                $rating->setTripAdvisorId($tripAdvisorId);
                            }
                            $locationIds[] = $tripAdvisorId;
                            $hotel->setTripAdvisorRating($rating);

                            $em->persist($rating);
                            $em->persist($hotel);
                            $i++;
                            if (($i % 1000) === 0) {
                                $em->flush();
                                $em->clear();
                                $output->writeln($i.' fields have been filled.');
                            }
                        }
                    }
                }
            }
            $em->flush();
            $output->writeln($i.' fields have been filled. Success!');
        } else {
            $output->writeln("Couldn't find file tripadvisor_hotels_pro.csv here: ".$path);
        }
    }
}
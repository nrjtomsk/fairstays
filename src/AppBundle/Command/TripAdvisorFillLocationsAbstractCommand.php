<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.11.2015
 * Time: 11:48
 */

namespace AppBundle\Command;


use AppBundle\Entity\TripAdvisorLocation;
use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Imports locations ids
 *
 * Class ImportTripAdvisorIdsCommand
 * @package AppBundle\Command
 */
abstract class TripAdvisorFillLocationsAbstractCommand extends ContainerAwareCommand
{

    /**
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oldDetectLineEndingsValue = ini_get('auto_detect_line_endings');
        ini_set('auto_detect_line_endings', true);

        $reader = $this->getCsvReader($this->getInAppResourcesCsvFileName());
        if ($reader->count()) {
            $this->updateHotels($reader);
        }

        ini_set('auto_detect_line_endings', $oldDetectLineEndingsValue);

        return 0;
    }

    /**
     * Prepares CsvReader
     *
     * @param $inAppResourcesCsvFileName
     * @return CsvReader
     */
    protected function getCsvReader($inAppResourcesCsvFileName)
    {
        $fullFileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            $inAppResourcesCsvFileName,
        ];
        $fullFileName = implode(DIRECTORY_SEPARATOR, $fullFileNameParts);
        $file = new \SplFileObject($fullFileName);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        return $reader;
    }

    /**
     * @return mixed
     */
    abstract protected function getInAppResourcesCsvFileName();

    /**
     * @param $csvFileLines
     * @return mixed
     */
    abstract protected function updateHotels($csvFileLines);
}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.11.2015
 * Time: 15:45
 */

namespace AppBundle\Command;


use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelImage;
use AppBundle\Repository\HotelRepository;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Prewk\XmlStringStreamer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class ImportTravelGuruHotelsCommand extends ContainerAwareCommand
{

    /**
     * @var Filesystem
     */
    private $fs;

    private $dumpPath;

    private $dumpEndPoint;

    private $authUser;

    private $authPass;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private $batchSize;
    /**
     * @var HotelRepository
     */
    private $hotelRepo;

    protected function configure()
    {
        $this->setName('app:import-hotels:travel-guru')
            ->setDescription('Import hotels from travel guru');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->init($input, $output);
            $dumpName = $this->getDumpName();
            $this->tryDownloadDump($dumpName);

            $this->unzipDump($dumpName);

            $this->syncData();

            $output->writeln('Done');
        } catch (\Exception $e) {
            $this->output->writeln($e->getMessage());
            $this->output->writeln($e->getTrace());
        }

    }

    protected function init(InputInterface $input, OutputInterface $output)
    {
        $this->fs = new Filesystem();
        $this->dumpPath = $this->getContainer()->getParameter('kernel.root_dir').'/Resources/dumps/data/';
        $this->dumpEndPoint = 'http://staticstore.travelguru.com/fairfest/1300001054/1300001054.zip';
        $this->authUser = 'fairfest';
        $this->authPass = 'f@irf3st';
        $this->httpClient = new Client();
        $this->input = $input;
        $this->output = $output;
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->hotelRepo = $this->getContainer()->get('app.repo.hotels');
        $this->entityManager->getConfiguration()->setSQLLogger(null);
        $this->batchSize = 200;
    }

    private function getDumpName()
    {
        return $this->dumpPath.date('Y-m-d').'.zip';
    }

    private function tryDownloadDump($dumpName)
    {
        if (!$this->fs->exists($dumpName)) {
            $content = $this->getDumpContent();
            if (!$content) {
                throw new \RuntimeException('Cannot download xml dumps');
            }
            $this->fs->dumpFile($dumpName, $content);

        }
    }

    private function getDumpContent()
    {
        $response = $this->httpClient->get(
            $this->dumpEndPoint,
            [
                'auth' => [$this->authUser, $this->authPass],
            ]
        );

        if ($response->getStatusCode() === Response::HTTP_OK) {
            return $response->getBody()->getContents();
        }

        return null;
    }

    private function unzipDump($dumpName)
    {
        $zip = new \ZipArchive();
        if ($zip->open($dumpName) === true) {
            $zip->extractTo(dirname($dumpName));
            $zip->close();
        }
    }

    private function syncData()
    {
        $fileName = implode(
            DIRECTORY_SEPARATOR,
            [
                $this->getContainer()->getParameter('kernel.root_dir'),
                'Resources',
                'dumps',
                'data',
                'HotelOverview.xml',
            ]
        );
        if (!$this->fs->exists($fileName)) {
            $this->output->writeln('Expected file: '.$fileName);

            return 1;
        }

        $streamer = XmlStringStreamer::createStringWalkerParser(
            $fileName,
            [
                'captureDepth' => 3,
            ]
        );

        $counter = 0;
        while ($node = $streamer->getNode()) {
            $data = simplexml_load_string($node);
            $hotelCode = $this->getAttributeValue($data->attributes()->HotelCode);
            if ($hotelCode) {
                $hotel = $this->hotelRepo->find($hotelCode);
                if (!$hotel) {
                    $hotel = new Hotel();
                    $hotel->setId($this->getAttributeValue($hotelCode));
                }
                $hotel->setOverview($this->getAttributeValue($data->HotelInfo->Descriptions->DescriptiveText));
                $hotel->setClass($this->getAttributeValue($data->HotelInfo->attributes()->HotelStatus));
                $hotel->setName($this->getAttributeValue($data->attributes()->HotelName));
                $hotel->setLatitude($this->getAttributeValue($data->HotelInfo->Position->attributes()->Latitude));
                $hotel->setLongitude($this->getAttributeValue($data->HotelInfo->Position->attributes()->Longitude));
                $address = $data->ContactInfos->ContactInfo->Addresses->Address->AddressLine;
                $hotel->setAddressLine1($this->getAttributeValue(str_replace('Address_1 : ', '', $address[0])));
                $hotel->setAddressLine2($this->getAttributeValue(str_replace('Address_2 : ', '', $address[1])));
                $hotel->setFormattedAddress($this->getAttributeValue(str_replace('Address: ', '', $address[2])));
                $hotel->setCity(
                    $this->getAttributeValue($data->ContactInfos->ContactInfo->Addresses->Address->CityName)
                );
                $hotel->setCountry(
                    $this->getAttributeValue($data->ContactInfos->ContactInfo->Addresses->Address->County)
                );
                $hotel->setDefaultCheckInTime(
                    $this->getAttributeValue($data->Policies->Policy->PolicyInfo->attributes()->CheckInTime)
                );
                $hotel->setDefaultCheckOutTime(
                    $this->getAttributeValue($data->Policies->Policy->PolicyInfo->attributes()->CheckOutTime)
                );

                $hotel->setReviewRating($this->getAttributeValue($data->TPA_Extensions->ReviewRating));
                $hotel->setReviewCount($this->getAttributeValue($data->TPA_Extensions->ReviewCount));
                $hotel->setStar($this->getAttributeValue($data->TPA_Extensions->Hotel_Star));
                $hotel->setImagePath($this->getAttributeValue($data->TPA_Extensions->ImagePath));
                $hotel->setSearchKey($this->getAttributeValue($data->TPA_Extensions->HotelSearchKey));
                $hotel->setAreaSeoId($this->getAttributeValue($data->TPA_Extensions->AreaSeoId));
                $hotel->setSecondaryAreaIds($this->getAttributeValue($data->TPA_Extensions->SecondaryAreaIds));
                $hotel->setSecondaryAreaName($this->getAttributeValue($data->TPA_Extensions->SecondaryAreaName));
                $hotel->setState($this->getAttributeValue($data->TPA_Extensions->HotelState));
                $hotel->setPinCode($this->getAttributeValue($data->TPA_Extensions->HotelPinCode));
                $hotel->setNumberOfRooms($this->getAttributeValue($data->TPA_Extensions->HotelNoOfRooms));
                $hotel->setNumberOfFloors($this->getAttributeValue($data->TPA_Extensions->HotelNoOfFloors));
                $hotel->setCityZone($this->getAttributeValue($data->TPA_Extensions->CityZone));
                $hotel->setWeekDayRank($this->getAttributeValue($data->TPA_Extensions->WeekDayRank));
                $hotel->setWeekEndRank($this->getAttributeValue($data->TPA_Extensions->WeekEndRank));
                $hotel->setThemeList($this->getAttributeValue($data->TPA_Extensions->ThemeList->Theme));
                $hotel->setCategoryList($this->getAttributeValue($data->TPA_Extensions->CategoryList->Category));

                $this->entityManager->persist($hotel);
                $this->entityManager->flush($hotel);

                $images = (array)$data->HotelInfo->Descriptions->MultimediaDescriptions->MultimediaDescription->ImageItems->ImageItem;
                if ($hotel->getImages()->count() === 0 && array_key_exists('ImageFormat', $images) && is_array(
                        $images['ImageFormat']
                    ) && count(
                        $images['ImageFormat']
                    )
                ) {
                    foreach ($images['ImageFormat'] as $image) {
                        $imageEntity = new HotelImage();
                        $imageEntity->setImagePath($this->getAttributeValue($image->URL));
                        $imageEntity->setTitle($this->getAttributeValue($image->URL));
                        $hotel->addImage($imageEntity);
                        $imageEntity->setHotel($hotel);
                        $this->entityManager->persist($imageEntity);
                    }

                    $this->entityManager->flush();
                }
                $this->entityManager->clear();
            }
            $counter++;
            if ($counter % 1000 === 0) {
                $this->output->writeln(sprintf('%d hotels imported', $counter));
            }
        }
        $this->entityManager->flush();

        $this->output->writeln(sprintf('%d hotels imported', $counter));
        $this->output->writeln(sprintf('Success'));


        return 0;
    }

    private function getAttributeValue($attr)
    {
        if (method_exists($attr, '__toString')) {
            return trim($attr->__toString());
        }

        return $attr;
    }

}
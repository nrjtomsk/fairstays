<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 01.03.2016
 * Time: 11:39
 */

namespace AppBundle\Command;


use AppBundle\Entity\Hotel;
use AppBundle\Entity\TripAdvisorRating;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TripAdvisorFillLocationsTravelGuruCommand extends TripAdvisorFillLocationsAbstractCommand
{
    protected function configure()
    {
        $this->setName('trip-advisor:fill-locations:travel-guru')
            ->setDescription('Fill hotels with rating data from csv');
    }


    /**
     * @return mixed
     */
    protected function getInAppResourcesCsvFileName()
    {
        return 'TripAdvisor_Travelguru.csv';
    }

    /**
     * @param $csvFileLines
     * @return mixed
     */
    protected function updateHotels($csvFileLines)
    {
        $hotelsRepository = $this->getContainer()->get('app.repo.hotels');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $ratingRepository = $em->getRepository('AppBundle:TripAdvisorRating');
        $locationIds = [];

        foreach ($csvFileLines as $row) {
            $hotelId = trim($row['VendorID']);
            $hotelName = trim($row['Hotel Name']);
            $locationId = trim($row['TripAdvisor ID']);

            if ($hotelId && $hotelName && $locationId) {

                if (!in_array($locationId, $locationIds, true)) {

                    $hotel = $hotelsRepository->findOneBy(
                        [
                            'id' => sprintf('%08d', $hotelId),
                            'name' => $hotelName,
                        ]
                    );

                    if ($hotel instanceof Hotel) {

                        $rating = $ratingRepository->findOneBy(['tripAdvisorId' => $locationId]);
                        if (!$rating) {
                            $rating = new TripAdvisorRating();
                            $rating->setTripAdvisorId($locationId);
                        }
                        $locationIds[] = $locationId;
                        $hotel->setTripAdvisorRating($rating);

                        $em->persist($rating);
                        $em->persist($hotel);
                    }
                }
            }
        }

        $em->flush();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 22.12.2015
 * Time: 16:33
 */

namespace AppBundle\Command;


use Prewk\XmlStringStreamer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Imports a hotels description
 *
 * Class HotelsProImportHotelDescrCommand
 * @package AppBundle\Command
 */
class HotelsProImportHotelDescrCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('hotels-pro:fill-descriptions')
            ->setDescription('Import hotel descriptions');
    }

    /**
     * Fills descriptions of hotels from hoteldescr.xml
     *
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $fileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            'HotelsPro',
            'hoteldescr.xml',
        ];

        $fileName = implode(DIRECTORY_SEPARATOR, $fileNameParts);

        if (!file_exists($fileName)) {
            $output->writeln('Expected file: '.$fileName);

            return 1;
        }

        $streamer = XmlStringStreamer::createStringWalkerParser(
            $fileName,
            [
                'captureDepth' => 3,
            ]
        );

        $hotelRepo = $this->getContainer()->get('app.repo.hotels_pro');
        $counter = 0;
        while ($node = $streamer->getNode()) {
            $data = simplexml_load_string($node);
            if (isset($data->HotelCode)) {
                $hotel = $hotelRepo->findHotelByHotelCode($data->HotelCode);
                if ($hotel) {
                    if (isset($data->HotelInfo) && mb_strlen($data->HotelInfo) > 0) {
                        $hotel->setDescription($data->HotelInfo);
                    }
                    $em->persist($hotel);
                }
            }
            if ($counter > 0 && $counter % 1000 === 0) {
                $em->flush();
                $em->clear();
            }
            $counter++;
        }

        $em->flush();
        $em->clear();

        return 0;
    }
}
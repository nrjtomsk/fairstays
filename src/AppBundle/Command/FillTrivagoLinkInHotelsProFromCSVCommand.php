<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.06.16
 * Time: 15:58
 */

namespace AppBundle\Command;


use AppBundle\Repository\HotelProRepository;
use AppBundle\Utils\FileUtils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FillTrivagoLinkInHotelsProFromCSV
 * @package AppBundle\Command
 */
class FillTrivagoLinkInHotelsProFromCSVCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $filePath;

    /**
     * @var OutputInterface
     */
    private $out;

    /**
     * @var HotelProRepository
     */
    private $hotelProRepository;


    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('hotels-pro:fill-trivago-link')
            ->setDescription('Import link hotel');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->filePath = $this->getContainer()->get('file_locator')->locate('trivago.csv');
        $this->out = $output;

        $this->hotelProRepository = $this->getContainer()->get('app.repo.hotels_pro');
    }


    /**
     * Fills trivago url from csv
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->fillTablePriceSelectedHotels();
        $output->writeln('Success fill table price_selected_hotels');
    }

    /**
     * Fill data form csv to DB table price_selected_hotels
     * @return void
     */
    private function fillTablePriceSelectedHotels()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $counter = 0;
        foreach (FileUtils::getFileLines($this->filePath) as $line) {
            $row = str_getcsv($line, ',');
            list($code, $link) = $row;
            $hotel = $this->hotelProRepository->findHotelByHotelCode($code);
            if ($hotel !== null) {
                $hotel->setTrivagoUrl($link);
                $em->persist($hotel);
            }

            $counter++;
            if ($counter % 1000 == 0) {
                $em->flush();
                $em->clear();
            }
        }
        $em->flush();
        $em->clear();
        $this->out->writeln(sprintf('%d items imported successfully for', $counter));
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 30.05.16
 * Time: 10:08
 */

namespace AppBundle\Command;

use AppBundle\Entity\PriceSelectedHotels;
use AppBundle\Repository\PriceSelectedHotelsRepository;
use AppBundle\Utils\FileUtils;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FillSelectedHotelsWithRateAndWithDiscountInterestPercentByCSVCommand
 * @package AppBundle\Command
 */
class FillSelectedHotelsWithRateAndWithDiscountInterestPercentByCSVCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $fileUrl;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $pathToTempStorageDirectory;

    /**
     * @var OutputInterface
     */
    private $out;

    /**
     * @var string
     */
    private $pathToAmazonFile;

    /**
     * @var string
     */
    private $pathToDumpFile;

    /**
     * @var PriceSelectedHotelsRepository
     */
    private $priceSelectedHotelRepository;

    /**
     * @var string
     */
    private $currentDateString;


    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('hotels-pro:perform-selected-hotels-with-rate-with-percent')
            ->addOption('date', $shortcat = null, InputOption::VALUE_OPTIONAL, 'Date for performing amazon file')
            ->setDescription('Import selected hotel');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->fileUrl = $this->getContainer()->getParameter('path_to_price_selected_hotels_with_rate_csv');
        $this->httpClient = new Client();

        $this->pathToTempStorageDirectory = $this->getContainer()->get('file_locator')->locate(
            'TempForHotelsProFilter'
        );
        $this->out = $output;

        $dateString = $input->getOption('date');
        if ($dateString == null) {
            $this->currentDateString = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d');
        } else {
            if (false !== date_parse($dateString)) {
                $this->currentDateString = (new \DateTime($dateString, new \DateTimeZone('UTC')))->format('Y-m-d');
            } else {
                throw new \RuntimeException(
                    sprintf(
                        'Invalid date format for input value %s. We suggest you to use following format "Y-m-d"',
                        $dateString
                    )
                );
            }
        }

        $this->pathToAmazonFile = sprintf($this->fileUrl, $this->currentDateString);
        $this->pathToDumpFile = $this->pathToTempStorageDirectory.'/hotels_s2_ip_'.$this->currentDateString.'.csv';
        $this->priceSelectedHotelRepository = $this->getContainer()->get('app.repo.price_selected_hotels');
    }


    /**
     * Fills price_selected_hotels from hotels_s1_YYYY-MM-DD.csv
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->downloadDataFileFromAmazon();
        $this->fillTablePriceSelectedHotels();
        $this->clearExpiredDataFromPriceSelectedHotels();
        $output->writeln('Success fill table price_selected_hotels');
    }

    private function downloadDataFileFromAmazon()
    {
        $this->removeFile($this->pathToDumpFile);
        $this->writeFileFromResponse($this->getFileFromAmazon());
    }

    /**
     * Remove download from Amazon .csv file
     * @param $file
     */
    private function removeFile($file)
    {
        $fs = new Filesystem();
        try {
            $fs->remove($file);
        } catch (IOException $e) {
            $this->out->writeln($e->getMessage());
        }
    }

    /**
     * Write local file hotels_YYYY-MM-DD.csv
     * @param ResponseInterface $response
     * @throws \Exception
     */
    private function writeFileFromResponse(ResponseInterface $response)
    {
        $handle = fopen($this->pathToDumpFile, 'w');
        try {
            while (!empty($line = $response->getBody()->read(1024))) {
                fwrite($handle, $line, 1024);
            }
        } finally {
            fclose($handle);
        }
    }

    /**
     * Get file from Amazon
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function getFileFromAmazon()
    {
        return $this->httpClient->get($this->pathToAmazonFile);
    }

    /**
     * Fill data form csv to DB table price_selected_hotels
     * @return void
     */
    private function fillTablePriceSelectedHotels()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $counter = 0;
        foreach (FileUtils::getFileLines($this->pathToDumpFile) as $line) {
            $row = str_getcsv($line, ',');
            list($date, $city, $hotels, $price, $save) = $row;
            $hotelsArray = explode(',', $hotels);
            $priceArray = explode(',', $price);
            $saveArray = explode(',', $save);
            $trimmedHotelsArray = [];
            $trimmedPriceArray = [];
            $trimmedSaveArray = [];

            if (is_array($hotelsArray) && count($hotelsArray)) {
                foreach ($hotelsArray as $hotel) {
                    $trimmedHotelsArray[] = trim($hotel);
                }
            }
            if (is_array($priceArray) && count($priceArray)) {
                foreach ($priceArray as $price) {
                    $trimmedPriceArray = trim($price);
                }
            }
            if (is_array($saveArray) && count($saveArray)) {
                foreach ($saveArray as $save) {
                    $trimmedSaveArray = trim($save);
                }
            }

            /** @var PriceSelectedHotels $selectedHotels */
            $selectedHotels = $this->priceSelectedHotelRepository->findOneByCityCodeAndCheckInDate($city, $date);

            if (!$selectedHotels) {
                $selectedHotels = new PriceSelectedHotels();
                $selectedHotels->setCityCode($city);
                $selectedHotels->setCheckInDate($date);
            }

            $selectedHotels->setSelectedHotels($trimmedHotelsArray);
            $selectedHotels->setSelectedMarkupPercent($trimmedPriceArray);
            $selectedHotels->setSaveAmount($trimmedSaveArray);
            $selectedHotels->setInsertTime(new \DateTime($this->currentDateString, new \DateTimeZone('UTC')));

            $em->persist($selectedHotels);
            $counter++;
            if ($counter % 1000 == 0) {
                $em->flush();
                $em->clear();
            }
        }
        $em->flush();
        $em->clear();
        $this->removeFile($this->pathToDumpFile);
        $this->out->writeln(sprintf('%d items imported successfully for date: %s', $counter, $this->currentDateString));
    }

    private function clearExpiredDataFromPriceSelectedHotels()
    {
        $this->out->writeln('Deleting expired PriceSelectedHotels data');
        $this->priceSelectedHotelRepository->deletePriceSelectedHotelsOlderThanTwoDays();
        $this->out->writeln('PriceSelectedHotels data was deleted successfully');
    }

}
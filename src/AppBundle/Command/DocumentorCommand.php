<?php
namespace AppBundle\Command;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Command for the console
 *
 * PhpDocumentorBundle's command to generate the project documentation with
 * phpDocumentor2.
 *
 * @author Artur Gajewski
 * You can also find the original here
 * https://github.com/artur-gajewski/DocumentorBundle/blob/master/Command/DocumentorCommand.php
 */
class DocumentorCommand extends ContainerAwareCommand
{
    /**
     * Set the configuration for the command syntax and description
     */
    protected function configure()
    {
        $this->setName('documentation:create')
            ->setDescription('Creates project documentation with phpDocumentor2 accessible with project\'s url.');
    }

    /**
     * Execution of the console command
     *
     * This function checks all source code from the src folder, ignores this bundle's
     * files, and generates documentation HTML with phpDocumentor2.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating project documentation, please wait...');

        $rootDir = $this->getContainer()->get('kernel')->getRootDir();

        $target = realpath($rootDir.'/../web/phpDoc');

        $configFile = realpath($rootDir.'/../phpdoc.dist.xml');

        $documentorPath = $rootDir.'/../phpDocumentor.phar';

        (new Filesystem())->dumpFile(
            $documentorPath,
            (new Client())->get($this->getContainer()->getParameter('php_doc_url'))->getBody()->getContents()
        );

        if (file_exists($configFile)) {

            $command = 'php '.$documentorPath.' -c '.$configFile.' -t '.$target;

            $process = new Process($command);
            $process->setTimeout(3600);
            $process->run();
            if (!$process->isSuccessful()) {
                throw new \RuntimeException($process->getErrorOutput());
            }
            $output->writeln('Documentation created. Go to http://yourdomain.com/phpDoc/index.html');
        } else {
            $output->writeln("Couldn't find configuration file phpdoc.dist.xml in root directory of project...");
        }

    }
}
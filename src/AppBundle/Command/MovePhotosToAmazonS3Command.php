<?php

namespace AppBundle\Command;

use AppBundle\Entity\HotelPro;
use AppBundle\Repository\HotelProRepository;
use Aws\Result;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MovePhotosToAmazonS3Command extends ContainerAwareCommand
{

    /**
     * @var HotelProRepository
     */
    private $hotelsProRepository;

    /**
     * @var S3Client
     */
    private $amazonS3Client;

    /**
     * @var EntityManager
     */
    private $entityManager;
    private $s3BucketName = 'hotel-images-selectrooms';

    private $batchSize = 5;
    private $counter = 0;
    private $hotelCounter = 0;
    private $imageCounter = 0;
    private $errorCounter = 0;
    private $skippedCounter = 0;

    private $initialOffset = 0;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException
     */
    protected function configure()
    {
        $this
            ->setName('app:move-photos-to-amazon')
            ->addOption('offset', 'offset', InputOption::VALUE_OPTIONAL, 'Initial offset', 0)
            ->setDescription('Move hotelsPro images to Amazon S3');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->output = $output;
        $this->hotelsProRepository = $this->getContainer()->get('app.repo.hotels_pro');
        $this->amazonS3Client = new S3Client(
            [
                'version' => 'latest',
                'region' => 'eu-west-1',
                'credentials' => [
                    'key' => 'AKIAIDGNEHCSQFKX2G5Q',
                    'secret' => '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',
                ],
            ]
        );
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->httpClient = new Client([
            'timeout' => 60
        ]);
        if ($input->getOption('offset')) {
            $this->initialOffset = $input->getOption('offset');
        }
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output->writeln('Importing images to S3 started');
        try {
            $offset = $this->initialOffset;
            while ($hotels = $this->hotelsProRepository->findBy(
                [],
                ['id' => 'ASC'],
                $limit = $this->batchSize,
                $offset
            )) {
                $this->performHotels($hotels);

                $this->hotelCounter += count($hotels);
                $offset += $this->batchSize;

                $this->output->writeln('');
                $this->output->writeln(
                    sprintf(
                        'Hotels: %d performed. Images: %d updated, %d skipped, %d errors',
                        $this->hotelCounter,
                        $this->imageCounter ,
                        $this->skippedCounter,
                        $this->errorCounter
                    )
                );
                $this->output->writeln('');
            }
        } catch (\Exception $e) {
            $this->output->writeln(sprintf('Error: %s. Trace: %s', $e->getMessage(), $e->getTraceAsString()));
        }

        $this->output->writeln(sprintf('Importing images to S3 complete'));
        $this->output->writeln(sprintf('Hotels performed: %d', $this->hotelCounter));
        $this->output->writeln(sprintf('Images uploaded: %d', $this->imageCounter));
        $this->output->writeln(sprintf('Images skipped: %d', $this->skippedCounter));
        $this->output->writeln(sprintf('Errors count: %d', $this->errorCounter));

    }

    /**
     * @param HotelPro[] $hotels
     */
    private function performHotels(array $hotels)
    {
        if (count($hotels)) {
            /** @var HotelPro $hotel */
            foreach ($hotels as $hotel) {
                $this->counter++;

                $this->output->writeln(
                    sprintf(
                        '%d. Hotel %s. Images to perform: %d',
                        $this->counter,
                        $hotel->getHotelCode(),
                        count($hotel->getImagesUrls())
                    )
                );

                $this->performHotel($hotel);
            }

            $this->entityManager->clear();
        }


    }

    private function performHotel(HotelPro $hotel)
    {
        if (count($hotel->getImagesUrls())) {
            /** @var Promise[] $promises */
            $promises = $this->getS3Promises($hotel);
            $imageUrls = $this->getS3ImageUrls($promises);
            if (count($imageUrls)) {
                $hotel->setImagesUrls($imageUrls);
                $this->entityManager->persist($hotel);
                $this->entityManager->flush($hotel);

                $this->imageCounter += count($imageUrls);
            }
        }
    }

    /**
     * @param HotelPro $hotel
     * @return Promise[]
     */
    private function getS3Promises(HotelPro $hotel)
    {
        $promises = [];
        $skipped = $success = $errors = 0;
        foreach ($hotel->getImagesUrls() as $imageUrl) {
            if (false !== mb_strpos($imageUrl, $this->s3BucketName)) {
                $this->output->write('.');
                $skipped++;
                $this->skippedCounter++;
                continue;
            }
            try {
                $fileName = $this->getFileNameForImage($hotel, $imageUrl);
                $promises[$fileName] = $this->getS3Promise($fileName, $imageUrl);
                $this->output->write('.');
                $success++;
            } catch (\Exception $e) {
                $errors++;
                $this->errorCounter++;
                $this->output->write('E');
            }
        }
        $this->output->write(sprintf(' skipped: %d, errors: %d, success: %d', $skipped, $errors, $success));
        $this->output->writeln('');

        return $promises;
    }

    private function getFileNameForImage(HotelPro $hotel, $imageUrl)
    {
        $nameParts = explode('.', $imageUrl);
        $ext = $nameParts[count($nameParts) - 1];

        return str_replace('.', '', uniqid($prefix = $hotel->getHotelCode().'_', $moreEntropy = true)).'.'.$ext;
    }

    /**
     * @param $fileName
     * @param $imageUrl
     * @return Promise
     */
    private function getS3Promise($fileName, $imageUrl)
    {
        $promise = $this->httpClient->sendAsync(new Request('GET', $imageUrl))
            ->then(
                function (Response $response) use ($fileName) {
                    return $this->amazonS3Client->putObjectAsync(
                        [
                            'Bucket' => $this->s3BucketName,
                            'Key' => $fileName,
                            'Body' => $response->getBody()->getContents(),
                            'contentType' => 'image/jpg',
                            'ACL' => 'public-read',
                        ]
                    );
                },
                function ($reason) {
                    return null;
                }
            );
        return $promise;
        
    }

    private function getS3ImageUrls($promises)
    {
        $imageUrls = [];
        if (count($promises)) {
            /** @var Result[] $results */
            $results = \GuzzleHttp\Promise\unwrap($promises);

            if (count($results)) {
                $imageUrls = array_values(
                    array_filter(
                        array_map(
                            function (Result $result = null) {
                                if ($result instanceof Result) {
                                    return $result->get('ObjectURL');
                                }
                            },
                            $results
                        )
                    )
                );
            }
        }

        return $imageUrls;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.11.2015
 * Time: 15:45
 */

namespace AppBundle\Command;


use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Creates a new Super Admin
 *
 * Class AddSuperAdminCommand
 * @package AppBundle\Command
 */
class AddSuperAdminCommand extends ContainerAwareCommand
{
    /**
     * Configures of name, description and args of commands
     */
    protected function configure()
    {
        $this->setName('custom:create-super-admin')
            ->setDescription('Create super admin user')
            ->addArgument('username', InputArgument::REQUIRED, 'super admin login')
            ->addArgument('password', InputArgument::REQUIRED, 'super admin password');
    }

    /**
     * Creates Super Admin
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $role = 'ROLE_SUPER_ADMIN';

        $userProvider = $this->getContainer()->get('app.service.user.provider');
        /** @var User $user */
        $user = $userProvider->createUser($username);
        $user->setPassword($this->getContainer()->get('security.password_encoder')->encodePassword($user, $password));
        $user->setRoles([$role]);
        $userProvider->save($user);

        return 0;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.11.2015
 * Time: 15:45
 */

namespace AppBundle\Command;


use AppBundle\Entity\TripAdvisorRating;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Requests hotels data from trip advisor
 *
 * Class TripAdvisorRequestHotelsDataCommand
 * @package AppBundle\Command
 */
class TripAdvisorUpdateRatingsCommand extends ContainerAwareCommand
{

    /**
     * Selection limit
     */
    const LIMIT = 100;

    /**
     * Trip advisor api url
     */
    private $apiUrl;

    /**
     * Trip advisor client key
     */
    private $appKey;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var JsonEncoder
     */
    private $decoder;

    /**
     * @var EntityRepository
     */
    private $ratingRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('trip-advisor:update-ratings')
            ->setDescription('Request hotels data from trip advisor');
    }

    /**
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->apiUrl = $this->getContainer()->getParameter('trip_advisor_base_uri');
        $this->appKey = $this->getContainer()->getParameter('trip_advisor_app_key');
        $this->client = new Client(['base_uri' => $this->apiUrl]);
        $this->decoder = $this->getContainer()->get('serializer.encoder.json');
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ratingRepository = $this->getContainer()->get('doctrine')->getRepository('AppBundle:TripAdvisorRating');
    }

    /**
     * Requests hotels data from trip advisor
     *
     * @inheritdoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Updating started');
        /** @var TripAdvisorRating[] $ratings */
        $hundredRatings = $this->ratingRepository->createQueryBuilder('r')
            ->where('r.updatedAt < :date')
            ->orWhere('r.rating IS NULL')
            ->orderBy('r.updatedAt', 'ASC')
            ->setParameter('date', new \DateTime('- 30 days'))
            ->getQuery()
            ->setMaxResults(self::LIMIT)
            ->getResult();

        $successCount = $errorsCount = 0;
        $skippedCount = count($hundredRatings);

        if (count($hundredRatings)) {
            $output->writeln(sprintf('%d rating for update. updating...', count($hundredRatings)));
            while (count($hundredRatings)) {

                $ratings = null;
                for ($i = 0; $i < 10; $i++) {
                    $ratings [] = array_shift($hundredRatings);
                }

                $hotelsIds = [];
                foreach ($ratings as $rating) {
                    $hotelsIds[] = trim($rating->getTripAdvisorId());
                    $rating->setUpdatedAt(new \DateTime());
                }
                $hotelsIdsString = implode(',', $hotelsIds);

                try {
                    $response = $this->sendRequest($hotelsIdsString);
                } catch (\Exception $e) {
                    $this->getContainer()->get('logger')->error($e->getMessage(), $e->getTrace());
                    $errorsCount++;
                }

                if (null !== $response) {
                    foreach ($response['data'] as $data) {

                        foreach ($ratings as $rating) {

                            if (null !== $rating && (int)$rating->getTripAdvisorId() === (int)$data['location_id']) {
                                $rating->setCityName(
                                    isset($data['address_obj']['city']) ? $data['address_obj']['city'] : null
                                );
                                $rating->setStateName(
                                    isset($data['address_obj']['state']) ? $data['address_obj']['state'] : null
                                );
                                $rating->setCountryName(
                                    isset($data['address_obj']['country']) ? $data['address_obj']['country'] : null
                                );
                                $rating->setRating(isset($data['rating']) ? (float)$data['rating'] : null);
                                $rating->setResponseBody(
                                    isset($data) ? $this->decoder->encode(
                                        $data,
                                        JsonEncoder::FORMAT
                                    ) : null
                                );

                                $successCount++;
                                $this->entityManager->persist($rating);
                            }
                        }
                    }
                }
                $this->entityManager->flush();
            }
            $output->writeln(
                sprintf(
                    'Ratings updated. Success: %d, skipped: %d, errors: %d',
                    $successCount,
                    $skippedCount - $successCount,
                    $errorsCount
                )
            );
        } else {
            $output->writeln('No ratings for update');
        }
        $output->writeln('Updating complete');

    }

    /**
     * Decodes and returns content
     *
     * @param $hotelsIdsString
     * @return array
     */
    private function sendRequest($hotelsIdsString)
    {
        $okResponse = false;
        try {
            $content = $this->client->get(
                $hotelsIdsString.'/hotels',
                [
                    'query' => [
                        'key' => $this->appKey,
                    ],
                ]
            )->getBody()->getContents();
            $okResponse = true;
        } catch (ServerException $e) {
            if ($e->hasResponse()) {
                $content = $e->getResponse()->getBody()->getContents();
            } else {
                $content = 'Exception: '.$e->getMessage()."\r\n".$e->getTraceAsString();
            }
            $exception = $e;
        } catch (\Exception $exc) {
            $content = 'Exception: '.$exc->getMessage()."\r\n".$exc->getTraceAsString();
            $exception = $exc;
        }
        if ($okResponse) {
            return $this->decoder->decode(
                $content,
                JsonEncoder::FORMAT
            );
        }

        throw new \RuntimeException($exception->getMessage(), $code = 0, $exception);
    }

}
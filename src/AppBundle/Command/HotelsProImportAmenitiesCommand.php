<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 22.12.2015
 * Time: 14:44
 */

namespace AppBundle\Command;


use Prewk\XmlStringStreamer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Imports a hotel amenities
 *
 * Class HotelsProImportAmenitiesCommand
 * @package AppBundle\Command
 */
class HotelsProImportAmenitiesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('hotels-pro:fill-amenities')
            ->setDescription('Import hotel amenities');
    }

    /**
     * Fills hotel amenities from hotelamenities.xml
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $fileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            'HotelsPro',
            'hotelamenities.xml',
        ];

        $fileName = implode(DIRECTORY_SEPARATOR, $fileNameParts);

        if (!file_exists($fileName)) {
            $output->writeln('Expected file: '.$fileName);

            return 1;
        }

        $streamer = XmlStringStreamer::createStringWalkerParser(
            $fileName,
            [
                'captureDepth' => 3,
            ]
        );

        $hotelRepo = $this->getContainer()->get('app.repo.hotels_pro');
        $counter = 0;
        while ($node = $streamer->getNode()) {
            $data = simplexml_load_string($node);
            if (isset($data->HotelCode)) {
                $hotel = $hotelRepo->findHotelByHotelCode($data->HotelCode);
                if ($hotel) {

                    if (isset($data->PAmenities) && mb_strlen($data->PAmenities) > 0) {
                        $hotel->setPropertyAmenities(explode(';', $data->PAmenities));
                    }
                    if (isset($data->RAmenities) && mb_strlen($data->RAmenities) > 0) {
                        $hotel->setRoomAmenities(explode(';', $data->RAmenities));
                    }
                    if (isset($data->RoomsNumber) && $data->RoomsNumber) {
                        $hotel->setRoomsNumber((int)$data->RoomsNumber);
                    } else {
                        $hotel->setRoomsNumber(0);
                    }
                    $em->persist($hotel);
                }
            }
            if ($counter > 0 && $counter % 1000 === 0) {
                $em->flush();
                $em->clear();
            }
            $counter++;
        }

        $em->flush();
        $em->clear();

        return 0;
    }
}
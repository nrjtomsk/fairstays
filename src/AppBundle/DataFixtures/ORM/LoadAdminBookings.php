<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 21.01.2016
 * Time: 10:35
 */
namespace AppBundle\DataFixtures\ORM;


use ApiBundle\Service\Booking\BookingParams;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Entity\BookingRequest;
use AppBundle\Entity\Hotel;
use AppBundle\Entity\User;
use AppBundle\Service\User\UserProvider;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\DataFixtures\SharedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Money\Currency;
use Money\Money;
use Symfony\Bridge\Doctrine\Tests\Fixtures\ContainerAwareFixture;

/**
 * Load data fixtures for Bookings
 *
 * Class LoadAdminBookings
 * @package AppBundle\DataFixtures\ORM
 */
class LoadAdminBookings extends ContainerAwareFixture implements OrderedFixtureInterface, SharedFixtureInterface
{
    /**
     * @var ReferenceRepository
     */
    private $repo;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $hotelTG = new Hotel();
        $hotelTG->setId('00005053'); // please don't change if tests works fine
        $hotelTG->setState('STATE');
        $hotelTG->setAddressLine1('address1');
        $hotelTG->setAddressLine2('address2');
        $hotelTG->setArea('area');
        $hotelTG->setAreaSeoId('areaSeoId');
        $hotelTG->setCategoryList('cat1;cat2');
        $hotelTG->setCity('New Delhi');
        $hotelTG->setCityZone('city zone');
        $hotelTG->setClass('5 stars');
        $hotelTG->setCountry('India');
        $hotelTG->setDefaultCheckInTime('12:00:00');
        $hotelTG->setDefaultCheckOutTime('12:00:00');
        $hotelTG->setFormattedAddress('formatted address');
        $hotelTG->setGroupId('groupId');
        $hotelTG->setGroupName('groupName');
        $hotelTG->setImagePath('image.path');
        $hotelTG->setLatitude('44.222222');
        $hotelTG->setLongitude('33.33333');
        $hotelTG->setLocation('location');
        $hotelTG->setNumberOfFloors(3);
        $hotelTG->setNumberOfRooms(10);
        $hotelTG->setName('hotel name');
        $hotelTG->setPinCode('12345');
        $hotelTG->setReviewCount(3);
        $hotelTG->setReviewRating(4);
        $hotelTG->setOverview('overview');
        $hotelTG->setSearchKey('searchKey');
        $hotelTG->setSecondaryAreaIds('secondaryAreaIds');
        $hotelTG->setSecondaryAreaName('secondaryAreaName');
        $hotelTG->setStar(4);
        $hotelTG->setThemeList('themeList');
        $hotelTG->setWeekDayRank('WeekDayRank');
        $hotelTG->setWeekEndRank('WeekEndRank');

        $manager->persist($hotelTG);
        $manager->flush();

        /** @var UserProvider $userProvider */
        $userProvider = $this->container->get('app.service.user.provider');
        /** @var User $user */
        $user = $userProvider->createUser('9112345678');
        $userProvider->save($user);

        $bookingParams = new BookingParams();
        $bookingParams->setMeta(
            '{"service_name": "hotelsPro","hotels_pro_search_id": "SJ-01161968","hotels_pro_process_id": "HA-35811048", "city_id":"2434"}'
        );

        $metaDataDTO = MetaDataDTO::fromJsonString($bookingParams->getMeta());

        $booking = new BookingRequest();
        $booking->setCorrelationId('123');
        $booking->setTransactionIdentifier('');
        $booking->setHotelCode('12321');
        $booking->setGuaranteeType('Type');
        $booking->setCommentText('CommentText');
        $booking->setGuestCounts('8');
        $booking->setBookingId('123123');
        $booking->setAmountAfterTax('');
        $booking->setPayuAmount('');
        $booking->setPayuTransactionId('12334');
        $booking->setPayuMihPayId('12334');
        $booking->setMetaData($metaDataDTO);
        $booking->setHotelsProAgencyReferenceNumber('12334');
        $booking->setHotelsProBookingStatus('');
        $booking->setHotelsProCancellationNote('');
        $booking->setHotelCode($hotelTG->getId());
        $booking->setUser($user);
        $booking->setTotalAmountBeforeTaxExternal('');
        $booking->setTotalTaxesAmount('');
        $booking->setTravelGuruBookingId('');
        $booking->setProfileAddressLine1('Address 1');
        $booking->setProfileAddressLine2('Address 2');
        $booking->setProfileAddressStateProv('');
        $booking->setProfileAddressStateProvCode('');
        $booking->setTrackingId('');
        $booking->setDeletedAt(null);
        $booking->setHotelsProCancelResponseId('');
        $booking->setProfileCityName('');
        $booking->setProfileCountryName('');
        $booking->setProfileCountryNameCode('');
        $booking->setProfileEmail('');
        $booking->setProfileGivenName('');
        $booking->setProfileLastName('');
        $booking->setProfileMiddleName('');
        $booking->setProfileNamePrefix('');
        $booking->setProfilePhoneAreaCityCode('');
        $booking->setProfilePhoneCountryAccessCode('');
        $booking->setProfilePhoneExtension('');
        $booking->setProfilePhoneNumber('');
        $booking->setProfilePhoneTechType('');
        $booking->setProfilePostalCode('');
        $booking->setRequestedCurrency('');
        $booking->setRatePlanCode('');
        $booking->setRoomTypeNumberOfUnits('');
        $booking->setRoomTypeCode('');
        $booking->setTimeSpanStartDate(new \DateTime());
        $booking->setTimeSpanEndDate(new \DateTime());
        $booking->setTotalAmountBeforeTax('');
        $booking->setPublicBookingId('');
        $booking->setStatus('');
        $booking->setTotalTaxesAmountExternal('');
        $booking->setTotalTaxesAmount('');
        $booking->setRoomName('');
        $booking->setRoomPricePerNight('');
        $booking->setGuestCharges('');
        $booking->setHotelsCancellationPolicy(['remarks' => 'test']);

        $manager->persist($booking);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 7000;
    }

    /**
     * Set the reference repository
     *
     * @param ReferenceRepository $referenceRepository
     */
    public function setReferenceRepository(ReferenceRepository $referenceRepository)
    {
        $this->repo = $referenceRepository;
    }
}
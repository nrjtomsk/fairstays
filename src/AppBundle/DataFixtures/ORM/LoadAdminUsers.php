<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.09.2015
 * Time: 15:09
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\AdminLogin;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\DataFixtures\SharedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Tests\Fixtures\ContainerAwareFixture;

/**
 * Load data fixtures for admins
 *
 * Class LoadAdminUsers
 * @package AppBundle\DataFixtures\ORM
 */
class LoadAdminUsers extends ContainerAwareFixture implements FixtureInterface, OrderedFixtureInterface, SharedFixtureInterface
{
    /**
     * @var ReferenceRepository
     */
    private $repo;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userProvider = $this->container->get('app.service.user.provider');
        /** @var User $user */
        $user = $userProvider->createUser('admin');
        $user->setPassword($this->container->get('security.password_encoder')->encodePassword($user, 'admin'));
        $user->setLastName('admin');
        $user->setFirstName('admin');
        $user->setFullName('admin');
        $user->setRoles(
            [
                'ROLE_USER',
                'ROLE_ADMIN',
                'ROLE_SUPER_ADMIN',
            ]
        );
        $userProvider->save($user);

        $this->repo->addReference('admin', $user);

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1000;
    }

    /**
     * Set the reference repository
     *
     * @param ReferenceRepository $referenceRepository
     */
    public function setReferenceRepository(ReferenceRepository $referenceRepository)
    {
        $this->repo = $referenceRepository;
    }
}
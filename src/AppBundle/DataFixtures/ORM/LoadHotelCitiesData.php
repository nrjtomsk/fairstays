<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 19.01.2016
 * Time: 16:13
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\HotelsDestination;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load cities data for testing of hotels search & hotel details API
 */
class LoadHotelCitiesData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $city1 = new HotelsDestination();
        $city1->setCityName('New Delhi');
        $city1->setCountryCode(null);
        $city1->setCountryName('India');
        $city1->setState('');
        $city1->setHotelsProId('N35C');
        $manager->persist($city1);
        $manager->flush();

        $this->addReference('city1', $city1);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5000;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 19.01.2016
 * Time: 16:30
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\HotelsDestination;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Load data fixtures for Cities
 *
 * Class LoadHotelsDestinationData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadHotelsDestination extends AbstractFixture implements OrderedFixtureInterface
{
    static public $cities = array();

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $citiy1 = new HotelsDestination();
        $citiy1->setCityName('New Delhi');
        $citiy1->setCountryCode('null');
        $citiy1->setCountryName('India');
        $citiy1->setHotelsProId('N35C');
        $citiy1->setState('null');

        $citiy2 = new HotelsDestination();
        $citiy2->setCityName('Ambalavayal');
        $citiy2->setCountryCode('null');
        $citiy2->setCountryName('India');
        $citiy2->setHotelsProId('A14A');
        $citiy2->setState('null');

        $manager->persist($citiy1);
        $manager->persist($citiy2);

        $manager->flush();

        $this->addReference('member-1', $citiy1);
        $this->addReference('member-2', $citiy2);

        return self::$cities = array($citiy1, $citiy2);

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2000;
    }
}
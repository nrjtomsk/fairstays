<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.03.2016
 * Time: 16:11
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\HotelPro;
use AppBundle\Entity\TripAdvisorRating;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\DataFixtures\SharedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Tests\Fixtures\ContainerAwareFixture;

/**
 * Class LoadHotelsData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadHotelsData extends ContainerAwareFixture implements OrderedFixtureInterface, SharedFixtureInterface
{

    const HOTEL_HOTELS_PRO = '_hotel_h_p';

    /**
     * @var ReferenceRepository
     */
    private $referenceRepository;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $ratingHP = new TripAdvisorRating();
        $ratingHP->setCityName('New York');
        $ratingHP->setCountryName('USA');
        $ratingHP->setMultipliedRating(8);
        $ratingHP->setRating(4);
        $ratingHP->setResponseBody('{}');
        $ratingHP->setStateName('North Dakota');
        $ratingHP->setTripAdvisorId('123123123');

        $hotelPro = new HotelPro();
        $hotelPro->setHotelCode('12345678');
        $hotelPro->setCountry('USA');
        $hotelPro->setDescription('desc');
        $hotelPro->setDestination('New York');
        $hotelPro->setHotelAddress('address');
        $hotelPro->setHotelArea('area');
        $hotelPro->setHotelName('hotelPro');
        $hotelPro->setHotelPhoneNumber('9234155045');
        $hotelPro->setHotelPostalCode('12345');
        $hotelPro->setImagesUrls(['image_url' => 'url']);
        $hotelPro->setLatitude('12.121212');
        $hotelPro->setLongitude('12.121212');
        $hotelPro->setStarRating(3);
        $hotelPro->setPropertyAmenities(['asd', 'dsa']);
        $hotelPro->setRoomAmenities(['asd', 'dsa']);
        $hotelPro->setRoomsNumber(3);
        $hotelPro->setTripAdvisorRating($ratingHP);

        $this->referenceRepository->addReference(self::HOTEL_HOTELS_PRO, $hotelPro);
        $manager->persist($ratingHP);
        $manager->persist($hotelPro);
        $manager->flush();
    }

    /**
     * Set the reference repository
     *
     * @param ReferenceRepository $referenceRepository
     */
    public function setReferenceRepository(ReferenceRepository $referenceRepository)
    {
        $this->referenceRepository = $referenceRepository;
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 103;
    }
}
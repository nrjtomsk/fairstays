<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.01.2016
 * Time: 11:24
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Hotel;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFakeHotelsTravelGuruAndHotelsPro extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $hotelTG = new Hotel();
        $hotelTG->setId('00005053'); // please don't change if tests works fine
        $hotelTG->setState('STATE');
        $hotelTG->setAddressLine1('address1');
        $hotelTG->setAddressLine2('address2');
        $hotelTG->setArea('area');
        $hotelTG->setAreaSeoId('areaSeoId');
        $hotelTG->setCategoryList('cat1;cat2');
        $hotelTG->setCity('New Delhi');
        $hotelTG->setCityZone('city zone');
        $hotelTG->setClass('5 stars');
        $hotelTG->setCountry('India');
        $hotelTG->setDefaultCheckInTime('12:00:00');
        $hotelTG->setDefaultCheckOutTime('12:00:00');
        $hotelTG->setFormattedAddress('formatted address');
        $hotelTG->setGroupId('groupId');
        $hotelTG->setGroupName('groupName');
        $hotelTG->setImagePath('image.path');
        $hotelTG->setLatitude('44.222222');
        $hotelTG->setLongitude('33.33333');
        $hotelTG->setLocation('location');
        $hotelTG->setNumberOfFloors(3);
        $hotelTG->setNumberOfRooms(10);
        $hotelTG->setName('hotel name');
        $hotelTG->setPinCode('12345');
        $hotelTG->setReviewCount(3);
        $hotelTG->setReviewRating(4);
        $hotelTG->setOverview('overview');
        $hotelTG->setSearchKey('searchKey');
        $hotelTG->setSecondaryAreaIds('secondaryAreaIds');
        $hotelTG->setSecondaryAreaName('secondaryAreaName');
        $hotelTG->setStar(4);
        $hotelTG->setThemeList('themeList');
        $hotelTG->setWeekDayRank('WeekDayRank');
        $hotelTG->setWeekEndRank('WeekEndRank');

        $manager->persist($hotelTG);
        $this->addReference('hotelTG', $hotelTG);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10000;
    }
}
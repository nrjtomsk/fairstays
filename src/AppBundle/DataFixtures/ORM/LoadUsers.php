<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 20.01.2016
 * Time: 17:08
 */
namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\DataFixtures\SharedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Tests\Fixtures\ContainerAwareFixture;

/**
 * Load data fixtures for Users
 *
 * Class LoadUsers
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUsers extends ContainerAwareFixture implements FixtureInterface, OrderedFixtureInterface, SharedFixtureInterface
{
    /**
     * @var ReferenceRepository
     */
    private $repo;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('9111111111');
        $user->setEmail('user@mail.com');
        $user->setFullName('Users Sresu');
        $user->setFirstName('Users');
        $user->setLastName('Sresu');
        $user->setPassword('userpwd');
        $user->setSalt('pwd');
        $user->setRoles('ROLE_USER');
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());

        $manager->persist($user);

        $manager->flush();

        $userProvider = $this->container->get('app.service.user.provider');
        /** @var User $user1 */
        $user1 = $userProvider->createUser('9112345678');
        $user1->setPassword($this->container->get('security.password_encoder')->encodePassword($user1, 'user'));
        $user1->setFullName('User Resu');
        $user1->setEmail('User1@mail.com');
        $user1->setRoles(
            [
                'ROLE_USER',
            ]
        );
        $userProvider->save($user1);

        $userProvider2 = $this->container->get('app.service.user.provider');
        /** @var User $user2 */
        $user2 = $userProvider2->createUser('9187654321');
        $user2->setPassword($this->container->get('security.password_encoder')->encodePassword($user2, 'user'));
        $user2->setFullName('Usery Yresu');
        $user2->setEmail('User2@mail.com');
        $user2->setRoles(
            [
                'ROLE_USER',
            ]
        );
        $userProvider2->save($user2);

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6000;
    }

    /**
     * Set the reference repository
     *
     * @param ReferenceRepository $referenceRepository
     */
    public function setReferenceRepository(ReferenceRepository $referenceRepository)
    {
        $this->repo = $referenceRepository;
    }
}
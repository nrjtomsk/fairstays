<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.03.2016
 * Time: 10:05
 */

namespace AppBundle\Utils;


use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Interface StopWatchAware
 * @package AppBundle\Utils
 */
interface StopWatchAware
{
    /**
     * @param Stopwatch|null $stopwatch
     * @return void
     */
    public function setStopWatch(Stopwatch $stopwatch = null);

    public function startWatch($name, $category = null);

    public function stopWatch($name);
}
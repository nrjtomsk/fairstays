<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 07.10.2015
 * Time: 12:21
 */

namespace AppBundle\Utils;

/**
 * Returns file lines
 *
 * Class FileUtils
 * @package AppBundle\Utils
 */
class FileUtils
{
    /**
     * @param $fileName
     * @return \Generator
     */
    public static function getFileLines($fileName)
    {
        $resource = fopen($fileName, 'r');
        try {
            while ($line = fgets($resource)) {
                yield $line;
            }
        } finally {
            fclose($resource);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.03.2016
 * Time: 10:08
 */

namespace AppBundle\Utils;


use Symfony\Component\Stopwatch\Stopwatch;

trait StopWatchAwareTrait
{
    /**
     * @var Stopwatch
     */
    protected $stopWatch;

    /**
     * @param Stopwatch|null $stopwatch
     * @return void
     */
    public function setStopWatch(Stopwatch $stopwatch = null)
    {
        $this->stopWatch = $stopwatch;
    }

    public function startWatch($name, $category = null)
    {
        if ($this->stopWatch) {
            $this->stopWatch->start($name, $category);
        }
    }

    public function stopWatch($name)
    {
        if ($this->stopWatch) {
            $this->stopWatch->stop($name);
        }
    }
}
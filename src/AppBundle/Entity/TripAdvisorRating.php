<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 01.03.2016
 * Time: 11:15
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class TripAdvisorRating
 *
 * @ORM\Entity()
 * @ORM\Table(name="`trip_advisor_rating`")
 *
 * @Gedmo\Loggable()
 *
 * @package AppBundle\Entity
 */
class TripAdvisorRating
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Gedmo\Versioned()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="trip_advisor_id", type="string", length=12, unique=true)
     *
     * @Gedmo\Versioned()
     */
    private $tripAdvisorId;

    /**
     * @var string
     * @ORM\Column(name="city_name", type="string", nullable=true, length=127)
     *
     * @Gedmo\Versioned()
     */
    private $cityName;

    /**
     * @var string
     * @ORM\Column(name="state_name", type="string", nullable=true, length=127)
     *
     * @Gedmo\Versioned()
     */
    private $stateName;

    /**
     * @var string
     * @ORM\Column(name="country_name", type="string", nullable=true, length=127)
     *
     * @Gedmo\Versioned()
     */
    private $countryName;

    /**
     * @var string
     * @ORM\Column(name="rating", type="float", nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $rating;

    /**
     * @var string
     * @ORM\Column(name="multiplied_rating", type="float", nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $multipliedRating;

    /**
     * @var string
     * @ORM\Column(name="response_body", type="text", nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $responseBody;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     *
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Set id
     *
     * @param string $id
     * @return TripAdvisorRating
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return TripAdvisorRating
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get cityName
     *
     * @return string 
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set stateName
     *
     * @param string $stateName
     * @return TripAdvisorRating
     */
    public function setStateName($stateName)
    {
        $this->stateName = $stateName;

        return $this;
    }

    /**
     * Get stateName
     *
     * @return string 
     */
    public function getStateName()
    {
        return $this->stateName;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return TripAdvisorRating
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set rating
     *
     * @param float $rating
     * @return TripAdvisorRating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        if (is_null($this->rating)) {
            $this->setMultipliedRating($this->rating);
        }

        switch ($this->rating) {
            case (4.5):
                if ((mt_rand() / mt_getrandmax()) > 0.50) {
                    $this->setMultipliedRating($this->rating * 2);
                } else {
                    $this->setMultipliedRating(($this->rating + 0.25) * 2);
                }
                break;
            case (5):
                $this->setMultipliedRating($this->rating * 2);
                break;
            default:
                if ((mt_rand() / mt_getrandmax()) > 0.50) {
                    $this->setMultipliedRating(($rating + 0.25) * 2);
                } else {
                    $this->setMultipliedRating(($this->rating + 0.50) * 2);
                }
                break;
        }
        return $this;
    }

    /**
     * Get rating
     *
     * @return float 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set multipliedRating
     *
     * @param float $multipliedRating
     * @return TripAdvisorRating
     */
    public function setMultipliedRating($multipliedRating)
    {
        $this->multipliedRating = $multipliedRating;

        return $this;
    }

    /**
     * Get multipliedRating
     *
     * @return float 
     */
    public function getMultipliedRating()
    {
        return $this->multipliedRating;
    }

    /**
     * Set responseBody
     *
     * @param string $responseBody
     * @return TripAdvisorRating
     */
    public function setResponseBody($responseBody)
    {
        $this->responseBody = $responseBody;

        return $this;
    }

    /**
     * Get responseBody
     *
     * @return string 
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TripAdvisorRating
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return TripAdvisorRating
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set tripAdvisorId
     *
     * @param string $tripAdvisorId
     * @return TripAdvisorRating
     */
    public function setTripAdvisorId($tripAdvisorId)
    {
        $this->tripAdvisorId = $tripAdvisorId;

        return $this;
    }

    /**
     * Get tripAdvisorId
     *
     * @return string 
     */
    public function getTripAdvisorId()
    {
        return $this->tripAdvisorId;
    }
}

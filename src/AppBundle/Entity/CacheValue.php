<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 15:50
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CacheValue
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CacheValueRepository")
 * @ORM\Table(name="`cache_value`")
 *
 * @package AppBundle\Entity
 */
class CacheValue
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(name="`key`", type="string", nullable=false)
     */
    private $key;

    /**
     * @var \DateTime
     * @ORM\Column(name="`expires`", type="datetime", nullable=false)
     */
    private $expires;

    /**
     * @var string
     * @ORM\Column(name="`value`", type="text")
     */
    private $value;

    /**
     * @var bool
     * @ORM\Column(name="archived", type="boolean", nullable=true)
     */
    private $archived;

    /**
     * Set key
     *
     * @param string $key
     * @return CacheValue
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set expires
     *
     * @param \DateTime $expires
     * @return CacheValue
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return \DateTime
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return CacheValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     * @return CacheValue
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean 
     */
    public function getArchived()
    {
        return $this->archived;
    }
}

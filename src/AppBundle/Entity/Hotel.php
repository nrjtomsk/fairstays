<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 06.10.2015
 * Time: 17:49
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Hotel
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HotelRepository")
 * @ORM\Table(name="hotels")
 *
 * @package AppBundle\Entity
 */
class Hotel implements TripAdvisorRatingAware
{

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="string")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="class", type="string", length=255, nullable=true)
     */
    private $class;

    /**
     * @var string
     * @ORM\Column(name="location", type="string", length=1023, nullable=true)
     */
    private $location;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(name="address_line1", type="string", length=511, nullable=true)
     */
    private $addressLine1;

    /**
     * @var string
     * @ORM\Column(name="address_line2", type="string", length=511, nullable=true)
     */
    private $addressLine2;

    /**
     * @var string
     * @ORM\Column(name="area", type="string", length=255, nullable=true)
     */
    private $area;

    /**
     * @var string
     * @ORM\Column(name="formatted_address", type="string", length=1023, nullable=true)
     */
    private $formattedAddress;

    /**
     * @var string
     * @ORM\Column(name="overview", type="text", nullable=true)
     */
    private $overview;

    /**
     * @var string
     * @ORM\Column(name="review_rating", type="float", nullable=true)
     */
    private $reviewRating;

    /**
     * @var string
     * @ORM\Column(name="review_count", type="integer", nullable=true)
     */
    private $reviewCount;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     * @ORM\Column(name="default_check_in_time", type="string", length=255, nullable=true)
     */
    private $defaultCheckInTime;

    /**
     * @var string
     * @ORM\Column(name="default_check_out_time", type="string", length=255, nullable=true)
     */
    private $defaultCheckOutTime;

    /**
     * @var string
     * @ORM\Column(name="star", type="string", length=255, nullable=true)
     */
    private $star;

    /**
     * @var string
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var string
     * @ORM\Column(name="group_name", type="string", length=255, nullable=true)
     */
    private $groupName;

    /**
     * @var string
     * @ORM\Column(name="image_path", type="string", length=511, nullable=true)
     */
    private $imagePath;

    /**
     * @var string
     * @ORM\Column(name="search_key", type="string", length=255, nullable=true)
     */
    private $searchKey;

    /**
     * @var string
     * @ORM\Column(name="area_seo_id", type="string", length=255, nullable=true)
     */
    private $areaSeoId;

    /**
     * @var string
     * @ORM\Column(name="secondary_area_ids", type="string", length=1023, nullable=true)
     */
    private $secondaryAreaIds;

    /**
     * @var string
     * @ORM\Column(name="secondary_area_name", type="string", length=255, nullable=true)
     */
    private $secondaryAreaName;

    /**
     * @var string
     * @ORM\Column(name="number_of_floors", type="integer", nullable=true)
     */
    private $numberOfFloors;

    /**
     * @var string
     * @ORM\Column(name="number_of_rooms", type="integer", nullable=true)
     */
    private $numberOfRooms;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(name="pin_code", type="string", length=15, nullable=true)
     */
    private $pinCode;

    /**
     * @var string
     * @ORM\Column(name="theme_list", type="string", length=1023, nullable=true)
     */
    private $themeList;

    /**
     * @var string
     * @ORM\Column(name="category_list", type="string", length=1023, nullable=true)
     */
    private $categoryList;

    /**
     * @var string
     * @ORM\Column(name="city_zone", type="string", length=255, nullable=true)
     */
    private $cityZone;

    /**
     * @var string
     * @ORM\Column(name="week_day_rank", type="integer", nullable=true)
     */
    private $weekDayRank;

    /**
     * @var string
     * @ORM\Column(name="week_end_rank", type="integer", nullable=true)
     */
    private $weekEndRank;

    /**
     * @var HotelImage[]|Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\HotelImage", mappedBy="hotel")
     */
    private $images;

    /**
     * @var TripAdvisorRating
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TripAdvisorRating", fetch="EAGER")
     * @ORM\JoinColumn(name="trip_advisor_rating_id", nullable=true, onDelete="SET NULL")
     */
    private $tripAdvisorRating;

    /**
     * @param array $item
     * @return Hotel
     */
    public static function fromArray(array $item)
    {
        $hotel = new Hotel();
        $hotel->setId((int)$item['VendorId']);
        $hotel->setName($item['VendorName']);
        $hotel->setClass($item['HotelClass']);
        $hotel->setLocation($item['Location']);
        $hotel->setCity($item['City']);
        $hotel->setCountry($item['Country']);
        $hotel->setAddressLine1($item['Address1']);
        $hotel->setAddressLine2($item['Address2']);
        $hotel->setArea($item['Area']);
        $hotel->setFormattedAddress($item['Address']);
        $hotel->setOverview($item['HotelOverview']);
        $hotel->setReviewRating($item['ReviewRating']);
        $hotel->setReviewCount($item['ReviewCount']);
        $hotel->setLatitude($item['Latitude']);
        $hotel->setLongitude($item['Longitude']);
        $hotel->setDefaultCheckInTime($item['DefaultCheckInTime']);
        $hotel->setDefaultCheckOutTime($item['DefaultCheckOutTime']);
        $hotel->setStar($item['Hotel_Star']);
        $hotel->setGroupId($item['HotelGroupID']);
        $hotel->setGroupName($item['HotelGroupName']);
        $hotel->setImagePath($item['ImagePath']);
        $hotel->setSearchKey($item['HotelSearchKey']);
        $hotel->setAreaSeoId($item['Area_Seo_Id']);
        $hotel->setSecondaryAreaIds($item['SecondaryAreaIds']);
        $hotel->setSecondaryAreaName($item['SecondaryAreaName']);
        $hotel->setNumberOfFloors($item['NoOfFloors']);
        $hotel->setNumberOfRooms($item['NoOfRooms']);
        $hotel->setState($item['State']);
        $hotel->setPinCode($item['PinCode']);
        $hotel->setThemeList($item['ThemeList']);
        $hotel->setCategoryList($item['CategoryList']);
        $hotel->setCityZone($item['City Zone']);
        $hotel->setWeekDayRank($item['WeekDay_Rank']);
        $hotel->setWeekEndRank($item['WeekEnd_Rank']);

        return $hotel;
    }

    /**
     * @param Hotel $hotel
     */
    public function fillFromOther(Hotel $hotel)
    {
        $this->setId((int)$hotel->getId());
        $this->setName($hotel->getName());
        $this->setClass($hotel->getClass());
        $this->setLocation($hotel->getLocation());
        $this->setCity($hotel->getCity());
        $this->setCountry($hotel->getCountry());
        $this->setAddressLine1($hotel->getAddressLine1());
        $this->setAddressLine2($hotel->getAddressLine2());
        $this->setArea($hotel->getArea());
        $this->setFormattedAddress($hotel->getFormattedAddress());
        $this->setOverview($hotel->getOverview());
        $this->setReviewRating($hotel->getReviewRating());
        $this->setReviewCount($hotel->getReviewCount());
        $this->setLatitude($hotel->getLatitude());
        $this->setLongitude($hotel->getLongitude());
        $this->setDefaultCheckInTime($hotel->getDefaultCheckInTime());
        $this->setDefaultCheckOutTime($hotel->getDefaultCheckOutTime());
        $this->setStar($hotel->getStar());
        $this->setGroupId($hotel->getGroupId());
        $this->setGroupName($hotel->getGroupName());
        $this->setImagePath($hotel->getImagePath());
        $this->setSearchKey($hotel->getSearchKey());
        $this->setAreaSeoId($hotel->getAreaSeoId());
        $this->setSecondaryAreaIds($hotel->getSecondaryAreaIds());
        $this->setSecondaryAreaName($hotel->getSecondaryAreaName());
        $this->setNumberOfFloors($hotel->getNumberOfFloors());
        $this->setNumberOfRooms($hotel->getNumberOfRooms());
        $this->setState($hotel->getState());
        $this->setPinCode($hotel->getPinCode());
        $this->setThemeList($hotel->getThemeList());
        $this->setCategoryList($hotel->getCategoryList());
        $this->setCityZone($hotel->getCityZone());
        $this->setWeekDayRank($hotel->getWeekDayRank());
        $this->setWeekEndRank($hotel->getWeekEndRank());
    }

    /**
     * @param Hotel $hotel
     * @return bool
     */
    public function equals(Hotel $hotel)
    {
        return (
            $hotel->getId() == $this->getId()
            && $hotel->getName() == $this->getName()
            && $hotel->getClass() == $this->getClass()
            && $hotel->getLocation() == $this->getLocation()
            && $hotel->getCity() == $this->getCity()
            && $hotel->getCountry() == $this->getCountry()
            && $hotel->getAddressLine1() == $this->getAddressLine1()
            && $hotel->getAddressLine2() == $this->getAddressLine2()
            && $hotel->getArea() == $this->getArea()
            && $hotel->getFormattedAddress() == $this->getFormattedAddress()
            && $hotel->getOverview() == $this->getOverview()
            && $hotel->getReviewRating() == $this->getReviewRating()
            && $hotel->getReviewCount() == $this->getReviewCount()
            && $hotel->getLatitude() == $this->getLatitude()
            && $hotel->getLongitude() == $this->getLongitude()
            && $hotel->getDefaultCheckInTime() == $this->getDefaultCheckInTime()
            && $hotel->getDefaultCheckOutTime() == $this->getDefaultCheckOutTime()
            && $hotel->getStar() == $this->getStar()
            && $hotel->getGroupId() == $this->getGroupId()
            && $hotel->getGroupName() == $this->getGroupName()
            && $hotel->getImagePath() == $this->getImagePath()
            && $hotel->getSearchKey() == $this->getSearchKey()
            && $hotel->getAreaSeoId() == $this->getAreaSeoId()
            && $hotel->getSecondaryAreaIds() == $this->getSecondaryAreaIds()
            && $hotel->getSecondaryAreaName() == $this->getSecondaryAreaName()
            && $hotel->getNumberOfFloors() == $this->getNumberOfFloors()
            && $hotel->getNumberOfRooms() == $this->getNumberOfRooms()
            && $hotel->getState() == $this->getState()
            && $hotel->getPinCode() == $this->getPinCode()
            && $hotel->getThemeList() == $this->getThemeList()
            && $hotel->getCategoryList() == $this->getCategoryList()
            && $hotel->getCityZone() == $this->getCityZone()
            && $hotel->getWeekDayRank() == $this->getWeekDayRank()
            && $hotel->getWeekEndRank() == $this->getWeekEndRank()
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getFormattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * @param mixed $formattedAddress
     */
    public function setFormattedAddress($formattedAddress)
    {
        $this->formattedAddress = $formattedAddress;
    }

    /**
     * @return mixed
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * @param mixed $overview
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;
    }

    /**
     * @return mixed
     */
    public function getReviewRating()
    {
        return $this->reviewRating;
    }

    /**
     * @param mixed $reviewRating
     */
    public function setReviewRating($reviewRating)
    {
        $this->reviewRating = $reviewRating;
    }

    /**
     * @return mixed
     */
    public function getReviewCount()
    {
        return $this->reviewCount;
    }

    /**
     * @param mixed $reviewCount
     */
    public function setReviewCount($reviewCount)
    {
        $this->reviewCount = $reviewCount;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getDefaultCheckInTime()
    {
        return $this->defaultCheckInTime;
    }

    /**
     * @param mixed $defaultCheckInTime
     */
    public function setDefaultCheckInTime($defaultCheckInTime)
    {
        $this->defaultCheckInTime = $defaultCheckInTime;
    }

    /**
     * @return mixed
     */
    public function getDefaultCheckOutTime()
    {
        return $this->defaultCheckOutTime;
    }

    /**
     * @param mixed $defaultCheckOutTime
     */
    public function setDefaultCheckOutTime($defaultCheckOutTime)
    {
        $this->defaultCheckOutTime = $defaultCheckOutTime;
    }

    /**
     * @return mixed
     */
    public function getStar()
    {
        return $this->star;
    }

    /**
     * @param mixed $star
     */
    public function setStar($star)
    {
        $this->star = $star;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param mixed $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param mixed $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    /**
     * @return mixed
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param mixed $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return mixed
     */
    public function getSearchKey()
    {
        return $this->searchKey;
    }

    /**
     * @param mixed $searchKey
     */
    public function setSearchKey($searchKey)
    {
        $this->searchKey = $searchKey;
    }

    /**
     * @return mixed
     */
    public function getAreaSeoId()
    {
        return $this->areaSeoId;
    }

    /**
     * @param mixed $areaSeoId
     */
    public function setAreaSeoId($areaSeoId)
    {
        $this->areaSeoId = $areaSeoId;
    }

    /**
     * @return mixed
     */
    public function getSecondaryAreaIds()
    {
        return $this->secondaryAreaIds;
    }

    /**
     * @param mixed $secondaryAreaIds
     */
    public function setSecondaryAreaIds($secondaryAreaIds)
    {
        $this->secondaryAreaIds = $secondaryAreaIds;
    }

    /**
     * @return mixed
     */
    public function getSecondaryAreaName()
    {
        return $this->secondaryAreaName;
    }

    /**
     * @param mixed $secondaryAreaName
     */
    public function setSecondaryAreaName($secondaryAreaName)
    {
        $this->secondaryAreaName = $secondaryAreaName;
    }

    /**
     * @return mixed
     */
    public function getNumberOfFloors()
    {
        return $this->numberOfFloors;
    }

    /**
     * @param mixed $numberOfFloors
     */
    public function setNumberOfFloors($numberOfFloors)
    {
        $this->numberOfFloors = $numberOfFloors;
    }

    /**
     * @return mixed
     */
    public function getNumberOfRooms()
    {
        return $this->numberOfRooms;
    }

    /**
     * @param mixed $numberOfRooms
     */
    public function setNumberOfRooms($numberOfRooms)
    {
        $this->numberOfRooms = $numberOfRooms;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getPinCode()
    {
        return $this->pinCode;
    }

    /**
     * @param mixed $pinCode
     */
    public function setPinCode($pinCode)
    {
        $this->pinCode = $pinCode;
    }

    /**
     * @return mixed
     */
    public function getThemeList()
    {
        return $this->themeList;
    }

    /**
     * @param mixed $themeList
     */
    public function setThemeList($themeList)
    {
        $this->themeList = $themeList;
    }

    /**
     * @return mixed
     */
    public function getCityZone()
    {
        return $this->cityZone;
    }

    /**
     * @param mixed $cityZone
     */
    public function setCityZone($cityZone)
    {
        $this->cityZone = $cityZone;
    }

    /**
     * @return mixed
     */
    public function getWeekDayRank()
    {
        return $this->weekDayRank;
    }

    /**
     * @param mixed $weekDayRank
     */
    public function setWeekDayRank($weekDayRank)
    {
        $this->weekDayRank = $weekDayRank;
    }

    /**
     * @return mixed
     */
    public function getWeekEndRank()
    {
        return $this->weekEndRank;
    }

    /**
     * @param mixed $weekEndRank
     */
    public function setWeekEndRank($weekEndRank)
    {
        $this->weekEndRank = $weekEndRank;
    }

    /**
     * @return mixed
     */
    public function getCategoryList()
    {
        return $this->categoryList;
    }

    /**
     * @param mixed $categoryList
     */
    public function setCategoryList($categoryList)
    {
        $this->categoryList = $categoryList;
    }

    /**
     * @param HotelImage $image
     */
    public function addImage(HotelImage $image)
    {
        $this->images[] = $image;
    }

    /**
     * @param HotelImage $image
     */
    public function removeImage(HotelImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * @return HotelImage[]|Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * The __toString method allows a class to decide how it will react when it is converted to a string.
     *
     * @return string
     * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
     */
    public function __toString()
    {
        return (string)$this->getId().$this->getLatitude().$this->getLongitude();
    }

    /**
     * Set tripAdvisorRating
     *
     * @param \AppBundle\Entity\TripAdvisorRating $tripAdvisorRating
     * @return Hotel
     */
    public function setTripAdvisorRating(\AppBundle\Entity\TripAdvisorRating $tripAdvisorRating = null)
    {
        $this->tripAdvisorRating = $tripAdvisorRating;

        return $this;
    }

    /**
     * Get tripAdvisorRating
     *
     * @return \AppBundle\Entity\TripAdvisorRating 
     */
    public function getTripAdvisorRating()
    {
        return $this->tripAdvisorRating;
    }
}

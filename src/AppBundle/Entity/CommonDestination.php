<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 29.04.2016
 * Time: 11:55
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CommonDestination
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommonDestinationRepository")
 * @ORM\Table(name="`common_destinations`", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="uk_common_destinations_country_name", columns={"country_name", "state", "city_name"}),
 *      @ORM\UniqueConstraint(name="uk_common_destinations_country_code", columns={"country_code", "state", "city_name"})
 * }, indexes={
 *      @ORM\Index(name="idx_common_destinations_city_name", columns={"city_name"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *
 * @package AppBundle\Entity
 */
class CommonDestination
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="`id`", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="`country_name`", type="string", length=127)
     */
    private $countryName;

    /**
     * @var string
     * @ORM\Column(name="`country_code`", type="string", length=3)
     */
    private $countryCode;

    /**
     * @var string
     * @ORM\Column(name="`state`", type="string", length=127, nullable=true)
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(name="`city_name`", type="string", length=127)
     */
    private $cityName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`created_at`", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`updated_at`", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`deleted_at`", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return CommonDestination
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return CommonDestination
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return CommonDestination
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return CommonDestination
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get cityName
     *
     * @return string 
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CommonDestination
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CommonDestination
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return CommonDestination
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.03.2016
 * Time: 15:23
 */

namespace AppBundle\Entity;

/**
 * Interface TripAdvisorRatingAware
 * @package AppBundle\Entity
 */
interface TripAdvisorRatingAware
{
    /**
     * @return TripAdvisorRating
     */
    public function getTripAdvisorRating();

    /**
     * @param TripAdvisorRating|null $rating
     */
    public function setTripAdvisorRating(TripAdvisorRating $rating = null);
}
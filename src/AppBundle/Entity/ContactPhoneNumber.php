<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.10.2015
 * Time: 11:03
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class ContactPhoneNumber
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactPhoneNumberRepository")
 * @ORM\Table(name="contact_phone_numbers")
 *
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *
 * @package AppBundle\Entity
 */
class ContactPhoneNumber
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Gedmo\Versioned()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="area_city_code", type="string", length=8, nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $areaCityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_access_code", type="string", length=8, nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $countryAccessCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", nullable=true, length=25)
     *
     * @Gedmo\Versioned()
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_tech_type", type="string", nullable=true, length=4)
     *
     * @Gedmo\Versioned()
     */
    private $phoneTechType;

    /**
     * @var BookingRequest
     *
     * @ORM\JoinColumn(name="booking_request_id", nullable=false, onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BookingRequest", fetch="EAGER", inversedBy="contactPhoneNumbers")
     */
    private $bookingRequest;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @Gedmo\Versioned()
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     *
     * @Gedmo\Versioned()
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     *
     * @Gedmo\Versioned()
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set areaCityCode
     *
     * @param string $areaCityCode
     * @return ContactPhoneNumber
     */
    public function setAreaCityCode($areaCityCode)
    {
        $this->areaCityCode = $areaCityCode;

        return $this;
    }

    /**
     * Get areaCityCode
     *
     * @return string
     */
    public function getAreaCityCode()
    {
        return $this->areaCityCode;
    }

    /**
     * Set countryAccessCode
     *
     * @param string $countryAccessCode
     * @return ContactPhoneNumber
     */
    public function setCountryAccessCode($countryAccessCode)
    {
        $this->countryAccessCode = $countryAccessCode;

        return $this;
    }

    /**
     * Get countryAccessCode
     *
     * @return string
     */
    public function getCountryAccessCode()
    {
        return $this->countryAccessCode;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return ContactPhoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set phoneTechType
     *
     * @param string $phoneTechType
     * @return ContactPhoneNumber
     */
    public function setPhoneTechType($phoneTechType)
    {
        $this->phoneTechType = $phoneTechType;

        return $this;
    }

    /**
     * Get phoneTechType
     *
     * @return string
     */
    public function getPhoneTechType()
    {
        return $this->phoneTechType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ContactPhoneNumber
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ContactPhoneNumber
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return ContactPhoneNumber
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set bookingRequest
     *
     * @param \AppBundle\Entity\BookingRequest $bookingRequest
     * @return ContactPhoneNumber
     */
    public function setBookingRequest(\AppBundle\Entity\BookingRequest $bookingRequest)
    {
        $this->bookingRequest = $bookingRequest;

        return $this;
    }

    /**
     * Get bookingRequest
     *
     * @return \AppBundle\Entity\BookingRequest
     */
    public function getBookingRequest()
    {
        return $this->bookingRequest;
    }
}

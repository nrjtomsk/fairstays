<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 9:54
 */

namespace AppBundle\Entity;

/**
 * Interface Currency
 *
 * @package AppBundle\Entity
 */
interface CurrencyInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getCode();

    /**
     * @return string
     */
    public function getName();
}
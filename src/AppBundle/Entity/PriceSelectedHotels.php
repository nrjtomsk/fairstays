<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 22.03.16
 * Time: 9:05
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * PriceSelectedHotels
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PriceSelectedHotelsRepository")
 * @ORM\Table(name="price_selected_hotels", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="uidx_price_selected_hotels_date_city", columns={"check_in_date", "city_code"})
 * })
 */
class PriceSelectedHotels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="check_in_date", type="string", length=10)
     */
    private $checkInDate;

    /**
     * @var string
     * @ORM\Column(name="city_code", type="string", length=10)
     */
    private $cityCode;

    /**
     * @var string
     * @ORM\Column(name="selected_hotels", type="json_array")
     */
    private $selectedHotels;

    /**
     * @var string
     * @ORM\Column(name="selected_markup_percent", type="json_array", nullable=true)
     */
    private $selectedMarkupPercent;

    /**
     * @var string
     * @ORM\Column(name="save_amount", type="json_array", nullable=true)
     */
    private $saveAmount;


    /**
     * @var \DateTime
     * @ORM\Column(name="insert_time", type="date")
     */
    private $insertTime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCheckInDate()
    {
        return $this->checkInDate;
    }

    /**
     * @param string $checkInDate
     */
    public function setCheckInDate($checkInDate)
    {
        $this->checkInDate = $checkInDate;
    }

    /**
     * @return string
     */
    public function getCityCode()
    {
        return $this->cityCode;
    }

    /**
     * @param string $cityCode
     */
    public function setCityCode($cityCode)
    {
        $this->cityCode = $cityCode;
    }

    /**
     * @return array
     */
    public function getSelectedHotels()
    {
        return $this->selectedHotels;
    }

    /**
     * @param string $selectedHotels
     */
    public function setSelectedHotels($selectedHotels)
    {
        $this->selectedHotels = $selectedHotels;
    }

    /**
     * @return array
     */
    public function getSelectedMarkupPercent()
    {
        return $this->selectedMarkupPercent;
    }

    /**
     * @param array $selectedMarkupPercent
     */
    public function setSelectedMarkupPercent($selectedMarkupPercent)
    {
        $this->selectedMarkupPercent = $selectedMarkupPercent;
    }

    /**
     * @return string
     */
    public function getInsertTime()
    {
        return $this->insertTime;
    }

    /**
     * @param string $insertTime
     */
    public function setInsertTime($insertTime)
    {
        $this->insertTime = $insertTime;
    }

    /**
     * @return array
     */
    public function getSaveAmount()
    {
        return $this->saveAmount;
    }

    /**
     * @param array $saveAmount
     */
    public function setSaveAmount($saveAmount)
    {
        $this->saveAmount = $saveAmount;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 13.10.2015
 * Time: 15:06
 */

namespace AppBundle\Entity;

use ApiBundle\DTO\HotelProvisionalBookingRequestParams;
use ApiBundle\Service\Booking\BookingParams;
use ApiBundle\Service\HotelsList\HotelDTO;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Service\Money\MoneyConverter;
use ApiBundle\Service\Utils\StringUtils;
use AppBundle\Service\Money\Money;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Stripe\Charge;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class BookingRequest
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookingRequestRepository")
 * @ORM\Table(name="booking_requests")
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *
 * @ORM\HasLifecycleCallbacks
 *
 * @package AppBundle\Entity
 */
class BookingRequest
{
    /**
     *
     */
    const STATUS_PROVISIONAL_PRE = 'ProvisionalPre';
    const STATUS_PROVISIONAL_COMPLETE = 'ProvisionalComplete';
    const STATUS_PAYMENT_COMPLETE = 'PaymentComplete';
    const STATUS_CANCELLING = 'Cancelling';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_REFUNDING = 'Refunding';
    const STATUS_REFUNDED = 'Refunded';
    const STATUS_CONFIRMING = 'Confirming';
    const STATUS_CONFIRMED = 'Confirmed';
    const STATUS_CANCELLATION_PROCESSING = 'Cancellation Processing';
    const STATUS_HOTELS_PRO_CANCELLED = 'Cancelled';

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", fetch="EAGER", inversedBy="bookingRequests")
     * @ORM\JoinColumn(name="user_id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="correlation_id", type="string", length=64, nullable=false)
     */
    private $correlationId;

    /**
     * @var string
     * @ORM\Column(name="transaction_identifier", type="string", length=64, nullable=false)
     */
    private $transactionIdentifier;

    /**
     * @var string
     * @ORM\Column(name="hotel_code", type="string", nullable=false)
     */
    private $hotelCode;

    /**
     * @var string
     * @ORM\Column(name="requested_currency", length=4, type="string", nullable=false)
     */
    private $requestedCurrency;

    /**
     * @var \DateTime
     * @ORM\Column(name="time_span_start_date", type="date", nullable=false)
     */
    private $timeSpanStartDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="time_span_end_date", type="date", nullable=false)
     */
    private $timeSpanEndDate;

    /**
     * @var string
     * @ORM\Column(name="room_type_number_of_units", type="smallint", nullable=true)
     */
    private $roomTypeNumberOfUnits;

    /**
     * @var string
     * @ORM\Column(name="room_name", type="text", nullable=false)
     */
    private $roomName;

    /**
     * @var string
     * @ORM\Column(name="room_type_code", length=127, type="string", nullable=true)
     */
    private $roomTypeCode;

    /**
     * @var string
     * @ORM\Column(name="rate_plan_code", length=127, type="string", nullable=true)
     */
    private $ratePlanCode;

    /**
     * @var string
     * @ORM\Column(name="total_amount_before_tax", type="float", nullable=false, options={"default": 0})
     */
    private $totalAmountBeforeTax = 0;

    /**
     * @var string
     * @ORM\Column(name="total_taxes_amount", type="float", nullable=false, options={"default": 0})
     */
    private $totalTaxesAmount = 0;

    /**
     * @var float
     * @ORM\Column(name="room_price_per_night", type="float", nullable=false, options={"default": 0})
     */
    private $roomPricePerNight = 0;

    /**
     * @var float
     * @ORM\Column(name="guest_charges", type="float", nullable=false, options={"default": 0})
     */
    private $guestCharges = 0;

    /**
     * @var string
     * @ORM\Column(name="comment_text", type="text", nullable=true)
     */
    private $commentText;

    /**
     * @var string
     * @ORM\Column(name="profile_name_prefix", length=4, type="string", nullable=false)
     */
    private $profileNamePrefix;

    /**
     * @var string
     * @ORM\Column(name="profile_given_name", length=127, type="string", nullable=false)
     */
    private $profileGivenName;

    /**
     * @var string
     * @ORM\Column(name="profile_middle_name", length=127, type="string", nullable=true)
     */
    private $profileMiddleName;

    /**
     * @var string
     * @ORM\Column(name="profile_last_name", length=127, type="string", nullable=false)
     */
    private $profileLastName;

    /**
     * @var string
     * @ORM\Column(name="profile_phone_area_city_code", length=4, type="string", nullable=false)
     */
    private $profilePhoneAreaCityCode;

    /**
     * @var string
     * @ORM\Column(name="profile_phone_country_access_code", length=4, type="string", nullable=false)
     */
    private $profilePhoneCountryAccessCode;

    /**
     * @var string
     * @ORM\Column(name="profile_phone_extension", length=15, type="string", nullable=false)
     */
    private $profilePhoneExtension;

    /**
     * @var string
     * @ORM\Column(name="profile_phone_number", length=20, type="string", nullable=false)
     */
    private $profilePhoneNumber;

    /**
     * @var string
     * @ORM\Column(name="profile_phone_tech_type", length=20, type="string", nullable=false)
     */
    private $profilePhoneTechType;

    /**
     * @var string
     * @ORM\Column(name="profile_email", length=127, type="string", nullable=false)
     */
    private $profileEmail;

    /**
     * @var string
     * @ORM\Column(name="profile_address_line1", length=255, type="string", nullable=false)
     */
    private $profileAddressLine1;

    /**
     * @var string
     * @ORM\Column(name="profile_address_line2", length=255, type="string", nullable=false)
     */
    private $profileAddressLine2;

    /**
     * @var string
     * @ORM\Column(name="profile_city_name", length=127, type="string", nullable=false)
     */
    private $profileCityName;

    /**
     * @var string
     * @ORM\Column(name="profile_postal_code", type="string", length=15, nullable=false)
     */
    private $profilePostalCode;

    /**
     * @var string
     * @ORM\Column(name="profile_address_state_prov", type="string", length=127, nullable=false)
     */
    private $profileAddressStateProv;

    /**
     * @var string
     * @ORM\Column(name="profile_address_state_prov_code", type="string", length=127, nullable=false)
     */
    private $profileAddressStateProvCode;

    /**
     * @var string
     * @ORM\Column(name="profile_country_name", type="string", length=127, nullable=false)
     */
    private $profileCountryName;

    /**
     * @var string
     * @ORM\Column(name="profile_country_name_code", type="string", length=127, nullable=false)
     */
    private $profileCountryNameCode;

    /**
     * @var string
     * @ORM\Column(name="guarantee_type", type="string", length=16, nullable=false)
     */
    private $guaranteeType;

    /**
     * @var array
     * @ORM\Column(name="guest_counts", type="json_array", nullable=false)
     */
    private $guestCounts;

    /**
     * TravelGuru Provisional id
     * @var string
     * @ORM\Column(name="booking_id", type="string", length=32, nullable=true)
     */
    private $bookingId;

    /**
     * TravelGuru Confirmed id
     * @var string
     * @ORM\Column(name="travel_guru_booking_id", type="string", length=32, nullable=true)
     */
    private $travelGuruBookingId;

    /**
     * @var ContactPhoneNumber[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ContactPhoneNumber", mappedBy="bookingRequest")
     */
    private $contactPhoneNumbers;

    /**
     * @var float
     * @ORM\Column(name="amount_after_tax", type="float", nullable=true)
     */
    private $amountAfterTax;

    /**
     * @var float
     * @ORM\Column(name="payu_amount", type="float", nullable=true)
     */
    private $payuAmount;

    /**
     * @var string
     * @ORM\Column(name="payu_transaction_id", type="text", nullable=true)
     */
    private $payuTransactionId;

    /**
     * @var string
     * @ORM\Column(name="payu_mih_pay_id", type="text", nullable=true)
     */
    private $payuMihPayId;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount_before_tax_external", type="float", nullable=true)
     */
    private $totalAmountBeforeTaxExternal;

    /**
     * @var float
     *
     * @ORM\Column(name="total_taxes_amount_external", type="float", nullable=true)
     */
    private $totalTaxesAmountExternal;

    /**
     * @var string
     * @ORM\Column(name="stripe_token", type="string", nullable=true)
     */
    private $stripeToken;


    /**
     * @var string
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(name="public_booking_id", type="string", length=12, nullable=true)
     */
    private $publicBookingId;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     * @ORM\Column(name="tracking_id", type="string", length=32, nullable=true)
     */
    private $trackingId;

    /**
     * @var int
     * @ORM\Column(name="hotels_pro_cancel_response_id", type="integer", nullable=true)
     */
    private $hotelsProCancelResponseId;

    /**
     * @var string
     * @ORM\Column(name="hotels_pro_agency_reference_number", type="string", length=32, nullable=true)
     */
    private $hotelsProAgencyReferenceNumber;

    /**
     * @var string
     * @ORM\Column(name="hotels_pro_booking_status", type="string", length=255, nullable=true)
     */
    private $hotelsProBookingStatus;

    /**
     * @var string
     * @ORM\Column(name="hotels_pro_cancellation_note", type="string", length=255, nullable=true)
     */
    private $hotelsProCancellationNote;

    /**
     * @var array
     * @ORM\Column(name="hotels_cancellation_policy", type="json_array", nullable=false)
     */
    private $hotelsCancellationPolicy;

    /**
     * @var string
     * @ORM\Column(name="meta_data_dto", type="text", nullable=true)
     */
    private $metaDataDTO;

    /**
     * @var MetaDataDTO
     *
     */
    private $metaData;

    /**
     * @var HotelDTO
     */
    private $hotel;

    /**
     * @var array
     * @ORM\Column(name="rates_per_night", type="json_array", nullable=true)
     */
    private $ratesPerNight;

    /**
     * @var float
     *
     * @ORM\Column(name="markup_rate", type="float", nullable=true)
     */
    private $markupRate;

    /**
     * @var float
     * @ORM\Column(name="pure_price_amount_service_provider", type="float", nullable=true)
     */
    private $purePriceAmountServiceProvider;

    /**
     * @var string
     * @ORM\Column(name="pure_price_currency_service_provider", type="string", length=3, nullable=true)
     */
    private $purePriceCurrencyServiceProvider;

    /**
     * @var float
     * @ORM\Column(name="pure_tax_amount_service_provider", type="float", nullable=true)
     */
    private $pureTaxAmountServiceProvider;

    /**
     * @var string
     * @ORM\Column(name="pure_tax_currency_service_provider", type="string", length=3, nullable=true)
     */
    private $pureTaxCurrencyServiceProvider;

    /**
     * @var float
     * @ORM\Column(name="pure_price_amount_internal", type="float", nullable=true)
     */
    private $purePriceAmountInternal;
    /**
     * @var string
     * @ORM\Column(name="pure_price_currency_internal", type="string", length=3, nullable=true)
     */
    private $purePriceCurrencyInternal;

    /**
     * @var float
     * @ORM\Column(name="pure_tax_amount_internal", type="float", nullable=true)
     */
    private $pureTaxAmountInternal;
    /**
     * @var string
     * @ORM\Column(name="pure_tax_currency_internal", type="string", length=3, nullable=true)
     */
    private $pureTaxCurrencyInternal;

    /**
     * @var float
     * @ORM\Column(name="sale_price_amount_service_provider", type="float", nullable=true)
     */
    private $salePriceAmountServiceProvider;
    /**
     * @var string
     * @ORM\Column(name="sale_price_currency_service_provider", type="string", length=3, nullable=true)
     */
    private $salePriceCurrencyServiceProvider;

    /**
     * @var float
     * @ORM\Column(name="sale_tax_amount_service_provider", type="float", nullable=true)
     */
    private $saleTaxAmountServiceProvider;
    /**
     * @var string
     * @ORM\Column(name="sale_tax_currency_service_provider", type="string", length=3, nullable=true)
     */
    private $saleTaxCurrencyServiceProvider;

    /**
     * @var float
     * @ORM\Column(name="sale_price_amount_internal", type="float", nullable=true)
     */
    private $salePriceAmountInternal;
    /**
     * @var string
     * @ORM\Column(name="sale_price_currency_internal", type="string", length=3, nullable=true)
     */
    private $salePriceCurrencyInternal;


    /**
     * @var float
     * @ORM\Column(name="sale_tax_amount_internal", type="float", nullable=true)
     */
    private $saleTaxAmountInternal;
    /**
     * @var string
     * @ORM\Column(name="sale_tax_currency_internal", type="string", length=3, nullable=true)
     */
    private $saleTaxCurrencyInternal;


    /**
     * @var float
     * @ORM\Column(name="pay_price_amount_payment_system", type="float", nullable=true)
     */
    private $payPriceAmountPaymentSystem;
    /**
     * @var string
     * @ORM\Column(name="pay_price_currency_payment_system", type="string", length=3, nullable=true)
     */
    private $payPriceCurrencyPaymentSystem;
    /**
     * @var float
     * @ORM\Column(name="pay_price_amount_internal", type="float", nullable=true)
     */
    private $payPriceAmountInternal;
    /**
     * @var string
     * @ORM\Column(name="pay_price_currency_internal", type="string", length=3, nullable=true)
     */
    private $payPriceCurrencyInternal;
    /**
     * @var string
     * @ORM\Column(name="stripe_charge_id", type="string", nullable=true)
     */
    private $stripeChargeId;
    /**
     * @var string
     * @ORM\Column(name="stripe_transaction_id", type="string", nullable=true)
     */
    private $stripeTransactionId;
    /**
     * @var Charge|array
     * @ORM\Column(name="stripe_response", type="json_array", nullable=true)
     */
    private $stripeResponse;
    /**
     * @var string
     */
    private $nationality;
    /**
     * @var string
     * @ORM\Column(name="confirmation_number", type="string", nullable=true)
     */
    private $confirmationNumber;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contactPhoneNumbers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param BookingParams $bookingParams
     * @return BookingRequest
     */
    public static function fromBookingParams(BookingParams $bookingParams)
    {
        $booking = new static;
        $booking->setStripeToken($bookingParams->getStripeToken());

        $booking->setRequestedCurrency($bookingParams->getRequestedCurrency());
        $booking->setGuaranteeType(HotelProvisionalBookingRequestParams::GUARANTEE_TYPE_PRE_PAY);

        $booking->setHotelCode($bookingParams->getHotelCode());
        $booking->setCorrelationId(StringUtils::generateRandomString($length = 60));
        $booking->setTransactionIdentifier(StringUtils::generateRandomString($length = 60));
        $booking->setTimeSpanStartDate(\DateTime::createFromFormat('Y-m-d', $bookingParams->getCheckIn()));
        $booking->setTimeSpanEndDate(\DateTime::createFromFormat('Y-m-d', $bookingParams->getCheckOut()));
        $booking->setRoomName($bookingParams->getRoomName());
        $booking->setRoomTypeNumberOfUnits($bookingParams->getRoomTypeNumberOfUnits());
        $booking->setRoomTypeCode($bookingParams->getRoomType());
        $booking->setRatePlanCode($bookingParams->getRatePlan());
        $booking->setProfileNamePrefix($bookingParams->getNamePrefix());
        $booking->setProfileGivenName($bookingParams->getFirstName());
        $booking->setProfileLastName($bookingParams->getLastName());
        $booking->setProfilePhoneAreaCityCode($bookingParams->getPhoneArea());
        $booking->setProfilePhoneCountryAccessCode($bookingParams->getPhoneCountryCode());
        $booking->setProfilePhoneExtension($bookingParams->getPhoneExtension());
        $booking->setProfilePhoneNumber($bookingParams->getPhoneNumber());
        $booking->setProfilePhoneTechType($bookingParams->getPhoneTechType());
        $booking->setProfileEmail($bookingParams->getEmail());
        $booking->setProfileAddressLine1($bookingParams->getAddress1());
        $booking->setProfileAddressLine2($bookingParams->getAddress2());
        $booking->setProfileCityName($bookingParams->getCityName());
        $booking->setProfilePostalCode($bookingParams->getPostalCode());
        $booking->setProfileAddressStateProv($bookingParams->getAddressStateProv());
        $booking->setProfileAddressStateProvCode($bookingParams->getAddressStateProvCode());
        $booking->setProfileCountryName($bookingParams->getCountryName());
        $booking->setProfileCountryNameCode($bookingParams->getCountryNameCode());
        $booking->setNationality($bookingParams->getNationality());
        $booking->setCommentText($bookingParams->getComment());
        $booking->setGuestCounts((new JsonEncoder())->decode($bookingParams->getRooms(), JsonEncoder::FORMAT));

        $metaDataDTO = MetaDataDTO::fromJsonString($bookingParams->getMeta());
        $booking->setMetaDataDTO($metaDataDTO);
        $booking->setMetaData($metaDataDTO);

        return $booking;
    }


    /**
     * @return Money
     */
    public function getSaleTaxInternal()
    {
        return Money::build(
            $this->saleTaxAmountInternal,
            $this->saleTaxCurrencyInternal
        );
    }


    /**
     * @param Money $money
     * @return $this
     */
    public function setSaleTaxInternal(Money $money)
    {
        $this->saleTaxAmountInternal = $money->getAmount();
        $this->saleTaxCurrencyInternal = $money->getCurrencyCode();

        return $this;

    }


    /**
     * @return Money
     */
    public function getSaleTaxServiceProvider()
    {
        return Money::build(
            $this->saleTaxAmountServiceProvider,
            $this->saleTaxCurrencyServiceProvider
        );
    }


    /**
     * @param Money $money
     * @return $this
     */
    public function setSaleTaxServiceProvider(Money $money)
    {
        $this->saleTaxAmountServiceProvider = $money->getAmount();
        $this->saleTaxCurrencyServiceProvider = $money->getCurrencyCode();

        return $this;

    }

    /**
     * @return Money
     */
    public function getPureTaxInternal()
    {
        return Money::build(
            $this->pureTaxAmountInternal,
            $this->pureTaxCurrencyInternal
        );
    }


    /**
     * @param Money $money
     * @return $this
     */
    public function setPureTaxInternal(Money $money)
    {
        $this->pureTaxAmountInternal = $money->getAmount();
        $this->pureTaxCurrencyInternal = $money->getCurrencyCode();

        return $this;

    }

    /**
     * @return Money
     */
    public function getPureTaxServiceProvider()
    {
        return Money::build(
            $this->pureTaxAmountServiceProvider,
            $this->pureTaxCurrencyServiceProvider
        );
    }


    /**
     * @param Money $money
     * @return $this
     */
    public function setPureTaxServiceProvider(Money $money)
    {
        $this->pureTaxAmountServiceProvider = $money->getAmount();
        $this->pureTaxCurrencyServiceProvider = $money->getCurrencyCode();

        return $this;

    }


    /**
     * @return Money
     */
    public function getPurePriceServiceProvider()
    {
        return Money::build(
            $this->purePriceAmountServiceProvider,
            $this->purePriceCurrencyServiceProvider
        );
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setPurePriceServiceProvider(Money $money)
    {
        $this->purePriceAmountServiceProvider = $money->getAmount();
        $this->purePriceCurrencyServiceProvider = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @return Money
     */
    public function getPurePriceInternal()
    {
        return Money::build($this->purePriceAmountInternal, $this->purePriceCurrencyInternal);
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setPurePriceInternal(Money $money)
    {
        $this->purePriceAmountInternal = $money->getAmount();
        $this->purePriceCurrencyInternal = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @return Money
     */
    public function getSalePriceServiceProvider()
    {
        return Money::build(
            $this->salePriceAmountServiceProvider,
            $this->salePriceCurrencyServiceProvider
        );
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setSalePriceServiceProvider(Money $money)
    {
        $this->salePriceAmountServiceProvider = $money->getAmount();
        $this->salePriceCurrencyServiceProvider = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @return Money
     */
    public function getSalePriceInternal()
    {
        return Money::build($this->salePriceAmountInternal, $this->salePriceCurrencyInternal);
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setSalePriceInternal(Money $money)
    {
        $this->salePriceAmountInternal = $money->getAmount();
        $this->salePriceCurrencyInternal = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @return Money
     */
    public function getPayPricePaymentSystem()
    {
        return Money::build(
            $this->payPriceAmountPaymentSystem,
            $this->payPriceCurrencyPaymentSystem
        );
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setPayPricePaymentSystem(Money $money)
    {
        $this->payPriceAmountPaymentSystem = $money->getAmount();
        $this->payPriceCurrencyPaymentSystem = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @return Money
     */
    public function getPayPriceInternal()
    {
        return Money::build($this->payPriceAmountInternal, $this->payPriceCurrencyInternal);
    }

    /**
     * @param Money $money
     * @return $this
     */
    public function setPayPriceInternal(Money $money)
    {
        $this->payPriceAmountInternal = $money->getAmount();
        $this->payPriceCurrencyInternal = $money->getCurrencyCode();

        return $this;
    }

    /**
     * @param HotelProvisionalBookingRequestParams $params
     * @return BookingRequest
     */
    public function fillFromProvisionalRequestParams(HotelProvisionalBookingRequestParams $params)
    {
        $this->setGuaranteeType($params->getGuaranteeType())
            ->setCommentText($params->getCommentText())
            ->setGuestCounts($params->getGuestCounts())
            ->setProfileAddressLine1($params->getProfileAddressLine1())
            ->setProfileAddressLine2($params->getProfileAddressLine2())
            ->setProfileAddressStateProv($params->getProfileAddressStateProv())
            ->setProfileCityName($params->getProfileCityName())
            ->setProfileCountryName($params->getProfileCountryName())
            ->setProfileEmail($params->getProfileEmail())
            ->setProfileGivenName($params->getProfileGivenName())
            ->setProfileLastName($params->getProfileLastName())
            ->setProfileMiddleName($params->getProfileMiddleName())
            ->setProfileNamePrefix($params->getProfileNamePrefix())
            ->setProfilePhoneAreaCityCode($params->getProfilePhoneAreaCityCode())
            ->setProfilePhoneCountryAccessCode($params->getProfilePhoneCountryAccessCode())
            ->setProfilePhoneExtension($params->getProfilePhoneExtension())
            ->setProfilePhoneNumber($params->getProfilePhoneNumber())
            ->setProfilePhoneTechType($params->getProfilePhoneTechType())
            ->setProfilePostalCode($params->getProfilePostalCode())
            ->setRequestedCurrency($params->getRequestedCurrency())
            ->setRatePlanCode($params->getRatePlanRatePlanCode())
            ->setRoomTypeNumberOfUnits($params->getRoomTypeNumberOfUnits())
            ->setRoomTypeCode($params->getRoomTypeRoomTypeCode())
            ->setTimeSpanStartDate(new \DateTime($params->getTimeSpanStartTime()))
            ->setTimeSpanEndDate(new \DateTime($params->getTimeSpanEndDate()))
            ->setTotalAmountBeforeTax($params->getTotalAmountBeforeTax())
            ->setTotalTaxesAmount($params->getTotalTaxesAmount())
            ->setRoomName($params->roomName)
            ->setRoomPricePerNight($params->roomPricePerNight)
            ->setGuestCharges($params->guestCharges);

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BookingRequest
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileNamePrefix()
    {
        return $this->profileNamePrefix;
    }

    /**
     * @param string $profileNamePrefix
     * @return BookingRequest
     */
    public function setProfileNamePrefix($profileNamePrefix)
    {
        $this->profileNamePrefix = $profileNamePrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileGivenName()
    {
        return $this->profileGivenName;
    }

    /**
     * @param string $profileGivenName
     * @return BookingRequest
     */
    public function setProfileGivenName($profileGivenName)
    {
        $this->profileGivenName = $profileGivenName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileMiddleName()
    {
        return $this->profileMiddleName;
    }

    /**
     * @param string $profileMiddleName
     * @return BookingRequest
     */
    public function setProfileMiddleName($profileMiddleName)
    {
        $this->profileMiddleName = $profileMiddleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileLastName()
    {
        return $this->profileLastName;
    }

    /**
     * @param string $profileLastName
     * @return BookingRequest
     */
    public function setProfileLastName($profileLastName)
    {
        $this->profileLastName = $profileLastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneAreaCityCode()
    {
        return $this->profilePhoneAreaCityCode;
    }

    /**
     * @param string $profilePhoneAreaCityCode
     * @return BookingRequest
     */
    public function setProfilePhoneAreaCityCode($profilePhoneAreaCityCode)
    {
        $this->profilePhoneAreaCityCode = $profilePhoneAreaCityCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneCountryAccessCode()
    {
        return $this->profilePhoneCountryAccessCode;
    }

    /**
     * @param string $profilePhoneCountryAccessCode
     * @return BookingRequest
     */
    public function setProfilePhoneCountryAccessCode($profilePhoneCountryAccessCode)
    {
        $this->profilePhoneCountryAccessCode = $profilePhoneCountryAccessCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneExtension()
    {
        return $this->profilePhoneExtension;
    }

    /**
     * @param string $profilePhoneExtension
     * @return BookingRequest
     */
    public function setProfilePhoneExtension($profilePhoneExtension)
    {
        $this->profilePhoneExtension = $profilePhoneExtension;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneNumber()
    {
        return $this->profilePhoneNumber;
    }

    /**
     * @param string $profilePhoneNumber
     * @return BookingRequest
     */
    public function setProfilePhoneNumber($profilePhoneNumber)
    {
        $this->profilePhoneNumber = $profilePhoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneTechType()
    {
        return $this->profilePhoneTechType;
    }

    /**
     * @param string $profilePhoneTechType
     * @return BookingRequest
     */
    public function setProfilePhoneTechType($profilePhoneTechType)
    {
        $this->profilePhoneTechType = $profilePhoneTechType;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileEmail()
    {
        return $this->profileEmail;
    }

    /**
     * @param string $profileEmail
     * @return BookingRequest
     */
    public function setProfileEmail($profileEmail)
    {
        $this->profileEmail = $profileEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressLine1()
    {
        return $this->profileAddressLine1;
    }

    /**
     * @param string $profileAddressLine1
     * @return BookingRequest
     */
    public function setProfileAddressLine1($profileAddressLine1)
    {
        $this->profileAddressLine1 = $profileAddressLine1;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressLine2()
    {
        return $this->profileAddressLine2;
    }

    /**
     * @param string $profileAddressLine2
     * @return BookingRequest
     */
    public function setProfileAddressLine2($profileAddressLine2)
    {
        $this->profileAddressLine2 = $profileAddressLine2;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCityName()
    {
        return $this->profileCityName;
    }

    /**
     * @param string $profileCityName
     * @return BookingRequest
     */
    public function setProfileCityName($profileCityName)
    {
        $this->profileCityName = $profileCityName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePostalCode()
    {
        return $this->profilePostalCode;
    }

    /**
     * @param string $profilePostalCode
     * @return BookingRequest
     */
    public function setProfilePostalCode($profilePostalCode)
    {
        $this->profilePostalCode = $profilePostalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressStateProv()
    {
        return $this->profileAddressStateProv;
    }

    /**
     * @param string $profileAddressStateProv
     * @return BookingRequest
     */
    public function setProfileAddressStateProv($profileAddressStateProv)
    {
        $this->profileAddressStateProv = $profileAddressStateProv;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressStateProvCode()
    {
        return $this->profileAddressStateProvCode;
    }

    /**
     * @param string $profileAddressStateProvCode
     * @return BookingRequest
     */
    public function setProfileAddressStateProvCode($profileAddressStateProvCode)
    {
        $this->profileAddressStateProvCode = $profileAddressStateProvCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCountryName()
    {
        return $this->profileCountryName;
    }

    /**
     * @param string $profileCountryName
     * @return BookingRequest
     */
    public function setProfileCountryName($profileCountryName)
    {
        $this->profileCountryName = $profileCountryName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCountryNameCode()
    {
        return $this->profileCountryNameCode;
    }

    /**
     * @param string $profileCountryNameCode
     * @return BookingRequest
     */
    public function setProfileCountryNameCode($profileCountryNameCode)
    {
        $this->profileCountryNameCode = $profileCountryNameCode;

        return $this;
    }


    /**
     * Get correlationId
     *
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * Set correlationId
     *
     * @param string $correlationId
     * @return BookingRequest
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;

        return $this;
    }

    /**
     * Get transactionIdentifier
     *
     * @return string
     */
    public function getTransactionIdentifier()
    {
        return $this->transactionIdentifier;
    }

    /**
     * Set transactionIdentifier
     *
     * @param string $transactionIdentifier
     * @return BookingRequest
     */
    public function setTransactionIdentifier($transactionIdentifier)
    {
        $this->transactionIdentifier = $transactionIdentifier;

        return $this;
    }

    /**
     * Get requestedCurrency
     *
     * @return string
     */
    public function getRequestedCurrency()
    {
        return $this->requestedCurrency;
    }

    /**
     * Set requestedCurrency
     *
     * @param string $requestedCurrency
     * @return BookingRequest
     */
    public function setRequestedCurrency($requestedCurrency)
    {
        $this->requestedCurrency = $requestedCurrency;

        return $this;
    }

    /**
     * Get roomTypeNumberOfUnits
     *
     * @return integer
     */
    public function getRoomTypeNumberOfUnits()
    {
        return $this->roomTypeNumberOfUnits;
    }

    /**
     * Set roomTypeNumberOfUnits
     *
     * @param integer $roomTypeNumberOfUnits
     * @return BookingRequest
     */
    public function setRoomTypeNumberOfUnits($roomTypeNumberOfUnits)
    {
        $this->roomTypeNumberOfUnits = $roomTypeNumberOfUnits;

        return $this;
    }

    /**
     * Get roomTypeCode
     *
     * @return string
     */
    public function getRoomTypeCode()
    {
        return $this->roomTypeCode;
    }

    /**
     * Set roomTypeCode
     *
     * @param string $roomTypeCode
     * @return BookingRequest
     */
    public function setRoomTypeCode($roomTypeCode)
    {
        $this->roomTypeCode = $roomTypeCode;

        return $this;
    }

    /**
     * Get ratePlanCode
     *
     * @return string
     */
    public function getRatePlanCode()
    {
        return $this->ratePlanCode;
    }

    /**
     * Set ratePlanCode
     *
     * @param string $ratePlanCode
     * @return BookingRequest
     */
    public function setRatePlanCode($ratePlanCode)
    {
        $this->ratePlanCode = $ratePlanCode;

        return $this;
    }

    /**
     * Get totalAmountBeforeTax
     *
     * @return float
     */
    public function getTotalAmountBeforeTax()
    {
        return $this->totalAmountBeforeTax;
    }

    /**
     * Set totalAmountBeforeTax
     *
     * @param float $totalAmountBeforeTax
     * @return BookingRequest
     */
    public function setTotalAmountBeforeTax($totalAmountBeforeTax)
    {
        $this->totalAmountBeforeTax = $totalAmountBeforeTax;

        return $this;
    }

    /**
     * Get totalTaxesAmount
     *
     * @return float
     */
    public function getTotalTaxesAmount()
    {
        return $this->totalTaxesAmount;
    }

    /**
     * Set totalTaxesAmount
     *
     * @param float $totalTaxesAmount
     * @return BookingRequest
     */
    public function setTotalTaxesAmount($totalTaxesAmount)
    {
        $this->totalTaxesAmount = $totalTaxesAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getStripeToken()
    {
        return $this->stripeToken;
    }

    /**
     * @param string $stripeToken
     */
    public function setStripeToken($stripeToken)
    {
        $this->stripeToken = $stripeToken;
    }

    /**
     * Get commentText
     *
     * @return string
     */
    public function getCommentText()
    {
        return $this->commentText;
    }

    /**
     * Set commentText
     *
     * @param string $commentText
     * @return BookingRequest
     */
    public function setCommentText($commentText)
    {
        $this->commentText = $commentText;

        return $this;
    }

    /**
     * Get guaranteeType
     *
     * @return string
     */
    public function getGuaranteeType()
    {
        return $this->guaranteeType;
    }

    /**
     * Set guaranteeType
     *
     * @param string $guaranteeType
     * @return BookingRequest
     */
    public function setGuaranteeType($guaranteeType)
    {
        $this->guaranteeType = $guaranteeType;

        return $this;
    }

    /**
     * Get guestCounts
     *
     * @return array
     */
    public function getGuestCounts()
    {
        return $this->guestCounts;
    }

    /**
     * Set guestCounts
     *
     * @param array $guestCounts
     * @return BookingRequest
     */
    public function setGuestCounts($guestCounts)
    {
        $this->guestCounts = $guestCounts;

        return $this;
    }

    /**
     * Get bookingId
     *
     * @return string
     */
    public function getBookingId()
    {
        return $this->bookingId;
    }

    /**
     * Set bookingId
     *
     * @param string $bookingId
     * @return BookingRequest
     */
    public function setBookingId($bookingId)
    {
        $this->bookingId = $bookingId;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BookingRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BookingRequest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BookingRequest
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return BookingRequest
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return BookingRequest
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get travelGuruBookingId
     *
     * @return string
     */
    public function getTravelGuruBookingId()
    {
        return $this->travelGuruBookingId;
    }

    /**
     * Set travelGuruBookingId
     *
     * @param string $travelGuruBookingId
     * @return BookingRequest
     */
    public function setTravelGuruBookingId($travelGuruBookingId)
    {
        $this->travelGuruBookingId = $travelGuruBookingId;

        return $this;
    }

    /**
     * Get amountAfterTax
     *
     * @return float
     */
    public function getAmountAfterTax()
    {
        return $this->amountAfterTax;
    }

    /**
     * Set amountAfterTax
     *
     * @param float $amountAfterTax
     * @return BookingRequest
     */
    public function setAmountAfterTax($amountAfterTax)
    {
        $this->amountAfterTax = $amountAfterTax;

        return $this;
    }

    /**
     * Add contactPhoneNumbers
     *
     * @param ContactPhoneNumber $contactPhoneNumbers
     * @return BookingRequest
     */
    public function addContactPhoneNumber(ContactPhoneNumber $contactPhoneNumbers)
    {
        $this->contactPhoneNumbers[] = $contactPhoneNumbers;

        return $this;
    }

    /**
     * Remove contactPhoneNumbers
     *
     * @param ContactPhoneNumber $contactPhoneNumbers
     */
    public function removeContactPhoneNumber(ContactPhoneNumber $contactPhoneNumbers)
    {
        $this->contactPhoneNumbers->removeElement($contactPhoneNumbers);
    }

    /**
     * Get contactPhoneNumbers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPhoneNumbers()
    {
        return $this->contactPhoneNumbers;
    }

    /**
     * Get roomName
     *
     * @return string
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    /**
     * Set roomName
     *
     * @param string $roomName
     * @return BookingRequest
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;

        return $this;
    }

    /**
     * Get roomPricePerNight
     *
     * @return float
     */
    public function getRoomPricePerNight()
    {
        return $this->roomPricePerNight;
    }

    /**
     * Set roomPricePerNight
     *
     * @param float $roomPricePerNight
     * @return BookingRequest
     */
    public function setRoomPricePerNight($roomPricePerNight)
    {
        $this->roomPricePerNight = $roomPricePerNight;

        return $this;
    }

    /**
     * Get guestCharges
     *
     * @return float
     */
    public function getGuestCharges()
    {
        return $this->guestCharges;
    }

    /**
     * Set guestCharges
     *
     * @param float $guestCharges
     * @return BookingRequest
     */
    public function setGuestCharges($guestCharges)
    {
        $this->guestCharges = $guestCharges;

        return $this;
    }

    /**
     * @return \DateTime[]
     */
    public function getDateRangeArray()
    {
        $aryRange = array();

        $strDateFrom = $this->getTimeSpanStartDate()->format('Y-m-d');
        $strDateTo = $this->getTimeSpanEndDate()->format('Y-m-d');

        $iDateFrom = mktime(
            1,
            0,
            0,
            substr($strDateFrom, 5, 2),
            substr($strDateFrom, 8, 2),
            substr($strDateFrom, 0, 4)
        );
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom));
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400;
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }

        return $aryRange;
    }

    /**
     * Get timeSpanStartDate
     *
     * @return \DateTime
     */
    public function getTimeSpanStartDate()
    {
        return $this->timeSpanStartDate;
    }

    /**
     * Set timeSpanStartDate
     *
     * @param \DateTime $timeSpanStartDate
     * @return BookingRequest
     */
    public function setTimeSpanStartDate(\DateTime $timeSpanStartDate)
    {
        $this->timeSpanStartDate = $timeSpanStartDate;

        return $this;
    }

    /**
     * Get timeSpanEndDate
     *
     * @return \DateTime
     */
    public function getTimeSpanEndDate()
    {
        return $this->timeSpanEndDate;
    }

    /**
     * Set timeSpanEndDate
     *
     * @param \DateTime $timeSpanEndDate
     * @return BookingRequest
     */
    public function setTimeSpanEndDate(\DateTime $timeSpanEndDate)
    {
        $this->timeSpanEndDate = $timeSpanEndDate;

        return $this;
    }

    /**
     * Get payuAmount
     *
     * @return float
     */
    public function getPayuAmount()
    {
        return $this->payuAmount;
    }

    /**
     * Set payuAmount
     *
     * @param float $payuAmount
     * @return BookingRequest
     */
    public function setPayuAmount($payuAmount)
    {
        $this->payuAmount = $payuAmount;

        return $this;
    }

    /**
     * Get payuMihPayId
     *
     * @return string
     */
    public function getPayuMihPayId()
    {
        return $this->payuMihPayId;
    }

    /**
     * Set payuMihPayId
     *
     * @param string $payuMihPayId
     * @return BookingRequest
     */
    public function setPayuMihPayId($payuMihPayId)
    {
        $this->payuMihPayId = $payuMihPayId;

        return $this;
    }

    /**
     * Get totalAmountBeforeTaxExternal
     *
     * @return float
     */
    public function getTotalAmountBeforeTaxExternal()
    {
        return $this->totalAmountBeforeTaxExternal;
    }

    /**
     * Set totalAmountBeforeTaxExternal
     *
     * @param float $totalAmountBeforeTaxExternal
     * @return BookingRequest
     */
    public function setTotalAmountBeforeTaxExternal($totalAmountBeforeTaxExternal)
    {
        $this->totalAmountBeforeTaxExternal = $totalAmountBeforeTaxExternal;

        return $this;
    }

    /**
     * Get totalTaxesAmountExternal
     *
     * @return float
     */
    public function getTotalTaxesAmountExternal()
    {
        return $this->totalTaxesAmountExternal;
    }

    /**
     * Set totalTaxesAmountExternal
     *
     * @param float $totalTaxesAmountExternal
     * @return BookingRequest
     */
    public function setTotalTaxesAmountExternal($totalTaxesAmountExternal)
    {
        $this->totalTaxesAmountExternal = $totalTaxesAmountExternal;

        return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
        return $this->hotelCode;
    }

    /**
     * @param string $hotelCode
     */
    public function setHotelCode($hotelCode)
    {
        $this->hotelCode = $hotelCode;
    }

    /**
     * Get trackingId
     *
     * @return string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set trackingId
     *
     * @param string $trackingId
     * @return BookingRequest
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;

        return $this;
    }

    /**
     * Get hotelsProCancelResponseId
     *
     * @return integer
     */
    public function getHotelsProCancelResponseId()
    {
        return $this->hotelsProCancelResponseId;
    }

    /**
     * Set hotelsProCancelResponseId
     *
     * @param integer $hotelsProCancelResponseId
     * @return BookingRequest
     */
    public function setHotelsProCancelResponseId($hotelsProCancelResponseId)
    {
        $this->hotelsProCancelResponseId = $hotelsProCancelResponseId;

        return $this;
    }

    /**
     * Get hotelsProAgencyReferenceNumber
     *
     * @return string
     */
    public function getHotelsProAgencyReferenceNumber()
    {
        return $this->hotelsProAgencyReferenceNumber;
    }

    /**
     * Set hotelsProAgencyReferenceNumber
     *
     * @param string $hotelsProAgencyReferenceNumber
     * @return BookingRequest
     */
    public function setHotelsProAgencyReferenceNumber($hotelsProAgencyReferenceNumber)
    {
        $this->hotelsProAgencyReferenceNumber = $hotelsProAgencyReferenceNumber;

        return $this;
    }

    /**
     * Get hotelsProBookingStatus
     *
     * @return string
     */
    public function getHotelsProBookingStatus()
    {
        return $this->hotelsProBookingStatus;
    }

    /**
     * Set hotelsProBookingStatus
     *
     * @param string $hotelsProBookingStatus
     * @return BookingRequest
     */
    public function setHotelsProBookingStatus($hotelsProBookingStatus)
    {
        $this->hotelsProBookingStatus = $hotelsProBookingStatus;

        return $this;
    }

    /**
     * Get hotelsProCancellationNote
     *
     * @return string
     */
    public function getHotelsProCancellationNote()
    {
        return $this->hotelsProCancellationNote;
    }

    /**
     * Set hotelsProCancellationNote
     *
     * @param string $hotelsProCancellationNote
     * @return BookingRequest
     */
    public function setHotelsProCancellationNote($hotelsProCancellationNote)
    {
        $this->hotelsProCancellationNote = $hotelsProCancellationNote;

        return $this;
    }

    /**
     * Get publicBookingId
     *
     * @return string
     */
    public function getPublicBookingId()
    {
        return $this->publicBookingId;
    }

    /**
     * Set publicBookingId
     *
     * @param string $publicBookingId
     * @return BookingRequest
     */
    public function setPublicBookingId($publicBookingId)
    {
        $this->publicBookingId = $publicBookingId;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function doMetaDataDTOPreUpdate()
    {
        $this->metaDataDTO = $this->getMetaData()->toJsonString();
    }

    /**
     * @return MetaDataDTO
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * @param $metaData
     * @return $this
     */
    public function setMetaData($metaData)
    {
        $this->metaData = $metaData;

        return $this;
    }

    /**
     * @ORM\PostLoad
     */
    public function doMetaDataPostLoad()
    {
        $this->metaData = MetaDataDTO::fromJsonString($this->getMetaDataDTO());
    }

    /**
     * Get metaDataDTO
     *
     * @return string
     */
    public function getMetaDataDTO()
    {
        return $this->metaDataDTO;
    }

    /**
     * Set metaDataDTO
     *
     * @param string $metaDataDTO
     * @return BookingRequest
     */
    public function setMetaDataDTO($metaDataDTO)
    {
        $this->metaDataDTO = $metaDataDTO;

        return $this;
    }


    /**
     * Get payuTransactionId
     *
     * @return string
     */
    public function getPayuTransactionId()
    {
        return $this->payuTransactionId;
    }

    /**
     * Set payuTransactionId
     *
     * @param string $payuTransactionId
     * @return BookingRequest
     */
    public function setPayuTransactionId($payuTransactionId)
    {
        $this->payuTransactionId = $payuTransactionId;

        return $this;
    }

    public function getHotel()
    {
        return $this->hotel;
    }

    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
    }

    /**
     * Get hotelsCancellationPolicy
     *
     * @return array
     */
    public function getHotelsCancellationPolicy()
    {
        return $this->hotelsCancellationPolicy;
    }

    /**
     * Set hotelsCancellationPolicy
     *
     * @param array $hotelsCancellationPolicy
     * @return BookingRequest
     */
    public function setHotelsCancellationPolicy($hotelsCancellationPolicy)
    {
        $this->hotelsCancellationPolicy = $hotelsCancellationPolicy;

        return $this;
    }

    /**
     * Get ratesPerNight
     *
     * @return array
     */
    public function getRatesPerNight()
    {
        return $this->ratesPerNight;
    }

    /**
     * Set ratesPerNight
     *
     * @param array $ratesPerNight
     * @return BookingRequest
     */
    public function setRatesPerNight($ratesPerNight)
    {
        $this->ratesPerNight = $ratesPerNight;

        return $this;
    }

    /**
     * Get markupRate
     *
     * @return float
     */
    public function getMarkupRate()
    {
        return $this->markupRate;
    }

    /**
     * Set markupRate
     *
     * @param float $markupRate
     * @return BookingRequest
     */
    public function setMarkupRate($markupRate)
    {
        $this->markupRate = $markupRate;

        return $this;
    }

    /**
     * @return string
     */
    public function getStripeChargeId()
    {
        return $this->stripeChargeId;
    }

    /**
     * @param string $stripeChargeId
     */
    public function setStripeChargeId($stripeChargeId)
    {
        $this->stripeChargeId = $stripeChargeId;
    }

    /**
     * @return string
     */
    public function getStripeTransactionId()
    {
        return $this->stripeTransactionId;
    }

    /**
     * @param string $stripeTransactionId
     */
    public function setStripeTransactionId($stripeTransactionId)
    {
        $this->stripeTransactionId = $stripeTransactionId;
    }

    /**
     * @return string
     */
    public function getStripeResponse()
    {
        return $this->stripeResponse;
    }

    /**
     * @param Charge $stripeResponse
     */
    public function setStripeResponse(Charge $stripeResponse)
    {
        $this->stripeResponse = $stripeResponse->jsonSerialize();
    }

    /**
     * Set purePriceAmountServiceProvider
     *
     * @param float $purePriceAmountServiceProvider
     * @return BookingRequest
     */
    public function setPurePriceAmountServiceProvider($purePriceAmountServiceProvider)
    {
        $this->purePriceAmountServiceProvider = $purePriceAmountServiceProvider;

        return $this;
    }

    /**
     * Get purePriceAmountServiceProvider
     *
     * @return float 
     */
    public function getPurePriceAmountServiceProvider()
    {
        return $this->purePriceAmountServiceProvider;
    }

    /**
     * Set purePriceCurrencyServiceProvider
     *
     * @param string $purePriceCurrencyServiceProvider
     * @return BookingRequest
     */
    public function setPurePriceCurrencyServiceProvider($purePriceCurrencyServiceProvider)
    {
        $this->purePriceCurrencyServiceProvider = $purePriceCurrencyServiceProvider;

        return $this;
    }

    /**
     * Get purePriceCurrencyServiceProvider
     *
     * @return string 
     */
    public function getPurePriceCurrencyServiceProvider()
    {
        return $this->purePriceCurrencyServiceProvider;
    }

    /**
     * Set pureTaxAmountServiceProvider
     *
     * @param float $pureTaxAmountServiceProvider
     * @return BookingRequest
     */
    public function setPureTaxAmountServiceProvider($pureTaxAmountServiceProvider)
    {
        $this->pureTaxAmountServiceProvider = $pureTaxAmountServiceProvider;

        return $this;
    }

    /**
     * Get pureTaxAmountServiceProvider
     *
     * @return float 
     */
    public function getPureTaxAmountServiceProvider()
    {
        return $this->pureTaxAmountServiceProvider;
    }

    /**
     * Set pureTaxCurrencyServiceProvider
     *
     * @param string $pureTaxCurrencyServiceProvider
     * @return BookingRequest
     */
    public function setPureTaxCurrencyServiceProvider($pureTaxCurrencyServiceProvider)
    {
        $this->pureTaxCurrencyServiceProvider = $pureTaxCurrencyServiceProvider;

        return $this;
    }

    /**
     * Get pureTaxCurrencyServiceProvider
     *
     * @return string 
     */
    public function getPureTaxCurrencyServiceProvider()
    {
        return $this->pureTaxCurrencyServiceProvider;
    }

    /**
     * Set purePriceAmountInternal
     *
     * @param float $purePriceAmountInternal
     * @return BookingRequest
     */
    public function setPurePriceAmountInternal($purePriceAmountInternal)
    {
        $this->purePriceAmountInternal = $purePriceAmountInternal;

        return $this;
    }

    /**
     * Get purePriceAmountInternal
     *
     * @return float 
     */
    public function getPurePriceAmountInternal()
    {
        return $this->purePriceAmountInternal;
    }

    /**
     * Set purePriceCurrencyInternal
     *
     * @param string $purePriceCurrencyInternal
     * @return BookingRequest
     */
    public function setPurePriceCurrencyInternal($purePriceCurrencyInternal)
    {
        $this->purePriceCurrencyInternal = $purePriceCurrencyInternal;

        return $this;
    }

    /**
     * Get purePriceCurrencyInternal
     *
     * @return string 
     */
    public function getPurePriceCurrencyInternal()
    {
        return $this->purePriceCurrencyInternal;
    }

    /**
     * Set pureTaxAmountInternal
     *
     * @param float $pureTaxAmountInternal
     * @return BookingRequest
     */
    public function setPureTaxAmountInternal($pureTaxAmountInternal)
    {
        $this->pureTaxAmountInternal = $pureTaxAmountInternal;

        return $this;
    }

    /**
     * Get pureTaxAmountInternal
     *
     * @return float 
     */
    public function getPureTaxAmountInternal()
    {
        return $this->pureTaxAmountInternal;
    }

    /**
     * Set pureTaxCurrencyInternal
     *
     * @param string $pureTaxCurrencyInternal
     * @return BookingRequest
     */
    public function setPureTaxCurrencyInternal($pureTaxCurrencyInternal)
    {
        $this->pureTaxCurrencyInternal = $pureTaxCurrencyInternal;

        return $this;
    }

    /**
     * Get pureTaxCurrencyInternal
     *
     * @return string 
     */
    public function getPureTaxCurrencyInternal()
    {
        return $this->pureTaxCurrencyInternal;
    }

    /**
     * Set salePriceAmountServiceProvider
     *
     * @param float $salePriceAmountServiceProvider
     * @return BookingRequest
     */
    public function setSalePriceAmountServiceProvider($salePriceAmountServiceProvider)
    {
        $this->salePriceAmountServiceProvider = $salePriceAmountServiceProvider;

        return $this;
    }

    /**
     * Get salePriceAmountServiceProvider
     *
     * @return float 
     */
    public function getSalePriceAmountServiceProvider()
    {
        return $this->salePriceAmountServiceProvider;
    }

    /**
     * Set salePriceCurrencyServiceProvider
     *
     * @param string $salePriceCurrencyServiceProvider
     * @return BookingRequest
     */
    public function setSalePriceCurrencyServiceProvider($salePriceCurrencyServiceProvider)
    {
        $this->salePriceCurrencyServiceProvider = $salePriceCurrencyServiceProvider;

        return $this;
    }

    /**
     * Get salePriceCurrencyServiceProvider
     *
     * @return string 
     */
    public function getSalePriceCurrencyServiceProvider()
    {
        return $this->salePriceCurrencyServiceProvider;
    }

    /**
     * Set saleTaxAmountServiceProvider
     *
     * @param float $saleTaxAmountServiceProvider
     * @return BookingRequest
     */
    public function setSaleTaxAmountServiceProvider($saleTaxAmountServiceProvider)
    {
        $this->saleTaxAmountServiceProvider = $saleTaxAmountServiceProvider;

        return $this;
    }

    /**
     * Get saleTaxAmountServiceProvider
     *
     * @return float 
     */
    public function getSaleTaxAmountServiceProvider()
    {
        return $this->saleTaxAmountServiceProvider;
    }

    /**
     * Set saleTaxCurrencyServiceProvider
     *
     * @param string $saleTaxCurrencyServiceProvider
     * @return BookingRequest
     */
    public function setSaleTaxCurrencyServiceProvider($saleTaxCurrencyServiceProvider)
    {
        $this->saleTaxCurrencyServiceProvider = $saleTaxCurrencyServiceProvider;

        return $this;
    }

    /**
     * Get saleTaxCurrencyServiceProvider
     *
     * @return string 
     */
    public function getSaleTaxCurrencyServiceProvider()
    {
        return $this->saleTaxCurrencyServiceProvider;
    }

    /**
     * Set salePriceAmountInternal
     *
     * @param float $salePriceAmountInternal
     * @return BookingRequest
     */
    public function setSalePriceAmountInternal($salePriceAmountInternal)
    {
        $this->salePriceAmountInternal = $salePriceAmountInternal;

        return $this;
    }

    /**
     * Get salePriceAmountInternal
     *
     * @return float 
     */
    public function getSalePriceAmountInternal()
    {
        return $this->salePriceAmountInternal;
    }

    /**
     * Set salePriceCurrencyInternal
     *
     * @param string $salePriceCurrencyInternal
     * @return BookingRequest
     */
    public function setSalePriceCurrencyInternal($salePriceCurrencyInternal)
    {
        $this->salePriceCurrencyInternal = $salePriceCurrencyInternal;

        return $this;
    }

    /**
     * Get salePriceCurrencyInternal
     *
     * @return string 
     */
    public function getSalePriceCurrencyInternal()
    {
        return $this->salePriceCurrencyInternal;
    }

    /**
     * Set saleTaxAmountInternal
     *
     * @param float $saleTaxAmountInternal
     * @return BookingRequest
     */
    public function setSaleTaxAmountInternal($saleTaxAmountInternal)
    {
        $this->saleTaxAmountInternal = $saleTaxAmountInternal;

        return $this;
    }

    /**
     * Get saleTaxAmountInternal
     *
     * @return float 
     */
    public function getSaleTaxAmountInternal()
    {
        return $this->saleTaxAmountInternal;
    }

    /**
     * Set saleTaxCurrencyInternal
     *
     * @param string $saleTaxCurrencyInternal
     * @return BookingRequest
     */
    public function setSaleTaxCurrencyInternal($saleTaxCurrencyInternal)
    {
        $this->saleTaxCurrencyInternal = $saleTaxCurrencyInternal;

        return $this;
    }

    /**
     * Get saleTaxCurrencyInternal
     *
     * @return string 
     */
    public function getSaleTaxCurrencyInternal()
    {
        return $this->saleTaxCurrencyInternal;
    }

    /**
     * Set payPriceAmountPaymentSystem
     *
     * @param float $payPriceAmountPaymentSystem
     * @return BookingRequest
     */
    public function setPayPriceAmountPaymentSystem($payPriceAmountPaymentSystem)
    {
        $this->payPriceAmountPaymentSystem = $payPriceAmountPaymentSystem;

        return $this;
    }

    /**
     * Get payPriceAmountPaymentSystem
     *
     * @return float 
     */
    public function getPayPriceAmountPaymentSystem()
    {
        return $this->payPriceAmountPaymentSystem;
    }

    /**
     * Set payPriceCurrencyPaymentSystem
     *
     * @param string $payPriceCurrencyPaymentSystem
     * @return BookingRequest
     */
    public function setPayPriceCurrencyPaymentSystem($payPriceCurrencyPaymentSystem)
    {
        $this->payPriceCurrencyPaymentSystem = $payPriceCurrencyPaymentSystem;

        return $this;
    }

    /**
     * Get payPriceCurrencyPaymentSystem
     *
     * @return string 
     */
    public function getPayPriceCurrencyPaymentSystem()
    {
        return $this->payPriceCurrencyPaymentSystem;
    }

    /**
     * Set payPriceAmountInternal
     *
     * @param float $payPriceAmountInternal
     * @return BookingRequest
     */
    public function setPayPriceAmountInternal($payPriceAmountInternal)
    {
        $this->payPriceAmountInternal = $payPriceAmountInternal;

        return $this;
    }

    /**
     * Get payPriceAmountInternal
     *
     * @return float 
     */
    public function getPayPriceAmountInternal()
    {
        return $this->payPriceAmountInternal;
    }

    /**
     * Set payPriceCurrencyInternal
     *
     * @param string $payPriceCurrencyInternal
     * @return BookingRequest
     */
    public function setPayPriceCurrencyInternal($payPriceCurrencyInternal)
    {
        $this->payPriceCurrencyInternal = $payPriceCurrencyInternal;

        return $this;
    }

    /**
     * Get payPriceCurrencyInternal
     *
     * @return string 
     */
    public function getPayPriceCurrencyInternal()
    {
        return $this->payPriceCurrencyInternal;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getConfirmationNumber()
    {
        return $this->confirmationNumber;
    }

    /**
     * @param string $confirmationNumber
     */
    public function setConfirmationNumber($confirmationNumber)
    {
        $this->confirmationNumber = $confirmationNumber;
    }


}

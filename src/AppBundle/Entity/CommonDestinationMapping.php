<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 15:41
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CommonDestinationMapping
 *
 * @ORM\Entity()
 * @ORM\Table(name="`common_destination_mapping`", indexes={
 *      @ORM\Index(name="common_destination_mapping_service_destination", columns={"service_name", "service_destination_identifier"})
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *
 * @package AppBundle\Entity
 */
class CommonDestinationMapping
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="`id`", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CommonDestination
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CommonDestination")
     * @ORM\JoinColumn(name="`common_destination_id`", nullable=false, onDelete="CASCADE")
     */
    private $commonDestination;

    /**
     * @var string
     * @ORM\Column(name="`service_name`", type="string", length=12)
     */
    private $serviceName;

    /**
     * @var string
     * @ORM\Column(name="`service_destination_identifier`", type="string", length=32)
     */
    private $serviceDestinationIdentifier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`created_at`", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`updated_at`", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`deleted_at`", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     * @return CommonDestinationMapping
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string 
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Set serviceDestinationIdentifier
     *
     * @param string $serviceDestinationIdentifier
     * @return CommonDestinationMapping
     */
    public function setServiceDestinationIdentifier($serviceDestinationIdentifier)
    {
        $this->serviceDestinationIdentifier = $serviceDestinationIdentifier;

        return $this;
    }

    /**
     * Get serviceDestinationIdentifier
     *
     * @return string 
     */
    public function getServiceDestinationIdentifier()
    {
        return $this->serviceDestinationIdentifier;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CommonDestinationMapping
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CommonDestinationMapping
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return CommonDestinationMapping
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set commonDestination
     *
     * @param \AppBundle\Entity\CommonDestination $commonDestination
     * @return CommonDestinationMapping
     */
    public function setCommonDestination(\AppBundle\Entity\CommonDestination $commonDestination)
    {
        $this->commonDestination = $commonDestination;

        return $this;
    }

    /**
     * Get commonDestination
     *
     * @return \AppBundle\Entity\CommonDestination 
     */
    public function getCommonDestination()
    {
        return $this->commonDestination;
    }
}

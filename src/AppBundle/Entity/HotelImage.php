<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HotelImage
 *
 * @ORM\Table(name="hotel_image", indexes={@ORM\Index(name="VendorID", columns={"VendorID"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HotelImageRepository")
 */
class HotelImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Hotel
     *
     * @ORM\JoinColumn(name="VendorID", nullable=false, onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Hotel", fetch="EAGER", inversedBy="images")
     */
    private $hotel;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=1023, nullable=false)
     */
    private $imagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private $title;

    public static function fromArray(array $item)
    {
        $image = new HotelImage();
        $image->setTitle($item['Content-Title']);
        $image->setHotel($item['Hotel']);
        $image->setImagePath($item['ImageURL']);

        return $image;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param Hotel $hotel
     * @return HotelImage
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get Hotel
     *
     * @return Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set imageurl
     *
     * @param string $imagePath
     * @return HotelImage
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imageurl
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set contentTitle
     *
     * @param string $title
     * @return HotelImage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get contentTitle
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param HotelImage $image
     * @return bool
     */
    public function equals(HotelImage $image)
    {
        return (
            $image->getHotel()->equals($this->getHotel())
            && $image->getImagePath() == $this->getImagePath()
            && $image->getTitle() == $this->getTitle()
        );
    }

    /**
     * @param HotelImage $image
     */
    public function fillFromOther(HotelImage $image)
    {
        $this->setId($image->getId());
        $this->setHotel($image->getHotel());
        $this->setImagePath($image->getImagePath());
        $this->setTitle($image->getTitle());
    }
}

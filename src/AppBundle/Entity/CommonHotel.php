<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 29.04.2016
 * Time: 11:56
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class CommonHotel
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommonHotelRepository")
 * @ORM\Table(name="`common_hotels`")
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 *
 * @package AppBundle\Entity
 */
class CommonHotel
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="`id`", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="`hotel_name`", type="string", length=255)
     */
    private $hotelName;

    /**
     * @var CommonDestination
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CommonDestination")
     * @ORM\JoinColumn(name="`common_destination_id`", nullable=true, onDelete="SET NULL")
     */
    private $commonDestination;

    /**
     * @var string
     * @ORM\Column(name="`hotels_pro_hotel_code`", type="string", length=6, unique=true, nullable=true)
     */
    private $hotelsProHotelCode;

    /**
     * @var string
     * @ORM\Column(name="`travel_guru_hotel_code`", type="string", length=8, unique=true, nullable=true)
     */
    private $travelGuruHotelCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="`created_at`", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`updated_at`", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="`deleted_at`", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotelName
     *
     * @param string $hotelName
     * @return CommonHotel
     */
    public function setHotelName($hotelName)
    {
        $this->hotelName = $hotelName;

        return $this;
    }

    /**
     * Get hotelName
     *
     * @return string 
     */
    public function getHotelName()
    {
        return $this->hotelName;
    }

    /**
     * Set hotelsProHotelCode
     *
     * @param string $hotelsProHotelCode
     * @return CommonHotel
     */
    public function setHotelsProHotelCode($hotelsProHotelCode)
    {
        $this->hotelsProHotelCode = $hotelsProHotelCode;

        return $this;
    }

    /**
     * Get hotelsProHotelCode
     *
     * @return string 
     */
    public function getHotelsProHotelCode()
    {
        return $this->hotelsProHotelCode;
    }

    /**
     * Set travelGuruHotelCode
     *
     * @param string $travelGuruHotelCode
     * @return CommonHotel
     */
    public function setTravelGuruHotelCode($travelGuruHotelCode)
    {
        $this->travelGuruHotelCode = $travelGuruHotelCode;

        return $this;
    }

    /**
     * Get travelGuruHotelCode
     *
     * @return string 
     */
    public function getTravelGuruHotelCode()
    {
        return $this->travelGuruHotelCode;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CommonHotel
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CommonHotel
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return CommonHotel
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set commonDestination
     *
     * @param CommonDestination $commonDestination
     * @return CommonHotel
     */
    public function setCommonDestination(CommonDestination $commonDestination = null)
    {
        $this->commonDestination = $commonDestination;

        return $this;
    }

    /**
     * Get commonDestination
     *
     * @return CommonDestination 
     */
    public function getCommonDestination()
    {
        return $this->commonDestination;
    }
}

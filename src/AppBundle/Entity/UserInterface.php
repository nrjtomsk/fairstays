<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 16:01
 */

namespace AppBundle\Entity;


use Symfony\Component\Security\Core\User\UserInterface as SfUserInterface;

/**
 * Interface UserInterface
 * @package AppBundle\Entity
 */
interface UserInterface extends SfUserInterface
{

    const DEFAULT_ROLE = 'ROLE_USER';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getPhoneNumber();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return File
     */
    public function getAvatar();

    /**
     * @return CurrencyInterface
     */
    public function getCurrency();

    /**
     * @return string
     */
    public function getFullName();


}
<?php
/**
 * Created by PhpStorm.
 * User: yarovikovyo
 * Date: 21.12.2015
 * Time: 12:23
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use SimpleXMLElement;

/**
 * HotelsCity
 *
 * @ORM\Table(name="hotels_destinations", indexes={
 *  @ORM\Index(name="idx_hotels_destinations_hotel_pro_id", columns={"hotels_pro_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HotelsDestinationRepository")
 */
class HotelsDestination
{

    /**
     * @var integer
     *
     * @ORM\Column(name="city_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cityId;

    /**
     * @var string
     * @ORM\Column(name="hotels_pro_id", type="string", nullable=true)
     */
    private $hotelsProId;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=200, nullable=false)
     */
    private $countryName;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=200, nullable=false)
     */
    private $cityName;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=200, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=50, nullable=true)
     */
    private $countryCode;

    /**
     * Get cityId
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set hotelsProId
     *
     * @param string $hotelsProId
     * @return HotelsDestination
     */
    public function setHotelsProId($hotelsProId)
    {
        $this->hotelsProId = $hotelsProId;

        return $this;
    }

    /**
     * Get hotelsProId
     *
     * @return string
     */
    public function getHotelsProId()
    {
        return $this->hotelsProId;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return HotelsDestination
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Set cityName
     *
     * @param string $cityName
     * @return HotelsDestination
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * Get cityName
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return HotelsDestination
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return HotelsDestination
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param SimpleXMLElement $data
     * @param HotelsDestination $destination
     * @return static
     */
    public static function fromSimpleXml(SimpleXMLElement $data, HotelsDestination $destination = null)
    {
        if ($destination == null) {
            $destination = new static;
        }
        $destination->setCountryName((string)$data->Country);
        $destination->setCityName((string)$data->City);
        $destination->setHotelsProId((string)$data->DestinationId);
        $destination->setState((string)$data->State);

        return $destination;
    }
}

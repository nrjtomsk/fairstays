<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SimpleXMLElement;

/**
 * @ORM\Table(name="hotels_pro", indexes={
 *  @ORM\Index(name="idx_hotels_pro_hotel_code", columns={"hotel_code"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HotelProRepository")
 */
class HotelPro implements TripAdvisorRatingAware
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="hotel_code", type="string", length=10, nullable=true)
     */
    private $hotelCode;

    /**
     * @var string
     * @ORM\Column(name="destination", type="string", length=50, nullable=true)
     */
    private $destination;

    /**
     * @var string
     * @ORM\Column(name="country", type="string", length=127, nullable=true)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(name="hotel_name", type="text", nullable=true)
     */
    private $hotelName;

    /**
     * @var int
     * @ORM\Column(name="star_rating", type="smallint", nullable=true)
     */
    private $starRating;

    /**
     * @var string
     * @ORM\Column(name="hotel_address", type="text", nullable=true)
     */
    private $hotelAddress;

    /**
     * @var string
     * @ORM\Column(name="hotel_postal_code", type="string", length=30, nullable=true)
     */
    private $hotelPostalCode;

    /**
     * @var string
     * @ORM\Column(name="hotel_phone_number", type="text", nullable=true)
     */
    private $hotelPhoneNumber;

    /**
     * @var string
     * @ORM\Column(name="hotel_area", type="text", nullable=true)
     */
    private $hotelArea;

    /**
     * @var string
     * @ORM\Column(name="latitude", type="string", length=20, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     * @ORM\Column(name="longitude", type="string", length=20, nullable=true)
     */
    private $longitude;

    /**
     * @var array
     * @ORM\Column(name="images_urls", type="json_array", nullable=true)
     */
    private $imagesUrls;

    /**
     * @var int
     * @ORM\Column(name="rooms_number", type="smallint", nullable=true)
     */
    private $roomsNumber;

    /**
     * @var array
     * @ORM\Column(name="property_amenities", type="json_array", nullable=true)
     */
    private $propertyAmenities;

    /**
     * @var array
     * @ORM\Column(name="room_amenities", type="json_array", nullable=true)
     */
    private $roomAmenities;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var TripAdvisorRating
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TripAdvisorRating", fetch="EAGER")
     * @ORM\JoinColumn(name="trip_advisor_rating_id", nullable=true, onDelete="SET NULL")
     */
    private $tripAdvisorRating;

    /**
     * @var string
     * @ORM\Column(name="trivago_url", type="string", length=256, nullable=true)
     */
    private $trivagoUrl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotelCode
     *
     * @param string $hotelCode
     * @return HotelPro
     */
    public function setHotelCode($hotelCode)
    {
        $this->hotelCode = $hotelCode;

        return $this;
    }

    /**
     * Get hotelCode
     *
     * @return string
     */
    public function getHotelCode()
    {
        return $this->hotelCode;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return HotelPro
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return HotelPro
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set hotelName
     *
     * @param string $hotelName
     * @return HotelPro
     */
    public function setHotelName($hotelName)
    {
        $this->hotelName = $hotelName;

        return $this;
    }

    /**
     * Get hotelName
     *
     * @return string
     */
    public function getHotelName()
    {
        return $this->hotelName;
    }

    /**
     * Set starRating
     *
     * @param integer $starRating
     * @return HotelPro
     */
    public function setStarRating($starRating)
    {
        $this->starRating = $starRating;

        return $this;
    }

    /**
     * Get starRating
     *
     * @return integer
     */
    public function getStarRating()
    {
        return $this->starRating;
    }

    /**
     * Set hotelAddress
     *
     * @param string $hotelAddress
     * @return HotelPro
     */
    public function setHotelAddress($hotelAddress)
    {
        $this->hotelAddress = $hotelAddress;

        return $this;
    }

    /**
     * Get hotelAddress
     *
     * @return string
     */
    public function getHotelAddress()
    {
        return $this->hotelAddress;
    }

    /**
     * Set hotelPostalCode
     *
     * @param string $hotelPostalCode
     * @return HotelPro
     */
    public function setHotelPostalCode($hotelPostalCode)
    {
        $this->hotelPostalCode = $hotelPostalCode;

        return $this;
    }

    /**
     * Get hotelPostalCode
     *
     * @return string
     */
    public function getHotelPostalCode()
    {
        return $this->hotelPostalCode;
    }

    /**
     * Set hotelPhoneNumber
     *
     * @param string $hotelPhoneNumber
     * @return HotelPro
     */
    public function setHotelPhoneNumber($hotelPhoneNumber)
    {
        $this->hotelPhoneNumber = $hotelPhoneNumber;

        return $this;
    }

    /**
     * Get hotelPhoneNumber
     *
     * @return string
     */
    public function getHotelPhoneNumber()
    {
        return $this->hotelPhoneNumber;
    }

    /**
     * Set hotelArea
     *
     * @param string $hotelArea
     * @return HotelPro
     */
    public function setHotelArea($hotelArea)
    {
        $this->hotelArea = $hotelArea;

        return $this;
    }

    /**
     * Get hotelArea
     *
     * @return string
     */
    public function getHotelArea()
    {
        return $this->hotelArea;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return HotelPro
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return HotelPro
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set imagesUrls
     *
     * @param array $imagesUrls
     * @return HotelPro
     */
    public function setImagesUrls($imagesUrls)
    {
        $this->imagesUrls = $imagesUrls;

        return $this;
    }

    /**
     * Get imagesUrls
     *
     * @return array
     */
    public function getImagesUrls()
    {
        return $this->imagesUrls;
    }

    /**
     * @param SimpleXMLElement $data
     * @param HotelPro $hotelPro
     * @return static
     */
    public static function fromSimpleXml(SimpleXMLElement $data, HotelPro $hotelPro = null)
    {
        if ($hotelPro == null) {
            $hotelPro = new static;
        }

        if (isset($data->HotelCode)) {
            $hotelPro->setHotelCode((string)$data->HotelCode);
        }
        if (isset($data->Destination)) {
            $hotelPro->setDestination((string)$data->Destination);
        }
        if (isset($data->Country)) {
            $hotelPro->setCountry((string)$data->Country);
        }
        if (isset($data->HotelName)) {
            $hotelPro->setHotelName((string)$data->HotelName);
        }
        if (isset($data->StarRating)) {
            $hotelPro->setStarRating((int)$data->StarRating);
        }
        if (isset($data->HotelAddress)) {
            $hotelPro->setHotelAddress((string)$data->HotelAddress);
        }
        if (isset($data->HotelPostalCode)) {
            $hotelPro->setHotelPostalCode((string)$data->HotelPostalCode);
        }
        if (isset($data->HotelPostalCode)) {
            $hotelPro->setHotelPostalCode((string)$data->HotelPostalCode);
        }
        if (isset($data->HotelPhoneNumber)) {
            $hotelPro->setHotelPhoneNumber((string)$data->HotelPhoneNumber);
        }
        if (isset($data->HotelArea)) {
            $hotelPro->setHotelArea((string)$data->HotelArea);
        }
        if (isset($data->Coordinates->Latitude)) {
            $hotelPro->setLatitude((string)$data->Coordinates->Latitude);
        }
        if (isset($data->Coordinates->Longitude)) {
            $hotelPro->setLongitude((string)$data->Coordinates->Longitude);
        }
        if (isset($data->HotelImages)) {

            $images = [];
            foreach ($data->HotelImages->children() as $imageUrl) {

                $images[] = (string)$imageUrl;
            }

            $hotelPro->setImagesUrls($images);
        }

        return $hotelPro;
    }

    /**
     * Set roomsNumber
     *
     * @param integer $roomsNumber
     * @return HotelPro
     */
    public function setRoomsNumber($roomsNumber)
    {
        $this->roomsNumber = $roomsNumber;

        return $this;
    }

    /**
     * Get roomsNumber
     *
     * @return integer
     */
    public function getRoomsNumber()
    {
        return $this->roomsNumber;
    }

    /**
     * Set propertyAmenities
     *
     * @param array $propertyAmenities
     * @return HotelPro
     */
    public function setPropertyAmenities($propertyAmenities)
    {
        $this->propertyAmenities = $propertyAmenities;

        return $this;
    }

    /**
     * Get propertyAmenities
     *
     * @return array
     */
    public function getPropertyAmenities()
    {
        return $this->propertyAmenities;
    }

    /**
     * Set roomAmenities
     *
     * @param array $roomAmenities
     * @return HotelPro
     */
    public function setRoomAmenities($roomAmenities)
    {
        $this->roomAmenities = $roomAmenities;

        return $this;
    }

    /**
     * Get roomAmenities
     *
     * @return array
     */
    public function getRoomAmenities()
    {
        return $this->roomAmenities;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return HotelPro
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tripAdvisorRating
     *
     * @param \AppBundle\Entity\TripAdvisorRating $tripAdvisorRating
     * @return HotelPro
     */
    public function setTripAdvisorRating(\AppBundle\Entity\TripAdvisorRating $tripAdvisorRating = null)
    {
        $this->tripAdvisorRating = $tripAdvisorRating;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrivagoUrl()
    {
        return $this->trivagoUrl;
    }

    /**
     * @param string $trivagoUrl
     */
    public function setTrivagoUrl($trivagoUrl)
    {
        $this->trivagoUrl = $trivagoUrl;
    }

    /**
     * Get tripAdvisorRating
     *
     * @return \AppBundle\Entity\TripAdvisorRating 
     */
    public function getTripAdvisorRating()
    {
        return $this->tripAdvisorRating;
    }
}

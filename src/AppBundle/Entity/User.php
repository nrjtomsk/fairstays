<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 15:05
 */

namespace AppBundle\Entity;

use ApiBundle\Service\Utils\StringUtils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Gedmo\Loggable()
 *
 * @package AppBundle\Entity
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Gedmo\Versioned()
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="username", type="string", nullable=false)
     * @Gedmo\Versioned()
     *
     */
    private $username;
    /**
     * @var File
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\File", fetch="EAGER")
     * @ORM\JoinColumn(name="avatar_id", nullable=true, onDelete="SET NULL")
     *
     */
    private $avatar;
    /**
     * @var Currency
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency", fetch="EAGER")
     * @ORM\JoinColumn(name="currency_id", nullable=true, onDelete="SET NULL")
     *
     */
    private $currency;
    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     *
     */
    private $fullName;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(name="phone_number", type="string", length=25, nullable=true)
     * @Gedmo\Versioned()
     */
    private $phoneNumber;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;
    /**
     * @var string
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var BookingRequest[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BookingRequest", mappedBy="user", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $bookingRequests;

    /**
     * @var array
     * @ORM\Column(name="roles", type="array", nullable=true)
     */
    private $roles;
    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     * @Gedmo\Versioned()
     */
    private $createdAt;
    /**
     * @var \DateTime
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="update")
     * @Gedmo\Versioned()
     */
    private $updatedAt;
    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Gedmo\Versioned()
     */
    private $deletedAt;

    public function __construct()
    {
        $this->salt = StringUtils::generateRandomString(64);
        $this->roles[] = static::DEFAULT_ROLE;
        $this->bookingRequests = new ArrayCollection();
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        $roles = $this->roles;
        if (is_string($this->roles)) {
            $roles = explode(',', $this->roles);
        }

        $roles[] = 'ROLE_USER';
        $this->roles = array_unique($roles);

        return $this->roles;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email = null)
    {
        $this->email = $email;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency($currency = null)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName = null)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return File
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($file = null)
    {
        $this->avatar = $file;

        return $this;
    }

    /**
     * Add bookingRequests
     *
     * @param \AppBundle\Entity\BookingRequest $bookingRequests
     * @return User
     */
    public function addBookingRequest(\AppBundle\Entity\BookingRequest $bookingRequests)
    {
        $this->bookingRequests[] = $bookingRequests;

        return $this;
    }

    /**
     * Remove bookingRequests
     *
     * @param \AppBundle\Entity\BookingRequest $bookingRequests
     */
    public function removeBookingRequest(\AppBundle\Entity\BookingRequest $bookingRequests)
    {
        $this->bookingRequests->removeElement($bookingRequests);
    }

    /**
     * Get bookingRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingRequests()
    {
        return $this->bookingRequests;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }


    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }
}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BookingRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contains three callbacks: success, failure, cancel
 * and one test payment
 *
 * Class PayUResponseController
 *
 * @Route("/payu")
 *
 * @package AppBundle\Controller
 */
class PayUResponseController extends Controller
{

    /**
     * @Route("/success", name="payu_success")
     * @param Request $request
     * @return Response
     */
    public function successAction(Request $request)
    {
        if ($this->get('api.service.payu_response_handler')->processSuccess($request)) {
            $message = 'Success!';
        } else {
            $message = 'Fail!';
        }

        return $this->render(
            '@App/PayUResponse/success.html.twig',
            [
                'message' => $message,
            ]
        );
    }

    /**
     * @Route("/failure", name="payu_failure")
     * @param Request $request
     * @return Response
     */
    public function failureAction(Request $request)
    {
        return $this->render(
            '@App/PayUResponse/failure.html.twig',
            [

            ]
        );
    }

    /**
     * @Route("/cancel", name="payu_cancel")
     * @param Request $request
     * @return Response
     */
    public function cancelAction(Request $request)
    {
        return $this->render(
            '@App/PayUResponse/cancel.html.twig',
            [

            ]
        );
    }

    /**
     * @Route("/test-payment/{id}", name="payu_test_payment",
     *   requirements={
     *     "id": "\d+"
     *   }
     * )
     */
    public function testPaymentAction($id)
    {
        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->get('app.repo.booking_request')->find($id);

        $requestObject = $this->get('api.service.payu_request_object_provider')
            ->getPayURequestObjectByBookingRequest($bookingRequest);

        return $this->render(
            '@App/PayUResponse/testPayment.html.twig',
            [
                'requestObject' => $requestObject,
            ]
        );
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.01.2016
 * Time: 16:00
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Cache(public=true, maxage=86400, smaxage=86400)
     * @return array
     */
    public function indexAction()
    {
        return $this->render('@App/Default/index.html.twig');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 16.03.16
 * Time: 11:19
 */

namespace AppBundle\Controller;


use AppBundle\Entity\BookingRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StripeResponseController extends Controller
{
    /**
     * @Route("/test-stripe-payment/{id}", name="stipe",
     *   requirements={
     *     "id": "\d+"
     *   }
     * )
     * @param Request $request
     * @param $id integer
     * @return Response
     */
    public function testStripePaymentAction(Request $request, $id)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            /** @var BookingRequest $bookingRequest */
            $data = $request->request->all();
            $bookingRequest = $this->get('app.repo.booking_request')->find($id);
            if ($bookingRequest !== null) {
                if ($bookingRequest->getStripeToken() !== null) {
                    $token = $bookingRequest->getStripeToken();
                }
                $amount = $bookingRequest->getPayPricePaymentSystem()->getAmount();
                $currency = $bookingRequest->getPayPricePaymentSystem()->getCurrencyCode();
                $email = $bookingRequest->getProfileEmail();
            } else {
                $token = [
                    'exp_month' => $data['mm'],
                    'exp_year' => $data['yy'],
                    'number' => $data['card'],
                    'object' => 'card',
                    'cvc' => $data['cvc'],
                ];
                $amount = 100;
                $currency = 'USD';
                $email = null;
            }
            $stripeResponse = $this->get('api.service.stripe_payment_gateway')
                ->pay($amount, $currency, $token, $email);
            $bookingRequest->setStatus(BookingRequest::STATUS_CONFIRMED);
            return $this->render(
                '@App/stripe/success.html.twig',
                [
                    'response' => $stripeResponse,
                ]
            );
        }

        return $this->render(
            '@App/stripe/stripe.html.twig'
        );
    }

}
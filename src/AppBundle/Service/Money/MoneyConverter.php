<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.03.2016
 * Time: 11:28
 */

namespace AppBundle\Service\Money;


interface MoneyConverter
{
    /**
     * @param Money $sourceMoney
     * @param $currencyCode
     * @return Money
     */
    public function convertMoney(Money $sourceMoney, $currencyCode);

    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @return float
     */
    public function getExchangeRate($fromCurrency, $toCurrency);

    /**
     * @param Money $money
     * @return Money
     */
    public function convertToInternalMoney(Money $money);
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.03.2016
 * Time: 11:28
 */

namespace AppBundle\Service\Money;


use AppBundle\Service\Money\Exception\MoneyException;
use JMS\Serializer\Annotation as JMS;
use Money\Currency;
use Money\InvalidArgumentException;
use Money\UnknownCurrencyException;

/**
 * Class Money
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package AppBundle\Service\Money
 */
class Money
{

    const MAX_VALUE = 5000000;
    const MIN_VALUE = 0;

    const TO_CENTS_MULTIPLIER = 100;

    /**
     * @var float
     * @JMS\Expose()
     */
    private $amount;

    /**
     * @var string
     * @JMS\Expose()
     */
    private $currencyCode;

    /**
     * @var \Money\Money
     */
    private $money;
    
    private $multyply = 1000;

    /**
     * @param $amount
     * @param $currencyCode
     * @return static
     * @throws MoneyException
     */
    public static function build($amount, $currencyCode)
    {
        $money = new static;
        $money->setAmount($amount);
        $money->currencyCode = $currencyCode;
        try {
            $money->money = new \Money\Money((int)($amount * $money->multyply), new Currency($currencyCode));
            return $money;
        } catch (UnknownCurrencyException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        } catch (InvalidArgumentException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param \Money\Money $money
     * @return static
     */
    public static function buildFromMoney(\Money\Money $money)
    {
        $new = new static;
        $new->money = $money;
        $new->currencyCode = $money->getCurrency()->getName();
        $new->setAmount((float)($money->getAmount()/$new->multyply));
        return $new;
    }


    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Get amount in cents (amount * 100)
     * @return integer
     */
    public function getInCentsAmount()
    {
        return $this->amount * static::TO_CENTS_MULTIPLIER;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = ceil((float)$amount);
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return Currency
     * @throws MoneyException
     */
    public function getCurrency()
    {
        try {
            return new Currency($this->currencyCode);
        } catch (UnknownCurrencyException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @return \Money\Money
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @param $multiplier
     * @return Money
     */
    public function multiply($multiplier)
    {
        return static::buildFromMoney($this->getMoney()->multiply($multiplier));
    }

    /**
     * @param Money $money
     * @return Money
     */
    public function add(Money $money)
    {
        return Money::buildFromMoney($this->getMoney()->add($money->getMoney()));
    }

    /**
     * @param Money $other
     * @return bool
     */
    public function lessThan(Money $other)
    {
        return $this->getMoney()->lessThan($other->getMoney());
    }

    /**
     * @param Money $other
     * @return bool
     */
    public function greaterThan(Money $other)
    {
        return $this->getMoney()->greaterThan($other->getMoney());
    }

    /**
     * @param Money $other
     * @return bool
     */
    public function equals(Money $other)
    {
        return $this->getMoney()->equals($other->getMoney());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '{Money [amount: '.$this->getAmount().', currency: '.$this->getCurrencyCode().']}';
    }

    /**
     * @param Money $money
     * @return Money
     * @throws MoneyException
     */
    public function minus(Money $money)
    {
        return $this->add(Money::build(-1 * $money->getAmount(), $money->getCurrencyCode()));
    }


}
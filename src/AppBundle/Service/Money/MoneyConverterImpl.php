<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.03.2016
 * Time: 12:12
 */

namespace AppBundle\Service\Money;


use AppBundle\Service\Money\Exception\MoneyException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\QueryException;
use Money\InvalidArgumentException;
use Money\UnknownCurrencyException;
use Tbbc\MoneyBundle\Pair\PairManager;

/**
 * Class MoneyConverterImpl
 * @package AppBundle\Service\Money
 */
class MoneyConverterImpl implements MoneyConverter
{
    /**
     * @var PairManager
     */
    private $pairManager;

    /**
     * @var string
     */
    private $systemCurrency;

    /**
     * MoneyConverterImpl constructor.
     * @param PairManager $pairManager
     * @param string $systemCurrency
     */
    public function __construct(PairManager $pairManager, $systemCurrency)
    {
        $this->pairManager = $pairManager;
        $this->systemCurrency = $systemCurrency;
    }

    /**
     * @param Money $money
     * @return Money
     * @throws MoneyException
     */
    public function convertToInternalMoney(Money $money)
    {
        return $this->convertMoney($money, $this->systemCurrency);
    }

    /**
     * @param Money $sourceMoney
     * @param $currencyCode
     * @return Money
     * @throws MoneyException
     */
    public function convertMoney(Money $sourceMoney, $currencyCode)
    {
        if ($sourceMoney->getCurrencyCode() === $currencyCode) {
            return Money::build($sourceMoney->getAmount(), $currencyCode);
        }
        try {
            return Money::buildFromMoney(
                $this->pairManager->convert($sourceMoney->getMoney(), $currencyCode)
            );
        } catch (UnknownCurrencyException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        } catch (InvalidArgumentException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        } catch (\Tbbc\MoneyBundle\MoneyException $e) {
            throw new MoneyException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @return float
     * @throws MoneyException
     */
    public function getExchangeRate($fromCurrency, $toCurrency)
    {
        return $this->convertMoney(Money::build(1, $fromCurrency), $toCurrency)->getAmount();
    }
}
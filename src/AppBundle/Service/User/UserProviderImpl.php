<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 15:57
 */

namespace AppBundle\Service\User;


use ApiBundle\Service\Utils\StringUtils;
use AppBundle\Entity\SmsVerificationToken;
use AppBundle\Entity\User;
use AppBundle\Entity\UserInterface as AppUserInterface;
use AppBundle\Repository\AuthTokenRepository;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserProviderImpl
 * @package AppBundle\Service\User
 */
class UserProviderImpl implements UserProvider
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EncoderFactory
     */
    private $encoderFactory;

    /**
     * @var AuthTokenRepository
     */
    private $authTokenRepo;

    /**
     * UserProviderImpl constructor.
     * @param UserRepository $userRepository
     * @param EncoderFactory $encoderFactory
     * @param AuthTokenRepository $authTokenRepository
     */
    public function __construct(
        UserRepository $userRepository,
        EncoderFactory $encoderFactory,
        AuthTokenRepository $authTokenRepository
    ) {
        $this->userRepository = $userRepository;
        $this->encoderFactory = $encoderFactory;
        $this->authTokenRepo = $authTokenRepository;
    }


    /**
     * Loads the user for the given phoneNumber
     *
     * @param $phoneNumber
     * @return AppUserInterface
     * @throws UsernameNotFoundException
     */
    public function loadUserByPhoneNumber($phoneNumber)
    {
        return $this->loadUserByUsername($phoneNumber);
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return AppUserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {

        $user = $this->userRepository->findOneBy(['username' => $username]);

        if (!$user) {
            throw new UsernameNotFoundException(
                sprintf(
                    'User %s not found',
                    $username
                )
            );
        }

        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return AppUserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException;
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'AppBundle\Entity\User' || is_subclass_of($class, 'AppBundle\Entity\User');
    }

    /**
     * Loads the user for the given apiKey
     *
     * @param $apiKey
     * @return AppUserInterface
     * @throws UsernameNotFoundException
     */
    public function loadUserByApiKey($apiKey)
    {
        $authToken = $this->authTokenRepo->findNotExpiredAuthTokenByToken($apiKey);
        if ($authToken && $authToken->getUser()) {
            return $authToken->getUser();
        }

        throw new UsernameNotFoundException(
            sprintf(
                'Cannot load user with token %s',
                $apiKey
            )
        );
    }

    /**
     * Creates the user
     *
     * @param $phoneNumber
     * @return AppUserInterface
     * @throws \RuntimeException
     */
    public function createUser($phoneNumber)
    {
        $user = new User();
        $user->setUsername($phoneNumber);
        $user->setPassword(
            $this->encoderFactory->getEncoder($user)
                ->encodePassword(
                    StringUtils::generateRandomString(10),
                    $user->getSalt()
                )
        );
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * Loads the user for the given SmsVerificationToken
     *
     * @param SmsVerificationToken $token
     * @return AppUserInterface
     */
    public function loadUserBySmsVerificationToken(SmsVerificationToken $token)
    {
        try {
            $user = $this->loadUserByPhoneNumber($token->getPhoneNumber());
        } catch (UsernameNotFoundException $e) {
            /** @var User $user */
            $user = $this->createUser($token->getPhoneNumber());
            $user->setPhoneNumber($token->getPhoneNumber());
            $this->save($user);
        }

        return $user;
    }

    /**
     * Removes the user
     *
     * @param AppUserInterface $user
     */
    public function deleteUser(AppUserInterface $user)
    {
        $this->userRepository->deleteUser($user);
    }

    /**
     * Saves the user
     *
     * @param AppUserInterface $user
     */
    public function save(AppUserInterface $user)
    {
        $this->userRepository->save($user);
    }
}
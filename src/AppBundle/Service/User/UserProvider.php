<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 15:55
 */

namespace AppBundle\Service\User;


use AppBundle\Entity\SmsVerificationToken;
use AppBundle\Entity\User;
use AppBundle\Entity\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Interface UserProvider
 * @package AppBundle\Service\User
 */
interface UserProvider extends UserProviderInterface
{
    /**
     * @param $phoneNumber
     * @return User
     * @throws UsernameNotFoundException
     */
    public function loadUserByPhoneNumber($phoneNumber);

    /**
     * @param $apiKey
     * @return User
     * @throws UsernameNotFoundException
     */
    public function loadUserByApiKey($apiKey);

    /**
     * @param $phoneNumber
     * @return User
     */
    public function createUser($phoneNumber);

    /**
     * @param SmsVerificationToken $token
     * @return User
     */
    public function loadUserBySmsVerificationToken(SmsVerificationToken $token);

    /**
     * @param UserInterface $user
     * @return void
     */
    public function deleteUser(UserInterface $user);

    /**
     * @param UserInterface $user
     * @return void
     */
    public function save(UserInterface $user);
}
<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 06.04.16
 * Time: 12:08
 */

namespace AppBundle\Service\HotelsPro;

use AppBundle\Entity\HotelsDestination;
use AppBundle\Entity\PriceSelectedHotels;
use AppBundle\Repository\HotelsDestinationRepository;
use AppBundle\Repository\PriceSelectedHotelsRepository;
use AppBundle\Service\HotelsPro\Exception\SelectedHotelsPriceException;


class SelectedHotelsPrice
{
    /**
     * @var HotelsDestinationRepository
     */
    private $hotelsDestinationRepository;

    /**
     * @var PriceSelectedHotelsRepository
     */
    private $priceSelectedHotelsRepository;

    public function __construct(
        HotelsDestinationRepository $hotelsDestinationRepository,
        PriceSelectedHotelsRepository $priceSelectedHotelsRepository
    ) {
        $this->hotelsDestinationRepository = $hotelsDestinationRepository;
        $this->priceSelectedHotelsRepository = $priceSelectedHotelsRepository;
    }

    /**
     * @var array
     */
    private $cachedPriceData = [];

    /**
     * @param $cityId
     * @param $dateFrom
     * @param $hotelCode
     * @throws SelectedHotelsPriceException
     * @throws \RuntimeException
     * @return float
     */
    public function getSelectedHotelsRate($cityId, $dateFrom, $hotelCode)
    {
        $destination = $this->hotelsDestinationRepository->findOneByCityId($cityId);

        if (!$destination) {
            throw new SelectedHotelsPriceException('City not found. Check metadata');
        }

        /** @var PriceSelectedHotels $priceSelected */
        $priceSelected = $this->getSelectedPrice(
            $destination->getHotelsProId(),
            $dateFrom
        );
        $rate = null;
        if ($priceSelected !== null) {
            if (is_array($priceSelected->getSelectedHotels()) && count($priceSelected->getSelectedHotels())) {
                if (is_array($priceSelected->getSelectedMarkupPercent()) && count(
                        $priceSelected->getSelectedMarkupPercent()
                    )
                ) {
                    if (count($priceSelected->getSelectedHotels()) === count(
                            $priceSelected->getSelectedMarkupPercent()
                        )
                    ) {
                        $key = array_search($hotelCode, $priceSelected->getSelectedHotels(), true);
                        $rateArray = $priceSelected->getSelectedMarkupPercent();
                        $rate = $rateArray[$key];
                    } else {
                        throw new SelectedHotelsPriceException(
                            sprintf(
                                'Markup percent rate count !== selected hotels count for hotels code %s and city %s and date %s',
                                $hotelCode,
                                $cityId,
                                $dateFrom
                            )
                        );
                    }
                }
            }
        }

        if ($rate === null) {
            throw new SelectedHotelsPriceException(
                sprintf('Rate not found for hotels code %s and city %s and date %s', $hotelCode, $cityId, $dateFrom)
            );
        }

        return $rate;
    }

    /**
     * @param $cityHotelsProId
     * @param $dateFrom
     * @return PriceSelectedHotels
     */
    private function getSelectedPrice($cityHotelsProId, $dateFrom)
    {
        $key = $cityHotelsProId.$dateFrom;

        if (!array_key_exists($key, $this->cachedPriceData)) {
            $this->cachedPriceData[$key] = $this->priceSelectedHotelsRepository->findOneByCityCodeAndCheckInDate(
                $cityHotelsProId,
                $dateFrom
            );
        }

        return $this->cachedPriceData[$key];
    }
}
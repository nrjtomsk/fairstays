<?php

/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 06.04.16
 * Time: 15:32
 */
namespace AppBundle\Service\HotelsPro\Exception;

/**
 * Class SelectedHotelsPriceException
 * @package AppBundle\Service\Exception
 */
class SelectedHotelsPriceException extends \RuntimeException
{

}
<?php
/**
 * Created by PhpStorm.
 * User: yarovikovyo
 * Date: 21.12.2015
 * Time: 12:55
 */

namespace AppBundle\Repository;


use AppBundle\Entity\HotelsDestination;
use Doctrine\ORM\EntityRepository;

class HotelsDestinationRepository extends EntityRepository
{
    /**
     * @param $cityId
     * @return null|HotelsDestination
     */
    public function findOneByCityId($cityId)
    {
        return $this->findOneBy(['cityId' => $cityId]);
    }
}
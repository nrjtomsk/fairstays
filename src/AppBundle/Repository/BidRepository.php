<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.04.2016
 * Time: 17:23
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Bid;
use AppBundle\Entity\User;
use AppBundle\Repository\Exception\BidException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class BidRepository
 * @package AppBundle\Repository
 */
class BidRepository extends EntityRepository
{

    /**
     * @param Bid $bid
     * @throws \AppBundle\Repository\Exception\BidException
     */
    public function save(Bid $bid)
    {
        try {
            $this->_em->persist($bid);
            $this->_em->flush($bid);
        } catch (OptimisticLockException $e) {
            throw BidException::saveBidException($e);
        } catch (ORMInvalidArgumentException $e) {
            throw BidException::saveBidException($e);
        }
    }

    /**
     * @param User $user
     * @return integer
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getUserBidCountForLastDay(User $user)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('bids_count', 'bids_count', 'integer');
        $bidsCount = $this->_em->createNativeQuery(
            'SELECT 
                COUNT(*) as bids_count 
            FROM `bids` `b`
            WHERE 
                `b`.user_id = :userId
                AND `b`.created_at >= :pastDate',
            $rsm
        )->setParameters([
            'userId' => $user->getId(),
            'pastDate' => (new \DateTime('now - 1 day'))
        ])->getSingleScalarResult();

        return $bidsCount;
    }

    /**
     * @param User $user
     * @param $hotelCode
     * @param $serviceName
     * @return integer
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getBidSessionBidCountForLastDay(User $user, $hotelCode, $serviceName)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('bids_count', 'bids_count', 'integer');
        $bidsCount = $this->_em->createNativeQuery(
            'SELECT 
                COUNT(*) as bids_count 
            FROM `bids` `b`
            WHERE 
                `b`.user_id = :userId
                AND `b`.hotel_code = :hotelCode
                AND `b`.service_name = :serviceName
                AND `b`.created_at >= :pastDate',
            $rsm
        )->setParameters([
            'userId' => $user->getId(),
            'hotelCode' => $hotelCode,
            'serviceName' => $serviceName,
            'pastDate' => (new \DateTime('now - 1 day'))
        ])->getSingleScalarResult();

        return $bidsCount;
    }

    /**
     * @param $hotelCode
     * @param $serviceName
     * @return Bid
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getLastBidToHotelForLastDay($hotelCode, $serviceName)
    {
        $query = $this->_em->createQuery('
            SELECT b
            FROM AppBundle:Bid b
            WHERE b.createdAt >= :pastDate
              AND b.hotelCode = :hotelCode
              AND b.serviceName = :serviceName
            ORDER BY b.createdAt DESC
        ')->setMaxResults(1);

        $query->setParameters([
            'pastDate' => new \DateTime('now - 1 day'),
            'hotelCode' => $hotelCode,
            'serviceName' => $serviceName,
        ]);

        return $query->getSingleResult();
    }

    /**
     * @param User $currentUser
     * @param $hotelCode
     * @param $serviceName
     * @return Bid[]
     */
    public function findUserBidsToHotelForLastDay(User $currentUser, $hotelCode, $serviceName)
    {
        return $this->createQueryBuilder('b')
            ->where('b.hotelCode = :hotelCode')
            ->andWhere('b.serviceName = :serviceName')
            ->andWhere('b.createdAt > :pastDate')
            ->andWhere('b.user = :user')
            ->orderBy('b.createdAt', 'DESC')
            ->setParameters([
                'hotelCode' => $hotelCode,
                'serviceName' => $serviceName,
                'user' => $currentUser,
                'pastDate' => new \DateTime('now - 1 day')
            ])
            ->getQuery()
            ->getResult();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 11:11
 */

namespace AppBundle\Repository;


use AppBundle\Entity\File;
use Doctrine\ORM\EntityRepository;

class FileRepository extends EntityRepository
{
    /**
     * @param File $file
     */
    public function save(File $file)
    {
        $this->_em->persist($file);
        $this->_em->flush();
    }

    /**
     * @param File $file
     */
    public function remove(File $file)
    {
        $this->_em->remove($file);
        $this->_em->flush();
    }

}
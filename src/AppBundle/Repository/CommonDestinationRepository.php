<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 29.04.2016
 * Time: 13:03
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class CommonDestinationRepository
 * @package AppBundle\Repository
 */
class CommonDestinationRepository extends EntityRepository
{
    /**
     * @param $query
     * @param $limit
     * @param $offset
     * @return array
     */
    public function searchByQueryAsArray($query, $limit, $offset)
    {
        return $this->createQueryBuilder('d')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->where('d.cityName LIKE :query')
            ->orderBy('d.cityName', 'ASC')
            ->setParameter('query', '%'.$query.'%')
            ->getQuery()
            ->getArrayResult();
    }
}
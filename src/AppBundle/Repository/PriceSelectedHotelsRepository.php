<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 22.03.16
 * Time: 9:44
 */

namespace AppBundle\Repository;


use AppBundle\Entity\PriceSelectedHotels;
use Doctrine\ORM\EntityRepository;


class PriceSelectedHotelsRepository extends EntityRepository
{
    /**
     * @param $cityCode
     * @param $checkInDate
     * @return null|PriceSelectedHotels
     */
    public function findOneByCityCodeAndCheckInDate($cityCode, $checkInDate)
    {
        return $this->findOneBy(['cityCode' => $cityCode, 'checkInDate' => $checkInDate]);
    }

    public function deletePriceSelectedHotelsOlderThanTwoDays()
    {
        $date = new \DateTime('now', new \DateTimeZone('UTC'));
        $date->modify('-2 days');

        $query = $this->_em->createQuery(
            'DELETE FROM AppBundle:PriceSelectedHotels h WHERE h.insertTime <= :date'
        );
        $query->setParameter('date', $date->format('Y-m-d'));
        $query->execute();
    }
}
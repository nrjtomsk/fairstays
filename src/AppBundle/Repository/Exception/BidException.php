<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 16:58
 */

namespace AppBundle\Repository\Exception;

/**
 * Class BidException
 * @package AppBundle\Repository\Exception
 */
class BidException extends \RuntimeException
{
    /**
     * @param \Exception $prev
     * @return BidException
     */
    public static function saveBidException(\Exception $prev)
    {
        return new BidException(sprintf('Error on save bid'), $prev->getCode(), $prev);
    }
}
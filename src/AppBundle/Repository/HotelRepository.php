<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 07.10.2015
 * Time: 15:57
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Hotel;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

class HotelRepository extends EntityRepository
{

    /**
     * @param $id
     * @return mixed|null
     */
    public function findHotelById($id)
    {
        $query = $this->_em->createQuery(
            'SELECT h, im, rating FROM AppBundle:Hotel h LEFT JOIN h.images im LEFT JOIN h.tripAdvisorRating rating WHERE h.id = :id'
        );
        $query->setParameter('id', $id);

        try {
            $hotel = $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }

        return $hotel;

    }
    /**
     * @param array $ids
     * @return array
     */
    public function findAllByIdsAsArray(array $ids)
    {
        $idss = array_chunk($ids, 666);
        $hotels = [];
        $query = $this->_em->createQuery(
            'SELECT h, im, rating FROM AppBundle:Hotel h LEFT JOIN h.images im LEFT JOIN h.tripAdvisorRating rating WHERE h.id IN (:ids)'
        );
        foreach ($idss as $ids) {
            $hotels = array_merge($hotels, $query->setParameter('ids', $ids)->getArrayResult());
        }

        return $hotels;
    }

}
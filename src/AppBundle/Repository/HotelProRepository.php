<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 15.12.2015
 * Time: 12:04
 */

namespace AppBundle\Repository;


use AppBundle\Entity\HotelPro;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class HotelProRepository extends EntityRepository
{
    /**
     * @param $code
     * @return null|HotelPro
     */
    public function findHotelByHotelCode($code)
    {
        $query = $this->_em->createQuery(
            'SELECT h, rating FROM AppBundle:HotelPro h LEFT JOIN h.tripAdvisorRating as rating WHERE h.hotelCode = :code'
        );
        $query->setParameter('code', $code);
        try {
            $hotel = $query->getSingleResult();
        } catch (NonUniqueResultException $e) {
            return null;
        } catch (NoResultException $e) {
            return null;
        }

        return $hotel;
    }

    /**
     * @param array $codes
     * @return array
     */
    public function findAllByHotelCodesAsArray(array $codes)
    {
        $idss = array_chunk($codes, 666);
        $hotels = [];
        $query = $this->_em->createQuery('SELECT h, rating FROM AppBundle:HotelPro h LEFT JOIN h.tripAdvisorRating as rating WHERE h.hotelCode IN (:ids)');
        foreach ($idss as $ids) {
            $hotels = array_merge($hotels, $query->setParameter('ids', $ids)->getArrayResult());
        }

        return $hotels;
    }
}
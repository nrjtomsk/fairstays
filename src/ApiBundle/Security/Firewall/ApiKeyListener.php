<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 16:46
 */

namespace ApiBundle\Security\Firewall;

use ApiBundle\Extractor\KeyExtractor;
use ApiBundle\Security\Authentication\Token\ApiKeyUserToken;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * Class ApiKeyListener
 * @package ApiBundle\Security\Firewall
 */
class ApiKeyListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;
    /**
     * @var KeyExtractor
     */
    protected $keyExtractor;

    /**
     * @inheritdoc
     * ApiKeyListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $manager
     * @param KeyExtractor $keyExtractor
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $manager,
        KeyExtractor $keyExtractor
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $manager;
        $this->keyExtractor = $keyExtractor;
    }

    /**
     * This interface must be implemented by firewall listeners.
     *
     * @param GetResponseEvent $event
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if ($this->keyExtractor->hasKey($request)) {
            $apiKey = $this->keyExtractor->extractKey($request);
            $token = new ApiKeyUserToken();
            $token->setApiKey($apiKey);

            try {
                $authToken = $this->authenticationManager->authenticate($token);
                $this->tokenStorage->setToken($authToken);

                return;
            } catch (AuthenticationException $failed) {
                $token = $this->tokenStorage->getToken();
                if ($token instanceof ApiKeyUserToken && $token->getCredentials() === $apiKey) {
                    $this->tokenStorage->setToken(null);
                }
            }
        }
    }
}
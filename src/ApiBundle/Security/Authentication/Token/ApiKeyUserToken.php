<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 16:47
 */

namespace ApiBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * Class ApiKeyUserToken
 * @package ApiBundle\Security\Authentication\Token
 */
class ApiKeyUserToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * ApiKeyUserToken constructor.
     * @param array $roles
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * Returns the user credentials.
     *
     * @return mixed The user credentials
     */
    public function getCredentials()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
}
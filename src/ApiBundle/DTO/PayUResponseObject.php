<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 15:18
 */

namespace ApiBundle\DTO;


use Symfony\Component\HttpFoundation\Request;

/**
 * Class PayUResponseObject
 * @package ApiBundle\DTO
 */
class PayUResponseObject
{

    /**
     * PayU transaction ID
     *
     * @var string
     */
    private $mihPayId = '';

    /**
     * Payment category
     *
     * @var string
     */
    private $mode = '';

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var string
     */
    private $unmappedStatus = '';

    /**
     * @var string
     */
    private $key = '';

    /**
     * @var string
     */
    private $txnid = '';

    /**
     * @var float
     */
    private $amount = 0;

    /**
     * @var string
     */
    private $cardCategory = '';

    /**
     * @var float
     */
    private $discount = 0;

    /**
     * @var float
     */
    private $netAmountDebit = 0;

    /**
     * @var string
     */
    private $addedOn = '';

    /**
     * @var string
     */
    private $productInfo = '';

    /**
     * @var string
     */
    private $firstName = '';

    /**
     * @var string
     */
    private $lastName = '';

    /**
     * @var string
     */
    private $address1 = '';

    /**
     * @var string
     */
    private $address2 = '';

    /**
     * @var string
     */
    private $city = '';

    /**
     * @var string
     */
    private $state = '';

    /**
     * @var string
     */
    private $country = '';

    /**
     * @var string
     */
    private $zipcode = '';

    /**
     * @var string
     */
    private $email = '';

    /**
     * @var string
     */
    private $phone = '';

    /**
     * @var string
     */
    private $udf1 = '';

    /**
     * @var string
     */
    private $udf2 = '';

    /**
     * @var string
     */
    private $udf3 = '';

    /**
     * @var string
     */
    private $udf4 = '';

    /**
     * @var string
     */
    private $udf5 = '';

    /**
     * @var string
     */
    private $udf6 = '';

    /**
     * @var string
     */
    private $udf7 = '';

    /**
     * @var string
     */
    private $udf8 = '';

    /**
     * @var string
     */
    private $udf9 = '';

    /**
     * @var string
     */
    private $udf10 = '';

    /**
     * @var string
     */
    private $hash = '';

    /**
     * @var string
     */
    private $field1 = '';

    /**
     * @var string
     */
    private $field2 = '';

    /**
     * @var string
     */
    private $field3 = '';

    /**
     * @var string
     */
    private $field4 = '';

    /**
     * @var string
     */
    private $field5 = '';

    /**
     * @var string
     */
    private $field6 = '';

    /**
     * @var string
     */
    private $field7 = '';

    /**
     * @var string
     */
    private $field8 = '';

    /**
     * @var string
     */
    private $field9 = '';

    /**
     * @var string
     */
    private $payment_source = '';

    /**
     * @var string
     */
    private $pgType = '';

    /**
     * @var string
     */
    private $bankRefNum = '';

    /**
     * @var string
     */
    private $bankcode = '';

    /**
     * @var string
     */
    private $error = '';

    /**
     * @var string
     */
    private $errorMessage = '';

    /**
     * @var string
     */
    private $nameOnCard = '';

    /**
     * @var string
     */
    private $cardNum = '';

    /**
     * @var string
     */
    private $issuingBank = '';

    /**
     * @var string
     */
    private $cardType = '';

    /**
     * @return string
     */
    public function getMihPayId()
    {
        return $this->mihPayId;
    }

    /**
     * @param string $mihPayId
     */
    public function setMihPayId($mihPayId)
    {
        $this->mihPayId = $mihPayId;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getUnmappedStatus()
    {
        return $this->unmappedStatus;
    }

    /**
     * @param string $unmappedStatus
     */
    public function setUnmappedStatus($unmappedStatus)
    {
        $this->unmappedStatus = $unmappedStatus;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getTxnid()
    {
        return $this->txnid;
    }

    /**
     * @param string $txnid
     */
    public function setTxnid($txnid)
    {
        $this->txnid = $txnid;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getNetAmountDebit()
    {
        return $this->netAmountDebit;
    }

    /**
     * @param float $netAmountDebit
     */
    public function setNetAmountDebit($netAmountDebit)
    {
        $this->netAmountDebit = $netAmountDebit;
    }

    /**
     * @return string
     */
    public function getAddedOn()
    {
        return $this->addedOn;
    }

    /**
     * @param string $addedOn
     */
    public function setAddedOn($addedOn)
    {
        $this->addedOn = $addedOn;
    }

    /**
     * @return string
     */
    public function getProductInfo()
    {
        return $this->productInfo;
    }

    /**
     * @param string $productInfo
     */
    public function setProductInfo($productInfo)
    {
        $this->productInfo = $productInfo;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getUdf1()
    {
        return $this->udf1;
    }

    /**
     * @param string $udf1
     */
    public function setUdf1($udf1)
    {
        $this->udf1 = $udf1;
    }

    /**
     * @return string
     */
    public function getUdf2()
    {
        return $this->udf2;
    }

    /**
     * @param string $udf2
     */
    public function setUdf2($udf2)
    {
        $this->udf2 = $udf2;
    }

    /**
     * @return string
     */
    public function getUdf3()
    {
        return $this->udf3;
    }

    /**
     * @param string $udf3
     */
    public function setUdf3($udf3)
    {
        $this->udf3 = $udf3;
    }

    /**
     * @return string
     */
    public function getUdf4()
    {
        return $this->udf4;
    }

    /**
     * @param string $udf4
     */
    public function setUdf4($udf4)
    {
        $this->udf4 = $udf4;
    }

    /**
     * @return string
     */
    public function getUdf5()
    {
        return $this->udf5;
    }

    /**
     * @param string $udf5
     */
    public function setUdf5($udf5)
    {
        $this->udf5 = $udf5;
    }

    /**
     * @return string
     */
    public function getUdf6()
    {
        return $this->udf6;
    }

    /**
     * @param string $udf6
     */
    public function setUdf6($udf6)
    {
        $this->udf6 = $udf6;
    }

    /**
     * @return string
     */
    public function getUdf7()
    {
        return $this->udf7;
    }

    /**
     * @param string $udf7
     */
    public function setUdf7($udf7)
    {
        $this->udf7 = $udf7;
    }

    /**
     * @return string
     */
    public function getUdf8()
    {
        return $this->udf8;
    }

    /**
     * @param string $udf8
     */
    public function setUdf8($udf8)
    {
        $this->udf8 = $udf8;
    }

    /**
     * @return string
     */
    public function getUdf9()
    {
        return $this->udf9;
    }

    /**
     * @param string $udf9
     */
    public function setUdf9($udf9)
    {
        $this->udf9 = $udf9;
    }

    /**
     * @return string
     */
    public function getUdf10()
    {
        return $this->udf10;
    }

    /**
     * @param string $udf10
     */
    public function setUdf10($udf10)
    {
        $this->udf10 = $udf10;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getField1()
    {
        return $this->field1;
    }

    /**
     * @param string $field1
     */
    public function setField1($field1)
    {
        $this->field1 = $field1;
    }

    /**
     * @return string
     */
    public function getField2()
    {
        return $this->field2;
    }

    /**
     * @param string $field2
     */
    public function setField2($field2)
    {
        $this->field2 = $field2;
    }

    /**
     * @return string
     */
    public function getField3()
    {
        return $this->field3;
    }

    /**
     * @param string $field3
     */
    public function setField3($field3)
    {
        $this->field3 = $field3;
    }

    /**
     * @return string
     */
    public function getField4()
    {
        return $this->field4;
    }

    /**
     * @param string $field4
     */
    public function setField4($field4)
    {
        $this->field4 = $field4;
    }

    /**
     * @return string
     */
    public function getField5()
    {
        return $this->field5;
    }

    /**
     * @param string $field5
     */
    public function setField5($field5)
    {
        $this->field5 = $field5;
    }

    /**
     * @return string
     */
    public function getField6()
    {
        return $this->field6;
    }

    /**
     * @param string $field6
     */
    public function setField6($field6)
    {
        $this->field6 = $field6;
    }

    /**
     * @return string
     */
    public function getField7()
    {
        return $this->field7;
    }

    /**
     * @param string $field7
     */
    public function setField7($field7)
    {
        $this->field7 = $field7;
    }

    /**
     * @return string
     */
    public function getField8()
    {
        return $this->field8;
    }

    /**
     * @param string $field8
     */
    public function setField8($field8)
    {
        $this->field8 = $field8;
    }

    /**
     * @return string
     */
    public function getField9()
    {
        return $this->field9;
    }

    /**
     * @param string $field9
     */
    public function setField9($field9)
    {
        $this->field9 = $field9;
    }

    /**
     * @return string
     */
    public function getPaymentSource()
    {
        return $this->payment_source;
    }

    /**
     * @param string $payment_source
     */
    public function setPaymentSource($payment_source)
    {
        $this->payment_source = $payment_source;
    }

    /**
     * @return string
     */
    public function getPgType()
    {
        return $this->pgType;
    }

    /**
     * @param string $pgType
     */
    public function setPgType($pgType)
    {
        $this->pgType = $pgType;
    }

    /**
     * @return string
     */
    public function getBankRefNum()
    {
        return $this->bankRefNum;
    }

    /**
     * @param string $bankRefNum
     */
    public function setBankRefNum($bankRefNum)
    {
        $this->bankRefNum = $bankRefNum;
    }

    /**
     * @return string
     */
    public function getBankcode()
    {
        return $this->bankcode;
    }

    /**
     * @param string $bankcode
     */
    public function setBankcode($bankcode)
    {
        $this->bankcode = $bankcode;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return string
     */
    public function getCardCategory()
    {
        return $this->cardCategory;
    }

    /**
     * @param string $cardCategory
     */
    public function setCardCategory($cardCategory)
    {
        $this->cardCategory = $cardCategory;
    }

    /**
     * @return string
     */
    public function getNameOnCard()
    {
        return $this->nameOnCard;
    }

    /**
     * @param string $nameOnCard
     */
    public function setNameOnCard($nameOnCard)
    {
        $this->nameOnCard = $nameOnCard;
    }

    /**
     * @return string
     */
    public function getCardNum()
    {
        return $this->cardNum;
    }

    /**
     * @param string $cardNum
     */
    public function setCardNum($cardNum)
    {
        $this->cardNum = $cardNum;
    }

    /**
     * @return string
     */
    public function getIssuingBank()
    {
        return $this->issuingBank;
    }

    /**
     * @param string $issuingBank
     */
    public function setIssuingBank($issuingBank)
    {
        $this->issuingBank = $issuingBank;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return string
     */
    public function getBookingRequestId()
    {
        $transactionId = $this->getTxnid();
        $transactionIdParts = explode('-', $transactionId);

        return $transactionIdParts[0];
    }

    /**
     * @param string $salt
     * @return bool
     */
    public function isValidHash($salt)
    {
        $hashedFields = [
            $salt,
            $this->getStatus(),
            '', // blank element 1 like in payU docs
            '', // blank element 2 like in payU docs
            '', // blank element 3 like in payU docs
            '', // blank element 4 like in payU docs
            '', // blank element 5 like in payU docs
            $this->getUdf5(),
            $this->getUdf4(),
            $this->getUdf3(),
            $this->getUdf2(),
            $this->getUdf1(),
            $this->getEmail(),
            $this->getFirstName(),
            $this->getProductInfo(),
            (string)$this->getAmount(),
            $this->getTxnid(),
            $this->getKey(),
        ];

        $hashedString = implode('|', $hashedFields);
        $hash = hash('sha512', $hashedString);

        return $hash === $this->getHash();
    }

    /**
     * @param Request $request
     * @return PayUResponseObject
     */
    public static function fromPostRequest(Request $request)
    {
        $post = $request->request;

        $responseObject = new self;
        $responseObject->setMihPayId($post->get('mihpayid', ''));
        $responseObject->setMode($post->get('mode', ''));
        $responseObject->setStatus($post->get('status', ''));
        $responseObject->setUnmappedStatus($post->get('unmappedstatus', ''));
        $responseObject->setKey($post->get('key', ''));
        $responseObject->setTxnid($post->get('txnid', ''));
        $responseObject->setAmount($post->get('amount', 0));
        $responseObject->setCardCategory($post->get('cardCategory', ''));
        $responseObject->setDiscount($post->get('discount', 0));
        $responseObject->setNetAmountDebit($post->get('net_amount_debit', 0));
        $responseObject->setAddedOn($post->get('addedon', ''));
        $responseObject->setProductInfo($post->get('productinfo', ''));
        $responseObject->setFirstName($post->get('firstname', ''));
        $responseObject->setLastName($post->get('lastname', ''));
        $responseObject->setAddress1($post->get('address1', ''));
        $responseObject->setAddress2($post->get('address2', ''));
        $responseObject->setCity($post->get('city', ''));
        $responseObject->setState($post->get('state', ''));
        $responseObject->setCountry($post->get('country', ''));
        $responseObject->setZipcode($post->get('zipcode', ''));
        $responseObject->setEmail($post->get('email', ''));
        $responseObject->setPhone($post->get('phone', ''));
        $responseObject->setUdf1($post->get('udf1', ''));
        $responseObject->setUdf2($post->get('udf2', ''));
        $responseObject->setUdf3($post->get('udf3', ''));
        $responseObject->setUdf4($post->get('udf4', ''));
        $responseObject->setUdf5($post->get('udf5', ''));
        $responseObject->setUdf6($post->get('udf6', ''));
        $responseObject->setUdf7($post->get('udf7', ''));
        $responseObject->setUdf8($post->get('udf8', ''));
        $responseObject->setUdf9($post->get('udf9', ''));
        $responseObject->setUdf10($post->get('udf10', ''));
        $responseObject->setHash($post->get('hash', ''));
        $responseObject->setField1($post->get('field1', ''));
        $responseObject->setField2($post->get('field2', ''));
        $responseObject->setField3($post->get('field3', ''));
        $responseObject->setField4($post->get('field4', ''));
        $responseObject->setField5($post->get('field5', ''));
        $responseObject->setField6($post->get('field6', ''));
        $responseObject->setField7($post->get('field7', ''));
        $responseObject->setField8($post->get('field8', ''));
        $responseObject->setField9($post->get('field9', ''));
        $responseObject->setPaymentSource($post->get('payment_source', ''));
        $responseObject->setPgType($post->get('PG_TYPE', ''));
        $responseObject->setBankRefNum($post->get('bank_ref_num', ''));
        $responseObject->setBankcode($post->get('bankcode', ''));
        $responseObject->setError($post->get('error', ''));
        $responseObject->setErrorMessage($post->get('error_Message', ''));
        $responseObject->setNameOnCard($post->get('name_on_card', ''));
        $responseObject->setCardNum($post->get('cardnum', ''));
        $responseObject->setIssuingBank($post->get('issuing_bank', ''));
        $responseObject->setCardType($post->get('card_type', ''));

        return $responseObject;
    }

}
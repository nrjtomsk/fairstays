<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 10:36
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\CurrencyInterface;

/**
 * Class Currency
 * @package ApiBundle\DTO
 */
class Currency
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * Currency constructor.
     * @param string $code
     * @param string $name
     */
    public function __construct($code = 'USD', $name = 'United States Dollar')
    {
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @param CurrencyInterface|null $currency
     * @return Currency
     */
    public static function fromCurrency(CurrencyInterface $currency = null)
    {
        if ($currency) {
            return new Currency(
                $currency->getCode(),
                $currency->getName()
            );
        } else {
            return new Currency();
        }
    }

}
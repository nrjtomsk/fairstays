<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.09.2015
 * Time: 16:33
 */

namespace ApiBundle\DTO;

/**
 * Class CityAndPrice
 * @package ApiBundle\DTO
 */
class CityAndPrice
{
    /**
     * @var array
     */
    public $city;

    /**
     * @var float
     */
    public $minPrice;

    /**
     * CityAndPrice constructor.
     * @param array $city
     * @param float $minPrice
     */
    public function __construct($city, $minPrice)
    {
        $this->city = $city;
        $this->minPrice = $minPrice;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 10:35
 */

namespace ApiBundle\DTO;

/**
 * Class Avatar
 * @package ApiBundle\DTO
 */
class Avatar
{
    /**
     * @var string
     */
    public $url;

    /**
     * Avatar constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @param $url
     * @return Avatar
     */
    public static function fromUrl($url)
    {
        return new Avatar($url);
    }

}
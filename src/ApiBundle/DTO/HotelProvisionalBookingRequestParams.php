<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 09.10.2015
 * Time: 14:19
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\BookingRequest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class HotelProvisionalBookingRequestParams
 * @package ApiBundle\DTO
 */
class HotelProvisionalBookingRequestParams
{
    const CURRENCY_INR = 'INR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_GBP = 'GBP';

    const GUARANTEE_TYPE_PRE_PAY = 'PrePay';
    const GUARANTEE_TYPE_PAY_AT_HOTEL = 'PayAt Hotel';

    /**
     * @var string
     */
    public $requestedCurrency;

    /**
     * @var string
     */
    public $timeSpanStartTime;

    /**
     * @var string
     */
    public $timeSpanEndDate;

    /**
     * @var string
     */
    public $hotelCode;

    /**
     * @var int
     */
    public $roomTypeNumberOfUnits;

    /**
     * @var string
     */
    public $roomTypeRoomTypeCode;

    /**
     * @var string
     */
    public $ratePlanRatePlanCode;

    /**
     * @var float
     */
    public $totalAmountBeforeTax;

    /**
     * @var float
     */
    public $totalTaxesAmount;

    /**
     * @var string
     */
    public $commentText;

    /**
     * @var string
     */
    public $profileNamePrefix;

    /**
     * @var string
     */
    public $profileGivenName;

    /**
     * @var string
     */
    public $profileMiddleName;

    /**
     * @var string
     */
    public $profileLastName;

    /**
     * @var string
     */
    public $profilePhoneAreaCityCode;

    /**
     * @var string
     */
    public $profilePhoneCountryAccessCode;

    /**
     * @var string
     */
    public $profilePhoneExtension;

    /**
     * @var string
     */
    public $profilePhoneNumber;

    /**
     * @var string
     */
    public $profilePhoneTechType;

    /**
     * @var string
     */
    public $profileEmail;

    /**
     * @var string
     */
    public $profileAddressLine1;

    /**
     * @var string
     */
    public $profileAddressLine2;

    /**
     * @var string
     */
    public $profileCityName;

    /**
     * @var string
     */
    public $profilePostalCode;

    /**
     * @var string
     */
    public $profileAddressStateProv;

    /**
     * @var string
     */
    public $profileCountryName;

    /**
     * @var string
     */
    public $guaranteeType;

    /**
     * @var string
     */
    public $correlationId;

    /**
     * @var string
     */
    public $transactionIdentifier;

    /**
     * @var array
     */
    public $guestCounts;

    /**
     * @var string
     */
    public $profileAddressStateProvCode;

    /**
     * @var string
     */
    public $profileCountryNameCode;

    /**
     * @var string
     */
    public $roomName;

    /**
     * @var float
     */
    public $roomPricePerNight;

    /**
     * @var float
     */
    public $guestCharges;

    /**
     * HotelProvisionalBookingRequestParams constructor.
     * @param string $requestedCurrency
     * @param string $timeSpanStartTime
     * @param string $timeSpanEndDate
     * @param string $hotelCode
     * @param int $roomTypeNumberOfUnits
     * @param string $roomTypeRoomTypeCode
     * @param string $ratePlanRatePlanCode
     * @param float $totalAmountBeforeTax
     * @param float $totalTaxesAmount
     * @param string $commentText
     * @param string $profileNamePrefix
     * @param string $profileGivenName
     * @param string $profileMiddleName
     * @param string $profileLastName
     * @param string $profilePhoneAreaCityCode
     * @param string $profilePhoneCountryAccessCode
     * @param string $profilePhoneExtension
     * @param string $profilePhoneNumber
     * @param string $profilePhoneTechType
     * @param string $profileEmail
     * @param string $profileAddressLine1
     * @param string $profileAddressLine2
     * @param string $profileCityName
     * @param string $profilePostalCode
     * @param string $profileAddressStateProv
     * @param string $profileCountryName
     * @param string $guaranteeType
     * @param string $correlationId
     * @param string $transactionIdentifier
     * @param array $guestCounts
     * @param null $profileAddressStateProvCode
     * @param null $profileCountryNameCode
     */
    public function __construct(
        $requestedCurrency = null,
        $timeSpanStartTime = null,
        $timeSpanEndDate = null,
        $hotelCode = null,
        $roomTypeNumberOfUnits = null,
        $roomTypeRoomTypeCode = null,
        $ratePlanRatePlanCode = null,
        $totalAmountBeforeTax = null,
        $totalTaxesAmount = null,
        $commentText = null,
        $profileNamePrefix = null,
        $profileGivenName = null,
        $profileMiddleName = null,
        $profileLastName = null,
        $profilePhoneAreaCityCode = null,
        $profilePhoneCountryAccessCode = null,
        $profilePhoneExtension = null,
        $profilePhoneNumber = null,
        $profilePhoneTechType = null,
        $profileEmail = null,
        $profileAddressLine1 = null,
        $profileAddressLine2 = null,
        $profileCityName = null,
        $profilePostalCode = null,
        $profileAddressStateProv = null,
        $profileCountryName = null,
        $guaranteeType = null,
        $correlationId = null,
        $transactionIdentifier = null,
        array $guestCounts = null,
        $profileAddressStateProvCode = null,
        $profileCountryNameCode = null,
        $roomName = null,
        $roomPricePerNight = null,
        $guestCharges = null
    ) {
        $this->requestedCurrency = $requestedCurrency;
        $this->timeSpanStartTime = $timeSpanStartTime;
        $this->timeSpanEndDate = $timeSpanEndDate;
        $this->hotelCode = $hotelCode;
        $this->roomTypeNumberOfUnits = $roomTypeNumberOfUnits;
        $this->roomTypeRoomTypeCode = $roomTypeRoomTypeCode;
        $this->ratePlanRatePlanCode = $ratePlanRatePlanCode;
        $this->totalAmountBeforeTax = $totalAmountBeforeTax;
        $this->totalTaxesAmount = $totalTaxesAmount;
        $this->commentText = $commentText;
        $this->profileNamePrefix = $profileNamePrefix;
        $this->profileGivenName = $profileGivenName;
        $this->profileMiddleName = $profileMiddleName;
        $this->profileLastName = $profileLastName;
        $this->profilePhoneAreaCityCode = $profilePhoneAreaCityCode;
        $this->profilePhoneCountryAccessCode = $profilePhoneCountryAccessCode;
        $this->profilePhoneExtension = $profilePhoneExtension;
        $this->profilePhoneNumber = $profilePhoneNumber;
        $this->profilePhoneTechType = $profilePhoneTechType;
        $this->profileEmail = $profileEmail;
        $this->profileAddressLine1 = $profileAddressLine1;
        $this->profileAddressLine2 = $profileAddressLine2;
        $this->profileCityName = $profileCityName;
        $this->profilePostalCode = $profilePostalCode;
        $this->profileAddressStateProv = $profileAddressStateProv;
        $this->profileCountryName = $profileCountryName;
        $this->guaranteeType = $guaranteeType;
        $this->correlationId = $correlationId;
        $this->transactionIdentifier = $transactionIdentifier;
        $this->guestCounts = $guestCounts;
        $this->profileAddressStateProvCode = $profileAddressStateProvCode;
        $this->profileCountryNameCode = $profileCountryNameCode;
        $this->roomName = $roomName;
        $this->roomPricePerNight = $roomPricePerNight;
        $this->guestCharges = $guestCharges;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @return HotelProvisionalBookingRequestParams
     * @throws \RuntimeException
     */
    public static function fromParamFetcher(ParamFetcherInterface $fetcher)
    {
        $guestCounts = [];
        $questCounts = $fetcher->get('guestCounts', $strict = true);

        if (is_array($questCounts)) {
            $decoder = new JsonEncoder();
            try {
                $guestCounts = $decoder->decode('['.implode(',', $questCounts).']', JsonEncoder::FORMAT);
            } catch (\Exception $e) {
                throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
            }
        }

        $strict = true;

        return new static(
            $fetcher->get('requestedCurrency', $strict),
            $fetcher->get('timeSpanStartDate', $strict),
            $fetcher->get('timeSpanEndDate', $strict),
            $fetcher->get('hotelCode', $strict),
            $fetcher->get('roomTypeNumberOfUnits', $strict),
            $fetcher->get('roomTypeRoomTypeCode', $strict),
            $fetcher->get('ratePlanRatePlanCode', $strict),
            $fetcher->get('totalAmountBeforeTax', $strict),
            $fetcher->get('totalTaxesAmount', $strict),
            $fetcher->get('commentText', $strict),
            $fetcher->get('profileNamePrefix', $strict),
            $fetcher->get('profileGivenName', $strict),
            $fetcher->get('profileMiddleName', $strict),
            $fetcher->get('profileLastName', $strict),
            $fetcher->get('profilePhoneAreaCityCode', $strict),
            $fetcher->get('profilePhoneCountryAccessCode', $strict),
            $fetcher->get('profilePhoneExtension', $strict),
            $fetcher->get('profilePhoneNumber', $strict),
            $fetcher->get('profilePhoneTechType', $strict),
            $fetcher->get('profileEmail', $strict),
            $fetcher->get('profileAddressLine1', $strict),
            $fetcher->get('profileAddressLine2', $strict),
            $fetcher->get('profileCityName', $strict),
            $fetcher->get('profilePostalCode', $strict),
            $fetcher->get('profileAddressStateProv', $strict),
            $fetcher->get('profileCountryName', $strict),
            $fetcher->get('guaranteeType', $strict),
            $correlationId = null,
            $transactionIdentifier = null,
            $guestCounts,
            $fetcher->get('profileAddressStateProvCode', $strict),
            $fetcher->get('profileCountryNameCode', $strict),
            $fetcher->get('roomName', $strict),
            $fetcher->get('roomPricePerNight', $strict),
            $fetcher->get('guestCharges', $strict)
        );
    }

    /**
     * @param BookingRequest $booking
     * @return HotelProvisionalBookingRequestParams
     */
    public static function fromBooking(BookingRequest $booking)
    {
        $params = new static();
        $params->requestedCurrency = static::CURRENCY_INR;
        $params->timeSpanStartTime = $booking->getTimeSpanStartDate()->format('Y-m-d');
        $params->timeSpanEndDate = $booking->getTimeSpanEndDate()->format('Y-m-d');
        $params->hotelCode = $booking->getHotelCode();
        $params->roomTypeNumberOfUnits = $booking->getRoomTypeNumberOfUnits();
        $params->roomTypeRoomTypeCode = $booking->getRoomTypeCode();
        $params->ratePlanRatePlanCode = $booking->getRatePlanCode();
        $params->totalAmountBeforeTax = $booking->getPurePriceServiceProvider()->getAmount();
        $params->totalTaxesAmount = (float)$booking->getPureTaxServiceProvider()->getAmount();
        $params->commentText = $booking->getCommentText();
        $params->profileNamePrefix = $booking->getProfileNamePrefix();
        $params->profileGivenName = $booking->getProfileGivenName();
        $params->profileMiddleName = '';
        $params->profileLastName = $booking->getProfileLastName();
        $params->profilePhoneAreaCityCode = $booking->getProfilePhoneAreaCityCode();
        $params->profilePhoneCountryAccessCode = $booking->getProfilePhoneCountryAccessCode();
        $params->profilePhoneExtension = $booking->getProfilePhoneExtension();
        $params->profilePhoneNumber = $booking->getProfilePhoneNumber();
        $params->profilePhoneTechType = $booking->getProfilePhoneTechType();
        $params->profileEmail = $booking->getProfileEmail();
        $params->profileAddressLine1 = $booking->getProfileAddressLine1();
        $params->profileAddressLine2 = $booking->getProfileAddressLine2();
        $params->profileCityName = $booking->getProfileCityName();
        $params->profilePostalCode = $booking->getProfilePostalCode();
        $params->profileAddressStateProv = $booking->getProfileAddressStateProv();
        $params->guaranteeType = static::GUARANTEE_TYPE_PRE_PAY;
        $params->correlationId = $booking->getCorrelationId();
        $params->transactionIdentifier = $booking->getTransactionIdentifier();
        $params->profileAddressStateProvCode = $booking->getProfileAddressStateProvCode();
        $params->profileCountryName = 'India';
        $params->profileCountryNameCode = 'IN';
        $params->roomName = $booking->getRoomName();

        $params->guestCounts = [];
        $guestCounts = $booking->getGuestCounts();
        foreach ($guestCounts as $index => $room) {

            $adultCount = 0;
            $childrenCount = 0;
            $childrenAges = [];
            foreach ($room['paxes'] as $pax) {

                if ($pax['type'] === 'Adult') {
                    $adultCount++;
                } elseif ($pax['type'] === 'Child') {
                    $childrenCount++;
                    $childrenAges[] = $pax['age'];
                }
            }

            $params->guestCounts[] = [
                'resGuestRPH' => $index,
                'ageQualifyingCode' => 10,
                'count' => $adultCount,
            ];
            if ($childrenCount > 0 && $childrenCount === count($childrenAges)) {
                $params->guestCounts[] = [
                    'resGuestRPH' => $index,
                    'ageQualifyingCode' => 8,
                    'count' => $childrenCount,
                    'age' => $childrenAges,
                ];
            }
        }

        return $params;
    }

    /**
     * @param string $correlationId
     * @return HotelProvisionalBookingRequestParams
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;

        return $this;
    }

    /**
     * @param string $transactionIdentifier
     * @return HotelProvisionalBookingRequestParams
     */
    public function setTransactionIdentifier($transactionIdentifier)
    {
        $this->transactionIdentifier = $transactionIdentifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestedCurrency()
    {
        return $this->requestedCurrency;
    }

    /**
     * @param string $requestedCurrency
     * @return HotelProvisionalBookingRequestParams
     */
    public function setRequestedCurrency($requestedCurrency)
    {
        $this->requestedCurrency = $requestedCurrency;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSpanStartTime()
    {
        return $this->timeSpanStartTime;
    }

    /**
     * @param string $timeSpanStartTime
     * @return HotelProvisionalBookingRequestParams
     */
    public function setTimeSpanStartTime($timeSpanStartTime)
    {
        $this->timeSpanStartTime = $timeSpanStartTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeSpanEndDate()
    {
        return $this->timeSpanEndDate;
    }

    /**
     * @param string $timeSpanEndDate
     * @return HotelProvisionalBookingRequestParams
     */
    public function setTimeSpanEndDate($timeSpanEndDate)
    {
        $this->timeSpanEndDate = $timeSpanEndDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getHotelCode()
    {
        return $this->hotelCode;
    }

    /**
     * @param string $hotelCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setHotelCode($hotelCode)
    {
        $this->hotelCode = $hotelCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoomTypeNumberOfUnits()
    {
        return $this->roomTypeNumberOfUnits;
    }

    /**
     * @param int $roomTypeNumberOfUnits
     * @return HotelProvisionalBookingRequestParams
     */
    public function setRoomTypeNumberOfUnits($roomTypeNumberOfUnits)
    {
        $this->roomTypeNumberOfUnits = $roomTypeNumberOfUnits;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoomTypeRoomTypeCode()
    {
        return $this->roomTypeRoomTypeCode;
    }

    /**
     * @param string $roomTypeRoomTypeCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setRoomTypeRoomTypeCode($roomTypeRoomTypeCode)
    {
        $this->roomTypeRoomTypeCode = $roomTypeRoomTypeCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getRatePlanRatePlanCode()
    {
        return $this->ratePlanRatePlanCode;
    }

    /**
     * @param string $ratePlanRatePlanCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setRatePlanRatePlanCode($ratePlanRatePlanCode)
    {
        $this->ratePlanRatePlanCode = $ratePlanRatePlanCode;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmountBeforeTax()
    {
        return $this->totalAmountBeforeTax;
    }

    /**
     * @param float $totalAmountBeforeTax
     * @return HotelProvisionalBookingRequestParams
     */
    public function setTotalAmountBeforeTax($totalAmountBeforeTax)
    {
        $this->totalAmountBeforeTax = $totalAmountBeforeTax;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalTaxesAmount()
    {
        return $this->totalTaxesAmount;
    }

    /**
     * @param float $totalTaxesAmount
     * @return HotelProvisionalBookingRequestParams
     */
    public function setTotalTaxesAmount($totalTaxesAmount)
    {
        $this->totalTaxesAmount = $totalTaxesAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommentText()
    {
        return $this->commentText;
    }

    /**
     * @param string $commentText
     * @return HotelProvisionalBookingRequestParams
     */
    public function setCommentText($commentText)
    {
        $this->commentText = $commentText;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileNamePrefix()
    {
        return $this->profileNamePrefix;
    }

    /**
     * @param string $profileNamePrefix
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileNamePrefix($profileNamePrefix)
    {
        $this->profileNamePrefix = $profileNamePrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileGivenName()
    {
        return $this->profileGivenName;
    }

    /**
     * @param string $profileGivenName
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileGivenName($profileGivenName)
    {
        $this->profileGivenName = $profileGivenName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileMiddleName()
    {
        return $this->profileMiddleName;
    }

    /**
     * @param string $profileMiddleName
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileMiddleName($profileMiddleName)
    {
        $this->profileMiddleName = $profileMiddleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileLastName()
    {
        return $this->profileLastName;
    }

    /**
     * @param string $profileLastName
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileLastName($profileLastName)
    {
        $this->profileLastName = $profileLastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneAreaCityCode()
    {
        return $this->profilePhoneAreaCityCode;
    }

    /**
     * @param string $profilePhoneAreaCityCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePhoneAreaCityCode($profilePhoneAreaCityCode)
    {
        $this->profilePhoneAreaCityCode = $profilePhoneAreaCityCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneCountryAccessCode()
    {
        return $this->profilePhoneCountryAccessCode;
    }

    /**
     * @param string $profilePhoneCountryAccessCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePhoneCountryAccessCode($profilePhoneCountryAccessCode)
    {
        $this->profilePhoneCountryAccessCode = $profilePhoneCountryAccessCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneExtension()
    {
        return $this->profilePhoneExtension;
    }

    /**
     * @param string $profilePhoneExtension
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePhoneExtension($profilePhoneExtension)
    {
        $this->profilePhoneExtension = $profilePhoneExtension;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneNumber()
    {
        return $this->profilePhoneNumber;
    }

    /**
     * @param string $profilePhoneNumber
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePhoneNumber($profilePhoneNumber)
    {
        $this->profilePhoneNumber = $profilePhoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePhoneTechType()
    {
        return $this->profilePhoneTechType;
    }

    /**
     * @param string $profilePhoneTechType
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePhoneTechType($profilePhoneTechType)
    {
        $this->profilePhoneTechType = $profilePhoneTechType;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileEmail()
    {
        return $this->profileEmail;
    }

    /**
     * @param string $profileEmail
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileEmail($profileEmail)
    {
        $this->profileEmail = $profileEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressLine1()
    {
        return $this->profileAddressLine1;
    }

    /**
     * @param string $profileAddressLine1
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileAddressLine1($profileAddressLine1)
    {
        $this->profileAddressLine1 = $profileAddressLine1;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressLine2()
    {
        return $this->profileAddressLine2;
    }

    /**
     * @param string $profileAddressLine2
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileAddressLine2($profileAddressLine2)
    {
        $this->profileAddressLine2 = $profileAddressLine2;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCityName()
    {
        return $this->profileCityName;
    }

    /**
     * @param string $profileCityName
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileCityName($profileCityName)
    {
        $this->profileCityName = $profileCityName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePostalCode()
    {
        return $this->profilePostalCode;
    }

    /**
     * @param string $profilePostalCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfilePostalCode($profilePostalCode)
    {
        $this->profilePostalCode = $profilePostalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressStateProv()
    {
        return $this->profileAddressStateProv;
    }

    /**
     * @param string $profileAddressStateProv
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileAddressStateProv($profileAddressStateProv)
    {
        $this->profileAddressStateProv = $profileAddressStateProv;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCountryName()
    {
        return $this->profileCountryName;
    }

    /**
     * @param string $profileCountryName
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileCountryName($profileCountryName)
    {
        $this->profileCountryName = $profileCountryName;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuaranteeType()
    {
        return $this->guaranteeType;
    }

    /**
     * @param string $guaranteeType
     * @return HotelProvisionalBookingRequestParams
     */
    public function setGuaranteeType($guaranteeType)
    {
        $this->guaranteeType = $guaranteeType;

        return $this;
    }

    /**
     * @return array
     */
    public function getGuestCounts()
    {
        return $this->guestCounts;
    }

    /**
     * @param array $guestCounts
     * @return HotelProvisionalBookingRequestParams
     */
    public function setGuestCounts($guestCounts)
    {
        $this->guestCounts = $guestCounts;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileAddressStateProvCode()
    {
        return $this->profileAddressStateProvCode;
    }

    /**
     * @param string $profileAddressStateProvCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileAddressStateProvCode($profileAddressStateProvCode)
    {
        $this->profileAddressStateProvCode = $profileAddressStateProvCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileCountryNameCode()
    {
        return $this->profileCountryNameCode;
    }

    /**
     * @param string $profileCountryNameCode
     * @return HotelProvisionalBookingRequestParams
     */
    public function setProfileCountryNameCode($profileCountryNameCode)
    {
        $this->profileCountryNameCode = $profileCountryNameCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    /**
     * @param string $roomName
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;
    }

    /**
     * @return float
     */
    public function getRoomPricePerNight()
    {
        return $this->roomPricePerNight;
    }

    /**
     * @param float $roomPricePerNight
     */
    public function setRoomPricePerNight($roomPricePerNight)
    {
        $this->roomPricePerNight = $roomPricePerNight;
    }

    /**
     * @return float
     */
    public function getGuestCharges()
    {
        return $this->guestCharges;
    }

    /**
     * @param float $guestCharges
     */
    public function setGuestCharges($guestCharges)
    {
        $this->guestCharges = $guestCharges;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 17:32
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\BookingRequest;

/**
 * Class HotelCancellationBookingRequestParams
 * @package ApiBundle\DTO
 */
class HotelCancellationBookingRequestParams
{
    /**
     * @var array
     */
    public $dates;
    public $profileEmail;
    public $profileLastName;
    public $bookingId;

    /**
     * HotelCancellationBookingRequestParams constructor.
     * @param array $dates
     * @param $profileEmail
     * @param $profileLastName
     * @param $bookingId
     */
    public function __construct(array $dates, $profileEmail, $profileLastName, $bookingId)
    {
        $this->dates = $dates;
        $this->profileEmail = $profileEmail;
        $this->profileLastName = $profileLastName;
        $this->bookingId = $bookingId;
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return static
     */
    public static function fromBookingRequest(BookingRequest $bookingRequest)
    {
        $params = new static(
            $bookingRequest->getDateRangeArray(),
            $bookingRequest->getProfileEmail(),
            $bookingRequest->getProfileLastName(),
            $bookingRequest->getTravelGuruBookingId()
        );

        return $params;
    }
}
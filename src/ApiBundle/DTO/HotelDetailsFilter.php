<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 30.09.2015
 * Time: 11:38
 */

namespace ApiBundle\DTO;


use ApiBundle\Service\HotelDetails\InputParams;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Class HotelDetailsFilter
 * @package ApiBundle\DTO
 */
class HotelDetailsFilter
{
    const CURRENCY_INR = 'INR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_GBP = 'GBP';
    const SORT_ORDER_TG_RANKING = 'TG_RANKING';
    const SORT_ORDER_GUEST_RATING = 'GUEST_RATING';
    const SORT_ORDER_STAR_RATING_ASCENDING = 'STAR_RATING_ASCENDING';
    const SORT_ORDER_STAR_RATING_DESCENDING = 'STAR_RATING_DESCENDING';
    const SORT_ORDER_DEALS = 'DEALS';

    public $sortOrder = self::SORT_ORDER_TG_RANKING;

    public $requestedCurrency;
    public $hotelCode;
    public $stayDateRangeStart;
    public $stayDateRangeEnd;

    /**
     * @var array
     */
    public $roomStayCandidates;

    /**
     * @param InputParams $params
     * @return HotelDetailsFilter
     */
    public static function fromInputParams(InputParams $params)
    {
        $filter = new HotelDetailsFilter();
        $filter->hotelCode = $params->code;
        $filter->requestedCurrency = $params->currency;
        $filter->roomStayCandidates = $params->rooms;
        $filter->stayDateRangeStart = $params->dateFrom;
        $filter->stayDateRangeEnd = $params->dateTo;
        $filter->sortOrder = HotelDetailsFilter::SORT_ORDER_TG_RANKING;

        return $filter;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @throws UnexpectedValueException
     * @return static
     */
    public static function fromParamFetcher(ParamFetcherInterface $fetcher)
    {
        $filter = new static();
        $filter->requestedCurrency = $fetcher->get('requestedCurrency');
        $filter->hotelCode = $fetcher->get('hotelCode');
        $filter->stayDateRangeStart = $fetcher->get('stayDateRangeStart');
        $filter->stayDateRangeEnd = $fetcher->get('stayDateRangeEnd');

        $rooms = ($fetcher->get('rooms'));
        if (is_array($rooms) && count($rooms)) {
            $decoder = new JsonEncoder();
            foreach ($rooms as $room) {
                try {
                    $filter->roomStayCandidates[] = $decoder->decode($room, JsonEncoder::FORMAT);
                } catch (UnexpectedValueException $e) {
                    throw $e;
                }
            }
        }

        return $filter;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.10.2015
 * Time: 15:49
 */

namespace ApiBundle\DTO;


use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Class HotelMapSearchFilter
 * @package ApiBundle\DTO
 */
class HotelMapSearchFilter
{
    const CURRENCY_INR = 'INR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_GBP = 'GBP';

    const DEFAULT_COUNTRY_CODE = 'INDIA';

    public $rateRangeMin;
    public $rateRangeMax;

    /**
     * @var string
     */
    public $requestedCurrency = self::CURRENCY_INR;

    /**
     * @var string
     */
    public $cityName = 'Delhi';

    /**
     * @var string
     */
    public $countryCode = 'INDIA';

    /**
     * @var string
     */
    public $stayDateRangeStart = '2015-10-01';

    /**
     * @var string
     */
    public $stayDateRangeEnd = '2015-10-15';

    /**
     * @var array
     */
    public $roomStayCandidates = [];

    /**
     * Hotel stars quantity from 0 to 5
     *
     * @var array
     */
    public $stars = [0, 1, 2, 3, 4, 5];

    /**
     * @param ParamFetcherInterface $fetcher
     * @throws UnexpectedValueException
     * @return static
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        $filter = new static();
        $filter->cityName = $fetcher->get('cityName', $strict = true);
        $filter->countryCode = static::DEFAULT_COUNTRY_CODE;
        $filter->requestedCurrency = $fetcher->get('requestedCurrency', $strict = true);
        $filter->stayDateRangeEnd = $fetcher->get('stayDateRangeEnd', $strict = true);
        $filter->stayDateRangeStart = $fetcher->get('stayDateRangeStart', $strict = true);
        $filter->rateRangeMin = $fetcher->get('rateRangeMin', $strict = true);
        $filter->rateRangeMax = $fetcher->get('rateRangeMax', $strict = true);

        $stars = $fetcher->get('stars', $strict = false);
        if (is_array($stars) && count($stars)) {
            $filter->stars = $stars;
        }

        $rooms = ($fetcher->get('rooms', $strict = true));
        if (is_array($rooms) && count($rooms)) {
            $decoder = new JsonEncoder();
            foreach ($rooms as $k => $room) {
                try {
                    $filter->roomStayCandidates[] = $decoder->decode($room, JsonEncoder::FORMAT);
                } catch (UnexpectedValueException $e) {
                    throw $e;
                }
            }
        }

        return $filter;
    }

}
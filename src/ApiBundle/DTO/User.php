<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 10:34
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\UserInterface;

/**
 * Class User
 * @package ApiBundle\DTO
 */
class User
{
    /**
     * @var string
     */
    public $phoneNumber;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var Avatar
     */
    public $avatar;

    /**
     * @var Currency
     */
    public $currency;

    /**
     * User constructor.
     * @param string $phoneNumber
     * @param string $email
     * @param string $fullName
     * @param Avatar $avatar
     * @param Currency $currency
     */
    public function __construct(
        $phoneNumber,
        $email = null,
        $fullName = null,
        Avatar $avatar = null,
        Currency $currency = null
    ) {
        $this->phoneNumber = $phoneNumber;
        $this->email = $email;
        $this->fullName = $fullName;
        $this->avatar = $avatar;
        $this->currency = $currency;
    }


    /**
     * @param UserInterface $user
     * @return User
     */
    public static function fromUser(UserInterface $user)
    {
        return new User(
            $user->getPhoneNumber(),
            $user->getEmail(),
            $user->getFullName(),
            Avatar::fromUrl($user->getAvatar()->getPath()),
            Currency::fromCurrency($user->getCurrency())
        );
    }
}
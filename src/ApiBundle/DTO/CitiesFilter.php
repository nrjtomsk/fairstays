<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.09.2015
 * Time: 15:17
 */

namespace ApiBundle\DTO;


use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class CitiesFilter
 * @package ApiBundle\DTO
 */
class CitiesFilter
{
    /**
     * @var string
     */
    public $query;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;

    /**
     * CitiesFilter constructor.
     * @param string $query
     * @param int $limit
     * @param int $offset
     */
    public function __construct($query, $limit, $offset)
    {
        $this->query = $query;
        $this->limit = (int)$limit;
        $this->offset = (int)$offset;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @return static
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        return new static(
            $fetcher->get('query'),
            $fetcher->get('limit'),
            $fetcher->get('offset')
        );
    }

}
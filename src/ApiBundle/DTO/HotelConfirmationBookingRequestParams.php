<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 16:56
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\BookingRequest;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class HotelConfirmationBookingRequestParams
 * @package ApiBundle\DTO
 */
class HotelConfirmationBookingRequestParams
{

    public $correlationId;
    public $transactionIdentifier;
    public $requestedCurrency;
    public $guaranteeType;
    public $bookingId;

    /**
     * HotelConfirmationBookingRequestParams constructor.
     * @param $requestedCurrency
     * @param $bookingId
     * @param $guaranteeType
     */
    public function __construct($requestedCurrency, $bookingId, $guaranteeType)
    {
        $this->requestedCurrency = $requestedCurrency;
        $this->bookingId = $bookingId;
        $this->guaranteeType = $guaranteeType;
    }

    /**
     * @return mixed
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param mixed $correlationId
     * @return HotelConfirmationBookingRequestParams
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactionIdentifier()
    {
        return $this->transactionIdentifier;
    }

    /**
     * @param mixed $transactionIdentifier
     * @return HotelConfirmationBookingRequestParams
     */
    public function setTransactionIdentifier($transactionIdentifier)
    {
        $this->transactionIdentifier = $transactionIdentifier;

        return $this;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @return static
     */
    public static function fromParamFetcher(ParamFetcherInterface $fetcher)
    {
        $params = new static(
            $fetcher->get('requestedCurrency', $strict = true),
            $fetcher->get('bookingId', $strict = true),
            $fetcher->get('guaranteeType', $strict = true)
        );

        return $params;
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return static
     */
    public static function fromBookingRequest(BookingRequest $bookingRequest)
    {
        $params = new static(
            $bookingRequest->getRequestedCurrency(),
            $bookingRequest->getBookingId(),
            $bookingRequest->getGuaranteeType()
        );

        $params->setCorrelationId($bookingRequest->getCorrelationId());
        $params->setTransactionIdentifier($bookingRequest->getTransactionIdentifier());

        return $params;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 28.09.2015
 * Time: 13:29
 */

namespace ApiBundle\DTO;


use AppBundle\Entity\HotelsDestination;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Class HotelSearchFilter
 * @package ApiBundle\DTO
 */
class HotelSearchFilter
{
    /**
     * Define constants
     */
    const CURRENCY_INR = 'INR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_GBP = 'GBP';

    const SORT_ORDER_PRICE = 'PRICE';
    const SORT_ORDER_PRICE_DESC = 'PRICE_DESC';
    const SORT_ORDER_STAR_RATING_ASCENDING = 'STAR_RATING_ASCENDING';
    const SORT_ORDER_STAR_RATING_DESCENDING = 'STAR_RATING_DESCENDING';
    const SORT_ORDER_SELECTED_HOTELS = 'SELECTED_HOTELS';

    const MAX_RESULT_SET_COUNT = 50;

    const DEFAULT_COUNTRY_CODE = 'INDIA';

    public $rateRangeMin;
    public $rateRangeMax;

    /**
     * @var string
     */
    public $requestedCurrency = self::CURRENCY_INR;

    /**
     * @var string
     */
    public $sortOrder = self::SORT_ORDER_SELECTED_HOTELS;

    /**
     * @var integer
     */
    public $cityId;

    /**
     * @var HotelsDestination
     */
    public $city;

    /**
     * @var string
     */
    public $cityName = '';

    /**
     * @var string
     */
    public $countryCode = self::DEFAULT_COUNTRY_CODE;

    /**
     * @var string
     */
    public $stayDateRangeStart = '2015-10-01';

    /**
     * @var string
     */
    public $stayDateRangeEnd = '2015-10-15';

    /**
     * @var array
     */
    public $roomStayCandidates = [];

    /**
     * @var int
     */
    public $hotelsFrom = 1;

    /**
     * @var int
     */
    public $hotelsTo = 2;

    /**
     * Hotel stars quantity from 0 to 5
     *
     * @var array
     */
    public $stars = [0, 1, 2, 3, 4, 5];

    /**
     * @var string
     */
    public $nationality;
    /**
     * @param HotelMapSearchFilter $filter
     * @return HotelSearchFilter
     */
    public static function fromMapFilter(HotelMapSearchFilter $filter)
    {
        $limitedFilter = new HotelSearchFilter();
        $limitedFilter->cityName = $filter->cityName;
        $limitedFilter->countryCode = $filter->countryCode;
        $limitedFilter->rateRangeMax = $filter->rateRangeMax;
        $limitedFilter->rateRangeMin = $filter->rateRangeMin;
        $limitedFilter->requestedCurrency = $filter->requestedCurrency;
        $limitedFilter->roomStayCandidates = $filter->roomStayCandidates;
        $limitedFilter->stayDateRangeEnd = $filter->stayDateRangeEnd;
        $limitedFilter->stayDateRangeStart = $filter->stayDateRangeStart;
        $limitedFilter->sortOrder = HotelSearchFilter::SORT_ORDER_SELECTED_HOTELS;
        $limitedFilter->stars = $filter->stars;

        return $limitedFilter;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @throws UnexpectedValueException
     * @return static
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        $filter = new static();
        $filter->cityId = $fetcher->get('cityId', true);
        $filter->countryCode = static::DEFAULT_COUNTRY_CODE;
        $filter->hotelsTo = $fetcher->get('to', $strict = true);
        $filter->hotelsFrom = $fetcher->get('from', $strict = true);
        $filter->requestedCurrency = $fetcher->get('requestedCurrency', $strict = true);
        $filter->sortOrder = $fetcher->get('sortOrder', $strict = true);
        $filter->stayDateRangeEnd = $fetcher->get('stayDateRangeEnd', $strict = true);
        $filter->stayDateRangeStart = $fetcher->get('stayDateRangeStart', $strict = true);
        $filter->rateRangeMin = $fetcher->get('rateRangeMin', $strict = true);
        $filter->rateRangeMax = $fetcher->get('rateRangeMax', $strict = true);

        try {
            $filter->nationality = $fetcher->get('nationality');
        } catch (\InvalidArgumentException $e) {
            $filter->nationality = null;
        }

        $stars = $fetcher->get('stars');
        if (is_array($stars) && count($stars)) {
            $filter->stars = $stars;
        }

        $rooms = ($fetcher->get('rooms', $strict = true));
        if (is_array($rooms) && count($rooms)) {
            $decoder = new JsonEncoder();
            foreach ($rooms as $k => $room) {
                try {
                    $filter->roomStayCandidates[] = $decoder->decode($room, JsonEncoder::FORMAT);
                } catch (UnexpectedValueException $e) {
                    throw $e;
                }
            }
        }

        return $filter;
    }

}
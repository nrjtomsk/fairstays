<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 10:52
 */

namespace ApiBundle\DTO;


use ApiBundle\Service\MoneyConverter;
use AppBundle\Entity\BookingRequest;

/**
 * Class PayURequestObject
 * @package ApiBundle\DTO
 */
class PayURequestObject
{

    /**
     * Unique merchant key
     *
     * @var string
     */
    private $key = '';

    /**
     * Transaction ID - booking request ID
     *
     * @var string
     */
    private $txnid = '';

    /**
     * @var float
     */
    private $amount = 0;

    /**
     * Text description
     *
     * @var string
     */
    private $productInfo = '';

    /**
     * @var string
     */
    private $firstName = '';

    /**
     * @var string
     */
    private $email = '';

    /**
     * @var string
     */
    private $phone = '';

    /**
     * @var string
     */
    private $lastName = '';

    /**
     * @var string
     */
    private $address1 = '';

    /**
     * @var string
     */
    private $address2 = '';

    /**
     * @var string
     */
    private $city = '';

    /**
     * @var string
     */
    private $state = '';

    /**
     * @var string
     */
    private $country = '';

    /**
     * @var string
     */
    private $zipcode = '';

    /**
     * User defined field 1
     *
     * @var string
     */
    private $udf1 = '';

    /**
     * @var string
     */
    private $udf2 = '';

    /**
     * @var string
     */
    private $udf3 = '';

    /**
     * @var string
     */
    private $udf4 = '';

    /**
     * @var string
     */
    private $udf5 = '';

    /**
     * Success url
     *
     * @var string
     */
    private $surl = '';

    /**
     * Failure url
     *
     * @var string
     */
    private $furl = '';

    /**
     * Cancel url
     *
     * @var string
     */
    private $curl = '';

    /**
     * @var string
     */
    private $hashPayment = '';

    /**
     * @var string
     */
    private $hashRelatedDetails = '';

    /**
     * @var string
     */
    private $hashVasForMobile = '';

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getTxnid()
    {
        return $this->txnid;
    }

    /**
     * @param string $txnid
     */
    public function setTxnid($txnid)
    {
        $this->txnid = $txnid;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getProductInfo()
    {
        return $this->productInfo;
    }

    /**
     * @param string $productInfo
     */
    public function setProductInfo($productInfo)
    {
        $this->productInfo = $productInfo;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getUdf1()
    {
        return $this->udf1;
    }

    /**
     * @param string $udf1
     */
    public function setUdf1($udf1)
    {
        $this->udf1 = $udf1;
    }

    /**
     * @return string
     */
    public function getUdf2()
    {
        return $this->udf2;
    }

    /**
     * @param string $udf2
     */
    public function setUdf2($udf2)
    {
        $this->udf2 = $udf2;
    }

    /**
     * @return string
     */
    public function getUdf3()
    {
        return $this->udf3;
    }

    /**
     * @param string $udf3
     */
    public function setUdf3($udf3)
    {
        $this->udf3 = $udf3;
    }

    /**
     * @return string
     */
    public function getUdf4()
    {
        return $this->udf4;
    }

    /**
     * @param string $udf4
     */
    public function setUdf4($udf4)
    {
        $this->udf4 = $udf4;
    }

    /**
     * @return string
     */
    public function getUdf5()
    {
        return $this->udf5;
    }

    /**
     * @param string $udf5
     */
    public function setUdf5($udf5)
    {
        $this->udf5 = $udf5;
    }

    /**
     * @return string
     */
    public function getSurl()
    {
        return $this->surl;
    }

    /**
     * @param string $surl
     */
    public function setSurl($surl)
    {
        $this->surl = $surl;
    }

    /**
     * @return string
     */
    public function getFurl()
    {
        return $this->furl;
    }

    /**
     * @param string $furl
     */
    public function setFurl($furl)
    {
        $this->furl = $furl;
    }

    /**
     * @return string
     */
    public function getCurl()
    {
        return $this->curl;
    }

    /**
     * @param string $curl
     */
    public function setCurl($curl)
    {
        $this->curl = $curl;
    }

    /**
     * @return string
     */
    public function getHashPayment()
    {
        return $this->hashPayment;
    }

    /**
     * @param string $hashPayment
     */
    public function setHashPayment($hashPayment)
    {
        $this->hashPayment = $hashPayment;
    }

    /**
     * @return string
     */
    public function getHashRelatedDetails()
    {
        return $this->hashRelatedDetails;
    }

    /**
     * @param string $hashRelatedDetails
     */
    public function setHashRelatedDetails($hashRelatedDetails)
    {
        $this->hashRelatedDetails = $hashRelatedDetails;
    }

    /**
     * @return string
     */
    public function getHashVasForMobile()
    {
        return $this->hashVasForMobile;
    }

    /**
     * @param string $hashVasForMobile
     */
    public function setHashVasForMobile($hashVasForMobile)
    {
        $this->hashVasForMobile = $hashVasForMobile;
    }

    /**
     * calculate & set hash
     * @param $salt
     */
    public function fillHashes($salt)
    {
        $this->fillPaymentHash($salt);
        $this->fillRelatedDetailsHash($salt);
        $this->fillVasForMobileHash($salt);
    }

    /**
     * @param $salt
     */
    private function fillPaymentHash($salt)
    {
        $hashedFields = [
            $this->getKey(),
            $this->getTxnid(),
            (string)$this->getAmount(),
            $this->getProductInfo(),
            $this->getFirstName(),
            $this->getEmail(),
            $this->getUdf1(),
            $this->getUdf2(),
            $this->getUdf3(),
            $this->getUdf4(),
            $this->getUdf5(),
            '', // blank element 1 like in payU docs
            '', // blank element 2 like in payU docs
            '', // blank element 3 like in payU docs
            '', // blank element 4 like in payU docs
            '', // blank element 5 like in payU docs
            $salt,
        ];

        $hash = $this->calculateHash($hashedFields);
        $this->setHashPayment($hash);
    }

    /**
     * @param $salt
     */
    private function fillRelatedDetailsHash($salt)
    {
        $userCredentials = (string)$this->getKey().':'.(string)$this->getEmail();

        $hashedFields = [
            $this->getKey(),
            'payment_related_details_for_mobile_sdk',
            $userCredentials,
            $salt,
        ];

        $hash = $this->calculateHash($hashedFields);
        $this->setHashRelatedDetails($hash);
    }

    /**
     * @param $salt
     */
    private function fillVasForMobileHash($salt)
    {
        $hashedFields = [
            $this->getKey(),
            'vas_for_mobile_sdk',
            'default',
            $salt,
        ];

        $hash = $this->calculateHash($hashedFields);
        $this->setHashVasForMobile($hash);
    }

    /**
     * @param array $hashedFields
     * @return string
     */
    private function calculateHash(array $hashedFields)
    {
        $hashedString = implode('|', $hashedFields);

        return strtolower(hash('sha512', $hashedString));
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return PayURequestObject
     */
    public static function fromBookingRequest(BookingRequest $bookingRequest)
    {
        $requestObject = new self;

        $transactionId = $bookingRequest->getId().'-'.$bookingRequest->getTransactionIdentifier();
        $transactionId = substr($transactionId, $start = 0, $length = 25);
        $requestObject->setTxnid($transactionId);

        $totalAmount = 99999999;
        throw new \RuntimeException('Need to set correct amount');
        $requestObject->setAmount($totalAmount);

        $requestObject->setProductInfo($transactionId);
        $requestObject->setFirstName($bookingRequest->getProfileGivenName());
        $requestObject->setEmail($bookingRequest->getProfileEmail());
        $requestObject->setPhone($bookingRequest->getProfilePhoneNumber());
        $requestObject->setLastName($bookingRequest->getProfileLastName());
        $requestObject->setAddress1($bookingRequest->getProfileAddressLine1());
        $requestObject->setAddress2($bookingRequest->getProfileAddressLine2());
        $requestObject->setCity($bookingRequest->getProfileCityName());
        $requestObject->setState($bookingRequest->getProfileAddressStateProv());
        $requestObject->setCountry($bookingRequest->getProfileCountryName());
        $requestObject->setZipcode($bookingRequest->getProfilePostalCode());

        return $requestObject;
    }

}
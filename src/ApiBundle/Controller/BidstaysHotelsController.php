<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 11:51
 */

namespace ApiBundle\Controller;

use ApiBundle\Exception\BookingRequestSaveException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Exception\InvalidFormException;
use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Form\BidFormType;
use ApiBundle\Service\Bid\BidParams;
use ApiBundle\Service\Bid\BidStatuses;
use ApiBundle\Service\Bid\Exception\BadBidAmountException;
use ApiBundle\Service\Bid\Exception\BidOnRoomImpossibleException;
use ApiBundle\Service\Bid\Exception\BidSessionMaxBidTriesExceededException;
use ApiBundle\Service\Bid\Exception\UserMaxBidTriesExceededException;
use ApiBundle\Service\Booking\BookingContext;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\InputParams;
use AppBundle\Repository\Exception\BidException;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;


/**
 * Class BidstaysHotelsController
 * @package ApiBundle\Controller
 */
class BidstaysHotelsController extends FOSRestController
{
    /**
     * Get bidstays hotel details
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Bidstays"
     * )
     *
     * @QueryParam(name="currency", nullable=false, requirements="(^INR$|^USD$|^EUR$|^GBP$|^CHF$)", description="Requested currency")
     * @QueryParam(name="code", nullable=false, description="Hotel code")
     * @QueryParam(name="dateFrom", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="Start Date")
     * @QueryParam(name="dateTo", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="End Date")
     * @QueryParam(name="rooms", array=true, default="{""adultCount"":1, ""childrenCount"":2, ""childrenAges"": [11, 9]}", description="Rooms")
     * @QueryParam(name="nationality", nullable=false, description="Two digit country code of client nationality")
     * @QueryParam(name="meta", nullable=false, description="Metadata")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcherInterface $fetcher
     * @return HotelDetails
     * @throws NotFoundHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function getBidstaysHotelAction(ParamFetcherInterface $fetcher)
    {
        $params = InputParams::fromParamFetcher($fetcher);

        try {

            return new Response($this->get('serializer')->serialize(
                $this->get('api.service.hotel_details_client_bidstays')->get($params),
                'json',
                SerializationContext::create()
                    ->setGroups(['Default', 'Fairstays', 'Bidstays'])
            ));
        } catch (HotelNotFoundException $e) {
            throw $this->createNotFoundException('Not Found', $e);
        } catch (\Exception $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+1 hour'), $e->getMessage(), $e);
        }
    }

    /**
     * Post new bid
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Bidstays",
     *     input="ApiBundle\Form\BidFormType"
     * )
     *
     * @View(statusCode=400)
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @return \Symfony\Component\Form\Form
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @throws \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException
     */
    public function postBidstaysBidAction(Request $request)
    {
        $bidParams = new BidParams();
        $bidForm = $this->createForm(new BidFormType(), $bidParams);
        $bidForm->handleRequest($request);
        if ($bidForm->isValid()) {
            $response = new JsonResponse();
            try {
                $this->get('api.service.bid.handler')->handle($bidParams);
                $response->setStatusCode(Response::HTTP_OK);
                $response->setData(['status' => BidStatuses::ACCEPTED]);
            } catch (UserMaxBidTriesExceededException $e) {
                $response->setStatusCode(Response::HTTP_OK);
                $response->setData(['status' => BidStatuses::MAX_BID_TRIES_USER_EXCEEDED]);
            } catch (BidSessionMaxBidTriesExceededException $e) {
                $response->setStatusCode(Response::HTTP_OK);
                $response->setData(['status' => BidStatuses::MAX_BID_TRIES_EXCEEDED]);
            } catch (BidOnRoomImpossibleException $e) {
                $response->setStatusCode(Response::HTTP_OK);
                $response->setData(['status' => BidStatuses::REJECTED]);
            } catch (BidException $e) {
                throw new ServiceUnavailableHttpException(null, $e->getMessage(), $e);
            } catch (RoomNotFound $e) {
                throw new BadRequestHttpException($e->getMessage(), $e);
            } catch (BadBidAmountException $e) {
                throw new BadRequestHttpException($e->getMessage(), $e);
            } catch (\Exception $e) {
                throw new ServiceUnavailableHttpException(null, $e->getMessage(), $e);
            }

            return $response;
        }
        return $bidForm;
    }


    /**
     * Post new bidstays booking request
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Bidstays",
     *  input="ApiBundle\Form\BookingFormType",
     *  statusCodes={
     *      200 = "OK",
     *      500 = "Error"
     *  }
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @View(statusCode=400)
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function postBidstaysBookingAction(Request $request)
    {
        try {
            $context = new BookingContext();
            $context->isBidstays = true;
            return $this->view(
                $this->get('api.service.handler.booking.stripe')->handle($request, $context),
                Codes::HTTP_CREATED
            );
        } catch (InvalidFormException $e) {
            return $e->getForm();
        } catch (BadNationalityCodeException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        } catch (HotelNotFoundException $e) {
            throw $this->createNotFoundException('Hotel Not Found', $e);
        } catch (RoomNotFound $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (InvalidOptionsException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (HotelServiceErrorResponse $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (BookingRequestSaveException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (\RuntimeException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        }

    }
}
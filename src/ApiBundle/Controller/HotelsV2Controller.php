<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 07.04.2016
 * Time: 11:44
 */

namespace ApiBundle\Controller;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Exception\CityNotFoundException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\HotelDetails\InputParams;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use ApiBundle\Service\Trivago\Exception\TrivagoServiceException;
use AppBundle\Entity\HotelsDestination;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Class HotelsV2Controller
 * @package ApiBundle\Controller
 */
class HotelsV2Controller extends FOSRestController
{
    /**
     * Get hotels
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Hotels",
     *  tags={"version 2"}
     * )
     *
     * @QueryParam(name="requestedCurrency", nullable=false, requirements="(^INR$|^USD$|^EUR$|^GBP$|^CHF$)", description="Requested currency")
     * @QueryParam(name="sortOrder", nullable=false, default="SELECTED_HOTELS", requirements="(PRICE|PRICE_DESC|STAR_RATING_ASCENDING|STAR_RATING_DESCENDING|SELECTED_HOTELS)", description="Sort parameter")
     * @QueryParam(name="cityId", nullable=false, description="City id")
     * @QueryParam(name="stayDateRangeStart", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="Start Date")
     * @QueryParam(name="stayDateRangeEnd", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="End Date")
     * @QueryParam(name="from", nullable=false, default="1", requirements="\d+", description="Offset Parameter")
     * @QueryParam(name="to", nullable=false, default="50", requirements="\d+", description="(Offset + Limit - 1) Parameter")
     * @QueryParam(name="rateRangeMin", nullable=false, requirements="\d+", description="Minimum rate of the Hotel's to be made available in the search response")
     * @QueryParam(name="rateRangeMax", nullable=false, requirements="\d+", description="Maximum rate of the Hotel's to be made available in the search response")
     * @QueryParam(name="stars", array=true, description="Hotel stars quantity (array)")
     * @QueryParam(name="rooms", array=true, default="{""adultCount"":1, ""childrenCount"":2, ""childrenAges"": [11, 9]}", description="Rooms (array)")
     * @QueryParam(name="nationality", nullable=true, description="Two digit country code of client nationality")
     * @param ParamFetcherInterface $fetcher
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function getV2HotelsAction(ParamFetcherInterface $fetcher)
    {
        try {

            $stopWatch = $this->get('debug.stopwatch');
            $stopWatch->start('start_action');
            if ($fetcher->get('nationality')) {
                $this->get('app.service.county_code_dictionary')->checkNationalityCodeInAllowedList($fetcher->get('nationality'));
            }
            $params = HotelSearchFilter::fromFetcher($fetcher);

            /** @var HotelsDestination $city */
            $city = $this->get('app.repo.hotels_destination')->find($params->cityId);
            if (!$city) {
                throw new CityNotFoundException;
            }


            $params->city = $city;
            $hotelsList = $this->get('api.service.hotels.list_provider_paginated')
                ->requestHotels($params);

            $this->get('api.service.hotels.unfiltered_list_logger')->dumpSearchLogs($hotelsList, $city);


            return $hotelsList;

        } catch (HotelServiceErrorResponse $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        } catch (CityNotFoundException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        } catch (BadNationalityCodeException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }
    }

    /**
     * Get hotel details v2
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Hotels",
     *     tags={"version 2"}
     * )
     *
     * @QueryParam(name="currency", nullable=false, requirements="(^INR$|^USD$|^EUR$|^GBP$|^CHF$)", description="Requested currency")
     * @QueryParam(name="code", nullable=false, description="Hotel code")
     * @QueryParam(name="dateFrom", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="Start Date")
     * @QueryParam(name="dateTo", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="End Date")
     * @QueryParam(name="rooms", array=true, default="{""adultCount"":1, ""childrenCount"":2, ""childrenAges"": [11, 9]}", description="Rooms")
     * @QueryParam(name="nationality", nullable=false, description="Two digit country code of client nationality")
     * @QueryParam(name="meta", nullable=false, description="Metadata")
     * @param ParamFetcherInterface $fetcher
     * @return \ApiBundle\Service\HotelDetails\HotelDetails
     * @throws NotFoundHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function getV2HotelAction(ParamFetcherInterface $fetcher)
    {
        $params = InputParams::fromParamFetcher($fetcher);
        try {
            $hotelDetails = $this->get('api.service.hotel_details_client_v2')->get($params);
            return new Response($this->get('serializer')->serialize(
                $hotelDetails,
                'json'
            ));
        } catch (HotelNotFoundException $e) {
            throw $this->createNotFoundException('Not Found', $e);
        } catch (\Exception $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+1 hour'), $e->getMessage(), $e);
        }
    }

    /**
     * Get hotel rooms cancellation policies
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Hotels",
     *     tags={"version 2"}
     * )
     *
     * @QueryParam(name="hotelCode", nullable=false, description="Hotel code")
     * @QueryParam(name="roomMetaData", nullable=false, array=true, description="Array of rooms metadata-s")
     * @QueryParam(name="currency", nullable=false, description="Displayed currency")
     * @QueryParam(name="hotelMetaData", nullable=false, description="Hotel details metadata")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException
     */
    public function getV2HotelCancellationpolicyAction(ParamFetcherInterface $fetcher)
    {
        try {
            $metaData = MetaDataDTO::fromJsonString($fetcher->get('hotelMetaData'));
            if ($metaData->serviceName != HotelsListApiManagerHotelsPro::NAME) {
                return new JsonResponse(
                    [
                        'error' => 'Error',
                        'message' => 'Only hotelsPro items supported',
                        'code' => 400,
                    ], Response::HTTP_BAD_REQUEST
                );
            }

            $metaDatas = [];
            foreach ($fetcher->get('roomMetaData') as $item) {
                $metaDatas[] = MetaDataDTO::fromJsonString($item);
            }

            return array_values($this->get('api.service.hotel_details.hotels_pro.cancellation_policy_provider')
                ->getCancellationPolicies(
                    $fetcher->get('hotelCode'),
                    $metaDatas,
                    $fetcher->get('currency')
                ));
        } catch (\Exception $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 min'), $e->getMessage(), $e);
        }
    }
}
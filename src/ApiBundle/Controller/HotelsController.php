<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 17:17
 */

namespace ApiBundle\Controller;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Exception\CityNotFoundException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\HotelDetails\InputParams;
use AppBundle\Entity\HotelsDestination;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Collects hotels info:
 * Gets hotels
 * Gets hotels for map
 * Gets hotel details
 * Gets hotel details v2
 *
 * Class HotelsController
 * @package ApiBundle\Controller
 */
class HotelsController extends FOSRestController
{
    /**
     * Get hotels
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Hotels",
     *  tags={"version 1"}
     * )
     *
     * @QueryParam(name="requestedCurrency", nullable=false, requirements="(^INR$|^USD$|^EUR$|^GBP$|^CHF$)", description="Requested currency")
     * @QueryParam(name="sortOrder", nullable=false, default="SELECTED_HOTELS", requirements="(PRICE|PRICE_DESC|STAR_RATING_ASCENDING|STAR_RATING_DESCENDING|SELECTED_HOTELS)", description="Sort parameter")
     * @QueryParam(name="cityId", nullable=false, description="City id")
     * @QueryParam(name="stayDateRangeStart", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="Start Date")
     * @QueryParam(name="stayDateRangeEnd", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="End Date")
     * @QueryParam(name="from", nullable=false, default="1", requirements="\d+", description="Offset Parameter")
     * @QueryParam(name="to", nullable=false, default="50", requirements="\d+", description="(Offset + Limit - 1) Parameter")
     * @QueryParam(name="rateRangeMin", nullable=false, requirements="\d+", description="Minimum rate of the Hotel's to be made available in the search response")
     * @QueryParam(name="rateRangeMax", nullable=false, requirements="\d+", description="Maximum rate of the Hotel's to be made available in the search response")
     * @QueryParam(name="stars", array=true, description="Hotel stars quantity (array)")
     * @QueryParam(name="rooms", array=true, default="{""adultCount"":1, ""childrenCount"":2, ""childrenAges"": [11, 9]}", description="Rooms (array)")
     * @param ParamFetcherInterface $fetcher
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function getHotelsAction(ParamFetcherInterface $fetcher)
    {
        try {
            $stopWatch = $this->get('debug.stopwatch');
            $stopWatch->start('start_action');
            $params = HotelSearchFilter::fromFetcher($fetcher);

            /** @var HotelsDestination $city */
            $city = $this->get('app.repo.hotels_destination')->find($params->cityId);
            if (!$city) {
                throw new CityNotFoundException;
            }


            $params->city = $city;
            $hotelsList = $this->get('api.service.hotels.list_provider_paginated')
                ->requestHotels($params);
            $this->get('api.service.hotels.unfiltered_list_logger')->dumpSearchLogs($hotelsList, $city);

            return $hotelsList;

        } catch (HotelServiceErrorResponse $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        } catch (CityNotFoundException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        }
    }

    /**
     * Get hotel details
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Hotels",
     *     tags={"version 1"}
     * )
     *
     * @QueryParam(name="currency", nullable=false, requirements="(^INR$|^USD$|^EUR$|^GBP$|^CHF$)", description="Requested currency")
     * @QueryParam(name="code", nullable=false, description="Hotel code")
     * @QueryParam(name="dateFrom", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="Start Date")
     * @QueryParam(name="dateTo", nullable=false, requirements="\d{4}-\d{2}-\d{2}", description="End Date")
     * @QueryParam(name="rooms", array=true, default="{""adultCount"":1, ""childrenCount"":2, ""childrenAges"": [11, 9]}", description="Rooms")
     * @QueryParam(name="meta", nullable=false, description="Metadata")
     *
     * @param ParamFetcherInterface $fetcher
     * @return \ApiBundle\Service\HotelDetails\HotelDetails
     * @throws NotFoundHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function getHotelAction(ParamFetcherInterface $fetcher)
    {
        $params = InputParams::fromParamFetcher($fetcher);
        $params->nationality = $this->getParameter('default_booking_client_nationality');

        try {
            return new Response($this->get('serializer')->serialize(
                $this->get('api.service.hotel_details_client')->get($params),
                'json'
            ));
        } catch (HotelNotFoundException $e) {
            throw $this->createNotFoundException('Not Found', $e);
        } catch (\Exception $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+1 hour'), $e->getMessage(), $e);
        }
    }
}
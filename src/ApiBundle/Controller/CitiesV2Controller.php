<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 14:37
 */

namespace ApiBundle\Controller;


use ApiBundle\Service\Destination\DestinationFilter;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class CitiesV2Controller
 * @package ApiBundle\Controller
 */
class CitiesV2Controller extends FOSRestController
{
    /**
     * Get cities v2
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Cities",
     *     tags={"version 2"}
     * )
     *
     * @QueryParam(name="query", nullable=false, description="Query string")
     * @QueryParam(name="limit", nullable=false, default="10", description="Limit pagination param")
     * @QueryParam(name="offset", nullable=false, default="0", description="Offset pagination param")
     *
     * @param ParamFetcherInterface $fetcher
     * @return \ApiBundle\Service\Destination\DestinationDto[]
     */
    public function getV2CitiesAction(ParamFetcherInterface $fetcher)
    {
        return $this->get('api.service.destinations.service')->search(DestinationFilter::fromFetcher($fetcher));
    }

}
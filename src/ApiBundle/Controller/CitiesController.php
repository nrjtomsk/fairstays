<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.09.2015
 * Time: 14:13
 */

namespace ApiBundle\Controller;


use ApiBundle\DTO\CitiesFilter;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Gets cities list
 *
 * Class CitiesController
 * @package ApiBundle\Controller
 */
class CitiesController extends FOSRestController
{

    /**
     * Get cities list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Cities"
     * )
     *
     * @QueryParam(name="query", nullable=true, description="Query")
     * @QueryParam(name="limit", nullable=true, default="50", requirements="\d+", description="Limit")
     * @QueryParam(name="offset", nullable=true, default="0", requirements="\d+", description="Offset")
     *
     * @param ParamFetcherInterface $fetcher
     * @internal param $query
     * @return array
     */
    public function getCitiesAction(ParamFetcherInterface $fetcher)
    {
        return $this->get('api.service.handler.destinations')->search(CitiesFilter::fromFetcher($fetcher));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.09.2015
 * Time: 17:50
 */

namespace ApiBundle\Controller;


use ApiBundle\DTO\User;
use ApiBundle\Exception\BookingCancellationException;
use ApiBundle\Exception\BookingRequestSaveException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Exception\InvalidFormException;
use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Service\Booking\BookingContext;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\HotelsList\HotelDTO;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\Utils\ExceptionsUtil;
use AppBundle\Entity\BookingRequest;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Manage the user:
 * Get current user profile info
 * Get user by phone number
 * Get user form
 * Create new user
 * Put current user
 * Patch current user
 * Delete user by phone number
 * Post avatar for current user
 * Post new booking request V2
 * Post new booking request
 * Get current user booking requests
 * Delete bookingDelete booking
 * Get booking request details
 *
 * Class UsersController
 * @package ApiBundle\Controller
 */
class UsersController extends FOSRestController
{

    /**
     * Get current user profile info
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return mixed
     * @throws \LogicException
     */
    public function getUserCurrentAction()
    {
        return $this->getUser();
    }

    /**
     * Get user by phone number
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  statusCodes={
     *      200 = "Returns when success",
     *      404 = "Returns when user not found"
     *  }
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param $phoneNumber
     * @return User
     * @throws NotFoundHttpException
     */
    public function getUserAction($phoneNumber)
    {
        try {
            return $this->get('app.service.user.provider')->loadUserByPhoneNumber($phoneNumber);
        } catch (UsernameNotFoundException $e) {
            throw new NotFoundHttpException(
                sprintf(
                    'User %s not found',
                    $phoneNumber
                )
            );
        }
    }

    /**
     * Patch current user
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Users",
     *  input="ApiBundle\Form\UserFormType"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(
     *  statusCode=Codes::HTTP_BAD_REQUEST
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function patchUserAction(Request $request)
    {
        try {
            $user = $this->get('api.service.handler.user')->patch($request);
            $routeParams = [
                'phoneNumber' => $user->getPhoneNumber(),
                '_format' => $request->get('_format'),
            ];

            return $this->redirectView($this->generateUrl('get_user', $routeParams), Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $e) {
            $this->get('logger')->warning($e->getMessage(), $e->getTrace());

            return $e->getForm();
        }
    }

    /**
     * Post avatar for current user
     *
     * @ApiDoc(
     *  resource=true,
     *  input="ApiBundle\Form\FileFormType",
     *  statusCodes={
     *      201 = "Returns when the avatar is created",
     *      400 = "Returns when the form has errors",
     *  },
     *  section="Users"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(statusCode=Codes::HTTP_BAD_REQUEST)
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postUserAvatarAction(Request $request)
    {
        try {
            $this->get('api.service.handler.user')->postUserAvatar($request);
            $routeParams = [
                'phoneNumber' => $this->getUser()->getPhoneNumber(),
                '_format' => $request->get('_format'),
            ];

            return $this->redirectView($this->generateUrl('get_user', $routeParams), Codes::HTTP_CREATED);

        } catch (InvalidFormException $e) {
            $this->get('logger')->warning($e->getMessage(), $e->getTrace());

            return $e->getForm()->getErrors(true, true);
        }
    }

    /**
     * Post new booking request V2
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Booking",
     *  input="ApiBundle\Form\BookingFormType",
     *  statusCodes={
     *      200 = "OK",
     *      500 = "Error"
     *  }
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @View(statusCode=400)
     * @return JsonResponse
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function postBookingAction(Request $request)
    {
        try {
            $context = new BookingContext();
            $context->isBidstays = false;
            return $this->view(
                $this->get('api.service.handler.booking.stripe')->handle($request, $context),
                Codes::HTTP_CREATED
            );
        } catch (InvalidFormException $e) {
            return $e->getForm();
        } catch (BadNationalityCodeException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e);
        } catch (HotelNotFoundException $e) {
            throw $this->createNotFoundException('Hotel Not Found', $e);
        } catch (RoomNotFound $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (InvalidOptionsException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (HotelServiceErrorResponse $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (BookingRequestSaveException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        } catch (\RuntimeException $e) {
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e);
        }

    }

    /**
     * Get current user booking requests
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Booking"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return array
     */
    public function getUserBookingrequestsAction()
    {
        $user = $this->getUser();

        if ($user instanceof \AppBundle\Entity\User && count($user->getBookingRequests())) {
            /** @var BookingRequest[]|ArrayCollection $bookings */
            $bookings = $user->getBookingRequests()->filter(
                    function (BookingRequest $bookingRequest) {
                        return $bookingRequest->getStatus() === BookingRequest::STATUS_CONFIRMED;
                    }
                );

            $hotelProRepo = $this->get('app.repo.hotels_pro');
            $hotelTravelGuruRepo = $this->get('app.repo.hotels');
            
            if ($bookings->count()) {
                foreach ($bookings as $booking) {
                    $hotelId = $booking->getHotelCode();
                    $serviceProvider = $booking->getMetaData()->serviceName;
                    switch ($serviceProvider) {
                        case HotelsListApiManagerTravelGuru::NAME:
                            $hotel = HotelDTO::fromTravelGuruHotel($hotelTravelGuruRepo->find($hotelId));
                            break;

                        case HotelsListApiManagerHotelsPro::NAME:
                            $hotel = HotelDTO::fromHotelsProHotel(
                                $hotelProRepo->findHotelByHotelCode($hotelId),
                                $booking->getMetaData()->hotelsProSearchId,
                                $booking->getMetaData()->cityId
                            );
                            break;
                    }
                    $booking->setHotel($hotel);
                }
            }
            
            return $bookings->getValues();
        }

        return [];
    }

    /**
     * Get Booking Refund Amount
     * @ApiDoc(resource=true,
     *     section="Booking")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param null $userId
     * @param $id
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws \RuntimeException
     * @throws Exception
     * @return NotFoundHttpException|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getUserBookingRefundamountAction($userId = null, $id){
        $currentUserId = $this->getUser()->getId();

        $isAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN');
        if (!$isAdmin) {
            if ($currentUserId != $userId) {
                return $this->createAccessDeniedException('You can see only your booking requests');
            }
        }

        /** @var BookingRequest $booking */
        $booking = $this->get('app.repo.booking_request')->findById($id);

        if ($booking == null) {
            throw $this->createNotFoundException('Booking request not found');
        }

        if($booking->getMetaData()->serviceName !== "hotelsPro"){
            throw new Exception('Method allowed only for HotelsPro.');
        }

        if (!$isAdmin) {
            $owner = $booking->getUser();
            if (!$owner) {
                throw new \RuntimeException(
                    sprintf(
                        'Booking owner not found for booking %s',
                        $booking->getPublicBookingId()
                    )
                );
            }

            if ($owner->getId() != $currentUserId) {
                return $this->createAccessDeniedException('Access denied');
            }
        }


        try{
            $cancellationPolicyJSON = $booking->getHotelsCancellationPolicy()[0]['remarks'];
            $checkIn_date = $booking->getTimeSpanStartDate()->format('d-m-Y');
            $fullBookingAmount = $booking->getPayuAmount();
            $nightlyArray = $booking->getRatesPerNight();
            $refundAmount = $this->get('api.service.cancellation_client.hotels_pro')->getRefundAmount(
                $cancellationPolicyJSON,
                $checkIn_date,
                $fullBookingAmount,
                $nightlyArray
            );

        return new JsonResponse(['refund_amount'=>$refundAmount],200);

        } catch (BookingCancellationException $e) {
            return new JsonResponse(
                [
                    'code' => 500,
                    'errors' => ExceptionsUtil::getExceptionsPipe($e),
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
    /**
     * Delete booking
     * @ApiDoc(resource=true,
     *     section="Booking")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param null $userId
     * @param $id
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws \RuntimeException
     */
    public function deleteUserBookingAction($userId = null, $id)
    {

        $currentUserId = $this->getUser()->getId();

        $isAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN');
        if (!$isAdmin) {
            if ($currentUserId != $userId) {
                throw $this->createAccessDeniedException('You can remove only your booking requests');
            }
        }


        /** @var BookingRequest $booking */
        $booking = $this->get('app.repo.booking_request')->findById($id);

        if ($booking == null) {
            throw $this->createNotFoundException('Booking request not found');
        }

        if (!$isAdmin) {
            $owner = $booking->getUser();
            if (!$owner) {
                throw new \RuntimeException(
                    sprintf(
                        'Booking owner not found for booking %s',
                        $booking->getPublicBookingId()
                    )
                );
            }

            if ($owner->getId() != $currentUserId) {
                throw $this->createAccessDeniedException('Access denied');
            }
        }

        $cancellationClient = $this->get('api.service.cancellation_client.common');
        try {
            $cancellationClient->cancelBooking($booking);
            
            return new JsonResponse([
                'message' => $cancellationClient->getCancellationResponseMessage($booking->getPublicBookingId()),
            ]);
            
        } catch (BookingCancellationException $e) {
            return new JsonResponse(
                [
                    'code' => 500,
                    'errors' => ExceptionsUtil::getExceptionsPipe($e),
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param null $userId
     * @param $id
     * @return JsonResponse
     */
    public function optionsUserBookingAction($userId = null, $id)
    {
        return new JsonResponse();
    }

    /**
     * Get booking request details
     *
     * @ApiDoc(
     *     resource=true,
     *     section="Booking"
     * )
     *
     * @Security("has_role('ROLE_USER')")
     * @param $userId
     * @param $id
     * @return null|BookingRequest
     */
    public function getUserBookingrequestAction($userId = null, $id)
    {
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();

        if (!$user) {
            return $this->createAccessDeniedException();
        }
        $booking = $this->get('app.repo.booking_request')->find($id);
        if ($booking->getUser()->getId() != $user->getId()) {
            throw $this->createAccessDeniedException();
        }

        return $booking;


    }

}
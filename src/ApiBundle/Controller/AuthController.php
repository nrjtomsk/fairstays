<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 11:07
 */

namespace ApiBundle\Controller;


use ApiBundle\Exception\InvalidCredentialsException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Authenticate user:
 * sends secret code
 * gets api token
 *
 * Class AuthController
 * @package ApiBundle\Controller
 */
class AuthController extends FOSRestController
{

    /**
     * Send secret code request
     *
     * @Route("/secret", methods={"GET"})
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Auth"
     * )
     *
     * @Rest\QueryParam(name="phone_number", nullable=false, description="Phone number")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function getSecretAction(ParamFetcherInterface $fetcher)
    {
        $phoneNumber = $fetcher->get('phone_number', $strict = true);
        $status = $this->get('api.service.handler.auth')->sendSecret($phoneNumber);

        return ['status' => $status];
    }

    /**
     * Get api token
     *
     * @Route("/token", methods={"GET"})
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Auth"
     * )
     *
     * @Rest\QueryParam(name="phone_number", nullable=false, allowBlank=false, description="Phone number")
     * @Rest\QueryParam(name="secret", nullable=false, allowBlank=false, description="Secret code from SMS message")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function getAuthTokenAction(ParamFetcherInterface $fetcher)
    {
        $phoneNumber = $fetcher->get('phone_number', $strict = true);
        $secret = $fetcher->get('secret', $strict = true);
        try {
            $authToken = $this->get('api.service.handler.auth')->getAuthToken($phoneNumber, $secret);

            return [
                'authToken' => $authToken->getToken(),
            ];
        } catch (InvalidCredentialsException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * Get api token by Google idToken
     *
     * @Route("/tokenByGoogle", methods={"GET"})
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Auth"
     * )
     *
     * @Rest\QueryParam(name="idToken", nullable=false, allowBlank=false, description="Google idToken")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     * @throws BadRequestHttpException
     * @throws ServiceUnavailableHttpException
     */
    public function getAuthTokenByGoogleAction(ParamFetcherInterface $fetcher)
    {
        try {
            $authToken = $this->get('api.service.handler.auth')->getAuthTokenGoogle($fetcher->get('idToken'));

            return new JsonResponse(
                [
                    'authToken' => $authToken->getToken(),
                ]
            );
        } catch (InvalidCredentialsException $e) {
            throw new BadRequestHttpException($e->getMessage(), $e, Response::HTTP_BAD_REQUEST);
        } catch (\RuntimeException $e){
            throw new ServiceUnavailableHttpException(new \DateTime('+ 1 day'), $e->getMessage(), $e, $e->getCode());
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 17:23
 */

namespace ApiBundle\Exception;

/**
 * Class InvalidCredentialsException
 * @package ApiBundle\Exception
 */
class InvalidCredentialsException extends \Exception
{

}
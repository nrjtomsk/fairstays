<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 17:23
 */

namespace ApiBundle\Exception;

/**
 * Class BookingRequestNotFoundException
 * @package ApiBundle\Exception
 */
class BookingRequestNotFoundException extends \Exception
{

}
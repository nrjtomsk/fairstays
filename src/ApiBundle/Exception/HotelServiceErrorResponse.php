<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 17:23
 */

namespace ApiBundle\Exception;

/**
 * Class HotelServiceErrorResponse
 * @package ApiBundle\Exception
 */
class HotelServiceErrorResponse extends \Exception
{

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 24.12.2015
 * Time: 15:08
 */

namespace ApiBundle\Exception;

/**
 * Class RoomNotFound
 * @package ApiBundle\Exception
 */
class RoomNotFound extends \Exception
{

}
<?php
/**
 * Created by PhpStorm.
 * User: yarovikovyo
 * Date: 21.12.2015
 * Time: 13:48
 */

namespace ApiBundle\Exception;

/**
 * Class CityNotFoundException
 * @package ApiBundle\Exception
 */
class CityNotFoundException extends \Exception
{

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 22.01.2016
 * Time: 15:50
 */

namespace ApiBundle\Exception;


use AppBundle\Entity\BookingRequest;

class BookingRequestSaveException extends \RuntimeException
{
    /**
     * @var BookingRequest
     */
    private $bookingRequest;

    /**
     * BookingRequestSaveException constructor.
     * @param BookingRequest $bookingRequest
     */
    public function __construct(BookingRequest $bookingRequest, \Exception $prev = null)
    {
        parent::__construct(
            sprintf('Error on save booking request: %s', $bookingRequest->getId()),
            $code = 0,
            $prev
        );
        $this->bookingRequest = $bookingRequest;
    }

    /**
     * @return BookingRequest
     */
    public function getBookingRequest()
    {
        return $this->bookingRequest;
    }
}
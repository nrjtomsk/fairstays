<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 12:15
 */

namespace ApiBundle\Exception;


use Symfony\Component\Form\FormInterface;

/**
 * Class InvalidFormException
 * @package ApiBundle\Exception
 */
class InvalidFormException extends \InvalidArgumentException
{
    /**
     * @var FormInterface
     */
    private $form;

    /**
     * @inheritdoc
     * InvalidFormException constructor.
     * @param FormInterface $form
     */
    public function __construct(FormInterface $form)
    {
        parent::__construct('Invalid form');
        $this->form = $form;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}
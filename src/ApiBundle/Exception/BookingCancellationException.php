<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 12:47
 */

namespace ApiBundle\Exception;


use AppBundle\Entity\BookingRequest;
use Exception;

/**
 * Class BookingCancellationException
 * @package ApiBundle\Exception
 */
class BookingCancellationException extends \LogicException
{
    /**
     * @var BookingRequest
     */
    private $bookingRequest;

    /**
     * @inheritdoc
     * Construct the exception. Note: The message is NOT binary safe.
     * @link http://php.net/manual/en/exception.construct.php
     * @param BookingRequest $bookingRequest
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param Exception $previous [optional] The previous exception used for the exception chaining. Since 5.3.0
     * @since 5.1.0
     */
    public function __construct(
        BookingRequest $bookingRequest,
        $message = null,
        $code = null,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->bookingRequest = $bookingRequest;
    }

    /**
     * @return BookingRequest
     */
    public function getBookingRequest()
    {
        return $this->bookingRequest;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 30.09.2015
 * Time: 11:39
 */

namespace ApiBundle\Service\Hotels\Details;


use ApiBundle\DTO\HotelDetailsFilter;

/**
 * Interface Client
 * @package ApiBundle\Service\Hotels\Details
 */
interface Client
{
    /**
     * @param HotelDetailsFilter $filter
     * @return mixed
     */
    public function get(HotelDetailsFilter $filter);
}
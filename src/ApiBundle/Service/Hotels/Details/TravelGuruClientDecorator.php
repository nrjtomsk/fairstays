<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 09.12.2015
 * Time: 14:49
 */

namespace ApiBundle\Service\Hotels\Details;


use ApiBundle\DTO\HotelDetailsFilter;

/**
 * Class TravelGuruClientDecorator
 * @package ApiBundle\Service\Hotels\Details
 */
class TravelGuruClientDecorator implements Client
{

    /**
     * @var Client
     */
    private $client;

    private $travelGuruInterestPercent;

    /**
     * TravelGuruClientDecorator constructor.
     * @param Client $client
     * @param $travelGuruInterestPercent
     */
    public function __construct(Client $client, $travelGuruInterestPercent)
    {
        $this->client = $client;
        $this->travelGuruInterestPercent = $travelGuruInterestPercent;
    }


    /**
     * @param HotelDetailsFilter $filter
     * @return mixed
     */
    public function get(HotelDetailsFilter $filter)
    {
        $resultForHandle = $this->client->get($filter);

        if ($resultForHandle['status']) {
            $resultForHandle['data'] = $this->mapHotelDetailsForPrice($resultForHandle['data']);
        }

        return $resultForHandle;

    }

    /**
     * @param $hotelDetails
     * @return mixed
     */
    private function mapHotelDetailsForPrice($hotelDetails)
    {
        if (isset($hotelDetails['rooms']['pricePerDay']) && is_array($hotelDetails['rooms']['pricePerDay'])) {
            $multiply = 1 + $this->travelGuruInterestPercent/100;
            foreach ($hotelDetails['rooms']['pricePerDay'] as &$priceData) {
                if (isset($priceData['priceFinal'])) {
                    $priceData['priceFinalBefore'] = $priceData['priceFinal'];
                    $price = $priceData['priceFinal'] * $multiply;
                    $priceData['priceFinal'] = ceil($price * 100)/100;
                }
                if (isset($priceData['additionalGuestsAmount'])) {
                    $priceData['additionalGuestsAmountBefore'] = $priceData['additionalGuestsAmount'];
                    $additionalGuestsAmount = $priceData['additionalGuestsAmount'] * $multiply;
                    $priceData['additionalGuestsAmount'] = ceil($additionalGuestsAmount * 100)/100;
                }
                if (isset($priceData['taxAmount'])) {
                    $priceData['taxAmountBefore'] = $priceData['taxAmount'];
                    $taxAmount = $priceData['taxAmount'];
                    $priceData['taxAmount'] = ceil($taxAmount * 100)/100;
                }
                if (isset($priceData['discount'])) {
                    $priceData['discountBefore'] = $priceData['discount'];
                    $discount = $priceData['discount'];
                    $priceData['discount'] = ceil($discount * 100)/100;
                }
            }
        }

        return $hotelDetails;
    }
}
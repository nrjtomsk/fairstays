<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 30.09.2015
 * Time: 11:40
 */

namespace ApiBundle\Service\Hotels\Details;


use ApiBundle\Service\Utils\StringUtils;

/**
 * Gets response from Api
 *
 * Class Response
 * @package ApiBundle\Service\Hotels\Details
 */
class Response
{
    private $error;

    private $hotelDetails = [];

    /**
     * Gets response from Api
     *
     * @param string $content
     * @return Response
     */
    public static function fromApiResponse($content)
    {
        $array = StringUtils::xmlStringToArray($content);
        $responseData = $array['soap:Body']['OTA_HotelAvailRS'];

        $response = new Response();

        if (array_key_exists('Success', $responseData)) {

            $response->setHotelDetails(self::mapApiResponseToHotelDetails($responseData['RoomStays']['RoomStay']));

        } elseif (isset($responseData['Errors']['Error']['@attributes'])) {

            $response->setError(
                [
                    'code' => 400,
                    'message' => $responseData['Errors']['Error']['@attributes']['ShortText'],
                    'tgType' => $responseData['Errors']['Error']['@attributes']['Type'],
                    'tgCode' => $responseData['Errors']['Error']['@attributes']['Code'],
                ]
            );
        } else {
            $response->setError(
                [
                    'code' => 400,
                    'message' => 'Cannot parse response',
                ]
            );
        }

        return $response;
    }

    /**
     * Map Api response to hotelDetails
     *
     * @param $roomStay
     * @return array
     */
    private static function mapApiResponseToHotelDetails($roomStay)
    {
        $hotel = [];
        $hotel['hotelCode'] = $roomStay['BasicPropertyInfo']['@attributes']['HotelCode'];
        $hotel['hotelName'] = $roomStay['BasicPropertyInfo']['@attributes']['HotelName'];
        if (isset($roomStay['BasicPropertyInfo']['Award']['@attributes']['Rating'])) {
            $hotel['rating'] = $roomStay['BasicPropertyInfo']['Award']['@attributes']['Rating'];
        } else {
            $hotel['rating'] = 0;
        }
        if (isset($roomStay['BasicPropertyInfo']['Position']['@attributes'])) {
            $hotel['latitude'] = (float)$roomStay['BasicPropertyInfo']['Position']['@attributes']['Latitude'];
            $hotel['longitude'] = (float)$roomStay['BasicPropertyInfo']['Position']['@attributes']['Longitude'];
        } else {
            $hotel['latitude'] = 0;
            $hotel['longitude'] = 0;
        }
        $hotel['address'] = $roomStay['BasicPropertyInfo']['Address'];
        if (is_array($hotel['address']['AddressLine'])) {
            $hotel['address']['AddressLine'] = $hotel['address']['AddressLine'][0];
        }

        if (array_key_exists('TPA_Extensions', $roomStay)) {

            $hotel['site'] = $roomStay['TPA_Extensions']['DeepLinkInformation']['@attributes']['overviewURL'];
            $hotel['phone'] = 'TODO';
            $hotel['numberOfRooms'] = self::getNubmerOfRooms($roomStay);
            $hotel['description'] = isset($roomStay['TPA_Extensions']['HotelBasicInformation']['@attributes']['Description'])
                ? $roomStay['TPA_Extensions']['HotelBasicInformation']['@attributes']['Description']
                : '';

            $mapAmenity = function ($item) {
                if (isset($item['description']) && is_string($item['description'])) {
                    return $item['description'];
                }

                return isset($item['@attributes']['description']) ? $item['@attributes']['description'] : '';
            };

            if (isset($roomStay['TPA_Extensions']['HotelBasicInformation']['Amenities']['PropertyAmenities'])) {
                $propertyAmenities = $roomStay['TPA_Extensions']['HotelBasicInformation']['Amenities']['PropertyAmenities'];
                $hotel['amenities']['property'] = array_values(
                    array_map(
                        $mapAmenity,
                        $propertyAmenities
                    )
                );
            }

            if (isset($roomStay['TPA_Extensions']['HotelBasicInformation']['Amenities']['RoomAmenities'])) {
                $roomAmenities = $roomStay['TPA_Extensions']['HotelBasicInformation']['Amenities']['RoomAmenities'];
                $hotel['amenities']['room'] = array_values(
                    array_map(
                        $mapAmenity,
                        $roomAmenities
                    )
                );
            }
        }


        $ratePlans = [];
        if (isset($roomStay['RatePlans']['RatePlan'])) {
            if (isset($roomStay['RatePlans']['RatePlan']['@attributes'])) {

                $plan = $roomStay['RatePlans']['RatePlan'];
                $ratePlans[] = self::getRatePlan($plan);

            } else {
                foreach ($roomStay['RatePlans']['RatePlan'] as $plan) {
                    if (isset($plan['@attributes'])) {
                        $ratePlans[] = self::getRatePlan($plan);
                    }
                }
            }
        }


        $roomTypes = [];
        if (isset($roomStay['RoomTypes']['RoomType'])) {

            if (isset($roomStay['RoomTypes']['RoomType']['@attributes'])) {

                $roomTypes[] = self::getRoomType($roomStay['RoomTypes']['RoomType']);

            } else {

                foreach ($roomStay['RoomTypes']['RoomType'] as $roomType) {
                    if (array_key_exists('@attributes', $roomType)) {

                        $currentRoomType = self::getRoomType($roomType);
                        $roomTypes[] = $currentRoomType;
                    }
                }
            }
        }


        $pricesPerDay = [];
        if (isset($roomStay['RoomRates']['RoomRate'])) {

            if (isset($roomStay['RoomRates']['RoomRate']['Rates']['Rate'])) {

                $price = $roomStay['RoomRates']['RoomRate'];
                $pricePerDay = self::getPricePerDay($price);
                if ($pricePerDay !== null) {

                    $pricesPerDay[] = $pricePerDay;
                }
            } else {

                foreach ($roomStay['RoomRates']['RoomRate'] as $price) {

                    $pricePerDay = self::getPricePerDay($price);
                    if ($pricePerDay !== null) {

                        $pricesPerDay[] = $pricePerDay;
                    }
                }
            }
        }


        $hotel['rooms'] = [
            'ratePlan' => $ratePlans,
            'roomType' => $roomTypes,
            'pricePerDay' => $pricesPerDay,
        ];

        return $hotel;
    }

    /**
     * @param array $roomType
     * @return array
     */
    private static function getRoomType($roomType)
    {
        $currentRoomType = [
            'roomTypeCode' => $roomType['@attributes']['RoomTypeCode'],
            'nonSmoking' => $roomType['@attributes']['NonSmoking'],
            'roomTypeTitle' => $roomType['@attributes']['RoomType'],
            'images' => [],
            'description' => $roomType['RoomDescription']['Text'],
            'occupancy' => $roomType['TPA_Extensions']['RoomType']['@attributes'],
        ];

        if (isset($roomType['RoomDescription']['Image'])) {

            foreach ($roomType['RoomDescription']['Image'] as $image) {
                if (is_string($image)) {
                    $currentRoomType['images'][] = $image;
                }
            }
        }

        return $currentRoomType;
    }

    /**
     * @param $plan
     * @return array
     */
    private static function getRatePlan($plan)
    {
        $ratePlan = [
            'ratePlanCode' => $plan['@attributes']['RatePlanCode'],
            'ratePlanName' => $plan['@attributes']['RatePlanName'],
            'cancellationPolicy' => $plan['CancelPenalties']['CancelPenalty'],
        ];

        if (isset($plan['RatePlanInclusions']['RatePlanInclusionDesciption']['Text'])) {

            $ratePlan['ratePlanInclusionDescription'] = array_map(function($item) {
                return trim($item, " \t\n\r\0\x0B,;:");
            }, array_values(
                array_filter(
                    $plan['RatePlanInclusions']['RatePlanInclusionDesciption']['Text'],
                    function ($element) {
                        return is_string($element);
                    }
                )
            ));
        }

        if (isset($ratePlan['cancellationPolicy']['PenaltyDescription']['Text'])) {
            $ratePlan['cancellationPolicy']['PenaltyDescription'] = [$ratePlan['cancellationPolicy']['PenaltyDescription']];
        }

        return $ratePlan;
    }

    /**
     * @param $price
     * @return array|null
     */
    private static function getPricePerDay($price)
    {
        if (isset($price['Rates']['Rate'])) {

            $pricePerDay = [
                'date' => $price['Rates']['Rate']['@attributes']['EffectiveDate'],
                'priceFinal' => (float)$price['Rates']['Rate']['Base']['@attributes']['AmountBeforeTax'],
                'ratePlanCode' => $price['@attributes']['RatePlanCode'],
                'roomTypeCode' => $price['Rates']['Rate']['TPA_Extensions']['Rate']['@attributes']['RoomTypeCode'],
                'additionalGuestsAmount' => (float)self::getAdditionalGuestAmount($price),
                'discount' => (float)self::getDiscount($price),
                'taxAmount' => (float)self::getTaxAmount($price),
            ];

            return $pricePerDay;
        }

        return null;
    }

    /**
     * @param $price
     * @return int
     */
    private static function getAdditionalGuestAmount($price)
    {
        $additionalSum = 0;

        if (isset($price['Rates']['Rate']['AdditionalGuestAmounts']['AdditionalGuestAmount']['Amount']['@attributes']['AmountBeforeTax'])) {

            $additionalSum = $price['Rates']['Rate']['AdditionalGuestAmounts']['AdditionalGuestAmount']['Amount']['@attributes']['AmountBeforeTax'];

        } elseif (isset($price['Rates']['Rate']['AdditionalGuestAmounts']['AdditionalGuestAmount'])) {

            $additionalSum = 0;

            foreach ($price['Rates']['Rate']['AdditionalGuestAmounts']['AdditionalGuestAmount'] as $amount) {

                if (isset($amount['Amount']['@attributes']['AmountBeforeTax'])) {
                    $additionalSum += (float)$amount['Amount']['@attributes']['AmountBeforeTax'];
                }
            }
        }

        return $additionalSum;
    }

    /**
     * @param $price
     * @return int
     */
    private static function getDiscount($price)
    {
        if (isset($price['Rates']['Rate']['Discount']['@attributes']['AmountBeforeTax'])) {
            return $price['Rates']['Rate']['Discount']['@attributes']['AmountBeforeTax'];
        }

        return 0;
    }

    /**
     * @param $price
     * @return int
     */
    private static function getTaxAmount($price)
    {
        if (isset($price['Rates']['Rate']['Base']['Taxes']['Tax']['@attributes']['Amount'])) {
            return $price['Rates']['Rate']['Base']['Taxes']['Tax']['@attributes']['Amount'];
        }

        return 0;
    }

    /**
     * @param $roomStay
     * @return null|string
     */
    private static function getNubmerOfRooms($roomStay)
    {
        if (isset($roomStay['TPA_Extensions']['HotelBasicInformation']['@attributes']['NumberOfRooms'])) {
            return $roomStay['TPA_Extensions']['HotelBasicInformation']['@attributes']['NumberOfRooms'];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getHotelDetails()
    {
        return $this->hotelDetails;
    }

    /**
     * @param mixed $hotelDetails
     */
    public function setHotelDetails($hotelDetails)
    {
        $this->hotelDetails = $hotelDetails;
    }

}
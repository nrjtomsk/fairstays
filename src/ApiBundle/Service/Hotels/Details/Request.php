<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 30.09.2015
 * Time: 11:40
 */

namespace ApiBundle\Service\Hotels\Details;


use ApiBundle\DTO\HotelDetailsFilter;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Util\LoggerAwareTrait;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

/**
 * Builds from segments and sends total request body
 *
 * Class Request
 * @package ApiBundle\Service\Hotels\Details
 */
class Request
{
    use LoggerAwareTrait;

    /**
     * @var HotelDetailsFilter
     */
    private $hotelDetailsFilter;

    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * Request constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }

    /**
     * @param HotelDetailsFilter $hotelDetailsFilter
     */
    public function setHotelDetailsFilter(HotelDetailsFilter $hotelDetailsFilter)
    {
        $this->hotelDetailsFilter = $hotelDetailsFilter;
    }

    /**
     * Sends total request body
     * @return Response
     */
    public function send()
    {
        try {
            $requestBody = $this->buildRequestBody();
            $apiResponse = (new \GuzzleHttp\Client())->send(
                new GuzzleRequest(
                    $method = 'POST',
                    $this->endPoint,
                    $headers = [
                        'Content-Type' => 'text/xml; charset=UTF-8',
                        'Content-Encoding' => 'UTF-8',
                        'Accept-Encoding' => 'gzip,deflate',
                    ],
                    $requestBody
                )
            );

            $content = $apiResponse->getBody()->getContents();
            $this->logInfo($content);

            return Response::fromApiResponse($content);

        } catch (\RuntimeException $e) {
            $this->logException($e);
            throw $e;
        } catch (\InvalidArgumentException $e) {
            $this->logException($e);
            throw $e;
        }
    }

    /**
     * Builds and returns total request body
     *
     * @return string
     */
    private function buildRequestBody()
    {
        return '
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05" RequestedCurrency="'.HotelsListApiManagerTravelGuru::INPUT_CURRENCY.'" SortOrder="'.$this->hotelDetailsFilter->sortOrder.'" CorrelationID="T199V222931099425307077331050305561683" SearchCacheLevel="Live">
                '.$this->buildRequestSegmentsBody().'
                </OTA_HotelAvailRQ>
            </soap:Body>
        </soap:Envelope>';
    }

    /**
     * Builds and returns request body of segments
     *
     * @return string
     */
    private function buildRequestSegmentsBody()
    {
        return '
        <AvailRequestSegments>
            <AvailRequestSegment>
                <HotelSearchCriteria>
                    <Criterion>
                        <HotelRef HotelCode="'.$this->hotelDetailsFilter->hotelCode.'" />
                        <StayDateRange Start="'.$this->hotelDetailsFilter->stayDateRangeStart.'" End="'.$this->hotelDetailsFilter->stayDateRangeEnd.'"/>
                        <RoomStayCandidates>'.$this->buildRequestRoomStayCandidatesBody().'</RoomStayCandidates>
                        <TPA_Extensions SeoEnabled="true">
                        <UserAuthentication password="'.$this->authPassword.'" propertyId="'.$this->authPropertyId.'" username="'.$this->authUsername.'"/>
                            <Pagination hotelsFrom="0" hotelsTo="0"/>
                        </TPA_Extensions>
                    </Criterion>
                </HotelSearchCriteria>
            </AvailRequestSegment>
        </AvailRequestSegments>';
    }

    /**
     * Builds and returns request body of roomStayCandidate
     *
     * @return string
     */
    private function buildRequestRoomStayCandidatesBody()
    {
        $result = '';
        if (count($this->hotelDetailsFilter->roomStayCandidates)) {

            foreach ($this->hotelDetailsFilter->roomStayCandidates as $candidate) {
                $roomCandidate = '
                    <RoomStayCandidate>
                        <GuestCounts>';

                if (array_key_exists('adultCount', $candidate) && $candidate['adultCount'] > 0) {
                    for ($i = 0; $i < $candidate['adultCount']; $i++) {
                        $roomCandidate .=
                            '<GuestCount AgeQualifyingCode="10"/>';
                    }
                }

                if (
                    array_key_exists('childrenCount', $candidate) && $candidate['childrenCount'] > 0
                    && array_key_exists('childrenAges', $candidate) && $candidate['childrenCount'] == count(
                        $candidate['childrenAges']
                    )
                ) {
                    for ($j = 0; $j < $candidate['childrenCount']; $j++) {
                        $roomCandidate .= sprintf(
                            '<GuestCount Age="%d" AgeQualifyingCode="8"/>',
                            $candidate['childrenAges'][$j]
                        );
                    }
                }

                $roomCandidate .= '
                        </GuestCounts>
                    </RoomStayCandidate>
                ';

                $result .= $roomCandidate;
            }


            return $result;
        }
        throw new \RuntimeException('Cannot build request body');
    }

    /**
     * @param \Exception $e
     */
    private function logException(\Exception $e)
    {
        if ($this->logger) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

}
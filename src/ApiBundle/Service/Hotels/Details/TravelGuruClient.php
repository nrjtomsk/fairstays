<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 30.09.2015
 * Time: 11:39
 */

namespace ApiBundle\Service\Hotels\Details;


use ApiBundle\DTO\HotelDetailsFilter;
use ApiBundle\Util\LoggerAwareTrait;

/**
 * Class TravelGuruClient
 * @package ApiBundle\Service\Hotels\Details
 */
class TravelGuruClient implements Client
{
    use LoggerAwareTrait;

    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * TravelGuruClient constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }


    /**
     * @param HotelDetailsFilter $filter
     * @return array
     */
    public function get(HotelDetailsFilter $filter)
    {
        $request = new Request(
            $this->endPoint,
            $this->authUsername,
            $this->authPropertyId,
            $this->authPassword
        );
        $request->setHotelDetailsFilter($filter);
        $request->setLogger($this->logger);
        $response = $request->send();
        if (!$response->getError()) {
            return [
                'status' => true,
                'data' => $response->getHotelDetails(),
            ];
        } else {
            return [
                'status' => false,
                'data' => $response->getError(),
            ];
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 28.09.2015
 * Time: 14:47
 */

namespace ApiBundle\Service\Hotels;


use ApiBundle\Exception\HotelServiceErrorResponseTravelGuru;
use ApiBundle\Service\Utils\StringUtils;

class TravelGuruSearchResponse
{

    const PRICE_KEY = 'price';
    const SUGGESTED_PRICE_KEY = 'suggestedPrice';

    /**
     * @var array
     */
    private $hotelCodes = [];

    /**
     * @var array
     */
    private $minPrices = [];

    /**
     * @var array
     */
    private $suggestedPrices = [];

    /**
     * @param string $content
     * @return TravelGuruSearchResponse
     * @throws HotelServiceErrorResponseTravelGuru
     */
    public static function fromApiResponse($content)
    {
        $array = StringUtils::xmlStringToArray($content);
        $responseData = $array['soap:Body']['OTA_HotelAvailRS'];

        $response = new TravelGuruSearchResponse();

        if (isset($responseData['Success'], $responseData['RoomStays']['RoomStay'])) {
            $response->setHotelCodes(self::mapApiResponseToHotelCodes($response, $responseData));

        } elseif (isset($responseData['Errors']['Error']['@attributes'])) {

            $errorAttributes = $responseData['Errors']['Error']['@attributes'];
            $messageParts = ['Error:'];

            if (isset($errorAttributes['Code'])) {
                $messageParts[] = $errorAttributes['Code'];
            }
            if (isset($errorAttributes['Type'])) {
                $messageParts[] = $errorAttributes['Type'];
            }
            if (isset($errorAttributes['ShortText'])) {
                $messageParts[] = $errorAttributes['ShortText'];
            }

            throw new HotelServiceErrorResponseTravelGuru(implode(' ', $messageParts));

        } else {
            throw new HotelServiceErrorResponseTravelGuru('Cannot parse response');
        }

        return $response;
    }

    private static function mapApiResponseToHotelCodes(TravelGuruSearchResponse $response, $responseData)
    {
        $hotelCodes = [];
        $getPrice = function ($rate) {

            $prices = [];
            $_getPrice = function ($rate) {
                if (isset($rate['Rates']['Rate']['Base']['@attributes']['AmountBeforeTax'])) {
                    return $rate['Rates']['Rate']['Base']['@attributes']['AmountBeforeTax'];
                }
            };
            $_getSuggestedPrice = function ($rate) {
                if (isset($rate['Rates']['Rate']['TPA_Extensions']['Rate']['Base']['@attributes']['AmountBeforeTax'])) {
                    return $rate['Rates']['Rate']['TPA_Extensions']['Rate']['Base']['@attributes']['AmountBeforeTax'];
                }
            };
            if (array_key_exists('Rates', $rate)) {

                $prices[static::PRICE_KEY] = $_getPrice($rate);
                $prices[static::SUGGESTED_PRICE_KEY] = $_getSuggestedPrice($rate);

                return $prices;
            } else {
                $minPrice = 99999999999;
                $suggestedPrice = 0;
                if (is_array($rate)) {
                    foreach ($rate as $rateItem) {
                        if (array_key_exists('Rates', $rateItem)) {
                            $_minPrice = $_getPrice($rateItem);
                            if ($_minPrice < $minPrice) {
                                $minPrice = $_minPrice;
                                $suggestedPrice = $_getSuggestedPrice($rateItem);
                            }
                        }
                    }
                }

                $prices[static::PRICE_KEY] = $minPrice;
                $prices[static::SUGGESTED_PRICE_KEY] = $suggestedPrice;

                return $prices;
            }
        };
        if (isset($responseData['RoomStays']['RoomStay']['BasicPropertyInfo']['@attributes']['HotelCode'])
            && count($responseData['RoomStays']['RoomStay']['RoomTypes'])
            && count($responseData['RoomStays']['RoomStay']['RatePlans'])
            && count($responseData['RoomStays']['RoomStay']['RoomRates'])
        ) {
            $code = $responseData['RoomStays']['RoomStay']['BasicPropertyInfo']['@attributes']['HotelCode'];

            $hotelCodes[] = $code;
            $prices = $getPrice($responseData['RoomStays']['RoomStay']['RoomRates']['RoomRate']);
            $response->minPrices[(int)$code] = $prices[static::PRICE_KEY];
            $response->suggestedPrices[(int)$code] = $prices[static::SUGGESTED_PRICE_KEY];

        } else {
            foreach ($responseData['RoomStays']['RoomStay'] as $item) {
                if (isset($item['BasicPropertyInfo']['@attributes']['HotelCode'])
                    && count($item['RoomTypes'])
                    && count($item['RatePlans'])
                    && count($item['RoomRates'])
                ) {
                    $code = $item['BasicPropertyInfo']['@attributes']['HotelCode'];

                    $hotelCodes[] = $code;
                    $prices = $getPrice($item['RoomRates']['RoomRate']);
                    $response->minPrices[(int)$code] = $prices[static::PRICE_KEY];
                    $response->suggestedPrices[(int)$code] = $prices[static::SUGGESTED_PRICE_KEY];
                }
            }
        }

        return $hotelCodes;
    }

    /**
     * @return mixed
     */
    public function getHotelCodes()
    {
        return $this->hotelCodes;
    }

    /**
     * @param mixed $hotelCodes
     * @return TravelGuruSearchResponse
     */
    public function setHotelCodes($hotelCodes)
    {
        $this->hotelCodes = $hotelCodes;

        return $this;
    }

    /**
     * @return array
     */
    public function getMinPrices()
    {
        return $this->minPrices;
    }

    /**
     * @return array
     */
    public function getSuggestedPrices()
    {
        return $this->suggestedPrices;
    }
}
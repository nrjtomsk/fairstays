<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.11.2015
 * Time: 15:38
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;

/**
 * Class MinimumPriceMarginThresoldRule
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
class MinimumPriceMarginThresoldRule extends FilterRule
{

    /**
     * @var float
     */
    private $limitTravelGuru;

    /**
     * @var float
     */
    private $limitHotelsPro;

    /**
     * @var bool
     */
    private $allowSuggestedZeroPriceHotel;

    /**
     * MinimumPriceMarginThresoldRule constructor.
     * @param $limitTravelGuru
     * @param $limitHotelsPro
     * @param $allowSuggestedZeroPriceHotel
     */
    public function __construct($limitTravelGuru, $limitHotelsPro, $allowSuggestedZeroPriceHotel)
    {
        $this->limitTravelGuru = $limitTravelGuru;
        $this->limitHotelsPro = $limitHotelsPro;
        $this->allowSuggestedZeroPriceHotel = $allowSuggestedZeroPriceHotel;
    }

    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        if ($this->allowSuggestedZeroPriceHotel && $hotel->suggestedPriceServiceProvider->getAmount() == 0) {
            return true;
        }
        switch ($hotel->metaData->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:
                return ($hotel->suggestedPriceServiceProvider->getAmount() > 0)
                    && (($hotel->suggestedPriceServiceProvider->getAmount() - $hotel->minRoomPriceServiceProvider->getAmount()) / $hotel->minRoomPriceServiceProvider->getAmount() >= $this->limitHotelsPro);
            case HotelsListApiManagerTravelGuru::NAME:
                return ($hotel->suggestedPriceServiceProvider->getAmount() > 0)
                    && (($hotel->suggestedPriceServiceProvider->getAmount() - $hotel->minRoomPriceServiceProvider->getAmount()) / $hotel->minRoomPriceServiceProvider->getAmount() >= $this->limitTravelGuru);
            default:
                return false;
        }

    }
}
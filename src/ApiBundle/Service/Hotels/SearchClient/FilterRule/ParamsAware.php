<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 17.12.2015
 * Time: 11:03
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\DTO\HotelSearchFilter;

/**
 * Interface ParamsAware
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
interface ParamsAware
{

    /**
     * @param HotelSearchFilter $params
     */
    public function setParams(HotelSearchFilter $params);

    /**
     * @return HotelSearchFilter
     */
    public function getParams();

}
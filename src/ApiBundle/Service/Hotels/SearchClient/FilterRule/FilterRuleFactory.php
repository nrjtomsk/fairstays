<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.03.16
 * Time: 13:00
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


abstract class FilterRuleFactory
{
    /**
     * @return FilterRule
     */
    public abstract function buildFilterRule();
}
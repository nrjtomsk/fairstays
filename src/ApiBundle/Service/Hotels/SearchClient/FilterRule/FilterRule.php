<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.11.2015
 * Time: 12:44
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

/**
 * Class FilterRule
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
abstract class FilterRule
{
    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public abstract function apply(HotelDTO $hotel);

}
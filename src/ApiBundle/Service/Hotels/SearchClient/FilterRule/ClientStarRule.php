<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 17.12.2015
 * Time: 11:17
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

/**
 * Class ClientStarRule
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
class ClientStarRule extends FilterRule implements ParamsAware
{
    use ParamsAwareTrait;

    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        return in_array($hotel->star, $this->getParams()->stars, false);
    }
}
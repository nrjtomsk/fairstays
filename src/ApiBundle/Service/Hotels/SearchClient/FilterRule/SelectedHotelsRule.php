<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 22.03.16
 * Time: 13:23
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use AppBundle\Entity\PriceSelectedHotels;


class SelectedHotelsRule extends FilterRule implements ParamsAware
{
    use ParamsAwareTrait;

    /**
     * @var PriceSelectedHotels
     */
    private $showHotels;

    /**
     * SelectedHotelsRule constructor.
     * @param PriceSelectedHotels|null $showHotels
     */
    public function __construct(PriceSelectedHotels $showHotels = null)
    {
        $this->showHotels = $showHotels;
    }

    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        switch ($hotel->metaData->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:
                if ($this->showHotels === null) {
                    return true;
                }
                if (in_array($hotel->code, $this->showHotels->getSelectedHotels(), true)) {
                    return true;
                }

                return false;

            case HotelsListApiManagerTravelGuru::NAME:
                return true;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 01.03.2016
 * Time: 16:24
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

class TripAdvisorRatingRule extends FilterRule
{

    private $displayNullRatingHotels;

    private $ratingLimit;

    /**
     * TripAdvisorRatingRule constructor.
     * @param $displayNullRatingHotels
     * @param $ratingLimit
     */
    public function __construct($displayNullRatingHotels, $ratingLimit)
    {
        $this->displayNullRatingHotels = $displayNullRatingHotels;
        $this->ratingLimit = $ratingLimit;
    }


    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        if ($hotel->tripAdvisorRating === null || ($hotel->tripAdvisorRating && $hotel->tripAdvisorRating['multiplied_rating'] === null)) {
            return $this->displayNullRatingHotels;
        } else {
            if ($hotel->tripAdvisorRating) {
                return $hotel->tripAdvisorRating['multiplied_rating'] >= $this->ratingLimit;
            } else {
                return $this->displayNullRatingHotels;
            }
        }
    }
}
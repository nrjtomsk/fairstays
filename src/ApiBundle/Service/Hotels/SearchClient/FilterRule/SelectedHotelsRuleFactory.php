<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.03.16
 * Time: 12:44
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use AppBundle\Repository\PriceSelectedHotelsRepository;

class SelectedHotelsRuleFactory extends FilterRuleFactory implements ParamsAware
{
    use ParamsAwareTrait;
    /**
     * PriceSelectedHotelsRepository $priceSelectedHotelsRepository
     */
    private $priceSelectedHotelsRepository;

    public function __construct(PriceSelectedHotelsRepository $priceSelectedHotelsRepository)
    {
        $this->priceSelectedHotelsRepository = $priceSelectedHotelsRepository;
    }

    /**
     * @return FilterRule
     */
    public function buildFilterRule()
    {
        $showHotel = $this->priceSelectedHotelsRepository->findOneByCityCodeAndCheckInDate(
            $this->getParams()->city->getHotelsProId(),
            $this->getParams()->stayDateRangeStart
        );

        return new SelectedHotelsRule($showHotel);
    }
}
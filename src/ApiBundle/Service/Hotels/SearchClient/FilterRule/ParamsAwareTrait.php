<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 17.12.2015
 * Time: 10:53
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\DTO\HotelSearchFilter;

/**
 * Class ParamsAwareTrait
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
trait ParamsAwareTrait
{

    /**
     * @var HotelSearchFilter
     */
    private $params;

    /**
     * @return HotelSearchFilter
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param HotelSearchFilter $params
     */
    public function setParams(HotelSearchFilter $params)
    {
        $this->params = $params;
    }
}
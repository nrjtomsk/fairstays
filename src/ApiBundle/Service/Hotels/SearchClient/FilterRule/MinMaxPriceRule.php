<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.11.2015
 * Time: 15:41
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

/**
 * Class MinMaxPriceRule
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
class MinMaxPriceRule extends FilterRule
{
    /**
     * @var float
     */
    private $minimumPrice;

    /**
     * @var float
     */
    private $maximumPrice;

    /**
     * MinMaxPriceRule constructor.
     * @param float $minimumPrice
     * @param float $maximumPrice
     */
    public function __construct($minimumPrice, $maximumPrice)
    {
        $this->minimumPrice = $minimumPrice;
        $this->maximumPrice = $maximumPrice;
    }


    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        return $hotel->priceInternal->getAmount() <= $this->maximumPrice && $hotel->priceInternal->getAmount() >= $this->minimumPrice;
    }
}
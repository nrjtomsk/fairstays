<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.01.2016
 * Time: 14:52
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

class PriceMarginFilterRule extends FilterRule
{

    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        return (boolean)$hotel->isMarginFormulaOk;
    }
}
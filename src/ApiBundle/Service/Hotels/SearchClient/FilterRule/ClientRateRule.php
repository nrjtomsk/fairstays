<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 17.12.2015
 * Time: 11:35
 */

namespace ApiBundle\Service\Hotels\SearchClient\FilterRule;


use ApiBundle\Service\HotelsList\HotelDTO;

/**
 * Class ClientRateRule
 * @package ApiBundle\Service\Hotels\SearchClient\FilterRule
 */
class ClientRateRule extends FilterRule implements ParamsAware
{
    use ParamsAwareTrait;

    /**
     * @param HotelDTO $hotel
     * @return boolean
     */
    public function apply(HotelDTO $hotel)
    {
        return $hotel->price->getAmount() >= $this->params->rateRangeMin
        && $hotel->price->getAmount() <= $this->params->rateRangeMax;
    }
}
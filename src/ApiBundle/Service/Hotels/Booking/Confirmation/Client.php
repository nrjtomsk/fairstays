<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 16:53
 */

namespace ApiBundle\Service\Hotels\Booking\Confirmation;


use ApiBundle\DTO\HotelConfirmationBookingRequestParams;

/**
 * Interface Client
 * @package ApiBundle\Service\Hotels\Booking\Confirmation
 */
interface Client
{
    /**
     * @param HotelConfirmationBookingRequestParams $params
     * @return array
     */
    public function getConfirmationRequestStatus(HotelConfirmationBookingRequestParams $params);

    /**
     * @param HotelConfirmationBookingRequestParams $params
     * @return TravelGuruResponse
     */
    public function getConfirmationResponse(HotelConfirmationBookingRequestParams $params);
}
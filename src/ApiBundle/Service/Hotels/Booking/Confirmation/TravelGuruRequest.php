<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 16:55
 */

namespace ApiBundle\Service\Hotels\Booking\Confirmation;


use ApiBundle\DTO\HotelConfirmationBookingRequestParams;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Request;

/**
 * Creates request body
 * Sends request
 *
 * Class TravelGuruRequest
 * @package ApiBundle\Service\Hotels\Booking\Confirmation
 */
class TravelGuruRequest
{
    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * @var HotelConfirmationBookingRequestParams
     */
    private $requestParams;

    /**
     * TravelGuruRequest constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }

    /**
     * @param HotelConfirmationBookingRequestParams $requestParams
     * @return TravelGuruRequest
     */
    public function setRequestParams(HotelConfirmationBookingRequestParams $requestParams)
    {
        $this->requestParams = $requestParams;

        return $this;
    }

    /**
     *  Sends request
     *
     * @return TravelGuruResponse
     * @throws \InvalidArgumentException
     */
    public function send()
    {
        $httpClient = new HttpClient([
            'timeout' => 90,
        ]);
        $httpRequestBody = $this->buildHttpRequestBody();
        $httpRequest = new Request(
            $method = 'POST',
            $this->endPoint,
            $headers = [
                'Content-Type' => 'text/xml; charset=UTF-8',
                'Content-Encoding' => 'UTF-8',
                'Accept-Encoding' => 'gzip,deflate',
            ],
            $httpRequestBody
        );

        $httpResponse = $httpClient->send($httpRequest);
        $result = TravelGuruResponse::fromApiResponse($httpResponse);

        return $result;
    }

    /**
     * Builds request body
     *
     * @return string
     */
    private function buildHttpRequestBody()
    {
        return '
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                    <OTA_HotelResRQ xmlns="http://www.opentravel.org/OTA/2003/05"
                        CorrelationID="'.$this->requestParams->correlationId.'"
                        TransactionIdentifier="'.$this->requestParams->transactionIdentifier.'"
                        Version="1.003">
                        <POS>
                            <Source ISOCurrency="'.$this->requestParams->requestedCurrency.'">
                                <RequestorID MessagePassword="'.$this->authPassword.'" ID="'.$this->authPropertyId.'">
                                    <CompanyName Code="'.$this->authUsername.'"></CompanyName>
                                </RequestorID>
                            </Source>
                        </POS>
                        <UniqueID Type="23" ID="'.$this->requestParams->bookingId.'" />
                        <HotelReservations>
                            <HotelReservation>
                                <ResGlobalInfo>
                                    <Guarantee GuaranteeType="'.$this->requestParams->guaranteeType.'" />
                                </ResGlobalInfo>
                            </HotelReservation>
                        </HotelReservations>
                    </OTA_HotelResRQ>
                </soap:Body>
            </soap:Envelope>';
    }
}
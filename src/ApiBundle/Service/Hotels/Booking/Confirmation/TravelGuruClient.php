<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 16:54
 */

namespace ApiBundle\Service\Hotels\Booking\Confirmation;


use ApiBundle\DTO\HotelConfirmationBookingRequestParams;

/**
 * Gets confirmation request, response and status of request
 *
 * Class TravelGuruClient
 * @package ApiBundle\Service\Hotels\Booking\Confirmation
 */
class TravelGuruClient implements Client
{
    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * TravelGuruClient constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }


    /**
     * @param HotelConfirmationBookingRequestParams $params
     * @return array
     */
    public function getConfirmationRequestStatus(HotelConfirmationBookingRequestParams $params)
    {


        try {
            $response = $this->getRequest($params)->send();
            if ($response->getError()) {
                return [
                    'status' => false,
                    'message' => $response->getError(),
                ];
            }
        } catch (\InvalidArgumentException $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'status' => true,
            'message' => null,
        ];

    }

    /**
     * @param HotelConfirmationBookingRequestParams $params
     * @return TravelGuruResponse
     */
    public function getConfirmationResponse(HotelConfirmationBookingRequestParams $params)
    {
        try {
            return $this->getRequest($params)->send();
        } catch (\InvalidArgumentException $e) {
            return (new TravelGuruResponse())->setError($e->getMessage());
        }
    }

    /**
     * @param HotelConfirmationBookingRequestParams $params
     * @return TravelGuruRequest
     */
    private function getRequest(HotelConfirmationBookingRequestParams $params)
    {
        $request = new TravelGuruRequest(
            $this->endPoint,
            $this->authUsername,
            $this->authPropertyId,
            $this->authPassword
        );
        $request->setRequestParams($params);

        return $request;
    }


}
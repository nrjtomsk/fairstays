<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 16:55
 */

namespace ApiBundle\Service\Hotels\Booking\Confirmation;


use ApiBundle\Service\Utils\StringUtils;
use GuzzleHttp\Psr7\Response;

/**
 * Gets response from Api
 *
 * Class TravelGuruResponse
 * @package ApiBundle\Service\Hotels\Booking\Confirmation
 */
class TravelGuruResponse
{
    private $error;

    /**
     * @var string
     */
    private $travelGuruBookingId;

    /**
     * @var float
     */
    private $amountAfterTax;

    /**
     * @var array
     */
    private $phoneNumbers;

    /**
     * @param Response $apiResponse
     * @return TravelGuruResponse
     * @throws \RuntimeException
     */
    public static function fromApiResponse(Response $apiResponse)
    {
        $responseContent = $apiResponse->getBody()->getContents();
        $responseArray = StringUtils::xmlStringToArray($responseContent);

        $response = new TravelGuruResponse();

        if (isset($responseArray['soap:Body']['OTA_HotelResRS']['Success'])
            && ($responseArrayBody = $responseArray['soap:Body']['OTA_HotelResRS'])
        ) {

            if (isset($responseArrayBody['HotelReservations']['HotelReservation'])
                && ($reservation = $responseArrayBody['HotelReservations']['HotelReservation'])
            ) {

                if (isset($reservation['UniqueID']['@attributes']['ID'])) {
                    $response->setTravelGuruBookingId($reservation['UniqueID']['@attributes']['ID']);
                }

                if (isset($reservation['RoomStays']['RoomStay'])
                    && ($roomStay = $reservation['RoomStays']['RoomStay'])
                ) {

                    if (isset($roomStay['Total']['@attributes']['AmountAfterTax'])) {
                        $response->setAmountAfterTax($roomStay['Total']['@attributes']['AmountAfterTax']);
                    }

                    if (isset($roomStay['BasicPropertyInfo']['ContactNumbers']['ContactNumber'])) {
                        $response->setPhoneNumbers(
                            array_map(
                                function ($item) {
                                    if (array_key_exists('@attributes', $item)) {
                                        return $item['@attributes'];
                                    }
                                },
                                $roomStay['BasicPropertyInfo']['ContactNumbers']['ContactNumber']
                            )
                        );
                    }
                }
            }

            return $response;
        }

        $response->setError(
            [
                'code' => 400,
                'message' => 'Error on confirmation response',
            ]
        );

        return $response;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     * @return TravelGuruResponse
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTravelGuruBookingId()
    {
        return $this->travelGuruBookingId;
    }

    /**
     * @param mixed $travelGuruBookingId
     * @return TravelGuruResponse
     */
    public function setTravelGuruBookingId($travelGuruBookingId)
    {
        $this->travelGuruBookingId = $travelGuruBookingId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmountAfterTax()
    {
        return $this->amountAfterTax;
    }

    /**
     * @param mixed $amountAfterTax
     * @return TravelGuruResponse
     */
    public function setAmountAfterTax($amountAfterTax)
    {
        $this->amountAfterTax = $amountAfterTax;

        return $this;
    }

    /**
     * @return array
     */
    public function getPhoneNumbers()
    {
        return $this->phoneNumbers;
    }

    /**
     * @param mixed $phoneNumbers
     * @return TravelGuruResponse
     */
    public function setPhoneNumbers($phoneNumbers)
    {
        $this->phoneNumbers = $phoneNumbers;

        return $this;
    }

}
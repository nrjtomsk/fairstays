<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 17:30
 */

namespace ApiBundle\Service\Hotels\Booking\Cancellation;


use ApiBundle\DTO\HotelCancellationBookingRequestParams;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use GuzzleHttp\Exception\ServerException;

/**
 * Gets cancellation request and response
 *
 * Class TravelGuruClient
 * @package ApiBundle\Service\Hotels\Booking\Cancellation
 */
class TravelGuruClient implements Client
{
    use LoggerAwareTrait;

    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * TravelGuruClient constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return TravelGuruResponse
     * @throws \InvalidArgumentException
     * @throws ServerException
     */
    public function getCancellationResponse(BookingRequest $bookingRequest)
    {
        $params = HotelCancellationBookingRequestParams::fromBookingRequest($bookingRequest);

        return $this->getRequest($params)->send();
    }

    /**
     * @param HotelCancellationBookingRequestParams $params
     * @return TravelGuruRequest
     */
    private function getRequest(HotelCancellationBookingRequestParams $params)
    {
        $request = new TravelGuruRequest(
            $this->endPoint,
            $this->authUsername,
            $this->authPropertyId,
            $this->authPassword
        );
        $request->setRequestParams($params);
        $request->setLogger($this->logger);

        return $request;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 17:30
 */

namespace ApiBundle\Service\Hotels\Booking\Cancellation;


use AppBundle\Entity\BookingRequest;
use GuzzleHttp\Exception\ServerException;

/**
 * Interface Client
 * @package ApiBundle\Service\Hotels\Booking\Cancellation
 */
interface Client
{
    /**
     * @param BookingRequest $bookingRequest
     * @return TravelGuruResponse
     * @throws \InvalidArgumentException
     * @throws ServerException
     */
    public function getCancellationResponse(BookingRequest $bookingRequest);
}
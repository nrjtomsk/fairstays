<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 17:31
 */

namespace ApiBundle\Service\Hotels\Booking\Cancellation;

use ApiBundle\DTO\HotelCancellationBookingRequestParams;
use ApiBundle\Util\LoggerAwareTrait;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;

/**
 * Creates request body
 * Sends request
 *
 * Class TravelGuruRequest
 * @package ApiBundle\Service\Hotels\Booking\Cancellation
 */
class TravelGuruRequest
{
    use LoggerAwareTrait;

    private $endPoint;
    private $authUsername;
    private $authPropertyId;
    private $authPassword;

    /**
     * @var HotelCancellationBookingRequestParams
     */
    private $requestParams;

    /**
     * TravelGuruRequest constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }

    /**
     * @param HotelCancellationBookingRequestParams $requestParams
     * @return TravelGuruRequest
     */
    public function setRequestParams(HotelCancellationBookingRequestParams $requestParams)
    {
        $this->requestParams = $requestParams;

        return $this;
    }

    /**
     * Sends request
     *
     * @return TravelGuruResponse
     * @throws \InvalidArgumentException
     */
    public function send()
    {
        $httpClient = new HttpClient();
        $httpRequestBody = $this->buildRequestBody();
        $httpRequestBody = str_replace("\r\n", '', $httpRequestBody);
        $httpRequest = new Request(
            $method = 'POST',
            $this->endPoint,
            $headers = [
                'Content-Type' => 'text/xml; charset=UTF-8',
                'Content-Encoding' => 'UTF-8',
                'Accept-Encoding' => 'gzip,deflate',
            ],
            $httpRequestBody
        );

        try {
            $httpResponse = $httpClient->send($httpRequest);
            $content = $httpResponse->getBody()->getContents();
            $this->logInfo($content);

            return TravelGuruResponse::fromApiResponse($content);
        } catch (ServerException $e) {
            if ($e->hasResponse()) {
                $this->logError($e->getResponse()->getBody()->getContents());
            }
            throw $e;
        }
    }

    /**
     * Builds request body
     *
     * @return string
     */
    private function buildRequestBody()
    {
        return '
            <soapenv:Envelope
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:ns="http://www.opentravel.org/OTA/2003/05">
                <soapenv:Header/>
                <soapenv:Body>
                    <ns:OTA_CancelRQ CancelType="Cancel"  Version="1.0" >
                        <ns:POS>
                            <ns:Source>
                                <ns:RequestorID ID="'.$this->authPropertyId.'" MessagePassword="'.$this->authPassword.'">
                                    <ns:CompanyName Code="'.$this->authUsername.'"></ns:CompanyName>
                                </ns:RequestorID>
                            </ns:Source>
                        </ns:POS>
                        <ns:UniqueID ID="'.$this->requestParams->bookingId.'" />
                        <ns:Verification>
                            <ns:PersonName>
                                <ns:Surname>'.$this->requestParams->profileLastName.'</ns:Surname>
                            </ns:PersonName>
                            <ns:Email>'.$this->requestParams->profileEmail.'</ns:Email>
                        </ns:Verification>
                        <ns:TPA_Extensions>
                            <ns:CancelDates>
                                './*$this->buildDatesRequestBody().*/
        '
                            </ns:CancelDates>
                        </ns:TPA_Extensions>
                    </ns:OTA_CancelRQ>
                </soapenv:Body>
            </soapenv:Envelope>
        ';
    }

}
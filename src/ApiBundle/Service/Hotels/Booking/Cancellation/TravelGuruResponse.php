<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 12.10.2015
 * Time: 17:31
 */

namespace ApiBundle\Service\Hotels\Booking\Cancellation;


use ApiBundle\Service\Utils\StringUtils;

/**
 * Gets response from Api
 *
 * Class TravelGuruResponse
 * @package ApiBundle\Service\Hotels\Booking\Cancellation
 */
class TravelGuruResponse
{
    private $error;

    /**
     * @param string $content
     * @return TravelGuruResponse
     * @throws \RuntimeException
     */
    public static function fromApiResponse($content)
    {
        $responseArray = StringUtils::xmlStringToArray($content)['soap:Body']['OTA_CancelRS'];

        $response = new TravelGuruResponse();

        if (isset($responseArray['Errors']['Error']['@attributes'])) {

            $attributes = $responseArray['Errors']['Error']['@attributes'];

            $error = 'Error: ';

            if (isset($attributes['ShortText'])) {
                $error .= $attributes['ShortText'].' ';
            }
            if (isset($attributes['Type'])) {
                $error .= $attributes['Type'].' ';
            }
            if (isset($attributes['Code'])) {
                $error .= $attributes['Code'];
            }

            $response->setError($error);
        }

        return $response;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }
}
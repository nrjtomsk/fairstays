<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 09.10.2015
 * Time: 13:36
 */

namespace ApiBundle\Service\Hotels\Booking\Provisional;


use ApiBundle\DTO\HotelProvisionalBookingRequestParams;
use ApiBundle\Util\LoggerAwareTrait;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Request;

/**
 * Builds and sends travelGuru request
 *
 * Class TravelGuruRequest
 * @package ApiBundle\Service\Hotels\Booking\Provisional
 */
class TravelGuruRequest
{
    use LoggerAwareTrait;

    private $endPoint;

    private $authUsername;
    private $authPropertyId;
    private $authPassword;


    /**
     * @var HotelProvisionalBookingRequestParams
     */
    private $requestParams;

    /**
     * TravelGuruRequest constructor.
     * @param $endPoint
     * @param $authUsername
     * @param $authPropertyId
     * @param $authPassword
     */
    public function __construct($endPoint, $authUsername, $authPropertyId, $authPassword)
    {
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
    }

    /**
     * @param HotelProvisionalBookingRequestParams $requestParams
     * @return TravelGuruRequest
     */
    public function setRequestParams(HotelProvisionalBookingRequestParams $requestParams)
    {
        $this->requestParams = $requestParams;

        return $this;
    }

    /**
     * Sends total body request
     *
     * @return TravelGuruResponse
     */
    public function send()
    {
        try {
            $requestBody = str_replace("\r\n", '', $this->buildRequestBody());

            $apiRequest = new Request(
                $method = 'POST',
                $this->endPoint,
                $headers = [
                    'Content-Type' => 'text/xml; charset=UTF-8',
                    'Content-Encoding' => 'UTF-8',
                    'Accept-Encoding' => 'gzip,deflate',
                ],
                $requestBody
            );

            $httpClient = new HttpClient();

            $apiResponse = $httpClient->send($apiRequest);

            $content = $apiResponse->getBody()->getContents();
            $this->logInfo($content);

            return TravelGuruResponse::fromApiResponse($content);

        } catch (\RuntimeException $e) {
            $this->logException($e);
            throw $e;
        } catch (\InvalidArgumentException $e) {
            $this->logException($e);
            throw $e;
        }
    }

    /**
     * Builds total body request
     *
     * @return string
     */
    private function buildRequestBody()
    {
        return '
        <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
            <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <OTA_HotelResRQ CorrelationID="'.$this->requestParams->correlationId.'" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05">
                    '.$this->buildRequestAuthBlockBody().'
                    <UniqueID Type="" ID="" />
                    <HotelReservations>
                        <HotelReservation>
                            <RoomStays>
                                <RoomStay>
                                    <RoomTypes>
                                        <RoomType NumberOfUnits="'.$this->requestParams->roomTypeNumberOfUnits.'" RoomTypeCode="'.$this->requestParams->roomTypeRoomTypeCode.'" />
                                    </RoomTypes>
                                    <RatePlans>
                                        <RatePlan RatePlanCode="'.$this->requestParams->ratePlanRatePlanCode.'" />
                                    </RatePlans>
                                    <GuestCounts IsPerRoom="false">
                                        '.$this->buildRequestGuestCountsBody().'
                                    </GuestCounts>
                                    <TimeSpan End="'.$this->requestParams->timeSpanEndDate.'" Start="'.$this->requestParams->timeSpanStartTime.'" />
                                    <Total AmountBeforeTax="'.$this->requestParams->totalAmountBeforeTax.'" CurrencyCode="'.$this->requestParams->requestedCurrency.'">
                                        <Taxes Amount="'.$this->requestParams->totalTaxesAmount.'" CurrencyCode="'.$this->requestParams->requestedCurrency.'" />
                                    </Total>
                                    <BasicPropertyInfo HotelCode="'.$this->requestParams->hotelCode.'" />
                                </RoomStay>
                            </RoomStays>
                            <ResGuests>
                                <ResGuest>
                                    <Profiles>
                                        <ProfileInfo>
                                            <Profile ProfileType="1">
                                                <Customer>
                                                    <PersonName>
                                                        <GivenName>'.$this->requestParams->profileGivenName.'</GivenName>
                                                        <Surname>'.$this->requestParams->profileLastName.'</Surname>
                                                    </PersonName>
                                                    <Telephone
                                                        CountryAccessCode="'.$this->requestParams->profilePhoneCountryAccessCode.'"
                                                        PhoneNumber="'.$this->requestParams->profilePhoneNumber.'"
                                                        PhoneTechType="'.$this->requestParams->profilePhoneTechType.'"/>
                                                    <Email>'.$this->requestParams->profileEmail.'</Email>
                                                    <Address>
                                                        <AddressLine>'.$this->requestParams->profileAddressLine1.'</AddressLine>
                                                        <AddressLine>'.$this->requestParams->profileAddressLine2.'</AddressLine>
                                                        <CityName>'.$this->requestParams->profileCityName.'</CityName>
                                                        <PostalCode>'.$this->requestParams->profilePostalCode.'</PostalCode>
                                                        <StateProv StateCode="'.$this->requestParams->profileAddressStateProvCode.'">'.$this->requestParams->profileAddressStateProv.'</StateProv>
                                                        <CountryName Code="'.$this->requestParams->profileCountryNameCode.'">'.$this->requestParams->profileCountryNameCode.'</CountryName>
                                                    </Address>
                                                </Customer>
                                            </Profile>
                                        </ProfileInfo>
                                    </Profiles>
                                </ResGuest>
                            </ResGuests>
                            <ResGlobalInfo>
                                <Guarantee GuaranteeType="'.$this->requestParams->guaranteeType.'" />
                            </ResGlobalInfo>
                        </HotelReservation>
                    </HotelReservations>
                </OTA_HotelResRQ>
            </s:Body>
        </s:Envelope>';
    }

    private function logException(\Exception $e)
    {
        if ($this->logger) {
            $this->logger->warning($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * Builds body request of AuthBlock
     * @return string
     */
    private function buildRequestAuthBlockBody()
    {
        return
            '<POS>
                <Source ISOCurrency="'.$this->requestParams->requestedCurrency.'">
                    <RequestorID MessagePassword="'.$this->authPassword.'" ID="'.$this->authPropertyId.'">
                        <CompanyName Code="'.$this->authUsername.'" />
                    </RequestorID>
                </Source>
            </POS>';
    }

    /**
     * Builds body request of guestCounts
     *
     * @return string
     * @throws \RuntimeException
     */
    private function buildRequestGuestCountsBody()
    {
        if (!count($this->requestParams->guestCounts)) {
            throw new \RuntimeException('Cannot build booking provisional request');
        }
        $body = '';
        foreach ($this->requestParams->guestCounts as $guestCount) {
            if ($guestCount['ageQualifyingCode'] == 10) {
                $body .= '
                <GuestCount
                    ResGuestRPH="'.$guestCount['resGuestRPH'].'"
                    AgeQualifyingCode="'.$guestCount['ageQualifyingCode'].'"
                    Count="'.$guestCount['count'].'"
                />';
            } elseif ($guestCount['ageQualifyingCode'] == 8) {

                if ((int)$guestCount['count'] === count($guestCount['age'])) {

                    if (is_array($guestCount['age'])) {

                        foreach ($guestCount['age'] as $age) {

                            $body .= '
                            <GuestCount
                            ResGuestRPH="'.$guestCount['resGuestRPH'].'"
                            AgeQualifyingCode="'.$guestCount['ageQualifyingCode'].'"
                            Age="'.$age.'"
                            Count="1"
                            />';
                        }
                    } else {

                        $body .= '
                        <GuestCount
                        ResGuestRPH="'.$guestCount['resGuestRPH'].'"
                        AgeQualifyingCode="'.$guestCount['ageQualifyingCode'].'"
                        Age="'.$guestCount['age'].'"
                        Count="1"
                        />';
                    }
                } else {
                    throw new \RuntimeException('Children count must be equal age count');
                }
            } else {
                throw new \RuntimeException('Invalid request data');
            }
        }

        return $body;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 09.10.2015
 * Time: 13:37
 */

namespace ApiBundle\Service\Hotels\Booking\Provisional;


use ApiBundle\Service\Utils\StringUtils;

/**
 * Gets response from Api
 *
 * Class TravelGuruResponse
 * @package ApiBundle\Service\Hotels\Booking\Provisional
 */
class TravelGuruResponse
{

    private $error;

    private $bookingId;

    private $transactionIdentifier;

    private $correlationId;

    /**
     * Returns response from Api
     *
     * @param string $content
     * @return TravelGuruResponse
     */
    public static function fromApiResponse($content)
    {
        $array = StringUtils::xmlStringToArray($content);

        $response = new TravelGuruResponse();
        $responseData = $array['soap:Body']['OTA_HotelResRS'];

        if (isset($responseData['Success'], $responseData['HotelReservations']['HotelReservation']['UniqueID']['@attributes']['ID'])) {

            $response->setBookingId(
                $responseData['HotelReservations']['HotelReservation']['UniqueID']['@attributes']['ID']
            );

            $response->setTransactionIdentifier($responseData['@attributes']['TransactionIdentifier']);
            $response->setCorrelationId($responseData['@attributes']['CorrelationID']);

        } elseif (isset($responseData['Errors']['Error']['@attributes'])) {

            $error = [
                'code' => 400,
            ];

            if (isset($responseData['Errors']['Error']['@attributes']['ShortText'])) {

                $error['message'] = $responseData['Errors']['Error']['@attributes']['ShortText'];
            }
            if (isset($responseData['Errors']['Error']['@attributes']['Type'])) {

                $error['tgType'] = $responseData['Errors']['Error']['@attributes']['Type'];
            }
            if (isset($responseData['Errors']['Error']['@attributes']['Code'])) {

                $error['tgCode'] = $responseData['Errors']['Error']['@attributes']['Code'];
            }

            $response->setError($error);

        } elseif (isset($responseData['Errors']['Error'])) {
            $response->setError(
                [
                    'code' => 400,
                    'message' => $responseData['Errors']['Error'],
                ]
            );
        } else {
            $response->setError(
                [
                    'code' => 400,
                    'message' => 'Cannot parse response',
                ]
            );
        }

        return $response;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     * @return TravelGuruResponse
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingId()
    {
        return $this->bookingId;
    }

    /**
     * @param mixed $bookingId
     * @return TravelGuruResponse
     */
    public function setBookingId($bookingId)
    {
        $this->bookingId = $bookingId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactionIdentifier()
    {
        return $this->transactionIdentifier;
    }

    /**
     * @param mixed $transactionIdentifier
     * @return TravelGuruResponse
     */
    public function setTransactionIdentifier($transactionIdentifier)
    {
        $this->transactionIdentifier = $transactionIdentifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * @param mixed $correlationId
     * @return TravelGuruResponse
     */
    public function setCorrelationId($correlationId)
    {
        $this->correlationId = $correlationId;

        return $this;
    }


}
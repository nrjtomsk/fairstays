<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 19.04.2016
 * Time: 15:37
 */

namespace ApiBundle\Service\Country\Exception;


class BadNationalityCodeException extends \InvalidArgumentException
{

}
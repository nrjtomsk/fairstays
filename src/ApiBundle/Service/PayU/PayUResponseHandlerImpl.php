<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 16:46
 */

namespace ApiBundle\Service\PayU;


use ApiBundle\DTO\PayUResponseObject;
use ApiBundle\Service\Booking\Exception\MakeBookingException;
use ApiBundle\Service\Booking\MakeBookingClient;
use ApiBundle\Service\Booking\MakeBookingHotelsProClient;
use ApiBundle\Service\Booking\MakeBookingTravelGuruClient;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use Symfony\Component\HttpFoundation\Request;

class PayUResponseHandlerImpl implements PayUResponseHandler
{
    use LoggerAwareTrait;

    /**
     * @var MakeBookingClient
     */
    private $travelGuruHandler;

    /**
     * @var MakeBookingClient
     */
    private $hotelsProHandler;

    /**
     * @var BookingRequestRepository
     */
    private $bookingRepository;

    /**
     * @var string
     */
    private $payuSalt;

    /**
     * PayUResponseHandlerImpl constructor.
     * @param MakeBookingClient $payuSuccessHandlerTravelGuru
     * @param MakeBookingClient $payuSuccessHandlerHotelsPro
     * @param BookingRequestRepository $bookingRepository
     * @param string $payuSalt
     */
    public function __construct(
        MakeBookingClient $payuSuccessHandlerTravelGuru,
        MakeBookingClient $payuSuccessHandlerHotelsPro,
        BookingRequestRepository $bookingRepository,
        $payuSalt
    ) {
        $this->travelGuruHandler = $payuSuccessHandlerTravelGuru;
        $this->hotelsProHandler = $payuSuccessHandlerHotelsPro;
        $this->bookingRepository = $bookingRepository;
        $this->payuSalt = $payuSalt;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function processSuccess(Request $request)
    {
        $responseObject = PayUResponseObject::fromPostRequest($request);

        $hashIsValid = $responseObject->isValidHash($this->payuSalt);

        $isProcessed = false;
        if ($hashIsValid) {

            /** @var BookingRequest $booking */
            $booking = $this->bookingRepository->find($responseObject->getBookingRequestId());
            if ($booking instanceof BookingRequest) {

                $booking->setStatus(BookingRequest::STATUS_PAYMENT_COMPLETE);
                $booking->setPayuAmount($responseObject->getAmount());
                $booking->setPayuMihPayId($responseObject->getMihPayId());
                $this->bookingRepository->save($booking);

                try {
                    $serviceName = $booking->getMetaData()->serviceName;
                    switch ($serviceName) {
                        case HotelsListApiManagerTravelGuru::NAME:
                            $this->travelGuruHandler->makeRealBooking($booking);
                            break;

                        case HotelsListApiManagerHotelsPro::NAME:
                            $this->hotelsProHandler->makeRealBooking($booking);
                            break;

                        default:
                            throw new \RuntimeException(sprintf('Invalid provider name: %s', $serviceName));
                            break;
                    }

                    if ($isProcessed) {
                        $booking->setStatus(BookingRequest::STATUS_CONFIRMED);
                        $this->bookingRepository->save($booking);
                    }

                } catch (MakeBookingException $e) {
                    $this->logError($e->getMessage(), $e->getTrace());
                    $this->bookingRepository->save($booking);
                }

            } else {
                $this->logError(sprintf('Booking #%s not found', $responseObject->getBookingRequestId()), [
                    'payuResponse' => $responseObject
                ]);
            }
        } else {
            $this->logError('Invalid payu hash', ['request' => $request, 'payuResponse' => $responseObject]);
        }
        $this->logInfo('Is processed: '.(string)((int)$isProcessed));

        return $isProcessed;
    }
}
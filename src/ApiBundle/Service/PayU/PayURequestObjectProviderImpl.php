<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 11:17
 */

namespace ApiBundle\Service\PayU;


use ApiBundle\DTO\PayURequestObject;
use AppBundle\Entity\BookingRequest;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class PayURequestObjectProviderImpl implements PayURequestObjectProvider
{

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $payuKey;

    /**
     * @var string
     */
    private $payuSalt;

    /**
     * @var string
     */
    private $environment;

    /**
     * PayURequestObjectProviderImpl constructor.
     * @param Router $router
     * @param string $payuKey
     * @param string $payuSalt
     * @param string $environment
     */
    public function __construct(Router $router, $payuKey, $payuSalt, $environment)
    {
        $this->router = $router;
        $this->payuKey = $payuKey;
        $this->payuSalt = $payuSalt;
        $this->environment = $environment;
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return PayURequestObject
     */
    public function getPayURequestObjectByBookingRequest(BookingRequest $bookingRequest)
    {
        $host = $this->getHost();

        $requestObject = PayURequestObject::fromBookingRequest($bookingRequest);
        $requestObject->setKey($this->payuKey);
        $requestObject->setSurl($host.$this->router->generate('payu_success', [], Router::NETWORK_PATH));
        $requestObject->setFurl($host.$this->router->generate('payu_failure', [], Router::NETWORK_PATH));
        $requestObject->setCurl($host.$this->router->generate('payu_cancel', [], Router::NETWORK_PATH));

        $requestObject->fillHashes($this->payuSalt);

        return $requestObject;
    }

    /**
     * @return string
     */
    private function getHost()
    {
        $host = 'https:';

        if ($this->environment === 'dev') {
            $host = 'http:';
        }

        return $host;
    }

}
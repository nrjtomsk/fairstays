<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 11:16
 */

namespace ApiBundle\Service\PayU;


use ApiBundle\DTO\PayURequestObject;
use AppBundle\Entity\BookingRequest;

interface PayURequestObjectProvider
{

    /**
     * @param BookingRequest $bookingRequest
     * @return PayURequestObject
     */
    public function getPayURequestObjectByBookingRequest(BookingRequest $bookingRequest);

}
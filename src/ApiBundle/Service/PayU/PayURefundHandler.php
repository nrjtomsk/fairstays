<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 01.12.2015
 * Time: 11:40
 */

namespace ApiBundle\Service\PayU;


use ApiBundle\Exception\BookingRequestNotFoundException;
use ApiBundle\Exception\NotRefundedException;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use GuzzleHttp\Client;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Refunds booking
 *
 * Class PayURefundHandler
 * @package ApiBundle\Service\PayU
 */
class PayURefundHandler
{
    use LoggerAwareTrait;

    /**
     * PayU Array response format property
     */
    const ARRAY_FORM = 1;

    /**
     * PayU Json response format property
     */
    const JSON_FORM = 2;

    /**
     * Refund command name
     */
    const COMMAND_REFUND = 'cancel_refund_transaction';

    /**
     * Command uri with needed query params
     */
    const COMMAND_URI = 'postservice.php?form=2';

    /**
     * @var BookingRequestRepository
     */
    private $bookingRequestRepository;

    /**
     * @var string
     */
    private $payuKey;

    /**
     * @var string
     */
    private $payuSalt;

    /**
     * @var string
     */
    private $payuMerchantUrl;

    /**
     * PayURefundHandler constructor.
     * @param BookingRequestRepository $bookingRequestRepository
     * @param string $payuKey
     * @param string $payuSalt
     * @param string $payuMerchantUrl
     */
    public function __construct(
        BookingRequestRepository $bookingRequestRepository,
        $payuKey,
        $payuSalt,
        $payuMerchantUrl
    ) {
        $this->bookingRequestRepository = $bookingRequestRepository;
        $this->payuKey = $payuKey;
        $this->payuSalt = $payuSalt;
        $this->payuMerchantUrl = $payuMerchantUrl;
    }

    /**
     * Refunds booking
     *
     * @param int $id BookingRequest id
     * @param int $userId
     * @throws BookingRequestNotFoundException
     * @throws NotRefundedException
     */
    public function refund($id, $userId)
    {
        $booking = $this->bookingRequestRepository->findOneBy(
            [
                'id' => $id,
                'user' => $userId,
            ]
        );

        if ($booking instanceof BookingRequest) {
            $this->refundBookingRequest($booking);
        } else {
            throw new BookingRequestNotFoundException('No booking for refund');
        }
    }

    /**
     * Sends refund booking request
     *
     * @param BookingRequest $bookingRequest
     * @throws NotRefundedException
     */
    public function refundBookingRequest(BookingRequest $bookingRequest)
    {
        $response = $this->sendRequest($bookingRequest);
        if ($response['status'] !== 1) {
            throw new NotRefundedException($response['msg']);
        }
    }

    /**
     * Sends bookingRequest and returns the decoded response
     *
     * @param BookingRequest $booking
     * @return array
     */
    private function sendRequest(BookingRequest $booking)
    {
        $this->checkPayment($booking);
        $client = new Client(['base_uri' => $this->payuMerchantUrl,]);

        $response = $client->post(
            static::COMMAND_URI,
            [
                'form_params' => $this->getFormParamsFromBooking($booking),
            ]
        );

        $responseContent = $response->getBody()->getContents();
        $this->logInfo($responseContent);
        $response = $this->decodeResponse($responseContent);

        return $response;
    }

    /**
     * Gets params from booking
     *
     * @param BookingRequest $booking
     * @return array
     */
    private function getFormParamsFromBooking(BookingRequest $booking)
    {
        $refundToken = (string)$booking->getId().$booking->getPayuMihPayId();

        return [
            'key' => $this->payuKey,
            'command' => static::COMMAND_REFUND,
            'hash' => $this->calculateHash($booking),
            'var1' => $booking->getPayuMihPayId(),
            'var2' => $refundToken,
            'var3' => $booking->getPayuAmount(),
        ];
    }

    /**
     * Returns the generated hash
     *
     * @param BookingRequest $booking
     * @return string
     */
    private function calculateHash(BookingRequest $booking)
    {
        $values = [
            $this->payuKey,
            static::COMMAND_REFUND,
            $booking->getPayuMihPayId(),
            $this->payuSalt,
        ];

        return hash('sha512', implode('|', $values));
    }

    /**
     * Returns the decoded response
     *
     * @param string $response
     * @return array
     */
    private function decodeResponse($response)
    {
        $encoder = new JsonEncoder();
        $response = $encoder->decode(
            $response,
            JsonEncoder::FORMAT,
            ['json_decode_options' => JSON_BIGINT_AS_STRING]
        );

        return $response;
    }

    /**
     * Returns the decoded check response
     *
     * @param BookingRequest $booking
     * @return array|\Psr\Http\Message\ResponseInterface
     */
    private function checkPayment(BookingRequest $booking)
    {
        $client = new Client(['base_uri' => $this->payuMerchantUrl,]);

        $response = $client->post(
            static::COMMAND_URI,
            [
                'form_params' => [
                    'key' => $this->payuKey,
                    'command' => 'check_payment',
                    'hash' => $this->calculateHashCheck($booking),
                    'var1' => $booking->getPayuMihPayId(),
                ],
            ]
        );

        $responseContent = $response->getBody()->getContents();
        $this->logInfo($responseContent);
        $response = $this->decodeResponse($responseContent);

        return $response;
    }

    /**
     * Returns the generated check hash
     *
     * @param BookingRequest $booking
     * @return string
     */
    private function calculateHashCheck(BookingRequest $booking)
    {
        $values = [
            $this->payuKey,
            'check_payment',
            $booking->getPayuMihPayId(),
            $this->payuSalt,
        ];

        return hash('sha512', implode('|', $values));
    }

}
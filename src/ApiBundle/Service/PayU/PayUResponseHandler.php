<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.10.2015
 * Time: 16:46
 */

namespace ApiBundle\Service\PayU;


use Symfony\Component\HttpFoundation\Request;

interface PayUResponseHandler
{

    /**
     * @param Request $request
     * @return bool
     */
    public function processSuccess(Request $request);

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 13:33
 */

namespace ApiBundle\Service\ApiHandler;


use ApiBundle\Exception\InvalidCredentialsException;
use ApiBundle\Service\SMS\Exception\SenderException;
use ApiBundle\Service\SMS\Message;
use ApiBundle\Service\SMS\Sender;
use AppBundle\Entity\AuthToken;
use AppBundle\Entity\SmsVerificationToken;
use AppBundle\Repository\AuthTokenRepository;
use AppBundle\Repository\SmsVerificationTokenRepository;
use AppBundle\Service\User\UserProvider;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use InvalidArgumentException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Generates OTP and authToken
 *
 * Class AuthHandler
 * @package ApiBundle\Service\ApiHandler
 */
class AuthHandler
{
    /**
     * @var Sender
     */
    private $sender;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SmsVerificationTokenRepository
     */
    private $smsVerificationTokenRepo;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var AuthTokenRepository
     */
    private $authTokenRepository;

    /**
     * @var string
     */
    private $googleVerifyURL;
    /**
     * AuthHandler constructor.
     * @param Sender $sender
     * @param LoggerInterface $logger
     * @param SmsVerificationTokenRepository $smsVerificationTokenRepo
     * @param UserProvider $userProvider
     * @param AuthTokenRepository $authTokenRepository
     * @param $googleVerifyURL
     */
    public function __construct(
        Sender $sender,
        LoggerInterface $logger,
        SmsVerificationTokenRepository $smsVerificationTokenRepo,
        UserProvider $userProvider,
        AuthTokenRepository $authTokenRepository,
        $googleVerifyURL
    ) {
        $this->sender = $sender;
        $this->logger = $logger;
        $this->smsVerificationTokenRepo = $smsVerificationTokenRepo;
        $this->userProvider = $userProvider;
        $this->authTokenRepository = $authTokenRepository;
        $this->googleVerifyURL = $googleVerifyURL;
    }


    /**
     * Shows message with OTP
     *
     * @param $phoneNumber
     * @throws SenderException
     * @return bool send status
     */
    public function sendSecret($phoneNumber)
    {
        $token = $this->smsVerificationTokenRepo->findNotExpiredPendingTokenByPhoneNumber($phoneNumber);
        if (!$token) {
            $token = $this->smsVerificationTokenRepo->createVerificationToken($phoneNumber);
        }
        $message = new Message($phoneNumber, 'Dear User, Your OTP is '.$token->getSecret().'.');
        try {
            $status = $this->sender->send($message);

            return $status->isOk();
        } catch (SenderException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());

            return false;
        }
    }

    /**
     * Generates and returns authToken
     *
     * @param $phoneNumber
     * @param $secret
     * @return AuthToken
     * @throws InvalidCredentialsException
     */
    public function getAuthToken($phoneNumber, $secret)
    {

        $token = $this->smsVerificationTokenRepo->findTokenBySecretAndPhoneNumber($secret, $phoneNumber);
        if (!$token) {
            throw new InvalidCredentialsException('Invalid secret or secret token already used');
        }

        if (new \DateTime() > $token->getExpiresAt()) {
            $token->setStatus(SmsVerificationToken::STATUS_EXPIRED);
            $this->smsVerificationTokenRepo->save($token);
            throw new InvalidCredentialsException('Secret token expired');
        }

        $user = $this->userProvider->loadUserBySmsVerificationToken($token);

        $authToken = $this->authTokenRepository->createAuthToken($user);
        $token->setStatus(SmsVerificationToken::STATUS_USED);
        $this->smsVerificationTokenRepo->save($token);

        return $authToken;
    }

    /**
     * Verify Google idToken
     *
     * @param $idToken
     * @return AuthToken
     * @throws InvalidCredentialsException
     */
    public function getAuthTokenGoogle($idToken)
    {
        $userInfo = $this->checkGoogleToken($idToken);
        $googleEmail = $userInfo['email'];
        try {
            $user = $this->userProvider->loadUserByPhoneNumber($googleEmail);
        } catch (UsernameNotFoundException $e) {
            $user = $this->userProvider->createUser($googleEmail);
            $user->setEmail($googleEmail);
            $user->setFullName($userInfo['name'] ?: null);
            $user->setLastName($userInfo['family_name'] ?: null);
            $user->setFirstName($userInfo['given_name'] ?: null);
            $this->userProvider->save($user);
        }
        
        return $this->authTokenRepository->createAuthToken($user);
    }

    /**
     * @param $idToken
     * @return array
     * @throws InvalidCredentialsException
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException
     * @throws \RuntimeException
     *
     */
    public function checkGoogleToken($idToken)
    {
        $tokenInfo = $this->googleVerifyURL.$idToken;
        $client = new Client(['timeout' => 90]);
        $request = new Request($method = 'GET', $tokenInfo);
        try {
            $userInfo = (new JsonEncoder())->decode(
                $client->send($request)->getBody(),
                JsonEncoder::FORMAT
            );

            return $userInfo;
        } catch (ClientException $e) {
            throw new InvalidCredentialsException('Invalid online Google token');
        }  catch (ServerException $e){
            throw new \RuntimeException($e->getMessage(),$e->getCode(), $e);
        }
    }
}
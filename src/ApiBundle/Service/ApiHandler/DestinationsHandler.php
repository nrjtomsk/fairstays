<?php
/**
 * Created by PhpStorm.
 * User: yarovikovyo
 * Date: 21.12.2015
 * Time: 12:51
 */

namespace ApiBundle\Service\ApiHandler;


use ApiBundle\DTO\CitiesFilter;
use AppBundle\Repository\HotelsDestinationRepository;

/**
 * Returns the destinations found by cityName
 *
 * Class DestinationsHandler
 * @package ApiBundle\Service\ApiHandler
 */
class DestinationsHandler
{

    /**
     * @var HotelsDestinationRepository
     */
    private $hotelsDestinationRepository;

    /**
     * DestinationsHandler constructor.
     * @param HotelsDestinationRepository $hotelsDestinationRepository
     */
    public function __construct(HotelsDestinationRepository $hotelsDestinationRepository)
    {
        $this->hotelsDestinationRepository = $hotelsDestinationRepository;
    }

    /**
     * Returns destinations
     *
     * @param CitiesFilter $filter
     * @return array
     */
    public function search(CitiesFilter $filter)
    {
        $destinations = $this->findDestinationsByFilter($filter);
        foreach ($destinations as $destination) {
            unset($destination['hotelsProId']);
        }

        return $destinations;
    }

    /**
     * Finds destinations by cityName
     *
     * @param CitiesFilter $filter
     * @return array
     */
    private function findDestinationsByFilter(CitiesFilter $filter)
    {
        $query = $this->hotelsDestinationRepository->createQueryBuilder('d')
            ->select('d')
            ->setFirstResult($filter->offset)
            ->setMaxResults($filter->limit)
            ->andWhere('d.cityName LIKE :cityName')
            ->setParameters([
                'cityName' => $filter->query.'%',
            ])
            ->getQuery();

        return $query->getArrayResult();
    }
}
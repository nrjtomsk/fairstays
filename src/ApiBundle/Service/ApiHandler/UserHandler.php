<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 12:05
 */

namespace ApiBundle\Service\ApiHandler;


use ApiBundle\Exception\InvalidFormException;
use ApiBundle\Form\FileFormType;
use ApiBundle\Form\UserFormType;
use ApiBundle\Service\Utils\StringUtils;
use AppBundle\Entity\AuthToken;
use AppBundle\Entity\File;
use AppBundle\Entity\User;
use AppBundle\Repository\AuthTokenRepository;
use AppBundle\Repository\FileRepository;
use AppBundle\Service\User\UserProvider;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class UserHandler
 * @package ApiBundle\Service\ApiHandler
 */
class UserHandler
{
    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $uploadDirectory = 'web/uploads';

    /**
     * @var FileRepository
     */
    private $fileRepository;

    /**
     * @var AuthTokenRepository
     */
    private $tokenRepository;


    /**
     * UserHandler constructor.
     * @param UserProvider $userProvider
     * @param FormFactory $formFactory
     * @param TokenStorageInterface $tokenStorage
     * @param FileRepository $fileRepository
     * @param $uploadDirectory
     * @param AuthTokenRepository $tokenRepository
     */
    public function __construct(
        UserProvider $userProvider,
        FormFactory $formFactory,
        TokenStorageInterface $tokenStorage,
        FileRepository $fileRepository,
        $uploadDirectory,
        AuthTokenRepository $tokenRepository
    ) {
        $this->userProvider = $userProvider;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->fileRepository = $fileRepository;
        $this->uploadDirectory = $uploadDirectory;
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * Checks form and on success creates the user
     *
     * @param Request $request
     * @return User|mixed
     * @throws InvalidFormException
     * @throws InvalidOptionsException
     */
    public function post(Request $request)
    {
        return $this->processForm(new User(), $request, 'POST');
    }

    /**
     * Validates the form
     *
     * @param User $user
     * @param Request $request
     * @param string $method
     * @return User|mixed
     * @throws InvalidFormException
     * @throws InvalidOptionsException
     */
    private function processForm(User $user, Request $request, $method = 'PATCH')
    {
        $form = $this->formFactory->create(
            new UserFormType(),
            $user,
            [
                'method' => $method,
            ]
        );
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                /** @var User $user */
                $user = $this->userProvider->loadUserByUsername($user->getUsername());

                if ($user->getUsername() !== $user->getEmail()) {
                    $user->setUsername($user->getPhoneNumber());
                    $this->tokenRepository->findAllUserTokenAndSetDeletedAtNow($user);
                }

                $this->userProvider->save($user);

                return $user;
            } catch (UsernameNotFoundException $e) {

                $created = $this->userProvider->createUser($user->getUsername());
                $created->setEmail($user->getEmail());
                $created->setFullName($user->getFullName());
                $created->setPhoneNumber($user->getPhoneNumber());
                $this->userProvider->save($created);

                return $created;
            }
        }

        throw new InvalidFormException($form);
    }

    /**
     * Checks form and on success update the user data
     *
     * @param Request $request
     * @return User|mixed
     * @throws InvalidFormException
     * @throws InvalidOptionsException
     */
    public function patch(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->processForm($user, $request, 'PATCH');
    }

    /**
     * Checks form and on success update the user data
     *
     * @param Request $request
     * @return User|mixed
     * @throws InvalidFormException
     * @throws InvalidOptionsException
     */
    public function put(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->processForm($user, $request, 'PUT');
    }

    /**
     * Delete user by phoneNumber
     *
     * @param $phoneNumber
     */
    public function delete($phoneNumber)
    {
        try {
            $user = $this->userProvider->loadUserByPhoneNumber($phoneNumber);
            $this->userProvider->deleteUser($user);
        } catch (UsernameNotFoundException $e) {
            // do nothing
        }
    }

    /**
     * Posts user avatar
     *
     * @param Request $request
     * @throws InvalidOptionsException
     * @throws FileException
     * @throws InvalidFormException
     */
    public function postUserAvatar(Request $request)
    {
        $form = $this->formFactory->create(
            new FileFormType(),
            $data = null,
            [
                'method' => 'POST',
            ]
        );
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $file = $data['file'];
            $user = $this->tokenStorage->getToken()->getUser();
            if ($file instanceof UploadedFile && $user instanceof User) {
                $fileName = StringUtils::generateRandomString(44).'.'.$file->getClientOriginalExtension();
                $uploadedFile = $file->move($this->uploadDirectory, $fileName);
                $storedFile = new File();
                $storedFile->setMimeType($file->getClientMimeType());
                $storedFile->setSize($uploadedFile->getSize());
                $storedFile->setPath($this->uploadDirectory.'/'.$fileName);
                $storedFile->setName($fileName);
                $this->fileRepository->save($storedFile);
                $user->setAvatar($storedFile);
                $this->userProvider->save($user);

                return;
            }
        }

        throw new InvalidFormException($form);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 28.01.2016
 * Time: 16:21
 */

namespace ApiBundle\Service\Utils;


class ExceptionsUtil
{
    /**
     * Get exceptions pipe using \Exception::getPrevious
     * @param \Exception $exception
     * @return \Exception[]
     */
    public static function getExceptionsPipe(\Exception $exception)
    {
        $exceptions = [$exception->getMessage()];
        while ($exception = $exception->getPrevious()) {
            $exceptions[] = $exception->getMessage();
        }
        return $exceptions;
    }
}
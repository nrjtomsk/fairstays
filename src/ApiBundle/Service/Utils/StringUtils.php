<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 14:24
 */

namespace ApiBundle\Service\Utils;


class StringUtils
{
    /**
     * @param int $length
     * @return string
     */
    public static function generateNumericRandomString($length = 5)
    {
        if ($length > 10 || $length < 1) {
            throw new \InvalidArgumentException;
        }
        $string = str_shuffle('0123456789');

        return substr($string, 0, $length);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param $xml
     * @return array|string
     */
    public static function xmlStringToArray($xml)
    {
        $document = new \DOMDocument();
        $document->loadXML($xml);

        return self::domElementAsArray($document->documentElement);
    }

    /**
     * @param $node
     * @return array|string
     */
    private static function domElementAsArray($node)
    {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = self::domElementAsArray($child);
                    if (isset($child->tagName)) {
                        $t = $child->tagName;
                        if (!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    } elseif ($v) {
                        $output = (string)$v;
                    }
                }
                if (is_array($output)) {
                    if ($node->attributes->length) {
                        $a = array();
                        if (is_array($node->attributes) || $node->attributes instanceof \Traversable) {
                            foreach ($node->attributes as $attrName => $attrNode) {
                                $a[$attrName] = (string)$attrNode->value;
                            }
                            $output['@attributes'] = $a;
                        }
                    }
                    foreach ($output as $t => $v) {
                        if (is_array($v) && count($v) == 1 && $t != '@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }

        return $output;
    }
}
<?php
namespace ApiBundle\Service\Stripe;

use ApiBundle\Service\Stripe\Exception\NotSuccessfullPaymentException;
use ApiBundle\Service\Stripe\Exception\PaymentException;
use Stripe\Charge;
use Stripe\Error\ApiConnection;
use Stripe\Error\Authentication;
use Stripe\Stripe;

/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 15.03.16
 * Time: 10:56
 */
class StripePaymentGateway
{
    const PAYMENT_STATUS_SUCCESS = 'succeeded';

    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var string
     */
    private $endPoint;

    /**
     * @var string;
     */
    private $description;

    /**
     * StripePaymentGateway constructor.
     * @param $privateKey string
     * @param $endPoint string
     * @param $description string
     */
    public function __construct($privateKey, $endPoint, $description)
    {
        $this->privateKey = $privateKey;
        $this->endPoint = $endPoint;
        $this->description = $description;
    }

    /**
     * @param integer $amount
     * @param string $currency
     * @param string|array $source stripe_token or credit card data
     * @return Charge
     * @throws PaymentException
     * @throws NotSuccessfullPaymentException
     */
    public function pay($amount, $currency, $source, $email)
    {
        $charge = $this->charge($amount, $currency, $source, $email);

        if ($charge->status === static::PAYMENT_STATUS_SUCCESS) {
            return $charge;
        }

        throw new NotSuccessfullPaymentException($charge);
    }

    /**
     * @param $amount integer
     * @param $currency string
     * @param $source string|array
     * @param $email string
     * @return Charge
     * @throws PaymentException
     */
    private function charge($amount, $currency, $source, $email)
    {
        $this->auth();
        try {
            return Charge::create(
                [
                    'amount' => $amount,
                    'currency' => $currency,
                    'source' => $source,
                    'receipt_email' => $email,
                    'description' => $this->description,
                ]
            );
        } catch (\Exception $e) {
            throw PaymentException::fromStripeException($e);
        }

    }

    /**
     * @throws PaymentException
     */
    private function auth()
    {
        try {
            Stripe::setApiKey($this->privateKey);
        } catch (Authentication $e) {
            throw PaymentException::fromStripeException($e);
        } catch (ApiConnection $e) {
            throw PaymentException::fromStripeException($e);
        } catch (\Exception $e) {
            throw PaymentException::fromStripeException($e);
        }
    }

    /**
     * @param Charge $charge
     * @return Charge
     */
    private function retrive(Charge $charge)
    {
        $retrieveCharge = Charge::retrieve($charge->id);
        $retrieveCharge->capture();

        return $retrieveCharge;
    }
}
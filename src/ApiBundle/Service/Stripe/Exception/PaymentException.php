<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.03.2016
 * Time: 18:04
 */

namespace ApiBundle\Service\Stripe\Exception;

/**
 * Class PaymentException
 * @package ApiBundle\Service\Stripe\Exception
 */
class PaymentException extends \RuntimeException
{
    /**
     * @param \Exception $e
     * @return PaymentException
     */
    public static function fromStripeException(\Exception $e)
    {
        return new static($e->getMessage(), $e->getCode(), $e);
    }
}
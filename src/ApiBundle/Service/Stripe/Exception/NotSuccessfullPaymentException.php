<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.03.2016
 * Time: 18:34
 */

namespace ApiBundle\Service\Stripe\Exception;


use Stripe\Charge;

class NotSuccessfullPaymentException extends \LogicException
{
    /**
     * @var Charge
     */
    private $charge;

    /**
     * NotSuccessfullPaymentException constructor.
     * @param Charge $charge
     */
    public function __construct(Charge $charge)
    {
        $this->charge = $charge;
        parent::__construct(sprintf('Not successful payment status: %s', $charge->status));
    }


    /**
     * @return Charge
     */
    public function getCharge()
    {
        return $this->charge;
    }
}
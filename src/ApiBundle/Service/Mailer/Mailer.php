<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 03.03.2016
 * Time: 15:04
 */

namespace ApiBundle\Service\Mailer;

use ApiBundle\Util\LoggerAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class Mailer
 * @package ApiBundle\Service\Mailer
 */
Class Mailer
{

    use LoggerAwareTrait;

    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * @var string
     */
    private $author;

    /**
     * Mailer constructor.
     * @param \Swift_Mailer $swiftMailer
     * @param $author
     */
    public function __construct(
        \Swift_Mailer $swiftMailer,
        $author
    ) {
        $this->swiftMailer = $swiftMailer;
        $this->author = $author;
    }

    /**
     * @param $email
     * @param $subject
     * @param $text
     */
    public function sendMailToClient($email, $subject, $text)
    {
        $this->sendMail($subject, $this->author, $email, $text);
    }

    /**
     * @param $subject
     * @param $author
     * @param $email
     * @param $text
     */
    private function sendMail($subject, $author, $email, $text)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($author, 'SelectRooms')
            ->setTo($email)
            ->setBcc($this->author, 'SelectRooms')
            ->setBody($text, 'text/plain');
        $failedRecipients = [];
        $this->swiftMailer->send($message, $failedRecipients);
        if (count($failedRecipients)) {
            foreach ($failedRecipients as $failedRecipient) {
                $this->logError(sprintf('Email message did not sent to %s', $failedRecipient), $failedRecipients);
            }
        }
    }
}
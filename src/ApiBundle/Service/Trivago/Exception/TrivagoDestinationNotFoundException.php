<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.06.16
 * Time: 15:41
 */

namespace ApiBundle\Service\Trivago\Exception;

/**
 * Class TrivagoDestinationNotFoundException
 * @package ApiBundle\Service\Trivago\Exception
 */
class TrivagoDestinationNotFoundException extends \Exception
{

}
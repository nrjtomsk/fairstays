<?php

/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.06.16
 * Time: 15:39
 */

namespace ApiBundle\Service\Trivago\Exception;

/**
 * Class TrivagoServiceException
 * @package ApiBundle\Service\Trivago\Exception
 */
class TrivagoServiceException extends \Exception
{

}
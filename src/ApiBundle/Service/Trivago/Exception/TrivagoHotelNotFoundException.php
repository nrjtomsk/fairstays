<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.06.16
 * Time: 15:41
 */

namespace ApiBundle\Service\Trivago\Exception;

/**
 * Class TrivagoHotelNotFoundException
 * @package ApiBundle\Service\Trivago\Exception
 */
class TrivagoHotelNotFoundException extends \Exception
{

}
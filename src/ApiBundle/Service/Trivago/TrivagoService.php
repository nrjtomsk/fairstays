<?php

/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 23.06.16
 * Time: 9:46
 */
namespace ApiBundle\Service\Trivago;

use ApiBundle\Service\Trivago\Exception\TrivagoDestinationNotFoundException;
use ApiBundle\Service\Trivago\Exception\TrivagoHotelNotFoundException;
use ApiBundle\Service\Trivago\Exception\TrivagoServiceException;
use AppBundle\Repository\HotelProRepository;

/**
 * Class TrivagoService
 */
class TrivagoService
{
    /**
     * @var HotelProRepository
     */
    private $hotelProRepo;

    /**
     * TrivagoService constructor.
     * @param HotelProRepository $hotelProRepo
     */
    public function __construct(HotelProRepository $hotelProRepo)
    {
        $this->hotelProRepo = $hotelProRepo;
    }

    /**
     * @param $hotelCode
     * @param $dateStart
     * @param $dateEnd
     * @return string
     * @throws TrivagoServiceException
     */
    public function generateLinkFromHotelAndDestenationAndDate($hotelCode, $dateStart, $dateEnd)
    {
        try {
            $hotelCodeId = $this->getHotelId($hotelCode);
            $destinationId = $this->getDestinationId($hotelCode);
            $link = 'http://www.trivago.com/?aDateRange%5Barr%5D='.$dateStart.'&aDateRange%5Bdep%5D='.$dateEnd.'&r=&cpt='.$hotelCodeId.'02&iRoomType=1&aPriceRange%5Bto%5D=0&aPriceRange%5Bfrom%5D=0&iGeoDistanceItem='.$hotelCodeId.'&iPathId='.$destinationId.'&iViewType=0&bIsSeoPage=false&bIsSitemap=false&';
        } catch (TrivagoDestinationNotFoundException $e) {
            $link = null;
        } catch (TrivagoHotelNotFoundException $e) {
            $link = null;
        }

        return $link;
    }

    protected function getDestinationId($hotelCode)
    {
        $hotel = $this->hotelProRepo->findHotelByHotelCode($hotelCode);
        if ($hotel !== null) {
            $link = $hotel->getTrivagoUrl();
            if ($link !== null) {
                preg_match('((\d*)\/hotel)', $link, $matches);

                return $matches[1];
            } else {
                throw new TrivagoDestinationNotFoundException('Destination not found');
            }
        }
    }

    protected function getHotelId($hotelCode)
    {
        $hotel = $this->hotelProRepo->findHotelByHotelCode($hotelCode);
        if ($hotel !== null) {
            $link = $hotel->getTrivagoUrl();
            if ($link !== null) {
                preg_match('(\d*\d$)', $link, $matches);

                return $matches[0];
            } else {
                throw new TrivagoDestinationNotFoundException('Hotel in link not found');
            }
        }
    }
}
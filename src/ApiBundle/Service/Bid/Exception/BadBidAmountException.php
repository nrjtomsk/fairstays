<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 27.04.2016
 * Time: 12:34
 */

namespace ApiBundle\Service\Bid\Exception;

/**
 * Class BadBidAmountException
 * @package ApiBundle\Service\Bid\Exception
 */
class BadBidAmountException extends \LogicException
{

}
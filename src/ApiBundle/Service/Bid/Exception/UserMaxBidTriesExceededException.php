<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 16:52
 */

namespace ApiBundle\Service\Bid\Exception;

/**
 * Class UserMaxBidTriesExceededException
 * @package ApiBundle\Service\Bid\Exception
 */
class UserMaxBidTriesExceededException extends \LogicException
{

}
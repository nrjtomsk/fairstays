<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 17:25
 */

namespace ApiBundle\Service\Bid\Exception;

/**
 * Class BidOnRoomImpossibleException
 * @package ApiBundle\Service\Bid\Exception
 */
class BidOnRoomImpossibleException extends \LogicException
{

}
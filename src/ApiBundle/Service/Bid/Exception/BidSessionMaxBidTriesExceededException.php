<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 16:53
 */

namespace ApiBundle\Service\Bid\Exception;

/**
 * Class BidSessionMaxBidTriesExceededException
 * @package ApiBundle\Service\Bid\Exception
 */
class BidSessionMaxBidTriesExceededException extends \LogicException
{

}
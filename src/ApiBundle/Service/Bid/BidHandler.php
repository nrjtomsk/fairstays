<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.04.2016
 * Time: 17:51
 */

namespace ApiBundle\Service\Bid;

use ApiBundle\Exception\RoomNotFound;
use AppBundle\Repository\Exception\BidException;

/**
 * Class BidHandler
 * @package ApiBundle\Service\Bid
 */
interface BidHandler
{
    /**
     * @param BidParams $params
     * @throws Exception\BidOnRoomImpossibleException
     * @throws BidException
     * @throws RoomNotFound
     * @throws Exception\BidSessionMaxBidTriesExceededException
     * @throws Exception\UserMaxBidTriesExceededException
     * @throws Exception\BadBidAmountException
     */
    public function handle(BidParams $params);
}
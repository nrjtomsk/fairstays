<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 16:47
 */

namespace ApiBundle\Service\Bid;

use ApiBundle\Service\Bid\Exception\BadBidAmountException;
use ApiBundle\Service\Bid\Exception\BidOnRoomImpossibleException;
use ApiBundle\Service\Bid\Exception\BidSessionMaxBidTriesExceededException;
use ApiBundle\Service\Bid\Exception\UserMaxBidTriesExceededException;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use AppBundle\Entity\Bid;
use AppBundle\Entity\User;

/**
 * Interface BidService
 * @package ApiBundle\Service\Bid
 */
interface BidService
{
    /**
     * @param User $user
     * @throws UserMaxBidTriesExceededException
     */
    public function checkUserCanMakeBid(User $user);

    /**
     * @param User $user
     * @param HotelDetails $hotelDetails
     * @throws BidSessionMaxBidTriesExceededException
     */
    public function checkUserCanMakeBidOnHotel(User $user, HotelDetails $hotelDetails);

    /**
     * @param Bid $bid
     * @param HotelDetailsRoom $room
     * @throws BidOnRoomImpossibleException
     * @throws BadBidAmountException
     */
    public function checkRoomBidPossibility(Bid $bid, HotelDetailsRoom $room);

    /**
     * @param Bid $bid
     * @param HotelDetailsRoom $room
     * @throws BidOnRoomImpossibleException
     */
    public function checkBidNotRejected(Bid $bid, HotelDetailsRoom $room);
}
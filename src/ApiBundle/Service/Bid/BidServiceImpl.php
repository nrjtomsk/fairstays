<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 26.04.2016
 * Time: 17:31
 */

namespace ApiBundle\Service\Bid;


use ApiBundle\Service\Bid\Exception\BadBidAmountException;
use ApiBundle\Service\Bid\Exception\BidOnRoomImpossibleException;
use ApiBundle\Service\Bid\Exception\BidSessionMaxBidTriesExceededException;
use ApiBundle\Service\Bid\Exception\UserMaxBidTriesExceededException;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use AppBundle\Entity\Bid;
use AppBundle\Entity\User;
use AppBundle\Repository\BidRepository;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverter;
use Doctrine\ORM\NoResultException;

/**
 * Class BidServiceImpl
 * @package ApiBundle\Service\Bid
 */
class BidServiceImpl implements BidService
{
    /**
     * @var BidRepository
     */
    private $bidRepository;

    /**
     * @var integer
     */
    private $maxBidTries;

    /**
     * @var integer
     */
    private $maxBidTriesUser;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * BidServiceImpl constructor.
     * @param BidRepository $bidRepository
     * @param int $maxBidTries
     * @param int $maxBidTriesUser
     * @param MoneyConverter $moneyConverter
     */
    public function __construct(
        BidRepository $bidRepository,
        $maxBidTries,
        $maxBidTriesUser,
        MoneyConverter $moneyConverter
    ) {
        $this->bidRepository = $bidRepository;
        $this->maxBidTries = $maxBidTries;
        $this->maxBidTriesUser = $maxBidTriesUser;
        $this->moneyConverter = $moneyConverter;
    }


    /**
     * @param User $user
     * @throws UserMaxBidTriesExceededException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function checkUserCanMakeBid(User $user)
    {
        $userBidsCount = $this->bidRepository->getUserBidCountForLastDay($user);
        if ($userBidsCount >= $this->maxBidTriesUser) {
            throw new UserMaxBidTriesExceededException();
        }
    }

    /**
     * @param User $user
     * @param HotelDetails $hotelDetails
     * @throws BidSessionMaxBidTriesExceededException
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function checkUserCanMakeBidOnHotel(User $user, HotelDetails $hotelDetails)
    {
        $bidSessionBidCount = $this->bidRepository->getBidSessionBidCountForLastDay(
            $user,
            $hotelDetails->code,
            $hotelDetails->metaData->serviceName
        );

        if ($bidSessionBidCount >= $this->maxBidTries) {
            throw new BidSessionMaxBidTriesExceededException();
        }
    }

    /**
     * @param Bid $bid
     * @param HotelDetailsRoom $room
     * @throws BidOnRoomImpossibleException
     * @throws \AppBundle\Service\Money\Exception\MoneyException
     * @throws \ApiBundle\Service\Bid\Exception\BadBidAmountException
     */
    public function checkRoomBidPossibility(Bid $bid, HotelDetailsRoom $room)
    {
        $diff = $room->bidUpperLimit->minus($room->bidLowerLimit);

        if ($bid->getAmount() < $room->bidLowerLimit->getAmount()) {
            throw new BadBidAmountException(
                sprintf(
                    'Bid amount cannot be less than bid minimum limit: %s %d',
                    $room->bidLowerLimit->getCurrencyCode(),
                    $room->bidLowerLimit->getAmount()
                )
            );
        }

        if ($bid->getAmount() > $room->bidUpperLimit->getAmount()) {
            throw new BadBidAmountException(
                sprintf(
                    'Bid amount cannot be greater than bid maximum limit: %s %d',
                    $room->bidUpperLimit->getCurrencyCode(),
                    $room->bidUpperLimit->getAmount()
                )
            );
        }


    }

    /**
     * @param Bid $bid
     * @param HotelDetailsRoom $room
     */
    public function checkBidNotRejected(Bid $bid, HotelDetailsRoom $room)
    {
        $diff = $room->bidUpperLimit->minus($room->bidLowerLimit);

        $result = (int)(!((($bid->getAmount() - $room->bidLowerLimit->getAmount()) / $diff->getAmount()) <= (mt_rand(
                ) / mt_getrandmax())));
        if ($result === 0) {
            throw new BidOnRoomImpossibleException;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.04.2016
 * Time: 17:38
 */

namespace ApiBundle\Service\Bid;

use Symfony\Component\Validator\Constraints as Assert;


class BidParams
{
    /**
     * @var string
     *
     * @Assert\NotNull(message="Hotel code is required")
     */
    private $hotelCode;

    /**
     * @var string
     * @Assert\NotNull(message="Requested currency is required")
     */
    private $currency;

    /**
     * @var float
     * @Assert\NotNull(message="Amount is required")
     */
    private $amount;

    /**
     * @var \DateTime
     * @Assert\NotNull(message="CheckIn date is required")
     * @Assert\Date()
     */
    private $checkIn;

    /**
     * @var \DateTime
     * @Assert\NotNull(message="CheckOut date is required")
     * @Assert\Date()
     */
    private $checkOut;

    /**
     * @var string
     */
    private $roomType;

    /**
     * @var string
     */
    private $ratePlan;

    /**
     * @var string
     * @Assert\NotNull(message="Metadata is required")
     */
    private $meta;

    /**
     * @var string
     * @Assert\NotNull(message="Nationality is required")
     */
    private $nationality;

    /**
     * @return string
     */
    public function getHotelCode()
    {
        return $this->hotelCode;
    }

    /**
     * @param string $hotelCode
     * @return BidParams
     */
    public function setHotelCode($hotelCode)
    {
        $this->hotelCode = $hotelCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return BidParams
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param \DateTime $checkIn
     * @return BidParams
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param \DateTime $checkOut
     * @return BidParams
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoomType()
    {
        return $this->roomType;
    }

    /**
     * @param string $roomType
     * @return BidParams
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;

        return $this;
    }

    /**
     * @return string
     */
    public function getRatePlan()
    {
        return $this->ratePlan;
    }

    /**
     * @param string $ratePlan
     * @return BidParams
     */
    public function setRatePlan($ratePlan)
    {
        $this->ratePlan = $ratePlan;

        return $this;
    }
    
    /**
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param string $meta
     * @return BidParams
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     * @return BidParams
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return BidParams
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    

}
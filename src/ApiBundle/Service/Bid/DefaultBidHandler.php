<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 25.04.2016
 * Time: 17:51
 */

namespace ApiBundle\Service\Bid;

use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Service\Bid\Exception\BidOnRoomImpossibleException;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsClient;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use ApiBundle\Service\HotelDetails\InputParams;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Entity\Bid;
use AppBundle\Entity\User;
use AppBundle\Repository\BidRepository;
use AppBundle\Repository\Exception\BidException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DefaultBidHandler
 * @package ApiBundle\Service\Bid
 */
class DefaultBidHandler implements BidHandler
{
    /**
     * @var BidRepository
     */
    private $bidRepository;

    /**
     * @var HotelDetailsClient
     */
    private $hotelDetailsClient;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var BidService
     */
    private $bidService;

    /**
     * DefaultBidHandler constructor.
     * @param BidRepository $bidRepository
     * @param HotelDetailsClient $hotelDetailsClient
     * @param TokenStorageInterface $tokenStorage
     * @param BidService $bidService
     */
    public function __construct(
        BidRepository $bidRepository,
        HotelDetailsClient $hotelDetailsClient,
        TokenStorageInterface $tokenStorage,
        BidService $bidService
    ) {
        $this->bidRepository = $bidRepository;
        $this->hotelDetailsClient = $hotelDetailsClient;
        $this->tokenStorage = $tokenStorage;
        $this->bidService = $bidService;
    }

    /**
     * @param BidParams $params
     * @throws Exception\BidOnRoomImpossibleException
     * @throws BidException
     * @throws RoomNotFound
     * @throws Exception\BidSessionMaxBidTriesExceededException
     * @throws Exception\UserMaxBidTriesExceededException
     * @throws Exception\BadBidAmountException
     */
    public function handle(BidParams $params)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $hotelDetails = $this->hotelDetailsClient->get(InputParams::fromBidParams($params));

        $this->bidService->checkUserCanMakeBidOnHotel($user, $hotelDetails);
        $this->bidService->checkUserCanMakeBid($user);
        
        $room = $this->getRoom($hotelDetails, $params);
        
        $bid = new Bid();
        $bid->setUser($user);
        $bid->setHotelCode($hotelDetails->code);
        $bid->setRoom($room->toArray());
        $bid->setServiceName($room->metaData->serviceName);
        $bid->setAmount($params->getAmount());
        $bid->setCurrency($params->getCurrency());
        $this->bidService->checkRoomBidPossibility($bid, $room);
        try {
            $this->bidService->checkBidNotRejected($bid, $room);
            $bid->setStatus(BidStatuses::DTO_STATUS_APPROVED);
            $this->bidRepository->save($bid);
        } catch (BidOnRoomImpossibleException $e) {
            $bid->setStatus(BidStatuses::DTO_STATUS_DEC);
            $this->bidRepository->save($bid);
            throw new BidOnRoomImpossibleException($e->getMessage(), $e->getCode(), $e);
        }

    }

    /**
     * @param HotelDetails $hotelDetails
     * @param BidParams $params
     * @return HotelDetailsRoom
     * @throws RoomNotFound
     */
    private function getRoom(HotelDetails $hotelDetails, BidParams $params)
    {
        if (is_array($hotelDetails->rooms) && count($hotelDetails->rooms)) {

            $meta = MetaDataDTO::fromJsonString($params->getMeta());

            /** @var HotelDetailsRoom $room */
            foreach ($hotelDetails->rooms as $room) {
                switch ($meta->serviceName) {
                    case HotelsListApiManagerTravelGuru::NAME:
                        if ($room->ratePlanCode == $params->getRatePlan()
                            && $room->roomTypeCode == $params->getRoomType()
                        ) {
                            return $room;
                        }
                        break;
                    case HotelsListApiManagerHotelsPro::NAME:
                        if ($room->metaData->hotelsProProcessId == $meta->hotelsProProcessId) {
                            return $room;
                        }
                        break;
                }
            }
        }

        throw new RoomNotFound('Room not found');
    }

}
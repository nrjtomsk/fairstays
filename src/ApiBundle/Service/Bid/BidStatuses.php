<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 27.04.2016
 * Time: 12:14
 */

namespace ApiBundle\Service\Bid;

/**
 * Interface BidStatuses
 * @package ApiBundle\Service\Bid
 */
interface BidStatuses
{
    const REJECTED = 0;
    const ACCEPTED = 1;
    const MAX_BID_TRIES_EXCEEDED = -1;
    const MAX_BID_TRIES_USER_EXCEEDED = -2;
    const DTO_STATUS_APPROVED = 'Approved';
    const DTO_STATUS_DEC = 'Dec';
}
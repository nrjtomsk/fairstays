<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 14:12
 */

namespace ApiBundle\Service\SMS\Exception;

/**
 * Class SenderException
 * @package ApiBundle\Service\SMS\Exception
 */
class SenderException extends \RuntimeException
{

}
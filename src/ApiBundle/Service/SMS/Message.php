<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 13:19
 */

namespace ApiBundle\Service\SMS;


class Message
{
    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $text;

    /**
     * Message constructor.
     * @param $phoneNumber
     * @param $text
     */
    public function __construct($phoneNumber, $text)
    {
        $this->phoneNumber = $phoneNumber;
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }
}
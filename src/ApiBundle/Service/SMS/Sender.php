<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 13:19
 */

namespace ApiBundle\Service\SMS;


use ApiBundle\Service\SMS\Exception\SenderException;

interface Sender
{
    /**
     * @param Message $message
     * @return SendStatus
     * @throws SenderException
     */
    public function send(Message $message);
}
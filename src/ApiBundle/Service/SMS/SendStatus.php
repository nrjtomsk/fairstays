<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 14:09
 */

namespace ApiBundle\Service\SMS;


class SendStatus
{
    /**
     * @var bool
     */
    private $ok;

    /**
     * SendStatus constructor.
     * @param bool $ok
     */
    public function __construct($ok)
    {
        $this->ok = (bool)$ok;
    }

    /**
     * @return bool
     */
    public function isOk()
    {
        return $this->ok;
    }

}
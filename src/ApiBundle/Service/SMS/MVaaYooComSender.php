<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 13:23
 */

namespace ApiBundle\Service\SMS;


use ApiBundle\Service\SMS\Exception\SenderException;

class MVaaYooComSender implements Sender
{

    private $user = "ranadip@fairfest.com:fairfest@123";

    private $senderId = "FSTAYS";

    private $endPoint = "http://api.mVaayoo.com/mvaayooapi/MessageCompose";

    /**
     * MVaaYooComSender constructor.
     * @param string $user
     * @param string $senderId
     * @param string $endPoint
     */
    public function __construct($user, $senderId, $endPoint)
    {
        $this->user = $user;
        $this->senderId = $senderId;
        $this->endPoint = $endPoint;
    }


    /**
     * @param Message $message
     * @return SendStatus
     * @throws SenderException
     */
    public function send(Message $message)
    {
        if (!extension_loaded('curl')) {
            throw new SenderException('Curl extension is not loaded');
        }

        $ch = curl_init();
        $user = $this->user;
        $receipientno = $message->getPhoneNumber();  //"9432139699";
        $senderID = $this->senderId;
        $msgtxt = $message->getText();
        curl_setopt($ch, CURLOPT_URL, $this->endPoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
        $buffer = curl_exec($ch);
        curl_close($ch);

        if (preg_match('/Status=0/', $buffer)) {
            $result = new SendStatus(true);
        } else {
            $result = new SendStatus(false);
        }

        return $result;
    }
}
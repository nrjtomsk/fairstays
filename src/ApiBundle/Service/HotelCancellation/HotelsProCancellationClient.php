<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 12:18
 */

namespace ApiBundle\Service\HotelCancellation;


use ApiBundle\Exception\BookingCancellationException;
use ApiBundle\Exception\BookingRequestSaveException;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;

/**
 * Class HotelsProCancellationClient
 * @package ApiBundle\Service\HotelCancellation
 */
class HotelsProCancellationClient implements CancellationClient
{
    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var BookingRequestRepository
     */
    private $bookingRequestRepository;

    /**
     * HotelsProCancellationClient constructor.
     * @param \SoapClient $soapClient
     * @param string $apiKey
     * @param BookingRequestRepository $bookingRequestRepository
     */
    public function __construct(\SoapClient $soapClient, $apiKey, BookingRequestRepository $bookingRequestRepository)
    {
        $this->soapClient = $soapClient;
        $this->apiKey = $apiKey;
        $this->bookingRequestRepository = $bookingRequestRepository;
    }


    /**
     * @param BookingRequest $bookingRequest
     * @return void
     * @throws BookingCancellationException
     */
    public function cancelBooking(BookingRequest $bookingRequest)
    {
        try {
            $cancelHotelBooking = $this->soapClient->cancelHotelBooking(
                $this->apiKey,
                $trackingId = $bookingRequest->getTrackingId()
            );

            $bookingRequest->setTrackingId(
                isset($cancelHotelBooking->trackingId) ? $cancelHotelBooking->trackingId : null
            );
            $bookingRequest->setHotelsProCancelResponseId(
                isset($cancelHotelBooking->responseId) ? $cancelHotelBooking->responseId : null
            );
            $bookingRequest->setHotelsProAgencyReferenceNumber(
                isset($cancelHotelBooking->agencyReferenceNumber) ? $cancelHotelBooking->agencyReferenceNumber : null
            );
            $bookingRequest->setHotelsProBookingStatus(
                isset($cancelHotelBooking->bookingStatus) ? $cancelHotelBooking->bookingStatus : null
            );
            $bookingRequest->setHotelsProCancellationNote(
                isset($cancelHotelBooking->note) ? $cancelHotelBooking->note : null
            );
            $this->bookingRequestRepository->save($bookingRequest);

        } catch (\SoapFault $e) {
            throw new BookingCancellationException($bookingRequest, $e->getMessage(), $e->getCode(), $e);
        } catch (BookingRequestSaveException $e) {
            throw new BookingCancellationException($e->getBookingRequest(), $e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Method for calculate Refund Amount HotelsPro when User cancelBooking
     *
     * @param $cancellationPolicyJSON
     * @param $checkin_date
     * @param $fullBookingAmount
     * @param $nightlyArray
     * @return mixed
     * @throws \Exception
     */
    public function getRefundAmount($cancellationPolicyJSON, $checkin_date, $fullBookingAmount,$nightlyArray)
    {
        if(!is_array($cancellationPolicyJSON)){
            throw new \Exception('Cancellation policy array not found.',500);
        }
        date_default_timezone_set('Etc/GMT-2');
        /*
        $checkin_date to be in 'd-m-Y' format
        for example, 02-02-2016 is 2nd February, 2016
        */
        $hotelsproOfficeTime =  new \DateTime(null, new \DateTimeZone('Etc/GMT-2'));

        /* cancellation policy is calculated as per checkin time in hotelspro documentation */
        /*	$checkin_date .= '12:00:00 +0200'; */
        /* however we take the start of check_in day so that we have extra time to handle service requests */

        $checkin_date .= '00:00:00 +0200';

        $checkin_dateObject =  \DateTime::createFromFormat('d-m-Y H:i:s O',$checkin_date);

        $diff = $checkin_dateObject->diff($hotelsproOfficeTime);

        $differenceInDays = $diff->d;
        $CancellationDays = [];

        foreach ($cancellationPolicyJSON['cancellationPolicy'] as $cancellationPolicy) {
            /*
            if cancellationday >250, the booking is non-refundable
            as per hotelspro documentation. Hence, the refund amount is 0.
            */
            if ((int)($cancellationPolicy['cancellationDay']) > 250) {

                return 0;
            }
            /*
            push all cancellationday into an array for
            all cancellationday value >= difference in days
            */
            if ($differenceInDays <= (int)($cancellationPolicy['cancellationDay'])){
                array_push($CancellationDays, (int)($cancellationPolicy['cancellationDay']));
            }
        }
        /*
        the cancellation policy for minimum of cancellationdays >= differenceindays will apply
        */
        if (!$CancellationDays) {
            return $fullBookingAmount;
        }

        $bucket = min($CancellationDays);
        /*
        loop through the JSON for the applicablecancellationpolicy corresponding to the $bucket
        */
        foreach ($cancellationPolicyJSON['cancellationPolicy'] as $cancellationPolicy) {

            if ((int)($cancellationPolicy['cancellationDay'] == $bucket)) {
                $applicableCancellationPolicy = $cancellationPolicy;
            }

        }
        /*
        calculate the refund amount for applicablecancellationpolicy with feetype == "Percent"
        */
        if ($applicableCancellationPolicy['feeType'] == "Percent") {
            /*
            return the refund amount = booking amount minus the cancellation charges
            */
            return $fullBookingAmount - ($fullBookingAmount * floatval($applicableCancellationPolicy['feeAmount'])/100);

        }

        /*
        calculate the refund amount for applicablecancellationpolicy with feetype == "Amount"
        */
        if ($applicableCancellationPolicy['feeType'] == "Amount") {

            /*
            return the refund amount = booking amount minus the cancellation charges
            */
            return  $fullBookingAmount - (int)($applicableCancellationPolicy['feeAmount']);
        }
        /*
        calculate the refund amount for applicablecancellationpolicy with feetype == "Night"
        If feeType is Night then cancellation charge will be night rates
        */
        if ($applicableCancellationPolicy['feeType'] == "Night") {

            $nightlyfee = 0;
            /*
            here, the feeAmount is the number of nights for which the amount will not be refunded.
            so walk through the nightlyArray (stored in the database as mentioned above) for the nightly rates
            to get the cancellation fee that will be will be first feeAmount night rates starting from the check_in date
            Note that: as mentioned above, the nightlyArray should be sorted with the check_in date rate at the top
            */

            for ($i=0; $i < $applicableCancellationPolicy['feeAmount'];$i++) {

                $nightlyfee += $nightlyArray[$i]['amount'];
            }
            /*
            return the refund amount = booking amount minus the cancellation charges
            */
            return $fullBookingAmount - $nightlyfee;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 12:18
 */

namespace ApiBundle\Service\HotelCancellation;


use ApiBundle\Exception\BookingCancellationException;
use ApiBundle\Service\Hotels\Booking\Cancellation\Client;
use AppBundle\Entity\BookingRequest;
use GuzzleHttp\Exception\ServerException;

/**
 * Class TravelGuruCancellationClient
 * @package ApiBundle\Service\HotelCancellation
 */
class TravelGuruCancellationClient implements CancellationClient
{

    /**
     * @var Client
     */
    private $cancellationClient;

    /**
     * TravelGuruCancellationClient constructor.
     * @param Client $cancellationClient
     */
    public function __construct(Client $cancellationClient)
    {
        $this->cancellationClient = $cancellationClient;
    }


    /**
     * @param BookingRequest $bookingRequest
     * @return void
     * @throws BookingCancellationException
     */
    public function cancelBooking(BookingRequest $bookingRequest)
    {
        try {
            $response = $this->cancellationClient->getCancellationResponse($bookingRequest);
            if ($response->getError()) {
                throw new BookingCancellationException($bookingRequest, $response->getError());
            }
        } catch (\InvalidArgumentException $e) {
            throw new BookingCancellationException($bookingRequest, $e->getMessage(), $e->getCode(), $e);
        } catch (ServerException $e) {
            throw new BookingCancellationException($bookingRequest, $e->getMessage(), $e->getCode(), $e);
        }
    }
}
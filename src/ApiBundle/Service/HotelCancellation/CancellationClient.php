<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 12:14
 */

namespace ApiBundle\Service\HotelCancellation;


use ApiBundle\Exception\BookingCancellationException;
use AppBundle\Entity\BookingRequest;

/**
 * Interface CancellationClient
 * @package ApiBundle\Service\HotelCancellation
 */
interface CancellationClient
{
    /**
     * @param BookingRequest $bookingRequest
     * @return void
     * @throws BookingCancellationException
     */
    public function cancelBooking(BookingRequest $bookingRequest);
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 12:18
 */

namespace ApiBundle\Service\HotelCancellation;


use ApiBundle\Exception\BookingCancellationException;
use ApiBundle\Exception\BookingRequestSaveException;
use ApiBundle\Exception\NotRefundedException;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\Mailer\Mailer;
use ApiBundle\Service\PayU\PayURefundHandler;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Sends cancel booking request and refund request to payment system
 * Updates current status of process
 *
 * Class CommonCancellationClient
 * @package ApiBundle\Service\HotelCancellation
 */
class CommonCancellationClient implements CancellationClient
{
    /**
     * @var CancellationClient
     */
    private $travelGuruCancellationClient;

    /**
     * @var CancellationClient
     */
    private $hotelsProCancellationClient;

    /**
     * @var PayURefundHandler
     */
    private $payuRefundService;

    /**
     * @var BookingRequestRepository
     */
    private $bookingRequestRepository;
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $cancellationResponseMessage;

    /**
     * CommonCancellationClient constructor.
     * @param CancellationClient $travelGuruCancellationClient
     * @param CancellationClient $hotelsProCancellationClient
     * @param PayURefundHandler $payuRefundService
     * @param BookingRequestRepository $bookingRequestRepository
     * @param Mailer $mailer
     */
    public function __construct(
        CancellationClient $travelGuruCancellationClient,
        CancellationClient $hotelsProCancellationClient,
        PayURefundHandler $payuRefundService,
        BookingRequestRepository $bookingRequestRepository,
        Mailer $mailer
    ) {
        $this->travelGuruCancellationClient = $travelGuruCancellationClient;
        $this->hotelsProCancellationClient = $hotelsProCancellationClient;
        $this->payuRefundService = $payuRefundService;
        $this->bookingRequestRepository = $bookingRequestRepository;
        $this->mailer = $mailer;
    }


    /**
     * Sends cancel booking request and refund request
     * to payment system
     *
     * @param BookingRequest $bookingRequest
     * @return void
     * @throws BookingCancellationException
     * @throws \RuntimeException
     */
    public function cancelBooking(BookingRequest $bookingRequest)
    {
        switch ($bookingRequest->getStatus()) {
            case  BookingRequest::STATUS_CONFIRMED:
                $this->sendCancelRequestToHotelsProvider($bookingRequest);
                $this->sendNotificationEmails($bookingRequest);
                break;
            case BookingRequest::STATUS_PAYMENT_COMPLETE:
                $bookingRequest->setStatus(BookingRequest::STATUS_CANCELLED);
                break;
            case BookingRequest::STATUS_CONFIRMING:
                $bookingRequest->setStatus(BookingRequest::STATUS_CANCELLED);
                break;
            default:
                throw new BookingCancellationException(
                    $bookingRequest, sprintf('Invalid booking status: %s', $bookingRequest->getStatus())
                );
                break;
        }

        try {
            $this->bookingRequestRepository->save($bookingRequest);
        } catch (BookingRequestSaveException $e) {
            throw new BookingCancellationException(
                $e->getBookingRequest(), $e->getMessage(), $e->getCode(), $e
            );
        }
    }

    /**
     * Sends cancel request to travelGuru or hotelsPro apiManager
     *
     * @param BookingRequest $bookingRequest
     * @throws BookingCancellationException
     */
    private function sendCancelRequestToHotelsProvider(BookingRequest $bookingRequest)
    {
        $this->beforeSendCancel($bookingRequest);
        switch ($bookingRequest->getMetaData()->serviceName) {
            case HotelsListApiManagerTravelGuru::NAME:
                $this->travelGuruCancellationClient->cancelBooking($bookingRequest);
                break;
            case HotelsListApiManagerHotelsPro::NAME:
                $this->hotelsProCancellationClient->cancelBooking($bookingRequest);
                break;
            default:
                throw new BookingCancellationException(
                    $bookingRequest, sprintf(
                        'Invalid cancellation client resolving parameter: %s',
                        $bookingRequest->getId()
                    )
                );
        }
        $this->afterSendCancel($bookingRequest);
    }

    /**
     * @param BookingRequest $bookingRequest
     * @throws \RuntimeException
     */
    private function sendNotificationEmails(BookingRequest $bookingRequest)
    {
        switch ($bookingRequest->getMetaData()->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:

                switch ($bookingRequest->getHotelsProBookingStatus()) {
                    case BookingRequest::STATUS_CANCELLATION_PROCESSING:
                        $text = $this->getTextMessageToSend($bookingRequest);
                        $subject = 'Cancellation Request Received - '.$bookingRequest->getPublicBookingId();

                        $this->mailer->sendMailToClient($bookingRequest->getProfileEmail(), $subject, $text);
                        break;
                    case BookingRequest::STATUS_HOTELS_PRO_CANCELLED:
                        $text = $this->getTextMessageToSend($bookingRequest);
                        $subject = 'Booking Cancelled - '.$bookingRequest->getPublicBookingId();

                        $this->mailer->sendMailToClient($bookingRequest->getProfileEmail(), $subject, $text);
                        break;
                    default:
                        throw new \RuntimeException(sprintf(
                            'Trying to send cancellation notification for non-cancelled booking %s', $bookingRequest->getPublicBookingId()
                        ));
                }
                break;
            case HotelsListApiManagerTravelGuru::NAME:
                $subject = 'Booking Cancelled - '.$bookingRequest->getPublicBookingId();
                $text = $this->getTextMessageToSend($bookingRequest);
                $this->mailer->sendMailToClient($bookingRequest->getProfileEmail(), $subject, $text);
                break;
            default:
                throw new \RuntimeException(sprintf('Invalid booking service name: %s', $bookingRequest->getMetaData()->serviceName));
        }
    }

    private function getTextMessageToSend(BookingRequest $bookingRequest)
    {
        switch ($bookingRequest->getMetaData()->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:
                switch ($bookingRequest->getHotelsProBookingStatus()) {
                    case BookingRequest::STATUS_CANCELLATION_PROCESSING:
                        $text = "Hi there!\n\nYour booking ".$bookingRequest->getPublicBookingId(
                            )." has been receieved.\n\nWe are working with the hotel to confirm the cancellation. Refunds, as per terms, will be processed automatically, and credited to your account within 10 business days.\n\nPlease write to us if you have any queries!\n\nHope to see you back on SelectRooms again!\n\nFrom the SelectRooms Team\nsupport@selectrooms.co";
                        break;
                    case BookingRequest::STATUS_HOTELS_PRO_CANCELLED:

                        $text = "Hi there!\n\nYour booking "
                            .$bookingRequest->getPublicBookingId()
                            ." is cancelled.\n\nRefunds, as per terms, will be processed automatically, and credited to your account within 10 business days.\n\nPlease write to us if you have any queries!\n\nHope to see you back on SelectRooms again!\n\nFrom the SelectRooms Team\nsupport@selectrooms.co";
                        break;
                }
                break;
            case HotelsListApiManagerTravelGuru::NAME:
                $text = 'Your booking '.$bookingRequest->getPublicBookingId().' is CANCELLED';
                break;
            default:
                throw new \RuntimeException(sprintf('Invalid booking service name: %s', $bookingRequest->getMetaData()->serviceName));
        }

        return $text;
    }

    /**
     * Sends refund request to payment system
     *
     * @param BookingRequest $bookingRequest
     * @throws BookingCancellationException
     */
    private function sendRefundRequestToPaymentSystem(BookingRequest $bookingRequest)
    {
        $this->beforeSendRefund($bookingRequest);
        try {
            $this->payuRefundService->refundBookingRequest($bookingRequest);
        } catch (NotRefundedException $e) {
            throw new BookingCancellationException($bookingRequest, $e->getMessage(), $e->getCode(), $e);
        }
        $this->afterSendRefund($bookingRequest);
    }

    /**
     * Sets status STATUS_CANCELLING
     *
     * @param BookingRequest $bookingRequest
     * @throws BookingCancellationException
     */
    private function beforeSendCancel(BookingRequest $bookingRequest)
    {
        if ($bookingRequest->getStatus() === BookingRequest::STATUS_CONFIRMED) {
            $bookingRequest->setStatus(BookingRequest::STATUS_CANCELLING);
        } else {
            throw new BookingCancellationException(
                $bookingRequest, sprintf(
                    'Cannot cancel not confirmed booking %s',
                    $bookingRequest->getPublicBookingId()
                )
            );
        }
    }

    /**
     * Sets status STATUS_CANCELLED
     *
     * @param BookingRequest $bookingRequest
     */
    private function afterSendCancel(BookingRequest $bookingRequest)
    {
        $bookingRequest->setStatus(BookingRequest::STATUS_CANCELLED);
    }

    /**
     * Sets status STATUS_REFUNDING
     *
     * @param BookingRequest $bookingRequest
     */
    private function beforeSendRefund(BookingRequest $bookingRequest)
    {
        $bookingRequest->setStatus(BookingRequest::STATUS_REFUNDING);
    }

    /**
     * Sets status STATUS_REFUNDED
     *
     * @param BookingRequest $bookingRequest
     */
    private function afterSendRefund(BookingRequest $bookingRequest)
    {
        $bookingRequest->setStatus(BookingRequest::STATUS_REFUNDED);
    }

    /**
     * @param $publicId string
     * @return string
     */
    public function getCancellationResponseMessage($publicId)
    {
        return 'CANCELLED - booking id: '.$publicId;
    }
}
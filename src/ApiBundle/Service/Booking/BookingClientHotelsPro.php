<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 31.12.2015
 * Time: 14:33
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class BookingClientHotelsPro
{
    use LoggerAwareTrait;

    const BOOKING_METHOD = 'makeHotelBooking';

    /**
     * @var string
     */
    private $endPoint;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $nationality;
    /**
     * BookingClientHotelsPro constructor.
     * @param string $endPoint
     * @param string $apiKey
     * @param string $nationality
     */
    public function __construct($endPoint, $apiKey, $nationality)
    {
        $this->endPoint = $endPoint;
        $this->apiKey = $apiKey;
        $this->nationality = $nationality;
    }

    /**
     * @param BookingRequest $booking
     * @return BookingClientHotelsProResponse
     */
    public function makeBooking(BookingRequest $booking)
    {
        $client = new Client(
            [
                'timeout' => 90,
            ]
        );
        $request = $this->createRequest($booking);

        return BookingClientHotelsProResponse::fromHttpResponse($client->send($request));
    }

    /**
     * @param BookingRequest $booking
     * @return Request
     */
    private function createRequest(BookingRequest $booking)
    {
        $travellersInfo = $this->getTravellersInfo($booking);
        $queryParams = [
            'apiKey' => $this->apiKey,
            'method' => static::BOOKING_METHOD,
            'processId' => $booking->getMetaData()->hotelsProProcessId,
            'leadTravellerInfo' => $travellersInfo['leadTravellerInfo'],
            'hotelCode' => $booking->getHotelCode(),
        ];

        if (isset($travellersInfo['otherTravellerInfo'])) {
            $queryParams['otherTravellerInfo'] = $travellersInfo['otherTravellerInfo'];
        }

        $uri = $this->endPoint . '?' . http_build_query($queryParams);

        $request = new Request(
            $method = 'GET',
            $uri,
            $headers = [
                'Content-Type' => 'application/json',
                'Content-Encoding' => 'UTF-8',
            ]
        );

        $this->logInfo(sprintf('HotelsPro booking request with uri: %s', $request->getUri()), ['request' => $request]);

        return $request;
    }

    /**
     * @param BookingRequest $booking
     * @return array
     */
    private function getTravellersInfo(BookingRequest $booking)
    {
        $travellersInfo = [];

        $guestCounts = $booking->getGuestCounts();

        $isFirstTraveller = true;
        if ($booking->getNationality() == null) {
            $nationality = $this->nationality;
        } else {
            $nationality = $booking->getNationality();
        }
        foreach ($guestCounts as $room) {

            foreach ($room['paxes'] as $pax) {

                if ($isFirstTraveller) {
                    $isFirstTraveller = false;
                    $travellersInfo['leadTravellerInfo'] = [
                        'paxInfo' => [
                            'paxType' => 'Adult',
                            'title' => $pax['title'],
                            'firstName' => $pax['firstName'],
                            'lastName' => $pax['lastName'],
                        ],
                        'nationality' => $nationality,
                    ];
                } else {
                    $travellersInfo['otherTravellerInfo'][] = [
                        'title' => $pax['title'],
                        'firstName' => $pax['firstName'],
                        'lastName' => $pax['lastName'],
                    ];
                }
            }
        }

        return $travellersInfo;
    }
}
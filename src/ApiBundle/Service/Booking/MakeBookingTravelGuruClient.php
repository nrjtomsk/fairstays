<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.12.2015
 * Time: 17:05
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\DTO\HotelConfirmationBookingRequestParams;
use ApiBundle\Service\Booking\Exception\MakeBookingException;
use ApiBundle\Service\Hotels\Booking\Confirmation\TravelGuruClient as ConfirmationTravelGuruClient;
use ApiBundle\Service\Hotels\Booking\Confirmation\TravelGuruResponse as ConfirmationResponse;
use AppBundle\Entity\BookingRequest;
use AppBundle\Entity\ContactPhoneNumber;
use AppBundle\Repository\ContactPhoneNumberRepository;

class MakeBookingTravelGuruClient implements MakeBookingClient
{

    /**
     * @var ConfirmationTravelGuruClient
     */
    private $confirmationClient;

    /**
     * @var ContactPhoneNumberRepository
     */
    private $contactPhoneNumberRepo;

    /**
     * PayuSuccessHandlerTravelGuru constructor.
     * @param ConfirmationTravelGuruClient $confirmationClient
     * @param ContactPhoneNumberRepository $contactPhoneNumberRepo
     */
    public function __construct(
        ConfirmationTravelGuruClient $confirmationClient,
        ContactPhoneNumberRepository $contactPhoneNumberRepo
    ) {
        $this->confirmationClient = $confirmationClient;
        $this->contactPhoneNumberRepo = $contactPhoneNumberRepo;
    }

    /**
     * @param BookingRequest $booking
     * @throws MakeBookingException
     * @return void
     */
    public function makeRealBooking(BookingRequest $booking)
    {
        try {
            $confirmationResponse = $this->confirmationClient->getConfirmationResponse(
                HotelConfirmationBookingRequestParams::fromBookingRequest($booking)
            );
        } catch (\Exception $e) {
            throw new MakeBookingException($booking, $e->getMessage(), $e);
        }

        if (!$confirmationResponse->getError()) {
            $this->completeConfirmation($confirmationResponse, $booking);

            return;
        }

        throw new MakeBookingException($booking, $confirmationResponse->getError());
    }

    /**
     * @param ConfirmationResponse $confirmationResponse
     * @param BookingRequest $booking
     */
    private function completeConfirmation(ConfirmationResponse $confirmationResponse, BookingRequest $booking)
    {
        $booking->setAmountAfterTax($confirmationResponse->getAmountAfterTax());
        $booking->setTravelGuruBookingId($confirmationResponse->getTravelGuruBookingId());
        if (is_array($confirmationResponse->getPhoneNumbers()) && count($confirmationResponse->getPhoneNumbers())) {
            foreach ($confirmationResponse->getPhoneNumbers() as $phoneNumber) {
                $contact = new ContactPhoneNumber();
                $contact->setAreaCityCode(
                    array_key_exists('AreaCityCode', $phoneNumber) ? $phoneNumber['AreaCityCode'] : null
                );
                $contact->setBookingRequest($booking);
                $contact->setCountryAccessCode(
                    array_key_exists('CountryAccessCode', $phoneNumber) ? $phoneNumber['CountryAccessCode'] : null
                );
                $contact->setPhoneNumber(
                    array_key_exists('PhoneNumber', $phoneNumber) ? $phoneNumber['PhoneNumber'] : null
                );
                $contact->setPhoneTechType(
                    array_key_exists('PhoneTechType', $phoneNumber) ? $phoneNumber['PhoneTechType'] : null
                );
                $booking->addContactPhoneNumber($contact);
                $this->contactPhoneNumberRepo->save($contact);
            }
        }
    }

}
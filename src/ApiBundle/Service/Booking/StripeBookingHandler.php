<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.12.2015
 * Time: 13:19
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\DTO\PayURequestObject;
use ApiBundle\Exception\BookingRequestSaveException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Exception\InvalidFormException;
use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Form\BookingFormType;
use ApiBundle\Service\Booking\Exception\MakeBookingException;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use ApiBundle\Service\Mailer\Mailer;
use ApiBundle\Service\Stripe\Exception\NotSuccessfullPaymentException;
use ApiBundle\Service\Stripe\Exception\PaymentException;
use ApiBundle\Service\Stripe\StripePaymentGateway;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use AppBundle\Service\Money\MoneyConverter;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Takes and validates booking params
 * Returns booking data
 *
 * Class BookingHandler
 * @package ApiBundle\Service\Booking
 */
class StripeBookingHandler
{

    use LoggerAwareTrait;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var BookingHandlerTravelGuru
     */
    private $handlerTravelGuru;

    /**
     * @var BookingHandlerHotelsPro
     */
    private $handlerHotelsPro;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var StripePaymentGateway
     */
    private $stripe;

    /**
     * @var BookingRequestRepository
     */
    private $bookingRequestRepository;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * @var MakeBookingClient
     */
    private $makeBookingHotelsProClient;

    /**
     * @var MakeBookingClient
     */
    private $makeBookingTravelGuruClient;

    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $defaultNationality;

    /**
     * BookingHandler constructor.
     * @param FormFactory $formFactory
     * @param BookingHandlerTravelGuru $handlerTravelGuru
     * @param BookingHandlerHotelsPro $handlerHotelsPro
     * @param TokenStorageInterface $tokenStorage
     * @param StripePaymentGateway $stripe
     * @param BookingRequestRepository $bookingRequestRepository
     * @param MoneyConverter $moneyConverter
     * @param MakeBookingClient $makeBookingHotelsProClient
     * @param MakeBookingClient $makeBookingTravelGuruClient
     * @param Mailer $mailer
     * @param $defaultNationality
     */
    public function __construct(
        FormFactory $formFactory,
        BookingHandlerTravelGuru $handlerTravelGuru,
        BookingHandlerHotelsPro $handlerHotelsPro,
        TokenStorageInterface $tokenStorage,
        StripePaymentGateway $stripe,
        BookingRequestRepository $bookingRequestRepository,
        MoneyConverter $moneyConverter,
        MakeBookingClient $makeBookingHotelsProClient,
        MakeBookingClient $makeBookingTravelGuruClient,
        Mailer $mailer,
        $defaultNationality
    ) {
        $this->formFactory = $formFactory;
        $this->handlerTravelGuru = $handlerTravelGuru;
        $this->handlerHotelsPro = $handlerHotelsPro;
        $this->tokenStorage = $tokenStorage;
        $this->stripe = $stripe;
        $this->bookingRequestRepository = $bookingRequestRepository;
        $this->moneyConverter = $moneyConverter;
        $this->makeBookingHotelsProClient = $makeBookingHotelsProClient;
        $this->makeBookingTravelGuruClient = $makeBookingTravelGuruClient;
        $this->mailer = $mailer;
        $this->defaultNationality = $defaultNationality;
    }

    /**
     * Returns booking data
     *
     * @param Request $request
     * @param BookingContext $context
     *
     * @return BookingRequest
     * @throws BadNationalityCodeException
     * @throws InvalidFormException If bad request data
     * @throws InvalidOptionsException Unchecked exception
     * @throws RoomNotFound Unchecked exception
     * @throws HotelServiceErrorResponse Unchecked exception
     * @throws BookingRequestSaveException Unchecked exception
     */
    public function handle(Request $request, BookingContext $context)
    {
        $form = $this->getBookingForm();
        $form->handleRequest($request);


        if ($form->isValid()) {
            /** @var BookingParams $bookingParams */
            $bookingParams = $form->getData();
            $bookingParams->setBookingContext($context);
            if ($bookingParams->getNationality() == null) {
                $bookingParams->setNationality($this->defaultNationality);
            }
            return $this->handleBookingRequest($bookingParams);
        }

        throw new InvalidFormException($form);
    }

    /**
     * Gets booking form
     *
     * @return \Symfony\Component\Form\Form|FormInterface
     * @throws InvalidOptionsException
     */
    private function getBookingForm()
    {
        return $this->formFactory->create(
            new BookingFormType(),
            new BookingParams(),
            [
                'method' => Request::METHOD_POST,
            ]
        );
    }

    /**
     * Returns booking data
     *
     * @param BookingParams $bookingParams
     * @throws RoomNotFound
     * @throws HotelServiceErrorResponse
     * @throws BookingRequestSaveException
     *
     * @return BookingRequest
     * @throws BadNationalityCodeException
     */
    private function handleBookingRequest(BookingParams $bookingParams)
    {
        $booking = $this->createBooking($bookingParams);
        $booking->setStatus(BookingRequest::STATUS_CONFIRMING);
        $metaDataDTO = MetaDataDTO::fromJsonString($bookingParams->getMeta());
        switch ($metaDataDTO->serviceName) {
            case HotelsListApiManagerTravelGuru::NAME:
                $this->handlerTravelGuru->handle($bookingParams, $booking);
                break;

            case HotelsListApiManagerHotelsPro::NAME:
                $this->handlerHotelsPro->handle($bookingParams, $booking);
                break;

            default:
                throw new HotelServiceErrorResponse(
                    sprintf(
                        'Invalid service name: %s',
                        $metaDataDTO->serviceName
                    )
                );
        }

        $this->fillInternalMoney($booking);

        $stripeResponseObject = null;
        try {
            $stripeResponseObject = $this->stripe->pay(
                $booking->getPayPricePaymentSystem()->getInCentsAmount(),
                $booking->getPayPricePaymentSystem()->getCurrencyCode(),
                $booking->getStripeToken(),
                $booking->getProfileEmail()
            );

            $booking->setStripeChargeId($stripeResponseObject->id);
            $booking->setStripeTransactionId($stripeResponseObject->balance_trasanction);
            $booking->setStripeResponse($stripeResponseObject);

            $this->bookingRequestRepository->save($booking);

            switch ($metaDataDTO->serviceName) {
                case HotelsListApiManagerTravelGuru::NAME:
                    $this->makeBookingTravelGuruClient->makeRealBooking($booking);
                    break;

                case HotelsListApiManagerHotelsPro::NAME:
                    $this->makeBookingHotelsProClient->makeRealBooking($booking);
                    break;

                default:
                    throw new HotelServiceErrorResponse(
                        sprintf(
                            'Invalid service name: %s',
                            $metaDataDTO->serviceName
                        )
                    );
            }
            $message = "Hi there!\n\nYour booking is confirmed.\n\nYour booking ID is %s.\n\nWe will write to you soon with the Hotel Confirmation Number.\n\nPlease note that a payment receipt has been sent to you in a separate email.\n\nWish you a happy stay!\n\nFrom the SelectRooms Team\nsupport@selectrooms.co";
            $subject = 'Booking confirmed - ';
            $booking->setStatus(BookingRequest::STATUS_CONFIRMED);
            $this->sendNotifyEmail($booking->getProfileEmail(), $booking->getPublicBookingId(), $subject, $message);
            $this->bookingRequestRepository->save($booking);

        } catch (PaymentException $e) {
            $this->logError($e->getMessage(), $e->getTrace());
            $this->bookingRequestRepository->save($booking);
            $message = "Hi there!\n\nYour booking request has been received. Your booking ID is %s.\n\nThis is a rare case where your booking could not be confirmed with the hotel.\n\nPayments, if any, will be refunded automatically and will be credited to your account within 10 business days.\n\nPlease note that a payment receipt will be sent to you in a separate email.\n\nApologies for the inconvenience. We will look into this and write to you as soon as we can!\n\nFrom the SelectRooms Team\nsupport@selectrooms.co";
            $subject = 'Booking received - ';
            $this->sendNotifyEmail($booking->getProfileEmail(), $booking->getPublicBookingId(), $subject, $message);


            throw new HotelServiceErrorResponse($e->getMessage(), $e->getCode(), $e);
        } catch (NotSuccessfullPaymentException $e) {
            $this->logError($e->getMessage(), $e->getCharge()->jsonSerialize());
            $this->bookingRequestRepository->save($booking);
            $message = "Hi there!\n\nYour booking request has been received. Your booking ID is %s.\n\nThis is a rare case where your booking could not be confirmed with the hotel.\n\nPayments, if any, will be refunded automatically and will be credited to your account within 10 business days.\n\nPlease note that a payment receipt will be sent to you in a separate email.\n\nApologies for the inconvenience. We will look into this and write to you as soon as we can!\n\nFrom the SelectRooms Team\nsupport@selectrooms.co";
            $subject = 'Booking received - ';
            $this->sendNotifyEmail($booking->getProfileEmail(), $booking->getPublicBookingId(), $subject, $message);
            $booking->setStripeResponse($stripeResponseObject);
            $booking->setStripeChargeId($stripeResponseObject->id);
            $this->bookingRequestRepository->save($booking);

            throw new HotelServiceErrorResponse($e->getMessage(), $e->getCode(), $e);
        } catch (MakeBookingException $e) {
            $this->bookingRequestRepository->save($booking);
            throw new HotelServiceErrorResponse($e->getMessage(), $e->getCode(), $e);
        }
        return $booking;
    }

    /**
     * @param BookingParams $bookingParams
     * @return BookingRequest
     */
    private function createBooking(BookingParams $bookingParams)
    {
        $booking = BookingRequest::fromBookingParams($bookingParams);
        $booking->setUser($this->tokenStorage->getToken()->getUser());

        return $booking;
    }

    private function fillInternalMoney(BookingRequest $booking)
    {
        $booking->setPurePriceInternal(
            $this->moneyConverter->convertToInternalMoney($booking->getPurePriceServiceProvider())
        );
        $booking->setSalePriceInternal(
            $this->moneyConverter->convertToInternalMoney($booking->getSalePriceServiceProvider())
        );
        $booking->setPureTaxInternal(
            $this->moneyConverter->convertToInternalMoney($booking->getPureTaxServiceProvider())
        );
        $booking->setSaleTaxInternal(
            $this->moneyConverter->convertToInternalMoney($booking->getSaleTaxServiceProvider())
        );

        $booking->setPayPriceInternal($booking->getSalePriceInternal()->add($booking->getSaleTaxInternal()));
        $booking->setPayPricePaymentSystem(
            $this->moneyConverter->convertMoney(
                $booking->getSalePriceServiceProvider()->add($booking->getSaleTaxServiceProvider()),
                $booking->getRequestedCurrency()
            )
        );
    }

    /**
     * @param string $email
     * @param string $id
     * @param string $message
     * @param string $subject
     */
    private function sendNotifyEmail($email, $id, $subject, $message)
    {
        $this->mailer->sendMailToClient($email, $subject.$id, sprintf($message, $id));
    }
}
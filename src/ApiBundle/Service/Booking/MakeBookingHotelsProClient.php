<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 30.12.2015
 * Time: 17:05
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\DTO\PayUResponseObject;
use ApiBundle\Service\Booking\Exception\MakeBookingException;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class MakeBookingHotelsProClient implements MakeBookingClient
{

    use LoggerAwareTrait;

    /**
     * @var BookingClientHotelsPro
     */
    private $bookingClientHotelsPro;

    /**
     * PayuSuccessHandlerHotelsPro constructor.
     * @param BookingClientHotelsPro $bookingClientHotelsPro
     */
    public function __construct(
        BookingClientHotelsPro $bookingClientHotelsPro
    ) {
        $this->bookingClientHotelsPro = $bookingClientHotelsPro;
    }

    /**
     * @param BookingRequest $booking
     * @return void
     * @throws MakeBookingException
     */
    public function makeRealBooking(BookingRequest $booking)
    {
        try {
            $response = $this->bookingClientHotelsPro->makeBooking($booking);
        } catch (\Exception $e) {
            throw new MakeBookingException($booking, $e->getMessage(), $e);
        }
        if (!$response->getError()) {
            if ($response->getTrackingId()) {
                $booking->setTrackingId($response->getTrackingId());
                $booking->setHotelsProAgencyReferenceNumber($response->getAgencyReferenceNumber());
                $booking->setConfirmationNumber($response->getConfirmationNumber());
                return;
            } else {
                throw new MakeBookingException($booking, 'Empty tracking ID from hotelsPro.');
            }
        } else {
            throw new MakeBookingException($booking, $response->getError());
        }
    }

}
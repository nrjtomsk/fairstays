<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.03.2016
 * Time: 11:01
 */

namespace ApiBundle\Service\Booking\Exception;


use AppBundle\Entity\BookingRequest;

/**
 * Class MakeBookingException
 * @package ApiBundle\Service\Booking\Exception
 */
class MakeBookingException extends \RuntimeException
{
    /**
     * @var BookingRequest
     */
    private $booking;

    /**
     * MakeBookingException constructor.
     * @param BookingRequest $booking
     * @param int $message
     * @param \Exception|null $previous
     */
    public function __construct(BookingRequest $booking, $message, \Exception $previous = null)
    {
        parent::__construct($message, $code = 1, $previous);
        $this->booking = $booking;
    }


    /**
     * @return BookingRequest
     */
    public function getBooking()
    {
        return $this->booking;
    }
}
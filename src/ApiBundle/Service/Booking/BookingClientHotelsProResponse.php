<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 28.01.2016
 * Time: 11:43
 */

namespace ApiBundle\Service\Booking;


use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class BookingClientHotelsProResponse
{
    /**
     * @var string
     */
    private $error;

    /**
     * @var string
     */
    private $trackingId;

    /**
     * @var string
     */
    private $agencyReferenceNumber;

    /**
     * @var string
     */
    private $confirmationNumber;

    /**
     * @param ResponseInterface $response
     * @return static
     */
    public static function fromHttpResponse(ResponseInterface $response)
    {
        $hotelsProResponse = new static;
        $responseBody = (new JsonEncoder())->decode(
            $response->getBody()->getContents(),
            JsonEncoder::FORMAT
        );

        $hotelsProResponse->setError(self::getErrors($responseBody));
        $hotelsProResponse->setTrackingId(isset($responseBody['trackingId']) ? $responseBody['trackingId'] : null);
        $hotelsProResponse->setAgencyReferenceNumber(
            isset($responseBody['agencyReferenceNumber']) ? $responseBody['agencyReferenceNumber'] : null
        );
        $hotelsProResponse->setConfirmationNumber(
            isset($responseBody['hotelBookingInfo']['confirmationNumber']) ? $responseBody['hotelBookingInfo']['confirmationNumber'] : null
        );

        return $hotelsProResponse;
    }

    /**
     * @param $responseBody
     * @return bool
     */
    private static function getErrors($responseBody)
    {

        if (is_array($responseBody)
            && count($responseBody) === 2
            && isset($responseBody[0], $responseBody[1])
            && $responseBody[0] > 500
        ) {
            return $responseBody[1];
        }
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @param string $trackingId
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
    }

    /**
     * @param string $agencyReferenceNumber
     */
    public function setAgencyReferenceNumber($agencyReferenceNumber)
    {
        $this->agencyReferenceNumber = $agencyReferenceNumber;
    }

    /**
     * @param string $confirmationNumber
     */
    public function setConfirmationNumber($confirmationNumber)
    {
        $this->confirmationNumber = $confirmationNumber;
    }

    /**
     * @return string
     */
    public function getAgencyReferenceNumber()
    {
        return $this->agencyReferenceNumber;
    }

    /**
     * @return string
     */
    public function getConfirmationNumber()
    {
        return $this->confirmationNumber;
    }



}
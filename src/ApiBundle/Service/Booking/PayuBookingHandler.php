<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.03.2016
 * Time: 10:25
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\DTO\PayURequestObject;
use ApiBundle\Exception\BookingRequestSaveException;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Exception\HotelServiceErrorResponseTravelGuru;
use ApiBundle\Exception\InvalidFormException;
use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Form\BookingFormType;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use ApiBundle\Service\PayU\PayURequestObjectProvider;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Takes and validates booking params
 * Returns booking data
 *
 * Class BookingHandler
 * @package ApiBundle\Service\Booking
 */
class PayuBookingHandler
{
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var BookingHandlerTravelGuru
     */
    private $handlerTravelGuru;
    /**
     * @var BookingHandlerHotelsPro
     */
    private $handlerHotelsPro;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var PayURequestObjectProvider
     */
    private $payURequestObjectProvider;
    /**
     * @var BookingRequestRepository
     */
    private $bookingRequestRepository;
    
    /**
     * BookingHandler constructor.
     * @param FormFactory $formFactory
     * @param BookingHandlerTravelGuru $handlerTravelGuru
     * @param BookingHandlerHotelsPro $handlerHotelsPro
     * @param TokenStorageInterface $tokenStorage
     * @param PayURequestObjectProvider $payURequestObjectProvider
     * @param BookingRequestRepository $bookingRequestRepository
     */
    public function __construct(
        FormFactory $formFactory,
        BookingHandlerTravelGuru $handlerTravelGuru,
        BookingHandlerHotelsPro $handlerHotelsPro,
        TokenStorageInterface $tokenStorage,
        PayURequestObjectProvider $payURequestObjectProvider,
        BookingRequestRepository $bookingRequestRepository
    ) {
        $this->formFactory = $formFactory;
        $this->handlerTravelGuru = $handlerTravelGuru;
        $this->handlerHotelsPro = $handlerHotelsPro;
        $this->tokenStorage = $tokenStorage;
        $this->payURequestObjectProvider = $payURequestObjectProvider;
        $this->bookingRequestRepository = $bookingRequestRepository;
    }
    /**
     * Returns booking data
     *
     * @param Request $request
     * @return PayURequestObject
     *
     * @throws InvalidFormException If bad request data
     * @throws InvalidOptionsException Unchecked exception
     * @throws RoomNotFound Unchecked exception
     * @throws HotelServiceErrorResponse Unchecked exception
     * @throws HotelServiceErrorResponseTravelGuru Unchecked exception
     * @throws BookingRequestSaveException Unchecked exception
     */
    public function handle(Request $request)
    {
        $form = $this->getBookingForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            /** @var BookingParams $bookingParams */
            $bookingParams = $form->getData();
            return $this->handleBookingRequest($bookingParams);
        }
        throw new InvalidFormException($form);
    }
    /**
     * Gets booking form
     *
     * @return \Symfony\Component\Form\Form|FormInterface
     * @throws InvalidOptionsException
     */
    private function getBookingForm()
    {
        return $this->formFactory->create(
            new BookingFormType(),
            new BookingParams(),
            [
                'method' => Request::METHOD_POST,
            ]
        );
    }
    /**
     * Returns booking data
     *
     * @param BookingParams $bookingParams
     * @return PayURequestObject
     * @throws RoomNotFound
     * @throws HotelServiceErrorResponse
     * @throws HotelServiceErrorResponseTravelGuru
     * @throws BookingRequestSaveException
     */
    private function handleBookingRequest(BookingParams $bookingParams)
    {
        $booking = $this->createBooking($bookingParams);
        $booking->setStatus(BookingRequest::STATUS_CONFIRMING);
        $metaDataDTO = MetaDataDTO::fromJsonString($bookingParams->getMeta());
        switch ($metaDataDTO->serviceName) {
            case HotelsListApiManagerTravelGuru::NAME:
                $this->handlerTravelGuru->handle($bookingParams, $booking);
                break;
            case HotelsListApiManagerHotelsPro::NAME:
                $this->handlerHotelsPro->handle($bookingParams, $booking);
                break;
            default:
                throw new HotelServiceErrorResponse(
                    sprintf(
                        'Invalid service name: %s',
                        $metaDataDTO->serviceName
                    )
                );
        }
        $this->bookingRequestRepository->save($booking);
        $payURequestObject = $this->payURequestObjectProvider->getPayURequestObjectByBookingRequest($booking);
        $this->fillPayuTransactionId($booking, $payURequestObject);
        $this->bookingRequestRepository->save($booking);
        return $payURequestObject;
    }
    /**
     * @param BookingParams $bookingParams
     * @return BookingRequest
     */
    private function createBooking(BookingParams $bookingParams)
    {
        $booking = BookingRequest::fromBookingParams($bookingParams);
        $booking->setUser($this->tokenStorage->getToken()->getUser());
        return $booking;
    }
    /**
     * @param BookingRequest $booking
     * @param PayURequestObject $payURequestObject
     */
    private function fillPayuTransactionId(BookingRequest $booking, PayURequestObject $payURequestObject)
    {
        $booking->setPayuTransactionId($payURequestObject->getTxnid());
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.12.2015
 * Time: 16:11
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\DTO\HotelProvisionalBookingRequestParams;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Exception\HotelServiceErrorResponseTravelGuru;
use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsClient;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use ApiBundle\Service\HotelDetails\InputParams;
use ApiBundle\Service\Hotels\Booking\Provisional\TravelGuruRequest;
use ApiBundle\Service\Hotels\Booking\Provisional\TravelGuruResponse;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BidRepository;
use AppBundle\Service\Money\Money;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BookingHandlerTravelGuru
 * @package ApiBundle\Service\Booking
 */
class BookingHandlerTravelGuru
{

    /**
     * @var HotelDetailsClient
     */
    private $hotelDetailsClient;

    /**
     * @var HotelDetailsClient
     */
    private $bidstaysHotelDetailsClient;

    /**
     * @var string
     */
    private $endPoint;

    /**
     * @var string
     */
    private $authUsername;

    /**
     * @var string
     */
    private $authPropertyId;

    /**
     * @var string
     */
    private $authPassword;

    /**
     * @var BidRepository
     */
    private $bidRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * BookingHandlerTravelGuru constructor.
     * @param HotelDetailsClient $hotelDetailsClient
     * @param string $endPoint
     * @param string $authUsername
     * @param string $authPropertyId
     * @param string $authPassword
     * @param HotelDetailsClient $bidstaysHotelDetailsClient
     * @param BidRepository $bidRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        HotelDetailsClient $hotelDetailsClient,
        $endPoint,
        $authUsername,
        $authPropertyId,
        $authPassword,
        HotelDetailsClient $bidstaysHotelDetailsClient,
        BidRepository $bidRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->hotelDetailsClient = $hotelDetailsClient;
        $this->endPoint = $endPoint;
        $this->authUsername = $authUsername;
        $this->authPropertyId = $authPropertyId;
        $this->authPassword = $authPassword;
        $this->bidstaysHotelDetailsClient = $bidstaysHotelDetailsClient;
        $this->bidRepository = $bidRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param BookingParams $bookingParams
     * @param BookingRequest $booking
     * @throws HotelServiceErrorResponse
     * @throws RoomNotFound
     */
    public function handle(BookingParams $bookingParams, BookingRequest $booking)
    {
        $hotelDetails = $this->requestHotelDetails($bookingParams);
        $hotelRoom = $this->findHotelRoom($hotelDetails, $bookingParams);

        $this->mapBidsToRoomPrice($hotelDetails, $hotelRoom, $bookingParams);

        $this->fillBookingPrices($booking, $hotelRoom);
        $this->fillBookingCancellationPolicy($booking, $hotelRoom);
        $response = $this->sendProvisionalRequest($booking);

        if ($response->getError()) {
            throw new HotelServiceErrorResponseTravelGuru($response->getError()['message']);
        }

        $this->fillBookingBookingId($booking, $response);
    }

    /**
     * Returns booking details
     *
     * @param BookingParams $bookingParams
     * @return HotelDetails
     */
    private function requestHotelDetails(BookingParams $bookingParams)
    {
        if ($bookingParams->getBookingContext()->isBidstays) {
            return $this->bidstaysHotelDetailsClient->get(InputParams::fromBookingParams($bookingParams));
        }
        return $this->hotelDetailsClient->get(InputParams::fromBookingParams($bookingParams));
    }

    /**
     * Finds and returns hotel room
     *
     * @param HotelDetails $hotelDetails
     * @param BookingParams $bookingParams
     * @return HotelDetailsRoom
     * @throws RoomNotFound
     */
    private function findHotelRoom(HotelDetails $hotelDetails, BookingParams $bookingParams)
    {
        foreach ($hotelDetails->rooms as $room) {
            if ($room->ratePlanCode === $bookingParams->getRatePlan()
                && $room->roomTypeCode === $bookingParams->getRoomType()
            ) {
                return $room;
            }
        }

        throw new RoomNotFound('Cannot find room');
    }

    /**
     * Saves booking prices data
     *
     * @param BookingRequest $booking
     * @param HotelDetailsRoom $hotelRoom
     */
    private function fillBookingPrices(BookingRequest $booking, HotelDetailsRoom $hotelRoom)
    {
        $booking->setPurePriceServiceProvider($hotelRoom->priceBefore);
        $booking->setSalePriceServiceProvider($hotelRoom->price);
        $booking->setPureTaxServiceProvider($hotelRoom->taxBefore);
        $booking->setSaleTaxServiceProvider($hotelRoom->tax);
    }

    /**
     * @param BookingRequest $booking
     * @return TravelGuruResponse
     */
    private function sendProvisionalRequest(BookingRequest $booking)
    {
        $request = new TravelGuruRequest(
            $this->endPoint,
            $this->authUsername,
            $this->authPropertyId,
            $this->authPassword
        );

        $requestParams = HotelProvisionalBookingRequestParams::fromBooking($booking);
        $request->setRequestParams($requestParams);

        return $request->send();
    }

    /**
     * @param BookingRequest $booking
     * @param TravelGuruResponse $response
     */
    private function fillBookingBookingId(BookingRequest $booking, TravelGuruResponse $response)
    {
        $booking->setBookingId($response->getBookingId());
    }

    /**
     * @param BookingRequest $booking
     * @param HotelDetailsRoom $hotelRoom
     */
    private function fillBookingCancellationPolicy(BookingRequest $booking, HotelDetailsRoom $hotelRoom)
    {
        $booking->setHotelsCancellationPolicy($hotelRoom->cancellationPolicy);
    }

    /**
     * @param HotelDetails $hotelDetails
     * @param HotelDetailsRoom $hotelRoom
     * @param BookingParams $bookingParams
     */
    private function mapBidsToRoomPrice(HotelDetails $hotelDetails, HotelDetailsRoom $hotelRoom, BookingParams $bookingParams)
    {
        if ($bookingParams->getBookingContext()->isBidstays) {
            $bids = $this->bidRepository->findUserBidsToHotelForLastDay(
                $this->tokenStorage->getToken()->getUser(),
                $hotelDetails->code,
                HotelsListApiManagerTravelGuru::NAME
            );
            if (is_array($bids) && count($bids)) {
                foreach ($bids as $bid) {
                    if ($bid->getRoom()['ratePlan'] == $hotelRoom->ratePlanCode
                        && $bid->getRoom()['roomType'] == $hotelRoom->roomTypeCode
                    ) {
                        $hotelRoom->price = Money::build($bid->getAmount(), $bid->getCurrency());
                        break;
                    }
                }
            }
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.12.2015
 * Time: 16:14
 */

namespace ApiBundle\Service\Booking;

use ApiBundle\Exception\RoomNotFound;
use ApiBundle\Service\Country\CountryCodeDictionary;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use ApiBundle\Service\HotelDetails\Bidstays\HotelDetailsRoomBidDto;
use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsClient;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use ApiBundle\Service\HotelDetails\InputParams;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BidRepository;
use AppBundle\Service\HotelsPro\Exception\SelectedHotelsPriceException;
use AppBundle\Service\HotelsPro\SelectedHotelsPrice;
use AppBundle\Service\Money\Money;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BookingHandlerHotelsPro
 * @package ApiBundle\Service\Booking
 */
class BookingHandlerHotelsPro
{
    /**
     * @var HotelDetailsClient
     */
    private $hotelDetailsClient;

    /**
     * @var float
     */
    private $hotelsProInterestPercent;

    /**
     * @var SelectedHotelsPrice
     */
    private $selectedHotelsPrice;

    /**
     * @var CountryCodeDictionary
     */
    private $countryCodeDictionary;

    /**
     * @var HotelDetailsClient
     */
    private $bidstaysHotelDetailsClient;

    /**
     * @var BidRepository
     */
    private $bidRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * BookingHandlerHotelsPro constructor.
     * @param HotelDetailsClient $hotelDetailsClient
     * @param $hotelsProInterestPercent
     * @param SelectedHotelsPrice $selectedHotelsPrice
     * @param CountryCodeDictionary $countryCodeDictionary
     * @param HotelDetailsClient $bidstaysHotelDetailsClient
     * @param BidRepository $bidRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        HotelDetailsClient $hotelDetailsClient,
        $hotelsProInterestPercent,
        SelectedHotelsPrice $selectedHotelsPrice,
        CountryCodeDictionary $countryCodeDictionary,
        HotelDetailsClient $bidstaysHotelDetailsClient,
        BidRepository $bidRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->hotelDetailsClient = $hotelDetailsClient;
        $this->hotelsProInterestPercent = $hotelsProInterestPercent;
        $this->selectedHotelsPrice = $selectedHotelsPrice;
        $this->countryCodeDictionary = $countryCodeDictionary;
        $this->bidstaysHotelDetailsClient = $bidstaysHotelDetailsClient;
        $this->bidRepository = $bidRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param BookingParams $bookingParams
     * @param BookingRequest $booking
     * @throws RoomNotFound
     * @throws BadNationalityCodeException
     * @throws \RuntimeException
     */
    public function handle(BookingParams $bookingParams, BookingRequest $booking)
    {
        $this->countryCodeDictionary->checkNationalityCodeInAllowedList($bookingParams->getNationality());
        $hotelDetails = $this->requestHotelDetails($bookingParams);

        $hotelRoom = $this->findHotelRoom($hotelDetails, $bookingParams);
        $this->mapBidsToRoomPrice($hotelDetails, $hotelRoom, $bookingParams);
        $this->fillBookingPrices($booking, $hotelRoom);
        $this->fillBookingCancellationPolicy($booking, $hotelRoom);
        $this->fillRatesPerNight($booking, $hotelRoom);

        try {
            $rate = $this->selectedHotelsPrice->getSelectedHotelsRate(
                $booking->getMetaData()->cityId,
                $bookingParams->getCheckIn(),
                $bookingParams->getHotelCode()
            );
            $this->fillMarkupRate($booking, $rate);
        } catch (SelectedHotelsPriceException $e) {
            $this->fillMarkupRate($booking, $this->hotelsProInterestPercent);
        }
    }

    /**
     * @param BookingParams $bookingParams
     * @return HotelDetails
     */
    private function requestHotelDetails(BookingParams $bookingParams)
    {
        if ($bookingParams->getBookingContext()->isBidstays) {
            return $this->bidstaysHotelDetailsClient->get(InputParams::fromBookingParams($bookingParams));
        }
        return $this->hotelDetailsClient->get(InputParams::fromBookingParams($bookingParams));
    }

    /**
     * @param HotelDetails $hotelDetails
     * @param BookingParams $bookingParams
     * @return HotelDetailsRoom
     * @throws RoomNotFound
     */
    private function findHotelRoom(HotelDetails $hotelDetails, BookingParams $bookingParams)
    {
        $metaData = MetaDataDTO::fromJsonString($bookingParams->getMeta());
        foreach ($hotelDetails->rooms as $room) {
            if ($room->metaData->hotelsProProcessId == $metaData->hotelsProProcessId) {
                return $room;
            }
        }

        throw new RoomNotFound('Cannot find room');
    }

    /**
     * Saves booking prices data
     *
     * @param BookingRequest $booking
     * @param HotelDetailsRoom $hotelRoom
     */
    private function fillBookingPrices(BookingRequest $booking, HotelDetailsRoom $hotelRoom)
    {
        $booking->setPurePriceServiceProvider($hotelRoom->priceBefore);
        $booking->setSalePriceServiceProvider($hotelRoom->price);
        $booking->setPureTaxServiceProvider($hotelRoom->taxBefore);
        $booking->setSaleTaxServiceProvider($hotelRoom->tax);
    }

    /**
     * @param BookingRequest $booking
     * @param HotelDetailsRoom $hotelRoom
     */
    private function fillBookingCancellationPolicy(BookingRequest $booking, HotelDetailsRoom $hotelRoom)
    {
        $booking->setHotelsCancellationPolicy($hotelRoom->cancellationPolicy);
    }

    /**
     * @param BookingRequest $booking
     * @param HotelDetailsRoom $hotelRoom
     */
    private function fillRatesPerNight(BookingRequest $booking, HotelDetailsRoom $hotelRoom)
    {
        $booking->setRatesPerNight($hotelRoom->ratesPerNight);
    }

    private function fillMarkupRate(BookingRequest $booking, $percent)
    {
        $booking->setMarkupRate($percent);
    }

    /**
     * @param HotelDetails $hotelDetails
     * @param HotelDetailsRoom $hotelRoom
     * @param BookingParams $bookingParams
     */
    private function mapBidsToRoomPrice(HotelDetails $hotelDetails, HotelDetailsRoom $hotelRoom, BookingParams $bookingParams)
    {
        if ($bookingParams->getBookingContext()->isBidstays) {
            $bids = $this->bidRepository->findUserBidsToHotelForLastDay(
                $this->tokenStorage->getToken()->getUser(),
                $hotelDetails->code,
                HotelsListApiManagerHotelsPro::NAME
            );
            if (is_array($bids) && count($bids)) {
                foreach ($bids as $bid) {
                    $meta = MetaDataDTO::fromJsonString($bid->getRoom()['meta']);
                    if ($meta->hotelsProProcessId == $hotelRoom->metaData->hotelsProProcessId) {
                        $hotelRoom->price = Money::build($bid->getAmount(), $bid->getCurrency());
                        break;
                    }
                }
            }
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Vitaly
 * Date: 18.05.2016
 * Time: 18:59
 */

namespace ApiBundle\Service\Booking;

/**
 * Class BookingContext
 * @package ApiBundle\Service\Booking
 */
class BookingContext
{
    /**
     * @var boolean
     */
    public $isBidstays = false;
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.03.2016
 * Time: 10:59
 */

namespace ApiBundle\Service\Booking;


use ApiBundle\Service\Booking\Exception\MakeBookingException;
use AppBundle\Entity\BookingRequest;

interface MakeBookingClient
{
    /**
     * @param BookingRequest $booking
     * @throws MakeBookingException
     * @return void
     */
    public function makeRealBooking(BookingRequest $booking);
}
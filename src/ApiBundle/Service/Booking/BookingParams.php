<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 09.10.2015
 * Time: 14:19
 */

namespace ApiBundle\Service\Booking;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BookingParams
 * @package ApiBundle\Service\Booking
 */
class BookingParams
{
    /**
     * @var BookingContext
     */
    private $bookingContext;

    /**
     * @var string
     *
     * @Assert\NotNull(message="Hotel code is required")
     */
    private $hotelCode;

    /**
     * @var string
     * @Assert\NotNull(message="Requested currency is required")
     */
    private $requestedCurrency;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $checkIn;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $checkOut;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $roomName;

    /**
     * @var string
     */
    private $roomType;

    /**
     * @var integer
     * @Assert\NotNull()
     */
    private $roomTypeNumberOfUnits;

    /**
     * @var string
     */
    private $ratePlan;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $namePrefix;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $phoneArea;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $phoneCountryCode;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $phoneExtension;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $phoneNumber;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $stripeToken;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $phoneTechType;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $cityName;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Length(min="4", max="10")
     */
    private $postalCode;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $addressStateProv;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $addressStateProvCode;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $countryName;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $countryNameCode;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $preferences;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $rooms;

    /**
     * @var string
     * @Assert\NotNull()
     */
    private $meta;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @return BookingContext
     */
    public function getBookingContext()
    {
        return $this->bookingContext;
    }

    /**
     * @param BookingContext $bookingContext
     * @return void
     */
    public function setBookingContext($bookingContext)
    {
        $this->bookingContext = $bookingContext;
    }

    /**
     * @return mixed
     */
    public function getHotelCode()
    {
        return $this->hotelCode;
    }

    /**
     * @param mixed $hotelCode
     */
    public function setHotelCode($hotelCode)
    {
        $this->hotelCode = $hotelCode;
    }

    /**
     * @return mixed
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param mixed $checkIn
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;
    }

    /**
     * @return mixed
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param mixed $checkOut
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;
    }

    /**
     * @return mixed
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    /**
     * @param mixed $roomName
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;
    }

    /**
     * @return mixed
     */
    public function getRoomType()
    {
        return $this->roomType;
    }

    /**
     * @param mixed $roomType
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;
    }

    /**
     * @return mixed
     */
    public function getRoomTypeNumberOfUnits()
    {
        return $this->roomTypeNumberOfUnits;
    }

    /**
     * @param mixed $roomTypeNumberOfUnits
     */
    public function setRoomTypeNumberOfUnits($roomTypeNumberOfUnits)
    {
        $this->roomTypeNumberOfUnits = $roomTypeNumberOfUnits;
    }

    /**
     * @return mixed
     */
    public function getRatePlan()
    {
        return $this->ratePlan;
    }

    /**
     * @param mixed $ratePlan
     */
    public function setRatePlan($ratePlan)
    {
        $this->ratePlan = $ratePlan;
    }

    /**
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param string $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    /**
     * @return mixed
     */
    public function getNamePrefix()
    {
        return $this->namePrefix;
    }

    /**
     * @param mixed $namePrefix
     */
    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getPhoneArea()
    {
        return $this->phoneArea;
    }

    /**
     * @param mixed $phoneArea
     */
    public function setPhoneArea($phoneArea)
    {
        $this->phoneArea = $phoneArea;
    }

    /**
     * @return mixed
     */
    public function getPhoneCountryCode()
    {
        return $this->phoneCountryCode;
    }

    /**
     * @param mixed $phoneCountryCode
     */
    public function setPhoneCountryCode($phoneCountryCode)
    {
        $this->phoneCountryCode = $phoneCountryCode;
    }

    /**
     * @return mixed
     */
    public function getPhoneExtension()
    {
        return $this->phoneExtension;
    }

    /**
     * @param mixed $phoneExtension
     */
    public function setPhoneExtension($phoneExtension)
    {
        $this->phoneExtension = $phoneExtension;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getPhoneTechType()
    {
        return $this->phoneTechType;
    }

    /**
     * @param mixed $phoneTechType
     */
    public function setPhoneTechType($phoneTechType)
    {
        $this->phoneTechType = $phoneTechType;
    }

    /**
     * @return string
     */
    public function getStripeToken()
    {
        return $this->stripeToken;
    }

    /**
     * @param string $stripeToken
     */
    public function setStripeToken($stripeToken)
    {
        $this->stripeToken = $stripeToken;
    }
    
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * @return mixed
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param mixed $cityName
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getAddressStateProv()
    {
        return $this->addressStateProv;
    }

    /**
     * @param mixed $addressStateProv
     */
    public function setAddressStateProv($addressStateProv)
    {
        $this->addressStateProv = $addressStateProv;
    }

    /**
     * @return mixed
     */
    public function getAddressStateProvCode()
    {
        return $this->addressStateProvCode;
    }

    /**
     * @param mixed $addressStateProvCode
     */
    public function setAddressStateProvCode($addressStateProvCode)
    {
        $this->addressStateProvCode = $addressStateProvCode;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param mixed $countryName
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return mixed
     */
    public function getCountryNameCode()
    {
        return $this->countryNameCode;
    }

    /**
     * @param mixed $countryNameCode
     */
    public function setCountryNameCode($countryNameCode)
    {
        $this->countryNameCode = $countryNameCode;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getPreferences()
    {
        return $this->preferences;
    }

    /**
     * @param mixed $preferences
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;
    }

    /**
     * @return mixed
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param mixed $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return string
     */
    public function getRequestedCurrency()
    {
        return $this->requestedCurrency;
    }

    /**
     * @param string $requestedCurrency
     */
    public function setRequestedCurrency($requestedCurrency)
    {
        $this->requestedCurrency = $requestedCurrency;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 15:46
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Service\Bid\BidParams;
use ApiBundle\Service\Booking\BookingParams;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Returns inputParams from paramFetcher and bookingParams
 *
 * Class InputParams
 * @package ApiBundle\Service\HotelDetails
 */
class InputParams
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var string
     */
    public $dateFrom;

    /**
     * @var string
     */
    public $dateTo;

    /**
     * @var string[]
     */
    public $rooms;

    /**
     * @var string
     */
    public $nationality;

    /**
     * @var MetaDataDTO
     */
    public $meta;


    /**
     * Returns inputParams from paramFetcher
     *
     * @param ParamFetcherInterface $fetcher
     * @return InputParams
     */
    public static function fromParamFetcher(ParamFetcherInterface $fetcher)
    {
        $input = new static();
        $input->code = $fetcher->get('code', $strict = true);
        $input->dateFrom = $fetcher->get('dateFrom', $strict = true);
        $input->dateTo = $fetcher->get('dateTo', $strict = true);
        $input->meta = $fetcher->get('meta', $strict = true);
        $input->currency = $fetcher->get('currency', $strict = true);

        try {
            $input->nationality = $fetcher->get('nationality');
        } catch (\InvalidArgumentException $e) {
            $input->nationality = null;
        }

        $decoder = new JsonEncoder();

        $input->meta = MetaDataDTO::fromJsonString($input->meta);

        /** @var string[] $rooms */
        $rooms = $fetcher->get('rooms');
        if (is_array($rooms) && count($rooms)) {
            foreach ($rooms as $room) {
                $input->rooms[] = $decoder->decode($room, JsonEncoder::FORMAT);
            }
        }
        return $input;
    }

    /**
     * Returns inputParams from bookingParams
     *
     * @param BookingParams $bookingParams
     * @return InputParams
     */
    public static function fromBookingParams(BookingParams $bookingParams)
    {
        $input = new static();
        $input->code = $bookingParams->getHotelCode();
        $input->dateFrom = $bookingParams->getCheckIn();
        $input->dateTo = $bookingParams->getCheckOut();
        $input->meta = MetaDataDTO::fromJsonString($bookingParams->getMeta());
        $input->currency = $bookingParams->getRequestedCurrency();
        $input->nationality = $bookingParams->getNationality();

        $jsonEncoder = new JsonEncoder();

        $rooms = $jsonEncoder->decode($bookingParams->getRooms(), JsonEncoder::FORMAT);
        foreach ($rooms as $room) {

            $adultCount = 0;
            $childrenCount = 0;
            $childrenAges = [];
            foreach ($room['paxes'] as $pax) {

                if ($pax['type'] === 'Adult') {

                    $adultCount++;

                } elseif ($pax['type'] === 'Child') {

                    $childrenCount++;
                    $childrenAges[] = $pax['age'];
                }
            }

            $input->rooms[] = [
                'adultCount' => $adultCount,
                'childrenCount' => $childrenCount,
                'childrenAges' => $childrenAges,
            ];
        }
        return $input;
    }

    /**
     * @param BidParams $params
     * @return InputParams
     */
    public static function fromBidParams(BidParams $params)
    {
        $input = new static;
        $input->code = $params->getHotelCode();
        $input->currency = $params->getCurrency();
        $input->dateFrom = $params->getCheckIn();
        $input->dateTo = $params->getCheckOut();
        $input->meta = MetaDataDTO::fromJsonString($params->getMeta());
        $input->nationality = $params->getNationality();

        return $input;
    }
}
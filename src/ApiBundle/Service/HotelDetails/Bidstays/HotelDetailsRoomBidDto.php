<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 05.05.2016
 * Time: 13:11
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;


use AppBundle\Entity\Bid;
use AppBundle\Service\Money\Money;
use JMS\Serializer\Annotation as JMS;

/**
 * Class HotelDetailsRoomBidDto
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package ApiBundle\Service\HotelDetails\Bidstays
 */
class HotelDetailsRoomBidDto
{
    /**
     * @var Money
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     */
    public $amount;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     * @var \DateTime
     */
    public $createdAt;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     * @var string
     */
    public $status;

    /**
     * @param Bid $bid
     * @return HotelDetailsRoomBidDto
     */
    public static function fromBid(Bid $bid)
    {
        $item = new static;
        $item->amount = Money::build($bid->getAmount(), $bid->getCurrency());
        $item->createdAt = $bid->getCreatedAt();
        $item->status = $bid->getStatus();
        return $item;
    }
}
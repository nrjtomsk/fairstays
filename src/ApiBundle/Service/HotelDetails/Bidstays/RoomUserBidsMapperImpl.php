<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 05.05.2016
 * Time: 12:14
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;

use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use AppBundle\Entity\Bid;
use AppBundle\Repository\BidRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class RoomUserBidsMapperImpl
 * @package ApiBundle\Service\HotelDetails\Bidstays
 */
class RoomUserBidsMapperImpl implements RoomUserBidsMapper
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var BidRepository
     */
    private $bidRepository;

    /**
     * RoomUserBidsMapperImpl constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param BidRepository $bidRepository
     */
    public function __construct(TokenStorageInterface $tokenStorage, BidRepository $bidRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->bidRepository = $bidRepository;
    }


    /**
     * @param HotelDetails $hotel
     * @return void
     */
    public function mapRooms(HotelDetails $hotel)
    {
        if (is_array($hotel->rooms) && count($hotel->rooms)) {

            $this->fillRoomsWithEmptyBids($hotel->rooms);
            
            $bids = $this->getCurrentUserBidListForHotel($hotel);
            if (!count($bids)) {
                return;
            }
            
            /** @var HotelDetailsRoom $room */
            foreach ($hotel->rooms as $room) {
                $this->mapBidsToRoom($room, $bids);
            }
        }
    }

    /**
     * @param HotelDetails $hotel
     * @return \AppBundle\Entity\Bid[]
     */
    private function getCurrentUserBidListForHotel(HotelDetails $hotel)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        return $this->bidRepository->findUserBidsToHotelForLastDay($currentUser, $hotel->code, $hotel->metaData->serviceName);
    }

    /**
     * @param HotelDetailsRoom $room
     * @param Bid[] $bids
     */
    private function mapBidsToRoom(HotelDetailsRoom $room, array $bids)
    {
        switch ($room->metaData->serviceName) {
            case HotelsListApiManagerTravelGuru::NAME:
                foreach ($bids as $bid) {
                    $bidRoom = $bid->getRoom();
                    if ($room->ratePlanCode == $bidRoom['ratePlan'] && $room->roomTypeCode == $bidRoom['rootType']) {
                        $room->bids[] = HotelDetailsRoomBidDto::fromBid($bid);
                    }
                }
                break;
            case HotelsListApiManagerHotelsPro::NAME:
                foreach ($bids as $bid) {
                    $bidRoom = $bid->getRoom();
                    if (false !== mb_strpos($bidRoom['meta'], $room->metaData->hotelsProProcessId)) {
                        $room->bids[] = HotelDetailsRoomBidDto::fromBid($bid);
                    }
                }
                break;
        }
    }

    /**
     * @param HotelDetailsRoom[] $rooms
     */
    private function fillRoomsWithEmptyBids(array $rooms)
    {
        foreach ($rooms as $room) {
            $room->bids = [];
        }
    }
}
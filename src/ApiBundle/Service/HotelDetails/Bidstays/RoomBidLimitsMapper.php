<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 14:08
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;


use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\InputParams;

interface RoomBidLimitsMapper
{
    /**
     * @param HotelDetails $hotel
     */
    public function mapHotelRooms(HotelDetails $hotel);

    /**
     * @param InputParams $params
     * @return void
     */
    public function setInputParams(InputParams $params);

    /**
     * @return InputParams
     */
    public function getInputParams();
}
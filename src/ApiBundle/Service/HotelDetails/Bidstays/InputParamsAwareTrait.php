<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 15:37
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;


use ApiBundle\Service\HotelDetails\InputParams;

trait InputParamsAwareTrait
{
    /**
     * @var InputParams
     */
    protected $params;

    /**
     * @param InputParams $params
     * @return void
     */
    public function setInputParams(InputParams $params)
    {
        $this->params = $params;
    }

    /**
     * @return InputParams
     */
    public function getInputParams()
    {
        return $this->params;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 05.05.2016
 * Time: 12:12
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;

use ApiBundle\Service\HotelDetails\HotelDetails;

/**
 * Interface RoomUserBidsMapper
 * @package ApiBundle\Service\HotelDetails\Bidstays
 */
interface RoomUserBidsMapper
{
    /**
     * @param HotelDetails $hotel
     * @return void
     */
    public function mapRooms(HotelDetails $hotel);
}
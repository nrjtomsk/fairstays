<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 14:08
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;


use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverter;

/**
 * Class HotelsProRoomBidsMapper
 * @package ApiBundle\Service\HotelDetails\Bidstays
 */
class HotelsProRoomBidLimitsMapper implements RoomBidLimitsMapper
{
    use InputParamsAwareTrait;

    /**
     * @var float
     */
    private $bidLowerMarkup;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * HotelsProRoomBidsMapper constructor.
     * @param float $bidLowerMarkup
     * @param MoneyConverter $moneyConverter
     */
    public function __construct($bidLowerMarkup, MoneyConverter $moneyConverter)
    {
        $this->bidLowerMarkup = $bidLowerMarkup;
        $this->moneyConverter = $moneyConverter;
    }


    /**
     * @param HotelDetails $hotel
     */
    public function mapHotelRooms(HotelDetails $hotel)
    {
        if (is_array($hotel->rooms)) {
            foreach ($hotel->rooms as $room) {
                $this->mapHotelRoom($room);
            }
        }
    }

    /**
     * @param HotelDetailsRoom $room
     */
    private function mapHotelRoom(HotelDetailsRoom $room)
    {
        $currency = $this->getInputParams()->currency;
        $room->bidLowerLimit = $this->moneyConverter->convertMoney(
            $room->priceBefore->multiply(1 + $this->bidLowerMarkup/100), $currency
        );
        $room->bidUpperLimit = $this->moneyConverter->convertMoney($room->price, $currency);
    }
}
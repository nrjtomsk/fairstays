<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 14:08
 */

namespace ApiBundle\Service\HotelDetails\Bidstays;


use ApiBundle\Service\HotelDetails\HotelDetails;
use ApiBundle\Service\HotelDetails\HotelDetailsRoom;
use AppBundle\Service\Money\MoneyConverter;

/**
 * Class TravelGuruRoomBidsMapper
 * @package ApiBundle\Service\HotelDetails\Bidstays
 */
class TravelGuruRoomBidLimitsMapper implements RoomBidLimitsMapper
{
    use InputParamsAwareTrait;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * @var float
     */
    private $bidLowerMarkup;

    /**
     * @var float
     */
    private $bidUpperMarkup;

    /**
     * TravelGuruRoomBidsMapper constructor.
     * @param MoneyConverter $moneyConverter
     * @param float $bidLowerMarkup
     * @param float $bidUpperMarkup
     */
    public function __construct(MoneyConverter $moneyConverter, $bidLowerMarkup, $bidUpperMarkup)
    {
        $this->moneyConverter = $moneyConverter;
        $this->bidLowerMarkup = $bidLowerMarkup;
        $this->bidUpperMarkup = $bidUpperMarkup;
    }


    /**
     * @param HotelDetails $hotel
     */
    public function mapHotelRooms(HotelDetails $hotel)
    {
        if (is_array($hotel->rooms)) {
            foreach ($hotel->rooms as $room) {
                $this->mapHotelRoom($room);
            }
        }
    }

    /**
     * @param HotelDetailsRoom $room
     */
    private function mapHotelRoom(HotelDetailsRoom $room)
    {
        $currencyCode = $this->getInputParams()->currency;
        $room->bidLowerLimit = $this->moneyConverter->convertMoney(
            $room->priceBefore->multiply(1 + $this->bidLowerMarkup / 100), $currencyCode
        );
        $room->bidUpperLimit = $this->moneyConverter->convertMoney(
            $room->price->multiply(1 + $this->bidUpperMarkup / 100), $currencyCode
        );
    }
}
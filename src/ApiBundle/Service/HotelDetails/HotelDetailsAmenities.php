<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 17:52
 */

namespace ApiBundle\Service\HotelDetails;

use JMS\Serializer\Annotation as JMS;

/**
 * Class HotelDetailsAmenities
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package ApiBundle\Service\HotelDetails
 */
class HotelDetailsAmenities
{
    /**
     * @var string[]
     * @JMS\Expose()
     */
    public $property;

    /**
     * @var string[]
     * @JMS\Expose()
     */
    public $room;
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:18
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\HotelDetails\Exception\UnknownProviderException;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\Trivago\TrivagoService;
use AppBundle\Entity\Hotel;
use AppBundle\Service\Money\MoneyConverter;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

/**
 * Returns the params from travelGuru or hotelsPro client
 *
 * Class ResolvedHotelDetailsClient
 * @package ApiBundle\Service\HotelDetails
 */
class ResolvedHotelDetailsClient implements HotelDetailsClient, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelDetailsClient
     */
    private $travelGuruHotelDetailsClient;

    /**
     * @var HotelDetailsClient
     */
    private $hotelsProHotelDetailsClient;

    /**
     * @var TrivagoService
     */
    private $trivagoService;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * ResolvedHotelDetailsClient constructor.
     * @param HotelDetailsClient $travelGuruHotelDetailsClient
     * @param HotelDetailsClient $hotelsProHotelDetailsClient
     * @param MoneyConverter $moneyConverter
     * @param TrivagoService $trivagoService
     */
    public function __construct(
        HotelDetailsClient $travelGuruHotelDetailsClient,
        HotelDetailsClient $hotelsProHotelDetailsClient,
        MoneyConverter $moneyConverter,
        TrivagoService $trivagoService
    ) {
        $this->travelGuruHotelDetailsClient = $travelGuruHotelDetailsClient;
        $this->hotelsProHotelDetailsClient = $hotelsProHotelDetailsClient;
        $this->moneyConverter = $moneyConverter;
        $this->trivagoService = $trivagoService;
    }


    /**
     * @inheritdoc
     * @param InputParams $params
     * @return HotelDetails
     * @throws UnknownProviderException
     */
    public function get(InputParams $params)
    {
        $this->startWatch('hotel_details');

        $this->startWatch('hotel_details_get');
        $hotel = $this->getHotelDetailsFromServiceProvider($params);
        $this->stopWatch('hotel_details_get');
        
        if (!$hotel || empty($hotel->rooms)) {
            throw new HotelNotFoundException;
        }

        $this->startWatch('hotel_details_convert_currency');
        $this->convertCurrenciesToClientAware($hotel, $params->currency);
        $this->fillTrivagoUrl($params, $hotel);
        $this->stopWatch('hotel_details_convert_currency');

        $this->stopWatch('hotel_details');
        return $hotel;
    }

    /**
     * @param InputParams $params
     * @return HotelDetails
     * @throws UnknownProviderException
     */
    private function getHotelDetailsFromServiceProvider(InputParams $params)
    {
        switch ($params->meta->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:
                return $this->hotelsProHotelDetailsClient->get($params);
            case HotelsListApiManagerTravelGuru::NAME:
                return $this->travelGuruHotelDetailsClient->get($params);
            default:
                throw new UnknownProviderException($params->meta->serviceName);
        }
    }

    /**
     * @param HotelDetails $hotel
     * @param $clientCurrency
     */
    private function convertCurrenciesToClientAware(HotelDetails $hotel, $clientCurrency)
    {
        if (is_array($hotel->rooms) && count($hotel->rooms)) {
            /** @var HotelDetailsRoom $room */
            foreach ($hotel->rooms as $l => $room) {
                $hotel->rooms[$l]->price = $this->moneyConverter->convertMoney($room->price, $clientCurrency);
                $hotel->rooms[$l]->tax = $this->moneyConverter->convertMoney($room->tax, $clientCurrency);
                if (is_array($hotel->rooms[$l]->ratesPerNight) && count($hotel->rooms[$l]->ratesPerNight)) {
                    foreach ($hotel->rooms[$l]->ratesPerNight as $k => $rate) {
                        $hotel->rooms[$l]->ratesPerNight[$k]['amount'] = $this->moneyConverter->convertMoney($rate['amount'], $clientCurrency);
                    }
                }
            }
        }
    }

    private function fillTrivagoUrl(InputParams $params, HotelDetails $hotel)
    {
        $link = $this->trivagoService->generateLinkFromHotelAndDestenationAndDate(
            $hotel->code,
            $params->dateFrom,
            $params->dateTo
        );
        $hotel->trivagoUrl = $link;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 24.12.2015
 * Time: 10:48
 */

namespace ApiBundle\Service\HotelDetails;

use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverter;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use function GuzzleHttp\Promise\inspect_all;


/**
 * Class HotelsProHotelDetailsClientCancellationPolicyDecorator
 * @package ApiBundle\Service\HotelDetails
 */
class HotelsProHotelDetailsClientCancellationPolicyDecorator implements HotelDetailsClient, StopWatchAware
{
    use StopWatchAwareTrait;
    use LoggerAwareTrait;

    /**
     * @var HotelDetailsClient
     */
    private $hotelsProHotelDetailsClient;

    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * @var string
     */
    private $hotelsProApiKey;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * @var string
     */
    private $hotelsProEndPoint;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * HotelsProHotelDetailsClientCancellationPolicyDecorator constructor.
     * @param HotelDetailsClient $hotelsProHotelDetailsClient
     * @param string $hotelsProApiKey
     * @param MoneyConverter $moneyConverter
     * @param $hotelsProEndpoint
     * @param Client $httpClient
     */
    public function __construct(
        HotelDetailsClient $hotelsProHotelDetailsClient,
        $hotelsProApiKey,
        MoneyConverter $moneyConverter,
        $hotelsProEndpoint,
        Client $httpClient
    ) {
        $this->hotelsProHotelDetailsClient = $hotelsProHotelDetailsClient;
        $this->hotelsProApiKey = $hotelsProApiKey;
        $this->moneyConverter = $moneyConverter;
        $this->hotelsProEndPoint = $hotelsProEndpoint;
        $this->httpClient = $httpClient;
    }

    /**
     * @inheritdoc
     * @param InputParams $params
     * @return HotelDetails
     */
    public function get(InputParams $params)
    {
        $this->startWatch('hotel_details_hp_cancellation_policy');
        $hotelDetails = $this->hotelsProHotelDetailsClient->get($params);

        if (is_array($hotelDetails->rooms) && count($hotelDetails->rooms)) {
            $promises = $this->requestPromises($hotelDetails);

            $responses = array_map(
                function ($inspect) {
                    if ($inspect['state'] === PromiseInterface::REJECTED) {
                        return null;
                    }
                    /** @var Response $response */
                    $response = $inspect['value'];

                    return $response;
                },
                inspect_all($promises)
            );

            $responses = array_filter($responses);

            $this->fillCancellationPolicies($hotelDetails, $responses, $params->currency);
        }
        $this->stopWatch('hotel_details_hp_cancellation_policy');

        return $hotelDetails;
    }

    /**
     * @param $hotelDetails
     * @return PromiseInterface[]
     */
    private function requestPromises($hotelDetails)
    {
        $promises = [];
        /** @var HotelDetailsRoom $room */
        foreach ($hotelDetails->rooms as $room) {
            $promises[$room->id] = $this->httpClient->sendAsync(
                new Request(
                    $method = 'GET',
                    $url = $this->hotelsProEndPoint.'?'.http_build_query(
                            [
                                'method' => 'getHotelCancellationPolicy',
                                'apiKey' => $this->hotelsProApiKey,
                                'trackingId' => $room->metaData->hotelsProProcessId,
                                'hotelCode' => $hotelDetails->code,
                            ]
                        ),
                    $headers = [
                        'Content-Type' => 'application/json',
                        'Content-Encoding' => 'UTF-8',
                        'Accept-Encoding' => 'gzip,deflate',
                    ]
                )
            );
        }

        return $promises;
    }

    /**
     * @param HotelDetails $hotelDetails
     * @param ResponseInterface[] $responses
     * @param string $displayedCurrency
     */
    private function fillCancellationPolicies(HotelDetails $hotelDetails, array $responses, $displayedCurrency)
    {
        if (count($responses)) {
            $decoder = new JsonEncoder();
            /** @var HotelDetailsRoom $room */
            foreach ($hotelDetails->rooms as $room) {
                /** @var ResponseInterface $response */
                foreach ($responses as $roomId => $response) {
                    if ($room->id == $roomId) {
                        $policyResponse = $decoder->decode(
                            $response->getBody()->getContents(),
                            JsonEncoder::FORMAT
                        );
                        if (isset($policyResponse['cancellationPolicy'])) {
                            $room->setCancellationPolicyFromHotelsPro(
                                $this->wrapCancellationPolicies(
                                    $policyResponse['cancellationPolicy'],
                                    $displayedCurrency,
                                    $room
                                )
                            );
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $cancellationPolicies
     * @param $displayedCurrency
     * @param HotelDetailsRoom $room
     * @return string
     */
    private function wrapCancellationPolicies($cancellationPolicies, $displayedCurrency, HotelDetailsRoom $room)
    {
        $cancellationText = $noShowText = '';

        ///looping over each cancellation policy

        foreach ($cancellationPolicies as $cancellationPolicy) {
            $cancellationPolicy = (array)$cancellationPolicy;
            if ((int)($cancellationPolicy['cancellationDay']) < 250 && (int)($cancellationPolicy['cancellationDay']) != 0) {
                $cancellationText .= 'If cancellation is made within '.$cancellationPolicy['cancellationDay'].' days of the checkin date, then the cancellation fee will be ';

                if ($cancellationPolicy['feeType'] === 'Percent') {
                    $cancellationText .= $cancellationPolicy['feeAmount']."% of the total amount.\n";
                } elseif ($cancellationPolicy['feeType'] === 'Amount') {
                    ///Do proper currency conversion.....

                    $cancellationText = $cancellationText.' '.
                        $displayedCurrency.' '.
                        $this->moneyConverter->convertMoney(
                            Money::build(
                                $cancellationPolicy['feeAmount'],
                                $cancellationPolicy['currency']
                            ),
                            $displayedCurrency
                        )->getAmount().".\n";

                } elseif ($cancellationPolicy['feeType'] === 'Night') {
                    $cancellationText = $cancellationText.'the first '.$cancellationPolicy['feeAmount']." nights' rate.\n";
                }

            } elseif ((int)($cancellationPolicy['cancellationDay']) > 250) {
                $cancellationText .= 'This reservation is non-refundable and cannot be amended or cancelled. No refund will be made upon cancellation, late check-in or early check out. Any extensions to the stay require a new booking.';
            } elseif ((int)($cancellationPolicy['cancellationDay']) == 0) {

                ///no show text
                $noShowText = 'In case of no-show at the hotel, the cancellation fee will be ';
                if ($cancellationPolicy['feeType'] === 'Percent') {
                    $noShowText = $noShowText.$cancellationPolicy['feeAmount'].'% of the total booking amount.';
                } elseif ($cancellationPolicy['feeType'] === 'Amount') {
                    ///Do proper currency conversion.....
                    $noShowText = $noShowText.$displayedCurrency.$this->moneyConverter->convertMoney(
                            Money::build(
                                $cancellationPolicy['feeAmount'],
                                $cancellationPolicy['currency']
                            ),
                            $displayedCurrency
                        )->getAmount();
                } elseif ($cancellationPolicy['feeType'] === 'Night') {
                    $noShowText = $noShowText.'the first '.$cancellationPolicy['feeAmount'].' nights\' rate.';
                }
            }
        }

        $remarks = $cancellationPolicies[0]['remarks'];
        $note = "\nPlease note that cancellation periods are based on GMT+2 time.";

        return $cancellationText.$noShowText."\nRemarks: ".$remarks.$note;


    }
}
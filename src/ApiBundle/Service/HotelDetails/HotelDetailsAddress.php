<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:55
 */

namespace ApiBundle\Service\HotelDetails;

use JMS\Serializer\Annotation as JMS;

/**
 * Class HotelDetailsAddress
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package ApiBundle\Service\HotelDetails
 */
class HotelDetailsAddress
{
    /**
     * @var string
     * @JMS\Expose()
     */
    public $addressLine;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $cityName;
}
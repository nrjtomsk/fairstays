<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.04.2016
 * Time: 14:00
 */

namespace ApiBundle\Service\HotelDetails;

use ApiBundle\Service\HotelDetails\Bidstays\RoomBidLimitsMapper;
use ApiBundle\Service\HotelDetails\Bidstays\RoomUserBidsMapper;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;

/**
 * Class BidstaysHotelDetailsClient
 * @package ApiBundle\Service\HotelDetails
 */
class BidstaysHotelDetailsClient implements HotelDetailsClient
{
    /**
     * @var HotelDetailsClient
     */
    private $resolvedHotelDetailsClient;

    /**
     * @var RoomBidLimitsMapper
     */
    private $travelGuruRoomBidLimitsMapper;

    /**
     * @var RoomBidLimitsMapper
     */
    private $hotelsProRoomBidLimitsMapper;

    /**
     * @var RoomUserBidsMapper
     */
    private $roomUserBidsMapper;

    /**
     * BidstaysHotelDetailsClient constructor.
     * @param HotelDetailsClient $resolvedHotelDetailsClient
     * @param RoomBidLimitsMapper $travelGuruRoomBidLimitsMapper
     * @param RoomBidLimitsMapper $hotelsProRoomBidLimitsMapper
     * @param RoomUserBidsMapper $roomUserBidsMapper
     */
    public function __construct(
        HotelDetailsClient $resolvedHotelDetailsClient,
        RoomBidLimitsMapper $travelGuruRoomBidLimitsMapper,
        RoomBidLimitsMapper $hotelsProRoomBidLimitsMapper,
        RoomUserBidsMapper $roomUserBidsMapper
    ) {
        $this->resolvedHotelDetailsClient = $resolvedHotelDetailsClient;
        $this->travelGuruRoomBidLimitsMapper = $travelGuruRoomBidLimitsMapper;
        $this->hotelsProRoomBidLimitsMapper = $hotelsProRoomBidLimitsMapper;
        $this->roomUserBidsMapper = $roomUserBidsMapper;
    }


    /**
     * @param InputParams $params
     * @return HotelDetails
     * @throws \RuntimeException
     */
    public function get(InputParams $params)
    {
        $details = $this->resolvedHotelDetailsClient->get($params);
        $this->mapBidLimitsToHotelRooms($details, $params);
        $this->roomUserBidsMapper->mapRooms($details);
        return $details;
    }

    /**
     * @param HotelDetails $details
     * @param InputParams $params
     */
    private function mapBidLimitsToHotelRooms(HotelDetails $details, InputParams $params)
    {
        switch ($details->metaData->serviceName) {
            case HotelsListApiManagerTravelGuru::NAME:
                $this->travelGuruRoomBidLimitsMapper->setInputParams($params);
                $this->travelGuruRoomBidLimitsMapper->mapHotelRooms($details);
                break;
            case HotelsListApiManagerHotelsPro::NAME:
                $this->hotelsProRoomBidLimitsMapper->setInputParams($params);
                $this->hotelsProRoomBidLimitsMapper->mapHotelRooms($details);
                break;
            default:
                throw new \RuntimeException(sprintf('Incorrect service name: %s', $details->metaData->serviceName));
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:15
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\DTO\HotelDetailsFilter;
use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;
use ApiBundle\Service\Hotels\Details\Client;
use AppBundle\Repository\HotelRepository;

/**
 * Returns travelGuru hotel details
 *
 * Class TravelGuruHotelDetailsClient
 * @package ApiBundle\Service\HotelDetails
 */
class TravelGuruHotelDetailsClient implements HotelDetailsClient
{

    /**
     * @var Client
     */
    private $legacyClient;

    /**
     * @var HotelRepository
     */
    private $hotelRepository;
    /**
     * TravelGuruHotelDetailsClient constructor.
     * @param Client $legacyClient
     */
    public function __construct(Client $legacyClient, HotelRepository $hotelRepository)
    {
        $this->legacyClient = $legacyClient;
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * @inheritdoc
     * @param InputParams $params
     * @return HotelDetails
     * @throws \ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException
     */
    public function get(InputParams $params)
    {
        $filter = HotelDetailsFilter::fromInputParams($params);
        $response = $this->legacyClient->get($filter);
        if ($response['status']) {
            $hotelTravelGuru = $this->hotelRepository->findHotelById($params->code);
            $details = (array)$response['data'];
            $hotel = HotelDetails::fromLegacyDetails($details, $hotelTravelGuru);
            return $hotel;
        }
        
        throw new HotelNotFoundException(sprintf('Hotel %s not found', $params->code));
    }
}
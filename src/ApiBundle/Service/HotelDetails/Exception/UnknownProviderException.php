<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.03.2016
 * Time: 18:12
 */

namespace ApiBundle\Service\HotelDetails\Exception;

/**
 * Class UnknownProviderException
 * @package ApiBundle\Service\HotelDetails\Exception
 */
class UnknownProviderException extends \InvalidArgumentException
{
    /**
     * @var string
     */
    private $inputProviderName;

    /**
     * UnknownProviderException constructor.
     * @param string $inputProviderName
     * @param \Exception $prev
     */
    public function __construct($inputProviderName, \Exception $prev = null)
    {
        $this->inputProviderName = $inputProviderName;
        parent::__construct(sprintf('Invalid provider name: %s', $inputProviderName), $code = 2, $prev);
    }

    /**
     * @return string
     */
    public function getInputProviderName()
    {
        return $this->inputProviderName;
    }
}
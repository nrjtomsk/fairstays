<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.03.2016
 * Time: 16:19
 */

namespace ApiBundle\Service\HotelDetails\Exception;


class HotelNotFoundException extends \Exception
{

}
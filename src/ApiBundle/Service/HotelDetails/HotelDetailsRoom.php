<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:54
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Service\HotelDetails\Bidstays\HotelDetailsRoomBidDto;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Entity\Bid;
use AppBundle\Service\Money\Money;
use JMS\Serializer\Annotation as JMS;

/**
 * Hotel details
 * Sets cancellation policy for hotelsPro
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * Class HotelDetailsRoom
 * @package ApiBundle\Service\HotelDetails
 */
class HotelDetailsRoom
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $ratePlanCode;

    /**
     * @var string
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $roomTypeCode;

    /**
     * @var string
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $ratePlanName;

    /**
     * @var string
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $roomTitle;

    /**
     * @var string
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $description;

    /**
     * @var array
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $inclusionDescription = [];


    /**
     * @var array
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $cancellationPolicy = [];

    /**
     * @var Money
     */
    public $priceBefore;

    /**
     * @var Money
     */
    public $taxBefore;

    /**
     * @var Money
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $price;

    /**
     * @var Money
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $tax;

    /**
     * @var array
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $occupancy = [];

    /**
     * @var string[]
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $images = [];

    /**
     * @var MetaDataDTO
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $metaData;

    /**
     * @var array
     * @JMS\Expose()
     * @JMS\Groups({"Fairstays", "Bidstays"})
     */
    public $ratesPerNight = [];

    /**
     * @var Money
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     */
    public $bidLowerLimit;

    /**
     * @var Money
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     */
    public $bidUpperLimit;

    /**
     * @var HotelDetailsRoomBidDto[]
     * @JMS\Expose()
     * @JMS\Groups({"Bidstays"})
     */
    public $bids;

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'ratePlan' => $this->ratePlanCode,
            'roomType' => $this->roomTypeCode,
            'meta' => $this->metaData->toJsonString(),
            'serviceName' => $this->metaData->serviceName,
        ];
    }

    /**
     * HotelDetailsRoom constructor.
     */
    public function __construct()
    {
        $this->id = uniqid($prefix = 'room#', $moreEntropy = true);
    }

    /**
     * Sets cancellation policy for hotelsPro
     *
     * @param $cancellationPolicy
     */
    public function setCancellationPolicyFromHotelsPro($cancellationPolicy)
    {
        $this->cancellationPolicy = [];
        $this->cancellationPolicy[0] = [];
        $this->cancellationPolicy[0]['remarks']= $cancellationPolicy;
    }
}
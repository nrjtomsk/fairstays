<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:16
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Exception\HotelServiceErrorResponseHotelsPro;
use ApiBundle\Service\MoneyConverter;
use AppBundle\Repository\HotelProRepository;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use GuzzleHttp;
use JMS\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Returns hotelsPro hotel details
 *
 * Class HotelsProHotelDetailsClient
 * @package ApiBundle\Service\HotelDetails
 */
class HotelsProHotelDetailsClient implements HotelDetailsClient, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var HotelProRepository
     */
    private $hotelsProRepository;

    /**
     * @var GuzzleHttp\Client
     */
    private $client;

    /**
     * @var string
     */
    private $endPoint;

    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;

    /**
     * HotelsProHotelDetailsClient constructor.
     * @param HotelProRepository $hotelProRepository
     * @param $apiKey
     * @param GuzzleHttp\Client $client
     * @param $endPoint
     * @param JsonEncoder $jsonEncoder
     */
    public function __construct(
        HotelProRepository $hotelProRepository,
        $apiKey,
        GuzzleHttp\Client $client,
        $endPoint,
        JsonEncoder $jsonEncoder
    ) {
        $this->hotelsProRepository = $hotelProRepository;
        $this->apiKey = $apiKey;
        $this->client = $client;
        $this->endPoint = $endPoint;
        $this->jsonEncoder = $jsonEncoder;
    }


    /**
     * @inheritdoc
     * @param InputParams $params
     * @return HotelDetails
     * @throws HotelServiceErrorResponseHotelsPro
     */
    public function get(InputParams $params)
    {


        $this->startWatch('hotel_details_hp');

        try {
            $data = [
                'method' => 'allocateHotelCode',
                'apiKey' => $this->apiKey,
                'searchId' => $params->meta->hotelsProSearchId,
                'hotelCode' => $params->code,
                'nationality' => $params->nationality,
            ];

            $hotelPro = $this->hotelsProRepository->findHotelByHotelCode($params->code);
            if (!$hotelPro) {
                throw new \RuntimeException(sprintf('Hotel with code %s not found', $params->code));
            }

            $requestUri = $this->endPoint.'?'.http_build_query($data);
            $details = $this->jsonEncoder->decode($this->client->get($requestUri)->getBody()->getContents(), JsonEncoder::FORMAT);
            if (is_array($details)) {
                if (isset($details[0], $details[1])) {
                    throw new \RuntimeException($details[1], $details[0]);
                }

                return HotelDetails::fromHotelsProDetails((object)$details, $hotelPro, $params->meta->cityId);
            } else {
                throw new \RuntimeException(sprintf('Bad response from travel guru on uri: %s', $requestUri));
            }
        } catch (\Exception $e) {
            throw new HotelServiceErrorResponseHotelsPro($e->getMessage(), $e->getCode(), $e);
        } finally {
            $this->stopWatch('hotel_details_hp');
        }
    }
}
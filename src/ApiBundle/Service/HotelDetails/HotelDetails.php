<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:12
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelPro;
use AppBundle\Entity\TripAdvisorRating;
use AppBundle\Entity\TripAdvisorRatingAware;
use AppBundle\Service\Money\Money;
use JMS\Serializer\Annotation as JMS;

/**
 * Class HotelDetails
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package ApiBundle\Service\HotelDetails
 */
class HotelDetails
{
    /**
     * @var string
     * @JMS\Expose()
     */
    public $code;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $name;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $description;

    /**
     * @var int
     * @JMS\Expose()
     */
    public $stars;

    /**
     * @var float
     * @JMS\Expose()
     */
    public $latitude;

    /**
     * @var float
     * @JMS\Expose()
     */
    public $longitude;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $site;

    /**
     * @var int
     * @JMS\Expose()
     */
    public $numberOfRooms;

    /**
     * @var HotelDetailsAmenities
     * @JMS\Expose()
     */
    public $amenities;

    /**
     * @var HotelDetailsAddress
     * @JMS\Expose()
     */
    public $address;

    /**
     * @var HotelDetailsRoom[]
     * @JMS\Expose()
     */
    public $rooms = [];

    /**
     * @var TripAdvisorRating
     * @JMS\Expose()
     */
    public $tripAdvisorRating;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $trivagoUrl;

    /**
     * @var MetaDataDTO
     * @JMS\Expose()
     */
    public $metaData;


    /**
     * Returns hotel details
     *
     * @param array $details
     * @param Hotel $hotelTravelGuru
     * @return HotelDetails
     */
    public static function fromLegacyDetails(array $details, Hotel $hotelTravelGuru)
    {
        $hotel = new HotelDetails();
        $hotel->code = isset($details['hotelCode']) ? $details['hotelCode'] : null;
        $hotel->latitude = isset($details['latitude']) ? (float)$details['latitude'] : null;
        $hotel->longitude = isset($details['longitude']) ? (float)$details['longitude'] : null;
        $hotel->stars = isset($details['rating']) ? (int)$details['rating'] : null;
        $hotel->name = isset($details['hotelName']) ? $details['hotelName'] : null;
        $hotel->site = isset($details['site']) ? $details['site'] : null;
        $hotel->numberOfRooms = isset($details['numberOfRooms']) ? (int)$details['numberOfRooms'] : null;
        $hotel->description = isset($details['description']) ? $details['description'] : null;
        $hotel->fillTripAdvisorRating($hotelTravelGuru);

        $hotel->address = new HotelDetailsAddress();
        $hotel->address->addressLine = isset($details['address']['AddressLine']) ? $details['address']['AddressLine'] : null;
        $hotel->address->cityName = isset($details['address']['CityName']) ? $details['address']['CityName'] : null;

        $hotel->amenities = new HotelDetailsAmenities();
        $hotel->amenities->property = isset($details['amenities']['property']) ? $details['amenities']['property'] : null;
        $hotel->amenities->room = isset($details['amenities']['room']) ? $details['amenities']['room'] : null;
        $hotel->rooms = [];


        if (isset($details['rooms']['pricePerDay']) && is_array($details['rooms']['pricePerDay'])) {
            $roomsData = [];

            foreach ($details['rooms']['pricePerDay'] as $pricePerDay) {
                if ($pricePerDay['ratePlanCode'] != null && $pricePerDay['roomTypeCode'] != null) {
                    $key = $pricePerDay['ratePlanCode'].$pricePerDay['roomTypeCode'];
                    if (isset($roomsData[$key])) {
                        if (isset($pricePerDay['priceFinal'], $pricePerDay['additionalGuestsAmount'], $pricePerDay['discount'])) {
                            $roomsData[$key]['price'] += $pricePerDay['priceFinal'] + $pricePerDay['additionalGuestsAmount'] - $pricePerDay['discount'];
                        }
                        if (isset($pricePerDay['taxAmount'])) {
                            $roomsData[$key]['tax'] += $pricePerDay['taxAmount'];
                        }

                        if (isset($pricePerDay['priceFinalBefore'], $pricePerDay['additionalGuestsAmountBefore'], $pricePerDay['discountBefore'])) {
                            $roomsData[$key]['priceBefore'] += $pricePerDay['priceFinalBefore'] + $pricePerDay['additionalGuestsAmountBefore'] - $pricePerDay['discountBefore'];
                        }

                        if (isset($pricePerDay['taxAmountBefore'])) {
                            $roomsData[$key]['taxBefore'] += $pricePerDay['taxAmountBefore'];
                        }

                    } else {
                        $roomsData[$key] = [
                            'priceBefore' => 0,
                            'taxBefore' => 0,
                            'price' => 0,
                            'tax' => 0,
                            'ratePlanCode' => null,
                            'roomTypeCode' => null,
                            'images' => null,
                            'description' => null,
                            'name' => null,
                            'occupancy' => null,
                            'cancellationPolicy' => null,
                            'ratePlanInclusionDescription' => [],
                        ];
                        $roomsData[$key]['priceBefore'] += $pricePerDay['priceFinalBefore'] + $pricePerDay['additionalGuestsAmountBefore'] - $pricePerDay['discountBefore'];
                        $roomsData[$key]['taxBefore'] += $pricePerDay['taxAmountBefore'];
                        $roomsData[$key]['price'] += $pricePerDay['priceFinal'] + $pricePerDay['additionalGuestsAmount'] - $pricePerDay['discount'];
                        $roomsData[$key]['tax'] += $pricePerDay['taxAmount'];
                        $roomsData[$key]['ratePlanCode'] = $pricePerDay['ratePlanCode'];
                        $roomsData[$key]['roomTypeCode'] = $pricePerDay['roomTypeCode'];
                    }
                }
            }

            if (count($roomsData)) {
                foreach ($roomsData as $k => $roomItem) {
                    if (isset($details['rooms']['roomType']) && is_array($details['rooms']['roomType'])) {
                        foreach ($details['rooms']['roomType'] as $roomType) {
                            if ($roomItem['roomTypeCode'] == $roomType['roomTypeCode']) {
                                $roomsData[$k]['images'] = isset($roomType['images']) ? $roomType['images'] : null;
                                $roomsData[$k]['description'] = isset($roomType['description']) ? $roomType['description'] : null;
                                $roomsData[$k]['name'] = isset($roomType['roomTypeTitle']) ? $roomType['roomTypeTitle'] : null;
                                $roomsData[$k]['occupancy'] = isset($roomType['occupancy']) ? $roomType['occupancy'] : null;
                            }
                        }
                    }


                    if (isset($details['rooms']['ratePlan']) && is_array($details['rooms']['ratePlan'])) {
                        foreach ($details['rooms']['ratePlan'] as $ratePlan) {
                            if ($roomItem['ratePlanCode'] == $ratePlan['ratePlanCode']) {
                                $roomsData[$k]['cancellationPolicy'] = isset($ratePlan['cancellationPolicy']) ? $ratePlan['cancellationPolicy'] : null;
                                $roomsData[$k]['ratePlanName'] = isset($ratePlan['ratePlanName']) ? $ratePlan['ratePlanName'] : null;
                                $roomsData[$k]['ratePlanInclusionDescription'] = isset($ratePlan['ratePlanInclusionDescription']) ? $ratePlan['ratePlanInclusionDescription'] : [];
                            }
                        }
                    }

                }

                foreach ($roomsData as $item) {
                    $room = new HotelDetailsRoom();
                    $room->roomTitle = $item['name'];
                    $room->description = $item['description'];
                    $room->priceBefore = Money::build(
                        $item['priceBefore'],
                        HotelsListApiManagerTravelGuru::INPUT_CURRENCY
                    );
                    $room->taxBefore = Money::build($item['taxBefore'], HotelsListApiManagerTravelGuru::INPUT_CURRENCY);
                    $room->price = Money::build($item['price'], HotelsListApiManagerTravelGuru::INPUT_CURRENCY);
                    $room->tax = Money::build($item['tax'], HotelsListApiManagerTravelGuru::INPUT_CURRENCY);
                    $room->ratePlanCode = $item['ratePlanCode'];
                    $room->ratePlanName = $item['ratePlanName'];
                    $room->roomTypeCode = $item['roomTypeCode'];
                    $room->cancellationPolicy = $item['cancellationPolicy'];
                    $room->images = $item['images'];
                    $room->occupancy = $item['occupancy'];
                    $room->inclusionDescription = $item['ratePlanInclusionDescription'];
                    $room->metaData = MetaDataDTO::forTravelGuruHotel();
                    $hotel->rooms[] = $room;
                }
            }
        }

        return $hotel;
    }

    /**
     * @param $hotel
     */
    private function fillTripAdvisorRating($hotel)
    {
        if ($hotel instanceof TripAdvisorRatingAware) {
            if ($hotel->getTripAdvisorRating() && $hotel->getTripAdvisorRating()->getMultipliedRating() != null) {
                $this->tripAdvisorRating = [
                    'multiplied_rating' => $hotel->getTripAdvisorRating()->getMultipliedRating(),
                ];
            } else {
                $this->tripAdvisorRating = null;
            }

            return;
        }
        if (isset($hotel['tripAdvisorRating']['multipliedRating']) && $hotel['tripAdvisorRating']['multipliedRating'] !== null) {
            $this->tripAdvisorRating = ['multiplied_rating' => $hotel['tripAdvisorRating']['multipliedRating']];
        } else {
            $this->tripAdvisorRating = null;
        }
    }

    /**
     * @param $details
     * @param HotelPro $hotelPro
     * @param $cityId
     * @return static
     */
    public static function fromHotelsProDetails($details, HotelPro $hotelPro, $cityId)
    {
        $hotel = new static();
        $hotel->metaData = MetaDataDTO::forHotelProHotel($details->searchId, $cityId);
        $hotel->code = $hotelPro->getHotelCode();
        $hotel->address = new HotelDetailsAddress();
        $hotel->address->addressLine = $hotelPro->getHotelAddress();
        $hotel->address->cityName = $hotelPro->getDestination();
        $hotel->stars = $hotelPro->getStarRating();
        $hotel->latitude = $hotelPro->getLatitude();
        $hotel->longitude = $hotelPro->getLongitude();
        $hotel->name = $hotelPro->getHotelName();
        $hotel->amenities = new HotelDetailsAmenities();
        $hotel->amenities->property = $hotelPro->getPropertyAmenities();
        $hotel->amenities->room = $hotelPro->getRoomAmenities();
        $hotel->numberOfRooms = $hotelPro->getRoomsNumber();
        $hotel->description = $hotelPro->getDescription();
        $hotel->fillTripAdvisorRating($hotelPro);


        $availableHotels = [];
        if (is_object($details->availableHotels)) {
            $availableHotels[] = $details->availableHotels;
        } else {
            $availableHotels = $details->availableHotels;
        }

        $hotel->rooms = [];
        foreach ($availableHotels as $availableHotel) {
            $availableHotel = (object)$availableHotel;
            foreach ($availableHotel->rooms as &$room) {
                $room = (object)$room;
                foreach ($room->paxes as &$pax) {
                    $pax = (object)$pax;
                    unset($pax);
                }
                foreach ($room->ratesPerNight as &$rate) {
                    $rate = (object)$rate;
                    unset($rate);
                }
                unset($room);
            }

            $hotel->rooms[] = $hotel->buildHotelRoom($hotelPro, $availableHotel, $details, $cityId);
        }

        return $hotel;
    }

    /**
     * @param HotelPro $hotelPro
     * @param $availableHotel
     * @param $details
     * @param $cityId
     * @return HotelDetailsRoom
     */
    private function buildHotelRoom(HotelPro $hotelPro, $availableHotel, $details, $cityId)
    {
        $hotelRoom = new HotelDetailsRoom();
        $hotelRoom->images = $hotelPro->getImagesUrls();
        $hotelRoom->priceBefore = Money::build($availableHotel->totalPrice, $availableHotel->currency);
        $hotelRoom->taxBefore = Money::build($availableHotel->totalTax, $availableHotel->currency);
        $hotelRoom->description = $hotelPro->getDescription();

        $hotelRoom->inclusionDescription = [$availableHotel->boardType];
        if (is_object($availableHotel->rooms)) {
            $availableRoom = $availableHotel->rooms;
        } else {
            $availableRoom = $availableHotel->rooms[0];
        }

        if (is_array($availableRoom->ratesPerNight) && count($availableRoom->ratesPerNight)) {
            foreach ($availableRoom->ratesPerNight as $ratePerNight) {
                $hotelRoom->ratesPerNight[] = [
                    'date' => $ratePerNight->date,
                    'amount' => Money::build($ratePerNight->amount, $availableHotel->currency),
                ];
            }
        }

        $hotelRoom->roomTitle = $availableRoom->roomCategory;
        if (is_object($availableRoom->paxes)) {
            $paxes[] = $availableRoom->paxes;
        } else {
            $paxes = $availableRoom->paxes;
        }


        $hotelRoom->occupancy = [];

        foreach ($paxes as $item) {
            $hotelRoom->occupancy[] = [
                'type' => $item->paxType,
                'age' => $item->age,
            ];
        }

        $hotelRoom->metaData = MetaDataDTO::forHotelProHotel($details->searchId, $cityId);
        $hotelRoom->metaData->hotelsProProcessId = $availableHotel->processId;


        return $hotelRoom;
    }
}
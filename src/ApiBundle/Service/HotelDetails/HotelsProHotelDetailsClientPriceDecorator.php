<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 23.12.2015
 * Time: 11:55
 */

namespace ApiBundle\Service\HotelDetails;


use ApiBundle\Service\MoneyConverter;
use AppBundle\Service\HotelsPro\Exception\SelectedHotelsPriceException;
use AppBundle\Service\HotelsPro\SelectedHotelsPrice;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

/**
 * Class HotelsProHotelDetailsClientPriceDecorator
 * @package ApiBundle\Service\HotelDetails
 */
class HotelsProHotelDetailsClientPriceDecorator implements HotelDetailsClient, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelDetailsClient
     */
    private $internalClient;

    /**
     * @var int
     */
    private $hotelsProInterestPercent;

    /**
     * @var SelectedHotelsPrice
     */
    private $selectedHotelsPrice;

    /**
     * HotelsProHotelDetailsClientPriceDecorator constructor.
     * @param HotelDetailsClient $internalClient
     * @param $hotelsProInterestPercent
     * @param SelectedHotelsPrice $selectedHotelsPrice
     */
    public function __construct(
        HotelDetailsClient $internalClient,
        $hotelsProInterestPercent,
        SelectedHotelsPrice $selectedHotelsPrice
    ) {
        $this->internalClient = $internalClient;
        $this->hotelsProInterestPercent = $hotelsProInterestPercent;
        $this->selectedHotelsPrice = $selectedHotelsPrice;
    }

    /**
     * @inheritdoc
     * @param InputParams $params
     * @return HotelDetails
     */
    public function get(InputParams $params)
    {
        $this->startWatch('hotel_details_hp_price');
        $hotel = $this->internalClient->get($params);

        try {
            $rate = $this->selectedHotelsPrice->getSelectedHotelsRate(
                $hotel->metaData->cityId,
                $params->dateFrom,
                $hotel->code
            );
            $multiply = 1 + $rate / 100;
        } catch (SelectedHotelsPriceException $e) {
            $multiply = 1 + $this->hotelsProInterestPercent / 100;
        }

        foreach ($hotel->rooms as $room) {
            $room->price = $room->priceBefore->multiply($multiply);
            $room->tax = $room->taxBefore->multiply($multiply);
            foreach ($room->ratesPerNight as $k => $item) {
                $room->ratesPerNight[$k]['amount'] = $item['amount']->multiply($multiply);
            }
        }

        $this->stopWatch('hotel_details_hp_price');

        return $hotel;
    }


}
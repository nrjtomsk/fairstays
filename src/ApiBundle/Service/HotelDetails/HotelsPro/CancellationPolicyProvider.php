<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 07.04.2016
 * Time: 12:40
 */

namespace ApiBundle\Service\HotelDetails\HotelsPro;

use ApiBundle\Service\HotelsList\MetaDataDTO;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverter;
use GuzzleHttp\Client;
use function GuzzleHttp\Promise\inspect_all;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class CancellationPolicyProvider
 * @package ApiBundle\Service\HotelDetails\HotelsPro
 */
class CancellationPolicyProvider
{
    const API_METHOD = 'getHotelCancellationPolicy';

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * @var string
     */
    private $hotelsProEndPoint;

    /**
     * @var string
     */
    private $hotelsProApiKey;

    /**
     * CancellationPolicyProvider constructor.
     * @param Client $httpClient
     * @param JsonEncoder $encoder
     * @param MoneyConverter $moneyConverter
     * @param string $hotelsProEndPoint
     * @param string $hotelsProApiKey
     */
    public function __construct(
        Client $httpClient,
        JsonEncoder $encoder,
        MoneyConverter $moneyConverter,
        $hotelsProEndPoint,
        $hotelsProApiKey
    ) {
        $this->httpClient = $httpClient;
        $this->encoder = $encoder;
        $this->moneyConverter = $moneyConverter;
        $this->hotelsProEndPoint = $hotelsProEndPoint;
        $this->hotelsProApiKey = $hotelsProApiKey;
    }

    public function getCancellationPolicies($hotelCode, array $roomMetadatas = [], $displayedCurrency)
    {
        $processIds = array_map(function (MetaDataDTO $metaData) {
            return $metaData->hotelsProProcessId;
        }, $roomMetadatas);
        $promises = $this->getPromises($hotelCode, $processIds);
        $responses = $this->getResponses($promises);
        return $this->getPolicies($responses, $displayedCurrency);
    }

    /**
     * @param $hotelCode
     * @param array $processIds
     * @return PromiseInterface[]
     */
    private function getPromises($hotelCode, array $processIds)
    {
        $promises = [];
        foreach ($processIds as $processId) {
            $promises[$processId] = $this->httpClient->sendAsync(
                new Request(
                    $method = 'GET',
                    $url = $this->hotelsProEndPoint.'?'.http_build_query(
                            [
                                'method' => self::API_METHOD,
                                'apiKey' => $this->hotelsProApiKey,
                                'trackingId' => $processId,
                                'hotelCode' => $hotelCode,
                            ]
                        ),
                    $headers = [
                        'Content-Type' => 'application/json',
                        'Content-Encoding' => 'UTF-8',
                        'Accept-Encoding' => 'gzip,deflate',
                    ]
                )
            );
        }

        return $promises;
    }

    /**
     * @param array $promises
     * @return Response[]
     */
    private function getResponses(array $promises)
    {
        if (count($promises)) {
            return array_filter(array_map(
                function ($inspect) {
                    if ($inspect['state'] === PromiseInterface::REJECTED) {
                        return null;
                    }
                    /** @var Response $response */
                    $response = $inspect['value'];

                    return $response;
                },
                inspect_all($promises)
            ));
        }

        return [];

    }

    /**
     * @param Response[] $responses
     * @param $currency
     * @return array
     */
    private function getPolicies(array $responses, $currency)
    {
        $policies = [];
        if (count($responses)) {
            foreach ($responses as $processId => $response) {
                $policyResponse = $this->getCancellationPolicyResponseArray($response);

                if (isset($policyResponse['cancellationPolicy'])) {
                    $policies[$processId] = [
                        'processId' => $processId,
                        'policy' => $this->wrapCancellationPolicies(
                            $policyResponse['cancellationPolicy'],
                            $currency
                        )
                    ];
                }
            }
        }
        return $policies;
    }

    private function wrapCancellationPolicies($cancellationPolicies, $displayedCurrency)
    {
        $cancellationText = $noShowText = '';

        ///looping over each cancellation policy

        foreach ($cancellationPolicies as $cancellationPolicy) {
            $cancellationPolicy = (array)$cancellationPolicy;
            if ((int)($cancellationPolicy['cancellationDay']) < 250 && (int)($cancellationPolicy['cancellationDay']) != 0) {
                $cancellationText .= 'If cancellation is made within '.$cancellationPolicy['cancellationDay'].' days of the checkin date, then the cancellation fee will be ';

                if ($cancellationPolicy['feeType'] === 'Percent') {
                    $cancellationText .= $cancellationPolicy['feeAmount']."% of the total amount.\n";
                } elseif ($cancellationPolicy['feeType'] === 'Amount') {
                    ///Do proper currency conversion.....

                    $cancellationText = $cancellationText.' '.
                        $displayedCurrency.' '.
                        $this->moneyConverter->convertMoney(
                            Money::build(
                                $cancellationPolicy['feeAmount'],
                                $cancellationPolicy['currency']
                            ),
                            $displayedCurrency
                        )->getAmount().".\n";

                } elseif ($cancellationPolicy['feeType'] === 'Night') {
                    $cancellationText = $cancellationText.'the first '.$cancellationPolicy['feeAmount']." nights' rate.\n";
                }

            } elseif ((int)($cancellationPolicy['cancellationDay']) > 250) {
                $cancellationText .= 'This reservation is non-refundable and cannot be amended or cancelled. No refund will be made upon cancellation, late check-in or early check out. Any extensions to the stay require a new booking.';
            } elseif ((int)($cancellationPolicy['cancellationDay']) == 0) {

                ///no show text
                $noShowText = 'In case of no-show at the hotel, the cancellation fee will be ';
                if ($cancellationPolicy['feeType'] === 'Percent') {
                    $noShowText = $noShowText.$cancellationPolicy['feeAmount'].'% of the total booking amount.';
                } elseif ($cancellationPolicy['feeType'] === 'Amount') {
                    ///Do proper currency conversion.....
                    $noShowText = $noShowText.$displayedCurrency.$this->moneyConverter->convertMoney(
                            Money::build(
                                $cancellationPolicy['feeAmount'],
                                $cancellationPolicy['currency']
                            ),
                            $displayedCurrency
                        )->getAmount();
                } elseif ($cancellationPolicy['feeType'] === 'Night') {
                    $noShowText = $noShowText.'the first '.$cancellationPolicy['feeAmount'].' nights\' rate.';
                }
            }
        }

        $remarks = $cancellationPolicies[0]['remarks'];
        $note = "\nPlease note that cancellation periods are based on GMT+2 time.";

        return $cancellationText.$noShowText."\nRemarks: ".$remarks.$note;


    }

    /**
     * @param Response $response
     * @return mixed
     */
    private function getCancellationPolicyResponseArray(Response $response)
    {
        $responseArray = $this->encoder->decode($response->getBody()->getContents(), JsonEncoder::FORMAT);
        if (isset($responseArray[0], $responseArray[1])) {
            throw new \RuntimeException($responseArray[1], $responseArray[0]);
        }

        return $responseArray;
    }
}
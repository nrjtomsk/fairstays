<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 21.12.2015
 * Time: 16:11
 */

namespace ApiBundle\Service\HotelDetails;

use ApiBundle\Service\HotelDetails\Exception\HotelNotFoundException;

/**
 * Interface HotelDetailsClient
 * @package ApiBundle\Service\HotelDetails
 */
interface HotelDetailsClient
{
    /**
     * @param InputParams $params
     * @return HotelDetails
     * @throws HotelNotFoundException
     */
    public function get(InputParams $params);
}
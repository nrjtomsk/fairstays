<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.12.2015
 * Time: 15:09
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\Currency;
use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Exception\HotelServiceErrorResponse;
use ApiBundle\Service\Hotels\SearchClient\FilterRule\FilterRule;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Service\Money\Money;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use function GuzzleHttp\Promise\unwrap;
use Psr\Log\LoggerInterface;

class HotelsListProviderAggregator implements HotelsListProvider, ApiManagerAware, LoggerAwareInterface, StopWatchAware
{
    use LoggerAwareTrait;
    use ApiManagerAwareTrait;
    use StopWatchAwareTrait;

    /**
     * @var LoggerInterface
     */
    private $defaultLogger;

    /**
     * @var FilterRule
     */
    private $marginPriceFormulaRule;

    /**
     * HotelsListProviderAggregator constructor.
     * @param FilterRule $marginPriceFormulaRule
     */
    public function __construct(FilterRule $marginPriceFormulaRule)
    {
        $this->marginPriceFormulaRule = $marginPriceFormulaRule;
    }

    /**
     * @param LoggerInterface $defaultLogger
     */
    public function setDefaultLogger($defaultLogger)
    {
        $this->defaultLogger = $defaultLogger;
    }

    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $this->startWatch('search_aggregator');
        if (!count($this->getManagers())) {
            throw new \RuntimeException('There are no registered managers');
        }
        $promises = $this->sendRequests($params);
        $this->startWatch('send_search_requests_to_services');

        $responses = [];
        if (count($promises)) {
            foreach ($promises as $managerName => $promise) {
                $this->startWatch($managerName . 'Promise');

                /** @var ResponseInterface[] $responses */
                $responses[$managerName] = $promise->wait();

                $this->stopWatch($managerName . 'Promise');
            }
        }

        $this->stopWatch('send_search_requests_to_services');

        $this->startWatch('parse_search_responses_from_services');
        $hotels = $this->handleResponses($responses);
        $this->stopWatch('parse_search_responses_from_services');
        $hotelList = $this->createHotelList($hotels);
        if (count($responses) > 1) {
            $this->startWatch('removeDuplicates', 'hotel_search');
            $this->removeDuplicates($hotelList);
            $this->stopWatch('removeDuplicates');
        }

        $this->startWatch('fill_response', 'hotel_search');
        $this->fillMarginFormula($hotelList);


        if (is_array($hotelList->hotels) && count($hotelList->hotels)) {
            $hotelList->hotels = array_values($hotelList->hotels);
        }

        $this->stopWatch('fill_response');
        $this->stopWatch('search_aggregator');

        return $hotelList;

    }

    /**
     * @param HotelSearchFilter $params
     * @return PromiseInterface[]
     */
    private function sendRequests(HotelSearchFilter $params)
    {
        $client = new Client(
            [
                'timeout' => 60,
            ]
        );
        $promises = [];

        foreach ($this->getManagers() as $manager) {
            if ($manager->getName() === HotelsListApiManagerTravelGuru::NAME && $params->city->getCountryName(
                ) !== 'India'
            ) {
                continue;
            }
            try {
                $promises[$manager->getName()] = $client->sendAsync($manager->buildRequest($params));
            } catch (ConnectException $e) {
                $this->defaultLogger->error(sprintf('Connection error for %s', $manager->getName()), $e->getTrace());
            } finally {
            }

        }

        return $promises;
    }

    /**
     * @param ResponseInterface[] $responses
     * @return HotelDTO[]
     * @throws HotelServiceErrorResponse
     */
    private function handleResponses(array $responses)
    {
        $data = [];
        $errorsCounter = 0;
        $errors = [];

        if (is_array($responses) && count($responses)) {
            foreach ($responses as $managerName => $response) {
                try {
                    $this->startWatch($managerName, 'handle_search_response');
                    $content = $response->getBody()->getContents();
                    $this->logInfo($content);
                    $data = array_merge($data, $this->getManager($managerName)->handleResponse($content));
                } catch (HotelServiceErrorResponse $e) {
                    $this->defaultLogger->error($e->getMessage(), $e->getTrace());
                    $errors[] = $e;
                    $errorsCounter++;
                } catch (\RuntimeException $e) {
                    $this->defaultLogger->error($e->getMessage(), $e->getTrace());
                    $errors[] = $e;
                    $errorsCounter++;
                } finally {
                    $this->stopWatch($managerName);
                }
            }
        }


        if ($errorsCounter >= count($responses)) {
            $this->defaultLogger->error($errorMessage = 'All services response with error', $errors);
            throw new HotelServiceErrorResponse($errorMessage);
        }

        return $data;
    }

    /**
     * @param HotelDTO[] $data
     * @return HotelsListDTO
     */
    private function createHotelList(array $data)
    {
        $hotelsListDTO = new HotelsListDTO();
        $hotelsListDTO->hotels = $data;

        return $hotelsListDTO;
    }

    /**
     * @param HotelsListDTO $hotelsListDTO
     */
    private function removeDuplicates(HotelsListDTO $hotelsListDTO)
    {
        $expensiveDuplicatesIndexes = $this->getDuplicatesIndexes($hotelsListDTO->hotels);
        foreach ($expensiveDuplicatesIndexes as $index) {
            unset($hotelsListDTO->hotels[$index]);
        }
    }

    /**
     * @param HotelDTO[] $hotels
     * @return array
     */
    private function getDuplicatesIndexes(array $hotels)
    {
        $expensiveDuplicatesIndexes = [];

        for ($i = 0, $hotelsQuantity = count($hotels); $i < $hotelsQuantity; $i++) {
            $hotelA = $hotels[$i];

            for ($j = $i + 1; $j < $hotelsQuantity; $j++) {
                $hotelB = $hotels[$j];

                if ($this->isDuplicate($hotelA, $hotelB)) {
                    if ($hotelA->price >= $hotelB->price) {
                        $expensiveDuplicatesIndexes[] = $i;
                    } else {
                        $expensiveDuplicatesIndexes[] = $j;
                    }
                }
            }
        }

        return array_unique($expensiveDuplicatesIndexes);
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return bool
     */
    private function isDuplicate(HotelDTO $a, HotelDTO $b)
    {
        return $a->name === $b->name
        && $a->latitude >= $b->latitude - 0.5
        && $a->latitude <= $b->latitude + 0.5
        && $a->longitude >= $b->longitude - 0.5
        && $a->longitude <= $b->longitude + 0.5;
    }

    /**
     * @param HotelsListDTO $hotelsListDTO
     */
    private function fillMarginFormula(HotelsListDTO $hotelsListDTO)
    {
        if (is_array($hotelsListDTO->hotels) && count($hotelsListDTO->hotels)) {
            foreach ($hotelsListDTO->hotels as $k => $hotel) {
                $hotelsListDTO->hotels[$k]->isMarginFormulaOk = $this->marginPriceFormulaRule->apply($hotel);
            }
        }
    }
}
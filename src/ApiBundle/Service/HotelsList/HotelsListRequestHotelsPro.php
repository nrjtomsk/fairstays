<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 14.12.2015
 * Time: 16:58
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use GuzzleHttp\Psr7\Request;

/**
 * Class HotelsListRequestHotelsPro
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListRequestHotelsPro
{
    const HOTELS_LIST_METHOD = 'getAvailableHotel';

    /**
     * @var string
     */
    private $endPoint;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var HotelSearchFilter
     */
    private $params;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @var string
     */
    private $systemCurrency;

    /**
     * HotelsListRequestHotelsPro constructor.
     * @param string $endPoint
     * @param string $apiKey
     * @param HotelSearchFilter $params
     * @param string $nationality
     * @param $systemCurrency
     */
    public function __construct($endPoint, $apiKey, HotelSearchFilter $params, $nationality, $systemCurrency)
    {
        $this->endPoint = $endPoint;
        $this->apiKey = $apiKey;
        $this->params = $params;
        $this->nationality = $nationality;
        $this->systemCurrency = $systemCurrency;
    }

    /**
     * @return Request
     */
    public function createRequest()
    {
        $uri = $this->endPoint.'?'.http_build_query(
                [
                    'apiKey' => $this->apiKey,
                    'method' => static::HOTELS_LIST_METHOD,
                    'destinationId' => $this->params->city->getHotelsProId(),
                    'checkIn' => $this->params->stayDateRangeStart,
                    'checkOut' => $this->params->stayDateRangeEnd,
                    'clientNationality' => $this->nationality,
                    'onRequest' => 'false',
                    'rooms' => $this->getRooms($this->params),
                    'currency' => $this->systemCurrency,
                ]
            );

        $request = new Request(
            $method = 'GET',
            $uri,
            $headers = [
                'Content-Type' => 'application/json',
                'Content-Encoding' => 'UTF-8',
                'Accept-Encoding' => 'gzip,deflate',
            ]
        );

        return $request;
    }

    /**
     * @param HotelSearchFilter $params
     * @return array
     */
    private function getRooms(HotelSearchFilter $params)
    {
        $rooms = [];

        foreach ($params->roomStayCandidates as $candidate) {

            $room = [];
            if (isset($candidate['adultCount'])) {

                for ($i = 0; $i < $candidate['adultCount']; $i++) {
                    $room[] = ['paxType' => 'Adult'];
                }
            }
            if (isset($candidate['childrenCount'], $candidate['childrenAges'])
                && is_array($candidate['childrenAges'])
                && count($candidate['childrenAges']) === (int)$candidate['childrenCount']
            ) {
                foreach ($candidate['childrenAges'] as $age) {
                    $room[] = [
                        'paxType' => 'Child',
                        'age' => $age,
                    ];
                }
            }

            $rooms[] = $room;
        }

        return $rooms;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 16.12.2015
 * Time: 10:40
 */

namespace ApiBundle\Service\HotelsList;


use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelPro;
use AppBundle\Entity\TripAdvisorRatingAware;
use AppBundle\Service\Money\Money;
use JMS\Serializer\Annotation as JMS;

/**
 * Class HotelDTO
 *
 * @JMS\ExclusionPolicy(value="all")
 *
 * @package ApiBundle\Service\HotelsList
 */
class HotelDTO
{
    /**
     * @var string
     * @JMS\Expose()
     */
    public $code;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $name;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $addressLine1;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $city;

    /**
     * @var integer
     * @JMS\Expose()
     */
    public $star;

    /**
     * @var float
     * @JMS\Expose()
     */
    public $latitude;

    /**
     * @var float
     * @JMS\Expose()
     */
    public $longitude;

    /**
     * @var Money
     */
    public $minRoomPriceServiceProvider;

    /**
     * @var Money
     */
    public $minRoomPriceInternal;

    /**
     * @var Money
     * @JMS\Expose()
     */
    public $price;

    /**
     * @var Money
     */
    public $priceServiceProvider;

    /**
     * @var Money
     */
    public $priceInternal;

    /**
     * @var Money
     */
    public $suggestedPriceServiceProvider;

    /**
     * @var Money
     */
    public $suggestedPriceInternal;

    /**
     * @var bool
     */
    public $isMarginFormulaOk = false;

    /**
     * @var array
     * @JMS\Expose()
     */
    public $images = [];

    /**
     * @var MetaDataDTO
     * @JMS\Expose()
     */
    public $metaData;

    /**
     * @var array
     * @JMS\Expose()
     */
    public $tripAdvisorRating;

    /**
     * @var float
     * @JMS\Expose()
     */
    public $saveAmount;

    /**
     * @var string
     * @JMS\Expose()
     */
    public $trivagoUrl;

    /**
     * @param HotelPro $hotel
     * @param string $searchId
     * @param string $cityId
     * @return static
     */
    public static function fromHotelsProHotel(HotelPro $hotel, $searchId, $cityId)
    {
        $dto = new static;
        $dto->name = $hotel->getHotelName();
        $dto->addressLine1 = $hotel->getHotelAddress();
        $dto->city = $hotel->getDestination();
        $dto->star = (int)$hotel->getStarRating();
        $dto->latitude = (float)$hotel->getLatitude();
        $dto->longitude = (float)$hotel->getLongitude();
        $dto->metaData = MetaDataDTO::forHotelProHotel($searchId, $cityId);
        $dto->code = $hotel->getHotelCode();
        $dto->fillTripAdvisorRating($hotel);

        $images = $hotel->getImagesUrls();
        foreach ($images as $image) {
            $dto->images[] = [
                'image_path' => $image,
            ];
        }

        return $dto;
    }

    public function fillTripAdvisorRating($hotel)
    {
        if ($hotel instanceof TripAdvisorRatingAware) {
            if ($hotel->getTripAdvisorRating() && $hotel->getTripAdvisorRating()->getMultipliedRating() != null) {
                $this->tripAdvisorRating = ['multiplied_rating' => $hotel->getTripAdvisorRating()->getMultipliedRating()];
            } else {
                $this->tripAdvisorRating = null;
            }
            return;
        }
        if (isset($hotel['tripAdvisorRating']['multipliedRating']) && $hotel['tripAdvisorRating']['multipliedRating'] !== null) {
            $this->tripAdvisorRating = ['multiplied_rating' => $hotel['tripAdvisorRating']['multipliedRating']];
        } else {
            $this->tripAdvisorRating = null;
        }
    }

    /**
     * @param Hotel $hotel
     * @return static
     */
    public static function fromTravelGuruHotel(Hotel $hotel)
    {
        $dto = new static;
        $dto->name = $hotel->getName();
        $dto->addressLine1 = $hotel->getAddressLine1();
        $dto->city = $hotel->getCity();
        $dto->star = (int)$hotel->getStar();
        $dto->latitude = (float)$hotel->getLatitude();
        $dto->longitude = (float)$hotel->getLongitude();
        $dto->metaData = MetaDataDTO::forTravelGuruHotel();
        $dto->code = sprintf('%08d', $hotel->getId());
        $dto->fillTripAdvisorRating($hotel);

        $images = $hotel->getImages();
        foreach ($images as $image) {
            $dto->images[] = [
                'image_path' => $image->getImagePath(),
            ];
        }

        return $dto;
    }

    /**
     * @param array $hotel
     * @return static
     */
    public static function fromTravelGuruHotelArray(array $hotel)
    {
        $dto = new static;
        $dto->name = $hotel['name'];
        $dto->addressLine1 = $hotel['addressLine1'];
        $dto->code = sprintf('%08d', $hotel['id']);
        $dto->minRoomPriceServiceProvider = $hotel['minRoomPrice'];
        $dto->priceServiceProvider = $hotel['price'];
        $dto->suggestedPriceServiceProvider = $hotel['suggestedPrice'];
        $dto->city = $hotel['city'];
        $dto->latitude = (float)$hotel['latitude'];
        $dto->longitude = (float)$hotel['longitude'];
        $dto->star = (int)$hotel['star'];
        $dto->metaData = MetaDataDTO::forTravelGuruHotel();
        $dto->fillTripAdvisorRating($hotel);

        if (is_array($hotel['images'])) {
            foreach ($hotel['images'] as $image) {
                $dto->images[] = [
                    'image_path' => $image['imagePath'],
                ];
            }
        }

        return $dto;
    }

    /**
     * @param array $hotel
     * @param $searchId
     * @param $cityId
     * @return static
     */
    public static function fromHotelsProHotelArray(array $hotel, $searchId, $cityId = null)
    {
        $dto = new static;
        $dto->name = $hotel['hotelName'];
        $dto->addressLine1 = $hotel['hotelAddress'];
        $dto->code = $hotel['hotelCode'];
        $dto->minRoomPriceServiceProvider = $hotel['minRoomPrice'];
        $dto->priceServiceProvider = $hotel['price'];
        $dto->suggestedPriceServiceProvider = $hotel['suggestedPrice'];
        $dto->city = $hotel['destination'];
        $dto->latitude = (float)$hotel['latitude'];
        $dto->longitude = (float)$hotel['longitude'];
        $dto->star = (int)$hotel['starRating'];
        $dto->metaData = MetaDataDTO::forHotelProHotel($searchId, $cityId);
        $dto->fillTripAdvisorRating($hotel);

        if (is_array($hotel['imagesUrls'])) {
            foreach ($hotel['imagesUrls'] as $image) {
                $dto->images[] = [
                    'image_path' => $image,
                ];
            }
        }

        return $dto;
    }
}
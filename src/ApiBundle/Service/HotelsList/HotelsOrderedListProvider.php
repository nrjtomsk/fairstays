<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 13.01.2016
 * Time: 11:55
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

/**
 * Class HotelsListProviderSorter
 * @package ApiBundle\Service\HotelsList
 */
class HotelsOrderedListProvider implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelsListProvider
     */
    private $hotelsListProvider;

    /**
     * @var HotelsListSorter
     */
    private $hotelsListSorter;

    /**
     * HotelsListProviderSorter constructor.
     * @param HotelsListProvider $hotelsListProvider
     * @param HotelsListSorter $hotelsListSorter
     */
    public function __construct(HotelsListProvider $hotelsListProvider, HotelsListSorter $hotelsListSorter)
    {
        $this->hotelsListProvider = $hotelsListProvider;
        $this->hotelsListSorter = $hotelsListSorter;
    }


    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $this->startWatch('sorting_hotels');
        $list = $this->hotelsListProvider->requestHotels($params);
        $this->hotelsListSorter->sortHotels($list, $params);
        $this->stopWatch('sorting_hotels');
        return $list;
    }
}
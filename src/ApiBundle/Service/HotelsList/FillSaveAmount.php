<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 31.05.16
 * Time: 14:40
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use AppBundle\Entity\HotelsDestination;
use AppBundle\Entity\PriceSelectedHotels;
use AppBundle\Repository\HotelsDestinationRepository;
use AppBundle\Repository\PriceSelectedHotelsRepository;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverterImpl;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

/**
 * Class FillSaveAmount
 * @package ApiBundle\Service\HotelsList
 */
class FillSaveAmount implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelsListProvider
     */
    private $provider;

    /**
     * @var PriceSelectedHotelsRepository
     */
    private $selectedRepository;

    /**
     * @var HotelsDestinationRepository;
     */
    private $hotelsDestenation;

    /**
     * @var MoneyConverterImpl
     */
    private $moneyConvert;

    /**
     * @var string
     */
    private $systemCurency;
    /**
     * FillSaveAmount constructor.
     * @param HotelsListProvider $provider
     * @param PriceSelectedHotelsRepository $selectedRepository
     * @param HotelsDestinationRepository $hotelsDestenation
     * @param MoneyConverterImpl $moneyConvert
     * @param $systemCurency
     */
    public function __construct(
        HotelsListProvider $provider,
        PriceSelectedHotelsRepository $selectedRepository,
        HotelsDestinationRepository $hotelsDestenation,
        MoneyConverterImpl $moneyConvert,
        $systemCurency
    )
    {
        $this->provider = $provider;
        $this->selectedRepository = $selectedRepository;
        $this->hotelsDestenation = $hotelsDestenation;
        $this->moneyConvert = $moneyConvert;
        $this->systemCurency = $systemCurency;
    }

    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO|void
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $list = $this->provider->requestHotels($params);
        $this->startWatch(get_class($this), 'fill_save_amount');
        $this->fill($list, $params->stayDateRangeStart, $params->requestedCurrency, $params->cityId);
        $this->stopWatch(get_class($this));

        return $list;
    }

    protected function fill(HotelsListDTO $list, $checkInDate, $currency, $cityId)
    {

        /** @var HotelDTO $hotel */
        if (is_array($list->hotels)) {

            /** @var HotelsDestination $city */
            $city = $this->hotelsDestenation->find($cityId);
            /** @var PriceSelectedHotels $selectedHotels */
            $selectedHotels = $this->selectedRepository->findOneByCityCodeAndCheckInDate(
                $city->getHotelsProId(),
                $checkInDate
            );

            foreach ($list->hotels as $hotel) {
                $hotel->saveAmount = null;

                if ($selectedHotels !== null) {
                    if (in_array($hotel->code, $selectedHotels->getSelectedHotels(), true)) {
                        $key = array_search($hotel->code, $selectedHotels->getSelectedHotels(), true);
                        if (array_key_exists($key, $selectedHotels->getSaveAmount())) {
                            $temp = $selectedHotels->getSaveAmount();
                            $money = Money::build($temp[$key], $this->systemCurency);
                            $hotel->saveAmount = $this->moneyConvert->convertMoney($money, $currency);
                        }
                    }
                }
            }
        }
    }
}
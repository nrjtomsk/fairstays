<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.01.2016
 * Time: 14:28
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Service\Hotels\SearchClient\FilterRule\FilterRule;
use ApiBundle\Service\Hotels\SearchClient\FilterRule\FilterRuleFactory;
use ApiBundle\Service\Hotels\SearchClient\FilterRule\ParamsAware;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class FilteredHotelsListProvider
 * @package ApiBundle\Service\HotelsList
 */
class FilteredHotelsListProvider implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var FilterRule
     */
    private $filterRule;

    /**
     * @var HotelsListProvider
     */
    private $hotelsListProvider;

    /**
     * @var NonDisplayedHotelsListLogger
     */
    private $unfilteredHotelListLogger;

    /**
     * @var FilterRuleFactory
     */
    private $filterRuleFactory;

    /**
     * FilteredHotelsListProvider constructor.
     * @param FilterRule $filterRule
     * @param HotelsListProvider $hotelsListProvider
     * @param NonDisplayedHotelsListLogger $unfilteredHotelListLogger
     * @param FilterRuleFactory $filterRuleFactory
     * @param $stopWatch
     */
    public function __construct(
        FilterRule $filterRule = null,
        HotelsListProvider $hotelsListProvider,
        NonDisplayedHotelsListLogger $unfilteredHotelListLogger,
        FilterRuleFactory $filterRuleFactory = null
    ) {
        $this->filterRule = $filterRule;
        $this->hotelsListProvider = $hotelsListProvider;
        $this->unfilteredHotelListLogger = $unfilteredHotelListLogger;
        $this->filterRuleFactory = $filterRuleFactory;
    }


    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $this->startWatch(get_class($this->filterRule), 'filtering');
        $list = $this->hotelsListProvider->requestHotels($params);
        if ($this->filterRule == null) {
            if ($this->filterRuleFactory == null) {
                throw new \RuntimeException('Neither filter rule nor filter rule factory passed');
            }
            if ($this->filterRuleFactory instanceof ParamsAware) {
                $this->filterRuleFactory->setParams($params);
            }
            $this->filterRule = $this->filterRuleFactory->buildFilterRule();
        }

        if ($this->filterRule instanceof ParamsAware) {
            $this->filterRule->setParams($params);
        }
        $this->filterList($list);
        $list->hotels = array_values($list->hotels);
        $this->stopWatch(get_class($this->filterRule));
        return $list;
    }

    /**
     * @param HotelsListDTO $list
     */
    private function filterList(HotelsListDTO $list)
    {

        if (count($list->hotels)) {
            foreach ($list->hotels as $k => $hotel) {
                if (!$this->filterRule->apply($hotel)) {
                    $this->unfilteredHotelListLogger->addNonDisplayedHotel($hotel);
                    unset($list->hotels[$k]);
                }
            }
        }

    }
}
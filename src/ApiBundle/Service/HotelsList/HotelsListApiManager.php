<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.12.2015
 * Time: 15:24
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use Psr\Http\Message\RequestInterface;

/**
 * Interface HotelsListApiManager
 * @package ApiBundle\Service\HotelsList
 */
interface HotelsListApiManager
{

    /**
     * Get name of the manager
     * @return string
     */
    public function getName();

    /**
     * @param HotelSearchFilter $params
     * @return RequestInterface
     */
    public function buildRequest(HotelSearchFilter $params);

    /**
     * @param string $rawContent
     * @return HotelDTO[]
     */
    public function handleResponse($rawContent);

}
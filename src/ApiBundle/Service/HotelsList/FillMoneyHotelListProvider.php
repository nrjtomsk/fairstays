<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 15.03.2016
 * Time: 10:25
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use AppBundle\Service\Money\Money;
use AppBundle\Service\Money\MoneyConverter;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

class FillMoneyHotelListProvider implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelsListProvider
     */
    private $provider;

    /**
     * @var MoneyConverter
     */
    private $moneyConverter;

    /**
     * @var string
     */
    private $systemCurrency;

    /**
     * FillMoneyHotelListProvider constructor.
     * @param HotelsListProvider $provider
     * @param MoneyConverter $moneyConverter
     * @param string $systemCurrency
     */
    public function __construct(HotelsListProvider $provider, MoneyConverter $moneyConverter, $systemCurrency)
    {
        $this->provider = $provider;
        $this->moneyConverter = $moneyConverter;
        $this->systemCurrency = $systemCurrency;
    }


    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $list = $this->provider->requestHotels($params);
        $this->startWatch(get_class($this), 'fill_money');
        $this->fillMoney($list, $params->requestedCurrency);
        $this->stopWatch(get_class($this));
        return $list;
    }

    /**
     * @param HotelsListDTO $list
     * @param $clientCurrency
     */
    private function fillMoney(HotelsListDTO $list, $clientCurrency)
    {

        if (is_array($list->hotels)) {
            /** @var HotelDTO $hotel */
            foreach ($list->hotels as $hotel) {
                $hotel->price = $this->moneyConverter->convertMoney($hotel->priceServiceProvider, $clientCurrency);
                $hotel->priceInternal = $this->moneyConverter->convertMoney($hotel->priceServiceProvider, $this->systemCurrency);

                $hotel->minRoomPriceInternal = $this->moneyConverter->convertMoney($hotel->minRoomPriceServiceProvider, $this->systemCurrency);

                $hotel->suggestedPriceInternal = $this->moneyConverter->convertMoney($hotel->suggestedPriceServiceProvider, $this->systemCurrency);
                if ($hotel->saveAmount !== null) {
                    $hotel->saveAmount = Money::build($hotel->saveAmount, $clientCurrency);
                }
            }
        }
        if ($list->getMetaMaxPrice() !== null && $list->getMetaMinPrice() !== null) {
            $list->setMetaMaxPrice($this->moneyConverter->convertMoney($list->getMetaMaxPrice(), $clientCurrency));
            $list->setMetaMinPrice($this->moneyConverter->convertMoney($list->getMetaMinPrice(), $clientCurrency));
        } else {
            $list->setMetaMaxPrice(
                Money::build(0, $clientCurrency)
            );
            $list->setMetaMinPrice(
                Money::build(5000000, $clientCurrency)
            );
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.12.2015
 * Time: 15:42
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Service\Hotels\TravelGuruSearchResponse;
use ApiBundle\Util\LoggerAwareTrait;
use AppBundle\Repository\HotelRepository;
use AppBundle\Service\Money\Money;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use Psr\Http\Message\RequestInterface;

/**
 * Class HotelsListApiManagerTravelGuru
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListApiManagerTravelGuru implements HotelsListApiManager, StopWatchAware
{
    use StopWatchAwareTrait;
    use LoggerAwareTrait;

    private $searchEndPoint;
    private $authPassword;
    private $authPropertyId;
    private $authUsername;
    private $travelGuruInterest;

    /**
     * @var HotelRepository
     */
    private $hotelRepository;

    /**
     * @var HotelSearchFilter
     */
    private $hotelsParams;

    /**
     * Api manager name
     */
    const NAME = 'travelGuru';

    /**
     * Incoming currency from the hotel provider
     */
    const INPUT_CURRENCY = 'INR';

    /**
     * HotelsListApiManagerTravelGuru constructor.
     * @param $searchEndPoint
     * @param $authPassword
     * @param $authPropertyId
     * @param $authUsername
     * @param $travelGuruInterest
     * @param HotelRepository $hotelRepository
     */
    public function __construct(
        $searchEndPoint,
        $authPassword,
        $authPropertyId,
        $authUsername,
        $travelGuruInterest,
        HotelRepository $hotelRepository
    ) {
        $this->searchEndPoint = $searchEndPoint;
        $this->authPassword = $authPassword;
        $this->authPropertyId = $authPropertyId;
        $this->authUsername = $authUsername;
        $this->travelGuruInterest = $travelGuruInterest;
        $this->hotelRepository = $hotelRepository;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return static::NAME;
    }

    /**
     * @param HotelSearchFilter $params
     * @return RequestInterface
     */
    public function buildRequest(HotelSearchFilter $params)
    {
        $this->setHotelsParams($params);

        $builder = new HotelsListRequestTravelGuru(
            $this->searchEndPoint,
            $this->authPassword,
            $this->authPropertyId,
            $this->authUsername
        );
        $builder->setLogger($this->logger);
        $builder->setHotelSearchFilter($params);

        return $builder->createRequest();
    }

    /**
     * @param string $rawContent
     * @return array
     */
    public function handleResponse($rawContent)
    {
        $this->startWatch('TG_build_response');
        $response = TravelGuruSearchResponse::fromApiResponse($rawContent);
        $this->stopWatch('TG_build_response');

        $this->startWatch('TG_fetch_db_set');
        $hotels = $this->hotelRepository->findAllByIdsAsArray($response->getHotelCodes());
        $this->stopWatch('TG_fetch_db_set');

        $this->startWatch('TG_fill_DTO_' . count($hotels));
        $realPrices = $response->getMinPrices();
        $suggestedPrices = $response->getSuggestedPrices();
        $hotelsDTO = [];

        $nightForTravelGuru = date_diff(
            new \DateTime($this->hotelsParams->stayDateRangeEnd),
            new \DateTime($this->hotelsParams->stayDateRangeStart),
            $alwaysPositive = true
        )->d;

        
        foreach ($hotels as $k => $hotel) {
            $realPrice = Money::build($realPrices[(int)$hotel['id']], self::INPUT_CURRENCY);
            $hotels[$k]['minRoomPrice'] = $realPrice->multiply($nightForTravelGuru);
            $hotels[$k]['price'] = $realPrice->multiply($nightForTravelGuru * (1 + $this->travelGuruInterest/100));
            $hotels[$k]['suggestedPrice'] = Money::build($suggestedPrices[(int)$hotel['id']], self::INPUT_CURRENCY)->multiply($nightForTravelGuru);

            $hotelsDTO[] = HotelDTO::fromTravelGuruHotelArray($hotels[$k]);
        }
        $this->stopWatch('TG_fill_DTO_' . count($hotels));

        return $hotelsDTO;
    }

    /**
     * @param HotelSearchFilter $hotelsParams
     */
    private function setHotelsParams($hotelsParams)
    {
        $this->hotelsParams = $hotelsParams;
    }
}
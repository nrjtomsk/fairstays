<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 28.09.2015
 * Time: 14:47
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Util\LoggerAwareTrait;
use GuzzleHttp\Psr7\Request;

/**
 * Class HotelsListRequestTravelGuru
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListRequestTravelGuru
{

    use LoggerAwareTrait;

    private $searchEndPoint;
    private $authPassword;
    private $authPropertyId;
    private $authUsername;

    /**
     * @var HotelSearchFilter
     */
    private $hotelSearchFilter;

    /**
     * TravelGuruSearchRequest constructor.
     * @param string $searchEndPoint
     * @param string $authPassword
     * @param string $authPropertyId
     * @param string $authUsername
     */
    public function __construct($searchEndPoint, $authPassword, $authPropertyId, $authUsername)
    {
        $this->searchEndPoint = $searchEndPoint;
        $this->authPassword = $authPassword;
        $this->authPropertyId = $authPropertyId;
        $this->authUsername = $authUsername;
    }

    /**
     * @param HotelSearchFilter $hotelSearchFilter
     */
    public function setHotelSearchFilter($hotelSearchFilter)
    {
        $this->hotelSearchFilter = $hotelSearchFilter;
    }

    /**
     * @return Request
     */
    public function createRequest()
    {
        $requestBody = str_replace("\r\n", '', $this->buildRequestBody());

        $request = new Request(
            $method = 'POST',
            $this->searchEndPoint,
            $headers = [
                'Content-Type' => 'text/xml; charset=UTF-8',
                'Content-Encoding' => 'UTF-8',
                'Accept-Encoding' => 'gzip,deflate',
            ],
            $requestBody
        );

        return $request;
    }

    /**
     * @return string
     */
    private function buildRequestBody()
    {
        return '
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <OTA_HotelAvailRQ xmlns="http://www.opentravel.org/OTA/2003/05" RequestedCurrency="'.HotelsListApiManagerTravelGuru::INPUT_CURRENCY.'" SortOrder="TG_RANKING" CorrelationID="T199V222931099425307077331050305561683" SearchCacheLevel="VeryRecent">
                '.$this->buildRequestSegmentsBody().'
                </OTA_HotelAvailRQ>
            </soap:Body>
        </soap:Envelope>';
    }

    private function buildRequestSegmentsBody()
    {
        return '
        <AvailRequestSegments>
            <AvailRequestSegment>
                <HotelSearchCriteria>
                    <Criterion>
                        <Address>
                            <CityName>'.strtoupper($this->hotelSearchFilter->city->getCityName()).'</CityName>
                            <CountryName Code="'.$this->hotelSearchFilter->countryCode.'"/>
                        </Address>
                        <StayDateRange Start="'.$this->hotelSearchFilter->stayDateRangeStart.'" End="'.$this->hotelSearchFilter->stayDateRangeEnd.'"/>
                        <RateRange MinRate="0" MaxRate="9999999" />
                        <RoomStayCandidates>'.$this->buildRequestRoomStayCandidatesBody().'</RoomStayCandidates>
                        <TPA_Extensions>
                        <UserAuthentication password="'.$this->authPassword.'" propertyId="'.$this->authPropertyId.'" username="'.$this->authUsername.'"/>
                            <Pagination enabled="false"/>
                        </TPA_Extensions>
                    </Criterion>
                </HotelSearchCriteria>
            </AvailRequestSegment>
        </AvailRequestSegments>';
    }

    /**
     * @return string
     */
    private function buildRequestRoomStayCandidatesBody()
    {
        $result = '';
        if (count($this->hotelSearchFilter->roomStayCandidates)) {

            foreach ($this->hotelSearchFilter->roomStayCandidates as $candidate) {
                $roomCandidate = '
                    <RoomStayCandidate>
                        <GuestCounts>';

                if (array_key_exists('adultCount', $candidate) && $candidate['adultCount'] > 0) {
                    for ($i = 0; $i < $candidate['adultCount']; $i++) {
                        $roomCandidate .=
                            '<GuestCount AgeQualifyingCode="10"/>';
                    }
                }

                if (
                    array_key_exists('childrenCount', $candidate) && $candidate['childrenCount'] > 0
                    && array_key_exists('childrenAges', $candidate) && $candidate['childrenCount'] == count(
                        $candidate['childrenAges']
                    )
                ) {
                    for ($j = 0; $j < $candidate['childrenCount']; $j++) {
                        $roomCandidate .= sprintf(
                            '<GuestCount Age="%d" AgeQualifyingCode="8"/>',
                            $candidate['childrenAges'][$j]
                        );
                    }
                }

                $roomCandidate .= '
                        </GuestCounts>
                    </RoomStayCandidate>
                ';

                $result .= $roomCandidate;
            }

            return $result;
        }

        throw new \RuntimeException('Cannot build request body');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 22.12.2015
 * Time: 10:13
 */

namespace ApiBundle\Service\HotelsList;


use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class MetaDataDTO
 * @package ApiBundle\Service\HotelsList
 */
class MetaDataDTO
{

    /**
     * @var string
     */
    public $serviceName;

    /**
     * @var string
     */
    public $hotelsProSearchId;

    /**
     * @var string
     */
    public $hotelsProProcessId;

    /**
     * @var int
     */
    public $cityId;

    /**
     * @param string $searchId
     * @param string $cityId
     * @return static
     */
    public static function forHotelProHotel($searchId, $cityId = null)
    {
        $metaData = new static;
        $metaData->serviceName = HotelsListApiManagerHotelsPro::NAME;
        $metaData->hotelsProSearchId = $searchId;
        $metaData->cityId = $cityId;
        return $metaData;
    }

    /**
     * @return static
     */
    public static function forTravelGuruHotel()
    {
        $metaData = new static;
        $metaData->serviceName = HotelsListApiManagerTravelGuru::NAME;

        return $metaData;
    }

    /**
     * @param $json
     * @return static
     */
    public static function fromJsonString($json)
    {
        $encoder = new JsonEncoder();
        $data = $encoder->decode($json, JsonEncoder::FORMAT);
        if (!isset($data['service_name'])) {
            throw new \RuntimeException('Invalid metadata object!');
        }

        $meta = new static;
        $meta->serviceName = $data['service_name'];

        switch ($data['service_name']) {
            case HotelsListApiManagerHotelsPro::NAME:

                if (isset($data['hotels_pro_search_id'])) {
                    $meta->hotelsProSearchId = $data['hotels_pro_search_id'];
                }
                if (isset($data['hotels_pro_process_id'])) {
                    $meta->hotelsProProcessId = $data['hotels_pro_process_id'];
                }
                if (isset($data['city_id'])) {
                    $meta->cityId = (int)$data['city_id'];
                }
                break;
            case HotelsListApiManagerTravelGuru::NAME:

                break;
            default:
                throw new \RuntimeException('Invalid metadata object!');
        }

        return $meta;
    }

    /**
     * @return string
     */
    public function toJsonString()
    {
        $encoder = new JsonEncoder();

        return $encoder->encode(
            [
                'service_name' => $this->serviceName,
                'hotels_pro_search_id' => $this->hotelsProSearchId,
                'hotels_pro_process_id' => $this->hotelsProProcessId,
                'city_id' => (int)$this->cityId,
            ],
            JsonEncoder::FORMAT
        );
    }
}
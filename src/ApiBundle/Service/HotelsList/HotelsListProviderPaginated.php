<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.01.2016
 * Time: 14:24
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Service\Trivago\TrivagoService;
use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelPro;
use AppBundle\Service\Money\Money;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;

class HotelsListProviderPaginated implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelsListProvider
     */
    private $hotelsListProvider;

    /**
     * @var TrivagoService;
     */
    private $trivagoService;

    /**
     * HotelsListProviderPaginated constructor.
     * @param HotelsListProvider $hotelsListProvider
     * @param TrivagoService $trivagoService
     */
    public function __construct(HotelsListProvider $hotelsListProvider, TrivagoService $trivagoService)
    {
        $this->hotelsListProvider = $hotelsListProvider;
        $this->trivagoService = $trivagoService;
    }


    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $this->stopWatch('start_action');
        $this->startWatch('search_pagination');
        $list = $this->hotelsListProvider->requestHotels($params);
        $list->setTotal(count($list->hotels));
        $this->fillMetaPrices($list, $params->requestedCurrency);
        $this->fillTrivago($list, $params);
        $list->hotels = array_values(
            array_slice(
                $list->hotels,
                $params->hotelsFrom - 1,
                $params->hotelsTo - $params->hotelsFrom + 1
            )
        );
        $this->stopWatch('search_pagination');
        return $list;
    }

    /**
     * @param HotelsListDTO $hotelsListDTO
     */
    private function fillMetaPrices(HotelsListDTO $hotelsListDTO, $currency)
    {
        $minPrice = Money::build(Money::MIN_VALUE, $currency);
        $maxPrice = Money::build(Money::MAX_VALUE, $currency);
        if (is_array($hotelsListDTO->hotels) && count($hotelsListDTO->hotels)) {

            $minPrice = Money::build(Money::MAX_VALUE, $currency);
            $maxPrice = Money::build(Money::MIN_VALUE, $currency);

            /** @var HotelDTO $hotel */
            foreach ($hotelsListDTO->hotels as $hotel) {
                if ($hotel->price->getAmount() < $minPrice->getAmount()) {
                    $minPrice = $hotel->price;
                }
                if ($hotel->price->getAmount() > $maxPrice->getAmount()) {
                    $maxPrice = $hotel->price;
                }
            }
        }

        $hotelsListDTO->setMetaMinPrice($minPrice);
        $hotelsListDTO->setMetaMaxPrice($maxPrice);
    }

    private function fillTrivago(HotelsListDTO $hotelsListDTO, HotelSearchFilter $params)
    {
        /** @var HotelDTO $hotel */
        foreach ($hotelsListDTO->hotels as $hotel) {
            $link = $this->trivagoService->generateLinkFromHotelAndDestenationAndDate(
                $hotel->code,
                $params->stayDateRangeStart,
                $params->stayDateRangeEnd
            );
            $hotel->trivagoUrl = $link;
        }
    }
}
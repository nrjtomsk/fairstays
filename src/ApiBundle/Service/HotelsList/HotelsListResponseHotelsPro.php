<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 15.12.2015
 * Time: 10:36
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\Exception\HotelServiceErrorResponseHotelsPro;
use ApiBundle\Service\MoneyConverter;
use AppBundle\Service\Money\Money;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class HotelsListResponseHotelsPro
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListResponseHotelsPro
{

    /**
     * @var array
     */
    private $hotelsCodes = [];

    /**
     * @var array
     */
    private $minPrices = [];

    /**
     * @var array
     */
    private $suggestedPrices = [];

    /**
     * @var string
     */
    private $searchId;

    /**
     * HotelsListResponseHotelsPro constructor.
     * @param string $rawContent
     * @throws HotelServiceErrorResponseHotelsPro
     */
    public function __construct($rawContent)
    {
        $encoder = new JsonEncoder();
        $response = $encoder->decode($rawContent, JsonEncoder::FORMAT);
        if (isset($response['availableHotels'])) {
            foreach ($response['availableHotels'] as $hotel) {
                if (isset($hotel['hotelCode'], $hotel['totalPrice'])) {
                    $this->pushHotel($hotel);
                }
            }
        } elseif (is_array($response)
            && count($response) === 2
            && isset($response[0], $response[1])
            && $response[0] > 500
        ) {
            // error case
            // $response[0] - code
            // $response[1] - message
            throw new HotelServiceErrorResponseHotelsPro(implode(' ', $response));
        } else {
            throw new HotelServiceErrorResponseHotelsPro('Cannot parse response');
        }

        if (isset($response['searchId'])) {
            $this->searchId = $response['searchId'];
        }
    }

    /**
     * @return array
     */
    public function getHotelsCodes()
    {
        return $this->hotelsCodes;
    }

    /**
     * @return array
     */
    public function getMinPrices()
    {
        return $this->minPrices;
    }

    /**
     * @return array
     */
    public function getSuggestedPrices()
    {
        return $this->suggestedPrices;
    }

    /**
     * @return string
     */
    public function getSearchId()
    {
        return $this->searchId;
    }

    /**
     * @param $hotel
     */
    private function pushHotel($hotel)
    {
        if (in_array($hotel['hotelCode'], $this->hotelsCodes)) {
            /** @var Money $totalPrice */
            $totalPrice = Money::build($hotel['totalPrice'], $hotel['currency']);
            /** @var Money $minPrice */
            $minPrice = $this->minPrices[$hotel['hotelCode']];
            if ($totalPrice->lessThan($minPrice)) {
                $this->minPrices[$hotel['hotelCode']] = $totalPrice;
                $this->suggestedPrices[$hotel['hotelCode']] = Money::build($hotel['totalSalePrice'] == null ? 0 : $hotel['totalSalePrice'], $hotel['currency']);
            }
        } else {
            $this->hotelsCodes[] = $hotel['hotelCode'];
            $this->minPrices[$hotel['hotelCode']] = Money::build($hotel['totalPrice'], $hotel['currency']);
            $this->suggestedPrices[$hotel['hotelCode']] = Money::build($hotel['totalSalePrice'] == null ? 0 : $hotel['totalSalePrice'], $hotel['currency']);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 14.01.2016
 * Time: 10:50
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\Exception\HotelServiceErrorResponse;

trait ApiManagerAwareTrait
{
    /**
     * @var HotelsListApiManager[]
     */
    private $managers;

    /**
     * @return HotelsListApiManager[]
     */
    public function getManagers()
    {
        return $this->managers;
    }

    /**
     * @param HotelsListApiManager $manager
     */
    public function addManager(HotelsListApiManager $manager)
    {
        $this->managers[$manager->getName()] = $manager;
    }

    /**
     * @param $name
     * @return HotelsListApiManager
     * @throws HotelServiceErrorResponse
     */
    public function getManager($name)
    {
        if (array_key_exists($name, $this->managers)) {
            return $this->managers[$name];
        }

        throw new HotelServiceErrorResponse(sprintf('Manager %s not found', $name));
    }
}

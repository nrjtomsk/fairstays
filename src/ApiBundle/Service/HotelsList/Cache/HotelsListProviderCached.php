<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 17:25
 */

namespace ApiBundle\Service\HotelsList\Cache;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Service\HotelsList\HotelsListDTO;
use ApiBundle\Service\HotelsList\HotelsListProvider;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class HotelsListProviderCached implements HotelsListProvider, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * @var HotelsListProvider
     */
    private $hotelsListProvider;

    /**
     * @var HotelsListCache
     */
    private $hotelsListCache;

    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * @var array
     */
    private $internalFilterParameters;

    /**
     * HotelsListProviderCached constructor.
     * @param HotelsListProvider $hotelsListProvider
     * @param HotelsListCache $hotelsListCache
     * @param JsonEncoder $encoder
     */
    public function __construct(
        HotelsListProvider $hotelsListProvider,
        HotelsListCache $hotelsListCache,
        JsonEncoder $encoder,
        array $internalFilterParameters
    ) {
        $this->hotelsListProvider = $hotelsListProvider;
        $this->hotelsListCache = $hotelsListCache;
        $this->encoder = $encoder;
        $this->internalFilterParameters = $internalFilterParameters;
    }

    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $this->startWatch('cached_search_result');
        $result = $this->hotelsListCache->get($cacheKey = $this->getCacheKey($params));
        if (!$result) {
            $result = $this->hotelsListProvider->requestHotels($params);
            try {
                $this->hotelsListCache->save($cacheKey, $result);
            } catch (\Exception $e) {/* do nothing */}
        }
        $this->stopWatch('cached_search_result');

        return $result;
    }

    /**
     * @param HotelSearchFilter $filter
     * @return string
     */
    private function getCacheKey(HotelSearchFilter $filter)
    {
        return md5(
            $filter->cityId .
            $filter->stayDateRangeStart .
            $filter->stayDateRangeEnd .
            $filter->requestedCurrency .
            serialize($filter->stars) .
            $this->encoder->encode($filter->roomStayCandidates, JsonEncoder::FORMAT) .
            serialize($this->internalFilterParameters)
        );
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 15:21
 */

namespace ApiBundle\Service\HotelsList\Cache;


use ApiBundle\Service\HotelsList\HotelsListDTO;

interface HotelsListCache
{
    const DEFAULT_CACHE_LIFE_TIME = 86400;

    /**
     * @param string $key
     * @return HotelsListDTO|null
     */
    public function get($key);

    /**
     * @param string $key
     * @param HotelsListDTO $list
     * @param int $lifeTime
     * @return void
     */
    public function save($key, HotelsListDTO $list, $lifeTime = self::DEFAULT_CACHE_LIFE_TIME);

    /**
     * @param string $key
     * @return void
     */
    public function clear($key);
}
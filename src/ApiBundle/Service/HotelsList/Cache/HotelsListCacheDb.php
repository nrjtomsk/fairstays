<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 16:09
 */

namespace ApiBundle\Service\HotelsList\Cache;


use ApiBundle\Service\HotelsList\HotelsListDTO;
use AppBundle\Entity\CacheValue;
use AppBundle\Repository\CacheValueRepository;

class HotelsListCacheDb implements HotelsListCache
{
    /**
     * @var CacheValueRepository
     */
    private $cacheValueRepository;

    /**
     * @var HotelsListToStringConverter
     */
    private $hotelListToStringConverter;

    /**
     * @var int
     */
    private $cacheLifeTime;

    /**
     * HotelsListCacheDb constructor.
     * @param CacheValueRepository $cacheValueRepository
     * @param HotelsListToStringConverter $hotelListToStringConverter
     * @param int $cacheLifeTime
     */
    public function __construct(
        CacheValueRepository $cacheValueRepository,
        HotelsListToStringConverter $hotelListToStringConverter,
        $cacheLifeTime
    ) {
        $this->cacheValueRepository = $cacheValueRepository;
        $this->hotelListToStringConverter = $hotelListToStringConverter;
        $this->cacheLifeTime = $cacheLifeTime;
    }


    /**
     * @param string $key
     * @return HotelsListDTO|null
     */
    public function get($key)
    {
        /** @var CacheValue $cacheValue */
        $cacheValue = $this->cacheValueRepository->find($key);

        if (!$cacheValue) {
            return null;
        }

        if ($cacheValue && $cacheValue->getExpires() < new \DateTime('now')) {
            $this->clear($key);

            return null;
        }

        return $this->hotelListToStringConverter->stringToList($cacheValue->getValue());
    }

    /**
     * @param string $key
     * @param HotelsListDTO $list
     * @param int $lifeTime
     * @return void
     */
    public function save($key, HotelsListDTO $list, $lifeTime = null)
    {
        if ($lifeTime === null) {
            if ($this->cacheLifeTime === null) {
                $this->cacheLifeTime = self::DEFAULT_CACHE_LIFE_TIME;
            }
            $lifeTime = $this->cacheLifeTime;
        }
        $cacheValue = new CacheValue();
        $cacheValue->setKey($key);
        $cacheValue->setValue($this->hotelListToStringConverter->listToString($list));
        $cacheValue->setExpires(new \DateTime('now + '.$lifeTime.' seconds'));
        $this->cacheValueRepository->save($cacheValue);
    }

    /**
     * @param string $key
     * @return void
     */
    public function clear($key)
    {
        $this->cacheValueRepository->deleteByKey($key);
    }
}
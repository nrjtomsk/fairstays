<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 16:20
 */

namespace ApiBundle\Service\HotelsList\Cache;


use ApiBundle\Service\HotelsList\HotelsListDTO;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class HotelsListToStringConverter
{
    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * HotelsListToStringConverter constructor.
     * @param JsonEncoder $encoder
     */
    public function __construct(JsonEncoder $encoder = null)
    {
        if ($encoder === null) {
            $encoder = new JsonEncoder();
        }
        $this->encoder = $encoder;
    }


    /**
     * @param HotelsListDTO $list
     * @return string
     */
    public function listToString(HotelsListDTO $list)
    {
        return $list->toString();
    }

    /**
     * @param string $string
     * @return HotelsListDTO
     */
    public function stringToList($string)
    {
        return HotelsListDTO::fromString($string);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 13.01.2016
 * Time: 14:21
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;

/**
 * Filter hotels
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListProviderFilter implements HotelsListProvider
{
    /**
     * @var HotelsListProvider
     */
    private $hotelsListProvider;

    /**
     * @var HotelsListFilter
     */
    private $hotelsListFilter;

    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params)
    {
        $list = $this->hotelsListProvider->requestHotels($params);
        $list->hotels = $this->hotelsListFilter->filterList($list->hotels);

        return $list;
    }
}
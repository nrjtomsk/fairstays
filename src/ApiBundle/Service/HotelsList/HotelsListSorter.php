<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 13.01.2016
 * Time: 14:27
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use AppBundle\Entity\PriceSelectedHotels;
use AppBundle\Repository\PriceSelectedHotelsRepository;

/**
 * Class HotelsListSorter
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListSorter
{
    /**
     * @var PriceSelectedHotels
     */
    private $selectedHotels;

    /**
     * @var HotelSearchFilter
     */
    private $params;

    /**
     * HotelsListSorter constructor.
     * @param PriceSelectedHotelsRepository $selectedHotels
     */
    public function __construct(PriceSelectedHotelsRepository $selectedHotels)
    {
        $this->selectedHotels = $selectedHotels;
    }

    /**
     * @param HotelsListDTO $hotelsList
     * @param HotelSearchFilter $params
     */
    public function sortHotels(HotelsListDTO $hotelsList, HotelSearchFilter $params)
    {
        $this->params = $params;
        switch ($params->sortOrder) {
            case HotelSearchFilter::SORT_ORDER_PRICE:
                $callbackName = [$this, 'sortPriceAsc'];
                break;
            case HotelSearchFilter::SORT_ORDER_PRICE_DESC:
                $callbackName = [$this, 'sortPriceDesc'];
                break;
            case HotelSearchFilter::SORT_ORDER_STAR_RATING_ASCENDING:
                $callbackName = [$this, 'sortStarAsc'];
                break;
            case HotelSearchFilter::SORT_ORDER_STAR_RATING_DESCENDING:
                $callbackName = [$this, 'sortStarDesc'];
                break;
            default:
                $callbackName = [$this, 'sortSelectedHotels'];
                break;
        }

        usort($hotelsList->hotels, $callbackName);
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return int
     */
    private function sortPriceAsc(HotelDTO $a, HotelDTO $b)
    {
        if ($a->price === $b->price) {
            return 0;
        }

        return $a->price < $b->price ? -1 : 1;
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return int
     */
    private function sortPriceDesc(HotelDTO $a, HotelDTO $b)
    {
        if ($a->price === $b->price) {
            return 0;
        }

        return $a->price > $b->price ? -1 : 1;
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return int
     */
    private function sortStarAsc(HotelDTO $a, HotelDTO $b)
    {
        if ($a->star === $b->star) {
            return 0;
        }

        return $a->star < $b->star ? -1 : 1;
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return int
     */
    private function sortStarDesc(HotelDTO $a, HotelDTO $b)
    {
        if ($a->star == $b->star) {
            return 0;
        }

        return $a->star > $b->star ? -1 : 1;
    }

    /**
     * @param HotelDTO $a
     * @param HotelDTO $b
     * @return int
     */
    private function sortSelectedHotels(HotelDTO $a, HotelDTO $b)
    {
        $selectedHotels = $this->selectedHotels->findOneByCityCodeAndCheckInDate(
            $this->params->city->getHotelsProId(),
            $this->params->stayDateRangeStart
        );
        if ($selectedHotels === null) {
            return 0;
        }

        if ($a->metaData->serviceName === HotelsListApiManagerTravelGuru::NAME) {
            return -1;
        }

        if ($b->metaData->serviceName === HotelsListApiManagerTravelGuru::NAME) {
            return 1;
        }

        $flipped = array_flip($selectedHotels->getSelectedHotels());

        if (!array_key_exists($a->code, $flipped) && !array_key_exists($b->code, $flipped)) {
            return 0;
        }

        if (!array_key_exists($a->code, $flipped)) {
            return -1;
        }

        if (!array_key_exists($b->code, $flipped)) {
            return 1;
        }

        $firstValue = array_key_exists($a->code, $flipped)
            ? $flipped[$a->code]
            : PHP_INT_MIN;

        if ($flipped[$b->code] === $firstValue) {
            return 0;
        }

        return $firstValue < $flipped[$b->code] ? -1 : 1;

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 13.01.2016
 * Time: 11:51
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\Exception\HotelServiceErrorResponse;

/**
 * Interface ApiManagerAware
 * @package ApiBundle\Service\HotelsList
 */
interface ApiManagerAware
{
    /**
     * @param HotelsListApiManager $manager
     * @return void
     */
    public function addManager(HotelsListApiManager $manager);

    /**
     * @param $name
     * @return HotelsListApiManager
     * @throws HotelServiceErrorResponse
     */
    public function getManager($name);
}
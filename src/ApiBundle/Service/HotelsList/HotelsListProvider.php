<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 11.01.2016
 * Time: 16:47
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use Psr\Log\LoggerAwareInterface;

/**
 * Interface HotelsListProvider
 * @package ApiBundle\Service\HotelsList
 */
interface HotelsListProvider
{
    /**
     * @param HotelSearchFilter $params
     * @return HotelsListDTO
     */
    public function requestHotels(HotelSearchFilter $params);
}
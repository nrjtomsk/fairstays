<?php
/**
 * Created by PhpStorm.
 * User: rassamakhinny
 * Date: 03.03.16
 * Time: 12:18
 */

namespace ApiBundle\Service\HotelsList;

use AppBundle\Entity\HotelsDestination;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Acl\Exception\Exception;

/**
 * Class UnfilteredHotelsListLogger
 * @package ApiBundle\Service\HotelsList
 */
class NonDisplayedHotelsListLogger
{
    /**
     * @var LoggerInterface
     */
    private $loggerDisplayed;

    /**
     * @var LoggerInterface
     */
    private $loggerNonDisplayed;

    /**
     * @var LoggerInterface
     */
    private $loggerIndiaHotelsPro;

    /**
     * @var LoggerInterface
     */
    private $loggerIndiaTravelGuru;

    /**
     * @var bool
     */
    private $loggersEnabled;


    /**
     * @var HotelDTO[]
     */
    private $hotelsNonDisplayed;

    /**
     * @var HotelDTO[]
     */
    private $hotelsDisplayed;

    /**
     * @var HotelsDestination
     */
    private $city;

    /**
     * NonDisplayedHotelsListLogger constructor.
     * @param LoggerInterface $loggerDisplayed
     * @param LoggerInterface $loggerNonDisplayed
     * @param LoggerInterface $loggerIndiaHotelsPro
     * @param LoggerInterface $loggerIndiaTravelGuru
     */
    public function __construct(
        LoggerInterface $loggerDisplayed,
        LoggerInterface $loggerNonDisplayed,
        LoggerInterface $loggerIndiaHotelsPro,
        LoggerInterface $loggerIndiaTravelGuru,
        $loggersEnabled
    ) {
        $this->loggerDisplayed = $loggerDisplayed;
        $this->loggerNonDisplayed = $loggerNonDisplayed;
        $this->loggerIndiaHotelsPro = $loggerIndiaHotelsPro;
        $this->loggerIndiaTravelGuru = $loggerIndiaTravelGuru;
        $this->loggersEnabled = $loggersEnabled;
    }

    public function addNonDisplayedHotel(HotelDTO $hotel)
    {
        if ($this->loggersEnabled) {
            $this->hotelsNonDisplayed[] = $hotel;
        }
    }

    /**
     * @param HotelsListDTO $hotelsList
     * @param HotelsDestination $city
     */
    public function dumpSearchLogs(HotelsListDTO $hotelsList, HotelsDestination $city)
    {
        if ($this->loggersEnabled) {
            $this->setHotelsDisplayed($hotelsList->hotels);
            $this->setCity($city);
            $this->logDisplayedHotels();
            $this->logNonDisplayedHotels();
            $this->logIndiaHotelsProHotels();
            $this->logIndiaTravelGuruHotels();
        }
    }

    public function setHotelsDisplayed($hotelsDisplayed)
    {
        if ($this->loggersEnabled) {
            $this->hotelsDisplayed = $hotelsDisplayed;
        }
    }

    public function setCity(HotelsDestination $city)
    {
        $this->city = $city;
    }

    public function logDisplayedHotels()
    {
        if (is_array($this->hotelsDisplayed) && count($this->hotelsDisplayed)) {
            foreach ($this->hotelsDisplayed as $hotel) {
                $this->loggerDisplayed->info('DISPLAYED Hotels: '.$this->formatHotel($hotel));
            }
        }
    }

    /**
     * @param \ApiBundle\Service\HotelsList\HotelDTO $hotel
     * @return string
     */
    private function formatHotel(HotelDTO $hotel)
    {
        return sprintf(
            '%s %s %s %s %s',
            $hotel->metaData->serviceName,
            $hotel->code,
            $hotel->name,
            $hotel->priceInternal,
            $hotel->suggestedPriceInternal
        );
    }

    public function logNonDisplayedHotels()
    {
        if (is_array($this->hotelsNonDisplayed) && count($this->hotelsNonDisplayed)) {
            foreach ($this->hotelsNonDisplayed as $hotel) {
                $this->loggerNonDisplayed->info('NOT DISPLAYED Hotels: '.$this->formatHotel($hotel));
            }
        }
    }

    public function logIndiaHotelsProHotels()
    {
        $forLoggingHotels = array_filter(
            $this->hotelsDisplayed,
            function (HotelDTO $item) {
                return $this->city->getCountryName() === 'India'
                && $item->metaData->serviceName === HotelsListApiManagerHotelsPro::NAME;
            }
        );

        if (is_array($forLoggingHotels) && count($forLoggingHotels)) {
            foreach ($forLoggingHotels as $hotel) {
                $this->loggerIndiaHotelsPro->info('DISPLAYED India HP Hotels: '.$this->formatHotel($hotel));
            }
        }
    }

    public function logIndiaTravelGuruHotels()
    {
        $forLoggingHotels = array_filter(
            $this->hotelsDisplayed,
            function (HotelDTO $item) {
                return $this->city->getCountryName() === 'India'
                && $item->metaData->serviceName === HotelsListApiManagerTravelGuru::NAME;
            }
        );


        if (is_array($forLoggingHotels) && count($forLoggingHotels)) {
            foreach ($forLoggingHotels as $hotel) {
                $this->loggerIndiaTravelGuru->info('DISPLAYED India TG Hotels: '.$this->formatHotel($hotel));
            }
        }
    }
}
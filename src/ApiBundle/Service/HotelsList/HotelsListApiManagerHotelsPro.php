<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 14.12.2015
 * Time: 16:19
 */

namespace ApiBundle\Service\HotelsList;


use ApiBundle\DTO\HotelSearchFilter;
use ApiBundle\Service\Country\CountryCodeDictionary;
use ApiBundle\Service\Country\Exception\BadNationalityCodeException;
use AppBundle\Repository\HotelProRepository;
use AppBundle\Service\HotelsPro\Exception\SelectedHotelsPriceException;
use AppBundle\Service\HotelsPro\SelectedHotelsPrice;
use AppBundle\Service\Money\Money;
use AppBundle\Utils\StopWatchAware;
use AppBundle\Utils\StopWatchAwareTrait;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class HotelsListApiManagerHotelsPro
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListApiManagerHotelsPro implements HotelsListApiManager, StopWatchAware
{
    use StopWatchAwareTrait;

    /**
     * Api manager name
     */
    const NAME = 'hotelsPro';
    private $endPoint;
    private $apiKey;
    private $hotelsProInterest;
    /**
     * @var CountryCodeDictionary
     */
    private $countryCodeDictionary;
    /**
     * @var HotelProRepository
     */
    private $hotelProRepository;
    /**
     * @var HotelSearchFilter
     */
    private $hotelsParams;
    /**
     * @var string
     */
    private $nationality;
    /**
     * @var string
     */
    private $systemCurrency;

    /**
     * @var SelectedHotelsPrice
     */
    private $selectedHotelsPrice;

    /**
     * HotelsListApiManagerHotelsPro constructor.
     * @param $endPoint
     * @param $apiKey
     * @param $hotelsProInterest
     * @param HotelProRepository $hotelProRepository
     * @param $nationality
     * @param $systemCurrency
     * @param SelectedHotelsPrice $selectedHotelsPrice
     * @param CountryCodeDictionary $countryCodeDictionary
     */
    public function __construct(
        $endPoint,
        $apiKey,
        $hotelsProInterest,
        HotelProRepository $hotelProRepository,
        $nationality,
        $systemCurrency,
        SelectedHotelsPrice $selectedHotelsPrice,
        CountryCodeDictionary $countryCodeDictionary
    ) {
        $this->endPoint = $endPoint;
        $this->apiKey = $apiKey;
        $this->hotelsProInterest = $hotelsProInterest;
        $this->hotelProRepository = $hotelProRepository;
        $this->nationality = $nationality;
        $this->systemCurrency = $systemCurrency;
        $this->selectedHotelsPrice = $selectedHotelsPrice;
        $this->countryCodeDictionary = $countryCodeDictionary;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return static::NAME;
    }

    /**
     * @param HotelSearchFilter $params
     * @throws BadNationalityCodeException
     * @return RequestInterface
     */
    public function buildRequest(HotelSearchFilter $params)
    {
        $this->setHotelsParams($params);

        if ($params->nationality !== null) {
            $nationality = $params->nationality;
        } else {
            $nationality = $this->nationality;
        }
        $this->countryCodeDictionary->checkNationalityCodeInAllowedList($nationality);
        $builder = new HotelsListRequestHotelsPro(
            $this->endPoint,
            $this->apiKey,
            $params,
            $nationality,
            $this->systemCurrency
        );

        return $builder->createRequest();
    }

    /**
     * @param HotelSearchFilter $hotelsParams
     */
    private function setHotelsParams(HotelSearchFilter $hotelsParams)
    {
        $this->hotelsParams = $hotelsParams;
    }

    /**
     * @param string $rawContent
     * @return array
     */
    public function handleResponse($rawContent)
    {
        $this->startWatch('HP_build_response');

        $response = new HotelsListResponseHotelsPro($rawContent);

        $this->stopWatch('HP_build_response');

        $this->startWatch('HP_fetch_db_set');
        $hotels = $this->hotelProRepository->findAllByHotelCodesAsArray($response->getHotelsCodes());
        $this->stopWatch('HP_fetch_db_set');

        $this->startWatch('HP_fill_DTO_'.count($hotels));
        $hotelsDTO = [];
        $minPrices = $response->getMinPrices();
        $suggestedPrices = $response->getSuggestedPrices();

        foreach ($hotels as $k => $hotel) {
            /** @var Money $minRoomPrice */
            $minRoomPrice = $minPrices[$hotel['hotelCode']];
            $hotels[$k]['minRoomPrice'] = $minRoomPrice;

            try {
                $rate = $this->selectedHotelsPrice->getSelectedHotelsRate(
                    $this->hotelsParams->cityId,
                    $this->hotelsParams->stayDateRangeStart,
                    $hotels[$k]['hotelCode']
                );
                $hotels[$k]['price'] = $minRoomPrice->multiply(1 + $rate / 100);
            } catch (SelectedHotelsPriceException $e) {
                $hotels[$k]['price'] = $minRoomPrice->multiply(1 + $this->hotelsProInterest / 100);
            }

            $hotels[$k]['suggestedPrice'] = $suggestedPrices[$hotel['hotelCode']];

            $hotelsDTO[] = HotelDTO::fromHotelsProHotelArray(
                $hotels[$k],
                $response->getSearchId(),
                $this->hotelsParams->cityId
            );
        }
        $this->stopWatch('HP_fill_DTO_'.count($hotels));

        return $hotelsDTO;
    }


}
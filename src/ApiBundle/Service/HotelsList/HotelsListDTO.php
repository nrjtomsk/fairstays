<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 14.12.2015
 * Time: 13:23
 */

namespace ApiBundle\Service\HotelsList;

use AppBundle\Service\Money\Money;

/**
 * Class HotelsListDTO
 * @package ApiBundle\Service\HotelsList
 */
class HotelsListDTO
{
    /**
     * @var HotelDTO[]
     */
    public $hotels = [];

    /**
     * @var array
     */
    public $meta = [
        'minPrice' => null,
        'maxPrice' => null,
        'total' => 0,
    ];

    /**
     * @return string
     */
    public function toString()
    {
        return serialize($this->hotels).':::::'.serialize($this->meta);
    }

    /**
     * @param $string
     * @return static
     */
    public static function fromString($string)
    {
        if (is_string($string)) {
            list($hotels, $meta) = explode(':::::', $string);
            $dto = new static;
            $dto->hotels = unserialize($hotels);
            $dto->meta = unserialize($meta);

            return $dto;
        } else {
            return new static;
        }
    }

    /**
     * @param Money $minPrice
     */
    public function setMetaMinPrice(Money $minPrice)
    {
        $this->meta['minPrice'] = $minPrice;
    }

    /**
     * @param Money $maxPrice
     */
    public function setMetaMaxPrice(Money $maxPrice)
    {
        $this->meta['maxPrice'] = $maxPrice;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->meta['total'] = (int) $total;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->meta['total'];
    }

    /**
     * @return Money
     */
    public function getMetaMinPrice()
    {
        return $this->meta['minPrice'];
    }

    /**
     * @return Money
     */
    public function getMetaMaxPrice()
    {
        return $this->meta['maxPrice'];
    }


}
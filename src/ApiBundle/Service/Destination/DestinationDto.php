<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 14:41
 */

namespace ApiBundle\Service\Destination;

/**
 * Class DestinationDto
 * @package ApiBundle\Service\Destination
 */
class DestinationDto
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $countryName;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $cityName;

    /**
     * @param array $destination
     * @return DestinationDto
     */
    public static function fromArray(array $destination)
    {
        $item = new static;
        $item->id = $destination['id'];
        $item->cityName = $destination['cityName'];
        $item->countryCode = $destination['countryCode'];
        $item->countryName = $destination['countryName'];
        $item->state = $destination['state'];
        return $item;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 14:40
 */

namespace ApiBundle\Service\Destination;

use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class DestinationFilter
 * @package ApiBundle\Service\Destination
 */
class DestinationFilter
{
    /**
     * @var string
     */
    public $query;

    /**
     * @var integer
     */
    public $limit;

    /**
     * @var integer
     */
    public $offset;

    /**
     * @param ParamFetcherInterface $fetcher
     * @return DestinationFilter
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        $filter = new static;
        $strict = true;
        $filter->query = $fetcher->get('query', $strict);
        $filter->limit = $fetcher->get('limit', $strict);
        $filter->offset = $fetcher->get('offset', $strict);
        return $filter;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 14:44
 */

namespace ApiBundle\Service\Destination;

use AppBundle\Repository\CommonDestinationRepository;

/**
 * Class DestinationServiceImpl
 * @package ApiBundle\Service\Destination
 */
class DestinationServiceImpl implements DestinationService
{


    /**
     * @var CommonDestinationRepository
     */
    private $commonDestinationRepository;

    /**
     * DestinationServiceImpl constructor.
     * @param CommonDestinationRepository $commonDestinationRepository
     */
    public function __construct(CommonDestinationRepository $commonDestinationRepository)
    {
        $this->commonDestinationRepository = $commonDestinationRepository;
    }

    /**
     * @param DestinationFilter $filter
     * @return DestinationDto[]
     */
    public function search(DestinationFilter $filter)
    {
        return array_map(
            function (array $item) {
                return DestinationDto::fromArray($item);
            },
            $this->commonDestinationRepository->searchByQueryAsArray(
                $filter->query,
                $filter->limit,
                $filter->offset
            )
        );
    }
}
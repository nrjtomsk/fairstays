<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 04.05.2016
 * Time: 14:40
 */

namespace ApiBundle\Service\Destination;

/**
 * Interface DestinationService
 * @package ApiBundle\Service\Destination
 */
interface DestinationService
{
    /**
     * @param DestinationFilter $filter
     * @return DestinationDto[]
     */
    public function search(DestinationFilter $filter);
}
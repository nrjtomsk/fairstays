<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 14:14
 */

namespace ApiBundle\Util;

/**
 * Class SimpleApiKeyGenerator
 * @package ApiBundle\Util
 */
class SimpleApiKeyGenerator implements ApiKeyGenerator
{

    private $characterSet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private $apiKeyLength = 64;

    /**
     * @return string
     */
    public function generateApiKey()
    {
        return self::generate($this->characterSet, $this->apiKeyLength);
    }

    /**
     * @param string $characterSet
     * @param int $apiKeyLength
     * @return string
     */
    public static function generate(
        $characterSet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        $apiKeyLength = 64
    ) {
        $characterSetLength = strlen($characterSet);
        $apikey = '';
        for ($i = 0; $i < $apiKeyLength; ++$i) {
            $apikey .= $characterSet[rand(0, $characterSetLength - 1)];
        }

        return rtrim(base64_encode(sha1(uniqid($apikey, true).$apikey)), '=');
    }
}
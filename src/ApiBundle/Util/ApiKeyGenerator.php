<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 14:13
 */

namespace ApiBundle\Util;

/**
 * Interface ApiKeyGenerator
 * @package ApiBundle\Util
 */
interface ApiKeyGenerator
{
    /**
     * @return string
     */
    public function generateApiKey();
}
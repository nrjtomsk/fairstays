<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 11.12.2015
 * Time: 13:42
 */

namespace ApiBundle\Util;


use Psr\Log\LoggerInterface;

/**
 * Class LoggerAwareTrait
 * @package ApiBundle\Util
 */
trait LoggerAwareTrait
{

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $message
     * @param array $context
     */
    public function logInfo($message, array $context = [])
    {
        if ($this->logger instanceof LoggerInterface) {
            $this->logger->info($message, $context);
        }
    }

    /**
     * @param $message
     * @param array $context
     */
    public function logError($message, array $context = [])
    {
        if ($this->logger instanceof LoggerInterface) {
            $this->logger->error($message, $context);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.12.2015
 * Time: 12:46
 */

namespace ApiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BookingFormType
 * @package ApiBundle\Form
 */
class BookingFormType extends AbstractType
{

    /**
     * @inheritdoc
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hotel_code', 'text');
        $builder->add('requested_currency', 'text');
        $builder->add('check_in', 'text');
        $builder->add('check_out', 'text');
        $builder->add('room_name', 'text');
        $builder->add('room_type', 'text');
        $builder->add('room_type_number_of_units', 'text');
        $builder->add('rate_plan', 'text');
        $builder->add('name_prefix', 'text');
        $builder->add('stripe_token', 'text');
        $builder->add('first_name', 'text');
        $builder->add('last_name', 'text');
        $builder->add('phone_area', 'text');
        $builder->add('phone_country_code', 'text');
        $builder->add('phone_extension', 'text');
        $builder->add('phone_number', 'text');
        $builder->add('phone_tech_type', 'text');
        $builder->add('email', 'email');
        $builder->add('address1', 'text');
        $builder->add('address2', 'text');
        $builder->add('city_name', 'text');
        $builder->add('postal_code', 'text');
        $builder->add('address_state_prov', 'text');
        $builder->add('address_state_prov_code', 'text');
        $builder->add('country_name', 'text');
        $builder->add('country_name_code', 'text');
        $builder->add('comment', 'text');
        $builder->add('preferences', 'text');
        $builder->add('rooms', 'text');
        $builder->add('nationality', 'text');
        $builder->add('meta', 'text');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'ApiBundle\Service\Booking\BookingParams',
                'csrf_protection' => false,
            ]
        );
    }

    /**
     * @return string The name of this type
     */
    public function getName()
    {
        return 'api_booking_form_type';
    }
}
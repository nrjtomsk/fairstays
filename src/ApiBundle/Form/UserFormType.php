<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFormType
 * @package ApiBundle\Form
 */
class UserFormType extends AbstractType
{
    /**
     * @inheritdoc
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'username',
            'text',
            [
                'required' => true,
                'label' => 'Phone number',
            ]
        );

        $builder->add(
            'fullName',
            'text',
            [
                'required' => false,
                'label' => 'Full Name',
            ]
        );

        $builder->add(
            'email',
            'email',
            [
                'required' => false,
                'label' => 'Email',
            ]
        );

        $builder->add(
            'phoneNumber',
            'text',
            [
                'required' => false,
                'label' => 'Phone number',
            ]
        );
    }

    /**
     * @inheritdoc
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\User',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getName()
    {
        return 'api_bundle_user_form_type';
    }
}

<?php

namespace ApiBundle\Form;

use ApiBundle\Service\Bid\BidParams;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BidFormType
 * @package ApiBundle\Form
 */
class BidFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hotel_code', 'text', [
            'label' => 'Hotel Code'
        ]);
        $builder->add('currency', 'text', [
            'label' => 'Currency'
        ]);
        $builder->add('amount', 'text', [
            'label' => 'Amount',
        ]);
        $builder->add('check_in', 'text', [
            'label' => 'Check in date'
        ]);
        $builder->add('check_out', 'text', [
            'label' => 'Check out date'
        ]);
        $builder->add('rate_plan', 'text', [
            'label' => 'Rate plan for travel guru',
        ]);
        $builder->add('room_type', 'text' , [
            'label' => 'Room type for travel guru'
        ]);
        $builder->add('nationality', 'text', [
            'label' => 'Nationality'
        ]);
        $builder->add('meta', 'text', [
            'label' => 'Room metadata as json string'
        ]);
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BidParams::class
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'api_bid_form';
    }
}

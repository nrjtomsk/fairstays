<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 18.09.2015
 * Time: 12:01
 */

namespace ApiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CurrencyFormType
 * @package ApiBundle\Form
 */
class CurrencyFormType extends AbstractType
{
    /**
     * @inheritdoc
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'text',
            [
                'required' => true,
                'label' => 'Currency Name',
            ]
        );

        $builder->add(
            'code',
            'text',
            [
                'required' => true,
                'label' => 'Currency Code',
            ]
        );
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'api_bundle_currency_form_type';
    }
}
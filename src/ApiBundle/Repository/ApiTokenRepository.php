<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 15:33
 */

namespace ApiBundle\Repository;


use AppBundle\Entity\ApiToken;

/**
 * Interface ApiTokenRepository
 * @package ApiBundle\Repository
 */
interface ApiTokenRepository extends ObjectRepository
{
    /**
     * @return ApiToken
     */
    public function createToken();

    /**
     * @param $token
     * @return ApiToken|null
     */
    public function findOneByToken($token);
}
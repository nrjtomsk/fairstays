<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 16.09.2015
 * Time: 15:34
 */

namespace ApiBundle\Repository;


use Doctrine\Common\Persistence\ObjectRepository as PersistenceObjectRepository;

/**
 * Interface ObjectRepository
 * @package ApiBundle\Repository
 */
interface ObjectRepository extends PersistenceObjectRepository
{

}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 19.01.2016
 * Time: 14:46
 */

namespace ApiBundle\Tests\Controller;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Symfony\Bundle\FrameworkBundle\Client;

class WebTestCase extends \Liip\FunctionalTestBundle\Test\WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var User
     */
    protected $adminUser;

    /**
     * @var ReferenceRepository
     */
    protected $referenceRepository;

    protected function setUp()
    {
        $this->referenceRepository = $this->loadFixtures(
            [
                'AppBundle\DataFixtures\ORM\LoadAdminUsers',
                'AppBundle\DataFixtures\ORM\LoadHotelsData',
            ]
        )->getReferenceRepository();
        $admin = $this->referenceRepository->getReference('admin');

        $this->adminUser = $admin;

        $this->loginAs($admin, 'admin')->loginAs($admin, 'api')->loginAs($admin, 'main')->loginAs($admin, 'dev');
        $this->client = static::makeClient(true);
    }

    protected function assertResponseJsonObjectContainsFields(array $fields, $object)
    {
        foreach ($fields as $field) {
            static::assertArrayHasKey($field, $object);
        }
    }
}
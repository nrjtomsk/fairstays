<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 19.01.2016
 * Time: 14:46
 */

namespace ApiBundle\Tests\Controller;


use AppBundle\DataFixtures\ORM\LoadHotelsData;
use AppBundle\Entity\HotelPro;
use AppBundle\Entity\HotelsDestination;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Test for Hotels and Hotel
 *
 * Class HotelsControllerTest
 * @package ApiBundle\Tests\Controller
 */
class HotelsControllerTest extends WebTestCase
{
    /** @var  HotelsDestination */
    private $city;

    public function testGetHotelsAction()
    {
        $this->client->request('GET', '/api/v1/hotels.json');
        $this->assertStatusCode(Response::HTTP_BAD_REQUEST, $this->client);

        $this->client->request(
            'GET',
            '/api/v1/hotels.json?'.http_build_query(
                [
                    'requestedCurrency' => 'USD',
                    'cityId' => $this->city->getCityId(),
                    'stayDateRangeEnd' => (new \DateTime('+5 days'))->format('Y-m-d'),
                    'stayDateRangeStart' => (new \DateTime('+4 days'))->format('Y-m-d'),
                    'rateRangeMin' => 0,
                    'rateRangeMax' => 999999,
                    'rooms[]' => '{"adultCount":1}',
                ]
            )
        );
        $this->assertStatusCode(Response::HTTP_OK, $this->client);

        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        static::assertJsonStringEqualsJsonString(
            '{"hotels":[], "meta":{"minPrice":{"amount": 0, "currency_code": "USD"},"maxPrice":{"amount":"5000000", "currency_code":"USD"},"total":0}}',
            $content
        );
    }

    public function testGetHotelAction()
    {
        $this->client->request('GET', '/api/v1/hotel.json');
        $this->assertStatusCode(Response::HTTP_BAD_REQUEST, $this->client);

        /** @var HotelPro $hotel */
        $hotel = $this->referenceRepository->getReference(LoadHotelsData::HOTEL_HOTELS_PRO);
        $hotelCode = $hotel->getHotelCode();
        $this->client->request(
            'GET',
            '/api/v1/hotel.json?'.http_build_query(
                [
                    'currency' => 'USD',
                    'code' => $hotelCode,
                    'dateFrom' => (new \DateTime('+ 2 days'))->format('Y-m-d'),
                    'dateTo' => (new \DateTime('+3 days'))->format('Y-m-d'),
                    'meta' => '{"service_name":"travelGuru"}',
                    'rooms[]' => '{"adultCount":1}',
                ]
            )
        );
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $object = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        $this->assertResponseJsonObjectContainsFields(
            [
                'code',
                'name',
                'description',
                'stars',
                'latitude',
                'longitude',
                'site',
                'number_of_rooms',
                'amenities',
                'address',
                'rooms',
                'meta_data',
            ],
            $object
        );
    }

    protected function setUp()
    {
        parent::setUp();
        $fixtures = $this->loadFixtures(
            [
                'AppBundle\DataFixtures\ORM\LoadHotelCitiesData',
            ]
        )->getReferenceRepository();
        $this->city = $fixtures->getReference('city1');
    }

}
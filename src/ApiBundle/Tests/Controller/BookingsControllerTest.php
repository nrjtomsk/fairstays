<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 20.01.2016
 * Time: 10:23
 */

namespace ApiBundle\Tests\Controller;


use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelsDestination;
use AppBundle\Service\User\UserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class BookingsControllerTest extends WebTestCase
{
    /**
     * @var Hotel
     */
    private $hotelTG;

    /**
     * @var HotelsDestination
     */
    private $city;

    public function testPostBookingsAction()
    {
        $this->client->request(
            'GET',
            '/api/v1/hotel.json?'.http_build_query(
                [
                    'currency' => 'USD',
                    'code' => $this->hotelTG->getId(),
                    'dateFrom' => (new \DateTime('+ 2 days'))->format('Y-m-d'),
                    'dateTo' => (new \DateTime('+3 days'))->format('Y-m-d'),
                    'meta' => '{"service_name":"travelGuru"}',
                    'rooms[]' => '{"adultCount":1}',
                ]
            )
        );

        $this->assertStatusCode(Response::HTTP_OK, $this->client);
        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $hotelDetails = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        static::assertResponseJsonObjectContainsFields(['rooms'], $hotelDetails);
        $room = $hotelDetails['rooms'][0];
        static::assertResponseJsonObjectContainsFields(
            [
                'room_title',
                'rate_plan_code',
                'room_type_code',
                'rate_plan_name',
                'description',
                'cancellation_policy',
                'price_before',
                'tax_before',
                'price',
                'tax',
                'occupancy',
                'images',
                'meta_data',
            ],
            $room
        );

        $requestParams = [
            'api_booking_form_type[hotel_code]' => $this->hotelTG->getId(),
            'api_booking_form_type[requested_currency]' => 'USD',
            'api_booking_form_type[check_in]' => (new \DateTime('+ 2 days'))->format('Y-m-d'),
            'api_booking_form_type[check_out]' => (new \DateTime('+ 3 days'))->format('Y-m-d'),
            'api_booking_form_type[room_name]' => $room['room_title'],
            'api_booking_form_type[room_type]' => $room['room_type_code'],
            'api_booking_form_type[room_type_number_of_units]' => 1,
            'api_booking_form_type[rate_plan]' => $room['rate_plan_code'],
            'api_booking_form_type[name_prefix]' => 'Mr',
            'api_booking_form_type[first_name]' => $this->adminUser->getFirstName(),
            'api_booking_form_type[last_name]' => $this->adminUser->getLastName(),
            'api_booking_form_type[phone_area]' => '8',
            'api_booking_form_type[phone_country_code]' => '8',
            'api_booking_form_type[phone_extension]' => '22',
            'api_booking_form_type[phone_number]' => '89234155045',
            'api_booking_form_type[phone_tech_type]' => '1',
            'api_booking_form_type[email]' => $this->adminUser->getEmail(),
            'api_booking_form_type[address1]' => 'address1',
            'api_booking_form_type[address2]' => 'address2',
            'api_booking_form_type[city_name]' => $this->city->getCityName(),
            'api_booking_form_type[postal_code]' => '12345',
            'api_booking_form_type[address_state_prov]' => 'Tomsk',
            'api_booking_form_type[address_state_prov_code]' => '70',
            'api_booking_form_type[country_name]' => $this->city->getCountryName(),
            'api_booking_form_type[country_name_code]' => $this->city->getCountryCode(),
            'api_booking_form_type[comment]' => 'Test comment',
            'api_booking_form_type[preferences]' => 'Y',
            'api_booking_form_type[rooms]' => '[{"paxes":[{"type":"Adult","title":"Mr", "firstName":"'.$this->adminUser->getFirstName(
                ).'", "lastName":"'.$this->adminUser->getLastName().'"}]}]',
            'api_booking_form_type[meta]' => $this->getContainer()->get('serializer.encoder.json')->encode(
                $room['meta_data'],
                JsonEncoder::FORMAT
            ),
        ];

        $this->client->request(Request::METHOD_POST, '/api/v1/bookings.json', $requestParams);

        $this->assertStatusCode(Response::HTTP_BAD_REQUEST, $this->client);

    }

    /**
     * Get user booking request
     * status code:
     * success: 200
     */
    public function testGetUserBookingrequestsAction()
    {
        $this->client->request(
            'GET',
            '/api/v1/user/bookingrequests.json'
        );
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * test get booking request details
     * status code:
     * error: 404
     */
    public function testGetUserBookingrequestAction()
    {
        $id = '-0';
        $this->client->request(
            'GET',
            '/api/v1/user/bookingrequests.'.$id.'.json'
        );
        $this->assertStatusCode(Response::HTTP_NOT_FOUND, $this->client);
    }

    /**
     * test delete booking request
     * status code:
     * success: 200
     * error: 500
     */
    public function testDeleteUserBookingAction()
    {
        $userId = '-0';
        $id = '-0';
        $this->client->request(
            'DELETE',
            '/api/v1/users/'.$userId.'/bookings/'.$id.'.json'
        );

        $this->assertStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, $this->client);
    }

    protected function setUp()
    {
        parent::setUp();
        $fixtures = $this->loadFixtures(
            [
                'AppBundle\DataFixtures\ORM\LoadFakeHotelsTravelGuruAndHotelsPro',
                'AppBundle\DataFixtures\ORM\LoadHotelCitiesData',
            ]
        )->getReferenceRepository();
        $this->hotelTG = $fixtures->getReference('hotelTG');
        $this->city = $fixtures->getReference('city1');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: RassamakhinNY
 * Date: 19.01.2016
 * Time: 16:10
 */

namespace ApiBundle\Tests\Controller;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


class UserControllerTest extends WebTestCase
{
    /**
     * test patch current user
     */
    public function testPatchUserAction()
    {
        $this->client->request('PATCH', '/api/v1/user.json');
        $this->assertStatusCode(Response::HTTP_BAD_REQUEST, $this->client);
    }

    /**
     * test get current user profile info
     */
    public function testGetUserCurrentAction()
    {
        $this->client->request('GET', '/api/v1/user/current.json');
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * test post avatar for current user
     * status code:
     * error: 400
     * success: 201
     */
    public function testPostUserAvatarAction()
    {
        $fileLocator = $this->getContainer()->get('file_locator');
        $path = $fileLocator->locate("demo.png");

        $client = new Client(['base_uri' => 'http://fairstays.loc/']);

        $response = $client->request(
            'POST',
            'app-dev.php/api/v1/users/avatars.json',
            [
                'multipart' => [
                    [
                        'name' => 'api_bundle_file_form_type[file]',
                        'contents' => fopen($path, 'r'),

                    ],
                ],
                'headers' => ['X-Auth-Token' => '123'],
            ]
        );

        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * test get user by phone number
     * status code:
     * error: 404
     * success: 200
     */
    public function testGetUserAction()
    {
        $number = 'admin';
        $this->client->request('GET', '/api/v1/users/'.$number.'.json');
        $this->assertStatusCode(Response::HTTP_OK, $this->client);


        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $object = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        $this->assertResponseJsonObjectContainsFields(
            [
                'id',
                'username',
                'avatar',
                'email',
                'full_name',
            ],
            $object
        );

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 19.01.2016
 * Time: 16:54
 */
namespace ApiBundle\Tests\Controller;

/**
 * Test for Cities
 *
 * Class CitiesControllerTest
 * @package ApiBundle\Tests\Controller
 */
class CitiesControllerTest extends WebTestCase
{
    static public $expected = '[{"cityId":1,"hotelsProId":"N35C","countryName":"India","cityName":"New Delhi","state":"null","countryCode":"null"},{"cityId":2,"hotelsProId":"A14A","countryName":"India","cityName":"Ambalavayal","state":"null","countryCode":"null"}]';

    public function testAllAction()
    {
        $fixtures = ['AppBundle\DataFixtures\ORM\LoadHotelsDestination'];
        $this->loadFixtures($fixtures);

        $this->client->request('GET', '/api/v1/cities.json', ['ACCEPT' => 'application/json']);
        $response = $this->client->getResponse();
        $content = $response->getContent();
        static::assertEquals(self::$expected, $content);

    }
}
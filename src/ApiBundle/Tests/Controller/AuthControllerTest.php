<?php
/**
 * Created by PhpStorm.
 * User: RassamakhinNY
 * Date: 21.01.2016
 * Time: 9:12
 */

namespace ApiBundle\Tests\Controller;


use AppBundle\Service\User\UserProvider;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends WebTestCase
{
    /**
     * test for get secret code
     * success code: 200
     */
    public function testGetSecretAction()
    {
        $phoneNumber = '11111111111';
        $this->client->request('GET', '/api/secret?_format=json&phone_number='.$phoneNumber);

        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * test for get token
     * success code: 200
     */
    public function testGetAuthTokenAction()
    {
        $phoneNumber = '11111111111';
        $secret = '11111';
        $this->client->request('GET', '/api/token?_format=json&phone_number='.$phoneNumber.'&secret='.$secret);
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }
}
<?php

namespace ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritdoc
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('api');
        $rootNode
            ->children()
            ->scalarNode('delivery')
            ->defaultValue('header')
            ->validate()
            ->ifNotInArray(array('header'))
            ->thenInvalid('Unknown authentication delivery type "%s".')
            ->end()
            ->end()
            ->scalarNode('parameter_name')
            ->defaultValue('X-Auth-Token')
            ->end()
            ->end();

        return $treeBuilder;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 16:42
 */

namespace ApiBundle\DependencyInjection\Security\Factory;


use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ApiKeyFactory
 * @package ApiBundle\DependencyInjection\Security\Factory
 */
class ApiKeyFactory implements SecurityFactoryInterface
{
    /**
     * Returns array which contains providerId, listenerId
     * and defaultEntryPoint
     *
     * @inheritdoc
     * @param ContainerBuilder $container
     * @param $id
     * @param $config
     * @param $userProvider
     * @param $defaultEntryPoint
     * @return array
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'api.api_key.provider.api_key.'.$id;
        $container->setDefinition($providerId, new DefinitionDecorator('api.api_key.provider.api_key'))
            ->replaceArgument(0, new Reference($userProvider));
        $listenerId = 'api.api_key.listener.api_key.'.$id;
        $listener = $container->setDefinition($listenerId, new DefinitionDecorator('api.api_key.listener.api_key'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getKey()
    {
        return 'api_key';
    }

    /**
     * @inheritdoc
     * @param NodeDefinition $builder
     */
    public function addConfiguration(NodeDefinition $builder)
    {
    }
}
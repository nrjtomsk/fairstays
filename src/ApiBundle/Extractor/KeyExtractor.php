<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 16:49
 */

namespace ApiBundle\Extractor;


use Symfony\Component\HttpFoundation\Request;

/**
 * Interface KeyExtractor
 * @package ApiBundle\Extractor
 */
interface KeyExtractor
{
    /**
     * Tells if the given requests carries an API key.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function hasKey(Request $request);

    /**
     * Extract the API key from the given request
     *
     * @param Request $request
     *
     * @return string
     */
    public function extractKey(Request $request);
}
<?php
/**
 * Created by PhpStorm.
 * User: KustovVA
 * Date: 17.07.2015
 * Time: 16:50
 */

namespace ApiBundle\Extractor;


use Symfony\Component\HttpFoundation\Request;

/**
 * Checks existence of key
 * Extracts key
 *
 * Class HeaderExtractor
 * @package ApiBundle\Extractor
 */
class HeaderExtractor implements KeyExtractor
{
    /**
     * @var string
     */
    private $parameterName;

    /**
     * @param string $parameterName The name of the URL parameter containing the API key.
     */
    public function __construct($parameterName)
    {
        $this->parameterName = $parameterName;
    }

    /**
     * Tells if the given requests carries an API key.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function hasKey(Request $request)
    {
        return $request->headers->has($this->parameterName);
    }

    /**
     * Extract the API key from the given request
     *
     * @param Request $request
     *
     * @return string
     */
    public function extractKey(Request $request)
    {
        return $request->headers->get($this->parameterName);
    }
}
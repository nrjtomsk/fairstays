<?php

namespace AdminApiBundle\Controller;

use ApiBundle\Exception\InvalidCredentialsException;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class AuthController
 *
 * Authorize the admin by password
 * on success returns authToken and profile info
 *
 * @package AdminApiBundle\Controller
 */
class AuthController extends Controller
{

    /**
     * @Route("/token", methods={"POST"})
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Auth"
     * )
     *
     * Method takes username & password and on success returns authToken and profile in json format
     *
     * @Rest\RequestParam(name="username", nullable=false, allowBlank=false, description="User name")
     * @Rest\RequestParam(name="password", nullable=false, allowBlank=false, description="Password")
     *
     * @param ParamFetcherInterface $fetcher
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function postAuthTokenAction(ParamFetcherInterface $fetcher)
    {
        try {
            $username = $fetcher->get('username');
            $password = $fetcher->get('password');
            $authToken = $this->get('admin_api.service.auth_handler')->getAuthToken($username, $password);
            /** @var User $user */
            $user = $this->get('app.service.user.provider')->loadUserByUsername($username);
            $profileDTO = $this->get('admin_api.service.profile_dto_provider')->getProfileDTOFromUser($user);

            return new JsonResponse(
                [
                    'authToken' => $authToken->getToken(),
                    'profile' => $profileDTO,
                ]
            );
        } catch (InvalidCredentialsException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * @Route("/token", methods={"OPTIONS"})
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Auth"
     * )
     *
     * @return JsonResponse
     */
    public function optionsAuthTokenAction()
    {
        return new JsonResponse();
    }

}

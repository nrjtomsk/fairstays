<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 18.11.2015
 * Time: 11:37
 */

namespace AdminApiBundle\Controller;

use AdminApiBundle\Exception\WrongPasswordException;
use ApiBundle\Exception\InvalidFormException;
use Gedmo\Exception\UploadableInvalidFileException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class ProfileController
 *
 * Managing a profile Admin
 *
 * @package AdminApiBundle\Controller
 */
class ProfileController extends Controller
{

    /**
     * Get current admin user profile
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Profile"
     * )
     *
     * @return array
     */
    public function getProfileAction()
    {
        return [
            'profile' => $this->get('admin_api.service.profile_dto_provider')->getProfileDTOFromUser($this->getUser()),
        ];
    }

    /**
     * Put current admin user profile
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Profile"
     * )
     *
     * @param Request $request
     * @return array
     */
    public function putProfileAction(Request $request)
    {
        try {
            return [
                'profile' => $this->get('admin_api.service.profile_handler')->put($request),
            ];
        } catch (InvalidFormException $e) {

            return new JsonResponse($e->getMessage(), JsonResponse::HTTP_BAD_REQUEST);

        } catch (WrongPasswordException $e) {

            return new JsonResponse(
                [
                    'errors' => [
                        'password' => 'Wrong password',
                    ],
                ], JsonResponse::HTTP_FORBIDDEN
            );
        }
    }

    /**
     * Post profile avatar
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Profile"
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function postProfileAvatarAction(Request $request)
    {
        try {
            return [
                'profile' => $this->get('admin_api.service.profile_handler')->putAvatar($request),
            ];
        } catch (UploadableInvalidFileException $e) {

            throw new BadRequestHttpException;
        }
    }

}
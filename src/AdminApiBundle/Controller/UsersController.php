<?php

namespace AdminApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class UsersController
 *
 * Getting list of Admins
 *
 * @package AdminApiBundle\Controller
 */
class UsersController extends Controller
{

    /**
     * Get users list
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Users"
     * )
     *
     * @QueryParam(name="fullName", nullable=true, default="", description="Users full name")
     * @QueryParam(name="per_page", nullable=false, default=10, requirements="\d+", description="Users per page")
     * @QueryParam(name="page", nullable=false, default=0, requirements="\d+", description="Page number")
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function getUsersAction(ParamFetcherInterface $fetcher)
    {
        return $this->get('admin_api.service.user_handler')->getUsers($fetcher);
    }

}

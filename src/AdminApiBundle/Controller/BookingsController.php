<?php

namespace AdminApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BookingsController
 *
 * Booking removing by Admin
 *
 * @package AdminApiBundle\Controller
 */
class BookingsController extends Controller
{

    /**
     * Get bookings list by params
     *
     * @ApiDoc(
     *  resource=true,
     *  section="Admin Bookings"
     * )
     *
     * @QueryParam(name="per_page", nullable=false, default=10, requirements="\d+", description="Bookings per page")
     * @QueryParam(name="page", nullable=false, default=1, requirements="\d+", description="Page number")
     * @QueryParam(name="type", nullable=true, default=0, requirements="\d+", description="Status type")
     * @QueryParam(name="searchQuery", nullable=true, default="", description="Search query for user full name and hotel name")
     *
     * @param ParamFetcherInterface $fetcher
     * @return JsonResponse
     */
    public function getBookingsAction(ParamFetcherInterface $fetcher)
    {
        return $this->get('admin_api.service.booking_handler')->getBookings($fetcher);
    }
}

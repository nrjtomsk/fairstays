<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 20.11.2015
 * Time: 17:04
 */

namespace AdminApiBundle\Exception;

/**
 * Class WrongPasswordException
 * @package AdminApiBundle\Exception
 */
class WrongPasswordException extends \Exception
{

}
<?php
/**
 * Created by PhpStorm.
 * User: GorbatkoAV
 * Date: 21.01.2016
 * Time: 15:54
 */
namespace ApiBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test for Admin profile
 *
 * Class HotelsControllerTest
 * @package ApiBundle\Tests\Controller
 */
class ProfileControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Test for GET Admin profile
     */
    public function testGetProfile()
    {
        $this->client->request('GET', '/admin-api/v1/profile', ['ACCEPT' => 'application/json']);
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * Test for PUT Admin profile
     */
    public function testPutProfiles()
    {
        // Important field password,
        // please don't change it
        $params = [
            'profile' => [
                'first_name' => 'admin',
                'last_name' => 'Nimda',
                'password' => 'admin',
                'email' => 'Admin@mail.com',
            ],
        ];
        $this->client->request(
            Request::METHOD_PUT,
            '/admin-api/v1/profile',
            $params,
            $files = [],
            $server = []
        );
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * Test for POST Avatar
     *
     * check existence file before test:
     * /app/Resources/avatar1.jpg
     */
    public function testPostProfilesAvatars()
    {
        $fileNameParts = [
            $this->getContainer()->getParameter('kernel.root_dir'),
            'Resources',
            'demo.png',
        ];
        $fileName = implode(DIRECTORY_SEPARATOR, $fileNameParts);

        $file = new UploadedFile($fileName, "demo.png", "image/png", 695, 0);

        $this->client->request(
            Request::METHOD_POST,
            '/admin-api/v1/profiles/avatars',
            $params = [],
            $files = [
                'file' => $file,
            ],
            $server = []
        );
        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }

    /**
     * Authorize as Admin user
     */
    protected function setUp()
    {
        $fixtures = $this->loadFixtures(
            [
                'AppBundle\DataFixtures\ORM\LoadAdminUsers',
            ]
        )->getReferenceRepository();
        $admin = $fixtures->getReference('admin');


        $this->loginAs($admin, 'admin')->loginAs($admin, 'api')->loginAs($admin, 'main')->loginAs($admin, 'dev');
        $this->client = static::makeClient(true);
    }

}
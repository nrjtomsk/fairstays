<?php

namespace AdminApiBundle\Tests\Controller;

use ApiBundle\Tests\Controller\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class BookingsControllerTest
 * @package AdminApiBundle\Tests\Controller
 */
class BookingsControllerTest extends WebTestCase
{
    /**
     * Test Bookings list
     */
    public function testAdminBookings()
    {
        $fixtures = ['AppBundle\DataFixtures\ORM\LoadAdminBookings'];
        $this->loadFixtures($fixtures);
        $this->client->request(
            'GET',
            '/admin-api/v1/bookings',
            ['per_page' => '10', 'page' => '1', 'type' => '0', '_format' => 'json', 'ACCEPT' => 'application/json']
        );


        $this->assertStatusCode(Response::HTTP_OK, $this->client);
        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $object = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        static::assertResponseJsonObjectContainsFields(['bookings', 'users', 'hotels', 'meta'], $object);
        foreach ($object['users'] as $user) {
            static::assertResponseJsonObjectContainsFields(
                [
                    'id',
                    'full_name',
                    'email',
                    'phone',
                    'avatar_url',
                    'bookings_count',
                ],
                $user
            );
        }
        foreach ($object['hotels'] as $hotel) {
            static::assertResponseJsonObjectContainsFields(
                [
                    'id',
                    'name',
                    'address',
                    'image_url',
                ],
                $hotel
            );
        }
        foreach ($object['bookings'] as $booking) {
            static::assertResponseJsonObjectContainsFields(
                [
                    'id',
                    'created_at',
                    'amount',
                    'status',
                    'date_check_in',
                    'date_check_out',
                    'user',
                    'hotel',
                    'public_id',
                    'status_labels',
                ],
                $booking
            );
        }
        foreach ($object['bookings'] as $status) {
            static::assertResponseJsonObjectContainsFields(
                [
                    'ProvisionalPre',
                    'ProvisionalComplete',
                    'PaymentComplete',
//                    'Canceled',
                    'Confirmed',
                ],
                $status['status_labels']
            );
        }

    }
}

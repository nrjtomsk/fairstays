<?php

namespace AdminApiBundle\Tests\Controller;

use ApiBundle\Tests\Controller\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class UsersControllerTest
 * @package AdminApiBundle\Tests\Controller
 */
class UsersControllerTest extends WebTestCase
{
    /**
     * Test Users list
     */
    public function testAdminUsers()
    {
        $fixtures = ['AppBundle\DataFixtures\ORM\LoadUsers'];
        $this->loadFixtures($fixtures);
        $this->client->request(
            'GET',
            '/admin-api/v1/users',
            ['per_page' => '10', 'page' => '1', 'ACCEPT' => 'application/json']
        );

        $this->assertStatusCode(Response::HTTP_OK, $this->client);
        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $object = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        static::assertResponseJsonObjectContainsFields(['users', 'meta'], $object);
        foreach ($object['users'] as $user) {
            static::assertResponseJsonObjectContainsFields(
                [
                    'id',
                    'full_name',
                    'email',
                    'phone',
                    'avatar_url',
                    'bookings_count',
                ],
                $user
            );
        }

    }
}

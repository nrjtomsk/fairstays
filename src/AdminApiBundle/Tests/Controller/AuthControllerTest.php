<?php

namespace AdminApiBundle\Tests\Controller;

use ApiBundle\Tests\Controller\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Class AuthControllerTest
 * @package AdminApiBundle\Tests\Controller
 */
class AuthControllerTest extends WebTestCase
{
    public function testPostAuthTokenAction()
    {
        $params = [
            'username' => 'admin',
            'password' => 'admin',
            '_format' => 'json',
        ];
        $this->client->request(
            Request::METHOD_POST,
            '/admin-api/token',
            $params,
            $files = [],
            $server = []
        );

        $this->assertStatusCode(Response::HTTP_OK, $this->client);
        $content = $this->client->getResponse()->getContent();
        static::assertJson($content);
        $object = $this->getContainer()->get('serializer.encoder.json')->decode($content, JsonEncoder::FORMAT);
        static::assertResponseJsonObjectContainsFields(['authToken', 'profile'], $object);
        static::assertResponseJsonObjectContainsFields(
            [
                'id',
                'avatarUrl',
                'firstName',
                'lastName',
                'email',
            ],
            $object['profile']
        );
    }

    public function testOptionsAuthTokenAction()
    {
        $this->client->request(
            Request::METHOD_OPTIONS,
            '/admin-api/token'
        );

        $this->assertStatusCode(Response::HTTP_OK, $this->client);
    }
}

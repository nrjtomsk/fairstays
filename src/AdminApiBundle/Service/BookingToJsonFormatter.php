<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 24.11.2015
 * Time: 9:32
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\DTO\BookingDTO;
use AdminApiBundle\DTO\HotelDTO;
use AdminApiBundle\Model\BookingsFilterParams;
use AdminApiBundle\Util\PaginatorHelper;
use ApiBundle\Service\HotelsList\HotelsListApiManagerHotelsPro;
use ApiBundle\Service\HotelsList\HotelsListApiManagerTravelGuru;
use AppBundle\Entity\BookingRequest;
use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelPro;
use AppBundle\Repository\HotelProRepository;
use AppBundle\Repository\HotelRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Transforms booking data to JSON format
 *
 * Class BookingToJsonFormatter
 * @package AdminApiBundle\Service
 */
class BookingToJsonFormatter
{

    /**
     * @var UserDTOProvider
     */
    private $userDTOProvider;

    /**
     * @var HotelProRepository
     */
    private $hotelProRepository;

    /**
     * @var HotelRepository
     */
    private $travelGuruHotelRepository;

    /**
     * BookingToJsonFormatter constructor.
     * @param UserDTOProvider $userDTOProvider
     * @param HotelProRepository $hotelProRepository
     * @param HotelRepository $travelGuruHotelRepository
     */
    public function __construct(
        UserDTOProvider $userDTOProvider,
        HotelProRepository $hotelProRepository,
        HotelRepository $travelGuruHotelRepository
    ) {
        $this->userDTOProvider = $userDTOProvider;
        $this->hotelProRepository = $hotelProRepository;
        $this->travelGuruHotelRepository = $travelGuruHotelRepository;
    }

    /**
     * Extractions data from Paginator
     *
     * @param Paginator $paginator
     * @return array
     */
    public function getDataFromBookingsPaginator(Paginator $paginator)
    {
        $data = [
            'bookings' => [],
            'users' => [],
            'hotels' => [],
            'meta' => [
                'total_pages' => PaginatorHelper::calculatePages($paginator->count(), BookingsFilterParams::LIMIT),
            ],
        ];

        $usersIds = [];
        $hotelsIds = [];
        $strict = true;

        foreach ($paginator as $booking) {

            /** @var BookingRequest $booking */
            $data['bookings'][] = BookingDTO::fromBookingRequest($booking);

            $user = $booking->getUser();
            if ($user && $user->getId() && !in_array($user->getId(), $usersIds, $strict)) {
                $usersIds[] = $user->getId();
                $data['users'][] = $this->userDTOProvider->getUserDTOFromUser($user);
            }

            $hotel = $this->getHotel($booking);
            if ($hotel && $hotel->getId() && !in_array($hotel->getId(), $hotelsIds, $strict)) {
                if ($hotel instanceof Hotel) {
                    $dto = HotelDTO::fromTravelGuru($hotel);
                    $hotelsIds[] = $dto->id;
                    $data['hotels'][] = $dto;
                }
                if ($hotel instanceof HotelPro) {
                    $dto = HotelDTO::fromHotelsPro($hotel);
                    $hotelsIds[] = $dto->id;
                    $data['hotels'][] = $dto;
                }
            }
        }

        return $data;
    }

    /**
     * @param BookingRequest $booking
     * @return Hotel|HotelPro|null
     */
    private function getHotel(BookingRequest $booking)
    {
        switch ($booking->getMetaData()->serviceName) {
            case HotelsListApiManagerHotelsPro::NAME:
                return $this->hotelProRepository->findHotelByHotelCode($booking->getHotelCode());
                break;
            case HotelsListApiManagerTravelGuru::NAME:
                return $this->travelGuruHotelRepository->find($booking->getHotelCode());
                break;
        }
    }

}
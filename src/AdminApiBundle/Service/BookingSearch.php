<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 12.11.2015
 * Time: 9:58
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\Model\BookingsFilterParams;
use AppBundle\Entity\BookingRequest;
use AppBundle\Repository\BookingRequestRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Pagination and searching bookings
 *
 * Class BookingSearch
 * @package AdminApiBundle\Service
 */
class BookingSearch
{

    /**
     * @var BookingRequestRepository
     */
    private $bookingRepository;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var array
     */
    private $queryParameters = [];

    /**
     * BookingHandler constructor.
     * @param BookingRequestRepository $bookingRepository
     */
    public function __construct(BookingRequestRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * Returns Paginator for correctly display bookings
     *
     * @param BookingsFilterParams $params
     * @return Paginator
     */
    public function getBookingsPaginatorByParams(BookingsFilterParams $params)
    {
        $paginator = new Paginator($this->getQueryBookingsByParams($params), $fetchJoinCollection = true);

        return $paginator;
    }

    /**
     * Returns SQL query for select all bookings and sorting these from first to last
     *
     * @param BookingsFilterParams $params
     * @return \Doctrine\ORM\Query
     */
    private function getQueryBookingsByParams(BookingsFilterParams $params)
    {
        $this->queryBuilder = $this->bookingRepository->createQueryBuilder('b');
        $this->queryBuilder->addSelect('b');

        $this->applyFilter($params);

        $this->queryBuilder->orderBy('b.createdAt', 'DESC');

        $this->queryBuilder->setParameters($this->queryParameters);

        return $this->queryBuilder->getQuery();
    }

    /**
     * Filtering incoming params
     *
     * @param BookingsFilterParams $params
     */
    private function applyFilter(BookingsFilterParams $params)
    {
        if (is_int($params->getLimit())) {
            $this->queryBuilder->setMaxResults($params->getLimit());
        }
        if (is_int($params->getOffset())) {
            $this->queryBuilder->setFirstResult($params->getOffset());
        }
        if ($params->getType()) {
            $this->applyTypeCondition($params->getType());
        }
        if ($params->getSearchQuery()) {
            $this->applySearchQueryCondition($params->getSearchQuery());
        }
    }

    /**
     * Chooses the type and builds query
     *
     * @param int $type
     */
    private function applyTypeCondition($type)
    {
        switch ($type) {

            case BookingsFilterParams::TYPE_ACTIVE:

                $this->queryBuilder->andWhere('b.status <> :cancelled AND b.status <> :refunded');
                $this->queryParameters['cancelled'] = BookingRequest::STATUS_CANCELLED;
                $this->queryParameters['refunded'] = BookingRequest::STATUS_REFUNDED;
                break;

            case BookingsFilterParams::TYPE_CANCELED:

                $this->queryBuilder->andWhere('b.status = :cancelled OR b.status = :refunded');
                $this->queryParameters['cancelled'] = BookingRequest::STATUS_CANCELLED;
                $this->queryParameters['refunded'] = BookingRequest::STATUS_REFUNDED;
                break;
        }
    }

    /**
     * Adds associations with tables hotel and user into Search Query
     *
     * @param string $searchQuery
     */
    private function applySearchQueryCondition($searchQuery)
    {
        $this->queryBuilder->join('b.user', 'u');

        $conditions = [
            '(u.firstName LIKE :searchQuery)',
            '(u.lastName LIKE :searchQuery)',
            '(u.fullName LIKE :searchQuery)',
            '(b.publicBookingId = :exactSearchQuery)',
        ];

        $this->queryBuilder->andWhere(implode(' OR ', $conditions));
        $this->queryParameters['searchQuery'] = '%'.$searchQuery.'%';
        $this->queryParameters['exactSearchQuery'] = $searchQuery;
    }

}
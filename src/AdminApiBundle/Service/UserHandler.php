<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 26.11.2015
 * Time: 10:03
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\Model\UserFilterParams;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Searches and presents users data in JSON format
 *
 * Class UserHandler
 * @package AdminApiBundle\Service
 */
class UserHandler
{

    /**
     * @var UserSearch
     */
    private $userSearch;

    /**
     * @var UserToJsonFormatter
     */
    private $userToJsonFormatter;

    /**
     * UserHandler constructor.
     * @param UserSearch $userSearch
     * @param UserToJsonFormatter $userToJsonFormatter
     */
    public function __construct(UserSearch $userSearch, UserToJsonFormatter $userToJsonFormatter)
    {
        $this->userSearch = $userSearch;
        $this->userToJsonFormatter = $userToJsonFormatter;
    }

    /**
     * Gets users data
     *
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function getUsers(ParamFetcherInterface $fetcher)
    {
        $params = UserFilterParams::fromFetcher($fetcher);
        $usersPaginator = $this->userSearch->getUsersPaginatorByParams($params);
        $usersData = $this->userToJsonFormatter->getDataFormUserPaginator($usersPaginator);

        return $usersData;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 19.11.2015
 * Time: 12:17
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\DTO\ProfileDTO;
use AdminApiBundle\Exception\WrongPasswordException;
use AdminApiBundle\Form\ProfileFormType;
use ApiBundle\Exception\InvalidFormException;
use AppBundle\Entity\File;
use AppBundle\Entity\User;
use AppBundle\Repository\FileRepository;
use AppBundle\Service\User\UserProvider;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMInvalidArgumentException;
use Gedmo\Exception\UploadableInvalidFileException;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * Works with data from User Form
 *
 * Class ProfileHandler
 * @package AdminApiBundle\Service
 */
class ProfileHandler
{

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ProfileDTOProvider
     */
    private $profileDTOProvider;

    /**
     * @var FileRepository
     */
    private $fileRepository;

    /**
     * @var UploadableManager
     */
    private $uploadableManager;

    /**
     * @var UserPasswordEncoder
     */
    private $userPasswordEncoder;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var User
     */
    private $user;

    /**
     * ProfileHandler constructor.
     * @param UserProvider $userProvider
     * @param FormFactory $formFactory
     * @param TokenStorageInterface $tokenStorage
     * @param ProfileDTOProvider $profileDTOProvider
     * @param FileRepository $fileRepository
     * @param UploadableManager $uploadableManager
     * @param UserPasswordEncoder $userPasswordEncoder
     * @param EntityManager $entityManager
     */
    public function __construct(
        UserProvider $userProvider,
        FormFactory $formFactory,
        TokenStorageInterface $tokenStorage,
        ProfileDTOProvider $profileDTOProvider,
        FileRepository $fileRepository,
        UploadableManager $uploadableManager,
        UserPasswordEncoder $userPasswordEncoder,
        EntityManager $entityManager
    ) {
        $this->userProvider = $userProvider;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->profileDTOProvider = $profileDTOProvider;
        $this->fileRepository = $fileRepository;
        $this->uploadableManager = $uploadableManager;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * Gets profileDTO if users data is correct
     *
     * @param Request $request
     * @return static
     * @throws WrongPasswordException
     */
    public function put(Request $request)
    {
        $this->checkUserPassword($request);
        $user = $this->processForm($request, Request::METHOD_PUT);
        $profileDTO = $this->profileDTOProvider->getProfileDTOFromUser($user);

        return $profileDTO;
    }

    /**
     * Adds avatar to current user
     *
     * @param Request $request
     * @return ProfileDTO
     * @throws UploadableInvalidFileException
     */
    public function putAvatar(Request $request)
    {
        $file = $request->files->get('file', null);
        $user = $this->getUser();

        if ($file instanceof UploadedFile && $user instanceof User) {

            $fileEntity = new File();
            $this->uploadableManager->markEntityToUpload($fileEntity, $file);
            $this->fileRepository->save($fileEntity);

            $oldAvatar = $user->getAvatar();
            if ($oldAvatar instanceof File) {
                $this->fileRepository->remove($oldAvatar);
            }
            $user->setAvatar($fileEntity);
            $this->userProvider->save($user);
            $profileDTO = $this->profileDTOProvider->getProfileDTOFromUser($user);

            return $profileDTO;
        }

        throw new UploadableInvalidFileException;
    }

    /**
     * Validates and saves data form user form
     *
     * @param Request $request
     * @param string $method
     * @return User|mixed
     * @throws InvalidFormException
     */
    private function processForm(Request $request, $method = Request::METHOD_PUT)
    {
        $user = $this->getUser();
        $form = $this->formFactory->create(
            new ProfileFormType(),
            $user,
            [
                'method' => $method,
            ]
        );

        $profileData = $this->getProfileDataFromRequest($request);

        $form->submit($profileData);
        if ($form->isValid()) {

            $this->userProvider->save($user);

            return $user;
        }

        throw new InvalidFormException($form);
    }

    /**
     * Extracts data from request
     *
     * @param Request $request
     * @return array
     */
    private function getProfileDataFromRequest(Request $request)
    {
        $user = $this->getUser();
        $request = $request->request;
        $deep = true;

        return [
            'firstName' => $request->get('profile[first_name]', $user->getFirstName(), $deep),
            'lastName' => $request->get('profile[last_name]', $user->getLastName(), $deep),
            'email' => $request->get('profile[email]', $user->getEmail(), $deep),
        ];
    }

    /**
     * Returns User after verifying the existence and updating persistent state of it
     *
     * @return User|mixed
     */
    private function getUser()
    {
        if (!$this->user instanceof User) {

            $user = $this->tokenStorage->getToken()->getUser();
            try {
                $this->entityManager->refresh($user);
            } catch (ORMInvalidArgumentException $e) {
                $currentUser = $this->entityManager->getRepository(User::class)->findBy(
                    ['username' => $this->tokenStorage->getToken()->getUser()->getUsername()]
                );
                $this->entityManager->flush($currentUser);
            }
            $this->user = $user;
        }

        return $this->user;
    }

    /**
     * Checks User password
     *
     * @param Request $request
     * @throws WrongPasswordException
     */
    private function checkUserPassword(Request $request)
    {
        $user = $this->getUser();
        $password = $request->request->get('profile[password]', '', $deep = true);

        if (!$this->userPasswordEncoder->isPasswordValid($user, $password)) {

            throw new WrongPasswordException;
        }
    }

}
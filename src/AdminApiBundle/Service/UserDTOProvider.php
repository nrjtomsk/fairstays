<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 25.11.2015
 * Time: 13:35
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\DTO\UserDTO;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class UserDTOProvider
 * @package AdminApiBundle\Service
 */
class UserDTOProvider
{

    /**
     * @var Router
     */
    private $router;

    /**
     * UserDTOProvider constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Gets UserDTO
     *
     * @param User $user
     * @return static
     */
    public function getUserDTOFromUser(User $user)
    {
        $userDTO = UserDTO::fromUser($user);

        if ($userDTO->avatarUrl) {

            $host = $this->router->getContext()->getHost();
            $userDTO->avatarUrl = '//'.$host.'/'.$userDTO->avatarUrl;
        }

        return $userDTO;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 26.11.2015
 * Time: 10:11
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\Model\UserFilterParams;
use AdminApiBundle\Util\PaginatorHelper;
use AppBundle\Entity\User;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Transforms Users data to JSON format
 *
 * Class UserToJsonFormatter
 * @package AdminApiBundle\Service
 */
class UserToJsonFormatter
{

    /**
     * @var UserDTOProvider
     */
    private $userDTOProvider;

    /**
     * UserToJsonFormatter constructor.
     * @param UserDTOProvider $userDTOProvider
     */
    public function __construct(UserDTOProvider $userDTOProvider)
    {
        $this->userDTOProvider = $userDTOProvider;
    }

    /**
     * Extractions data from Paginator
     *
     * @param Paginator $paginator
     * @return array
     */
    public function getDataFormUserPaginator(Paginator $paginator)
    {
        $data = [
            'users' => [],
            'meta' => [
                'total_pages' => PaginatorHelper::calculatePages($paginator->count(), UserFilterParams::LIMIT),
            ],
        ];

        foreach ($paginator as $userItem) {

            /** @var User $user */
            $user = $userItem['user'];
            $userDTO = $this->userDTOProvider->getUserDTOFromUser($user);
            $userDTO->bookingsCount = $userItem['bookingsCount'];

            $data['users'][] = $userDTO;
        }

        return $data;
    }

}
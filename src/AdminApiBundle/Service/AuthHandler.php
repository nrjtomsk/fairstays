<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 10.11.2015
 * Time: 13:52
 */

namespace AdminApiBundle\Service;


use ApiBundle\Exception\InvalidCredentialsException;
use AppBundle\Entity\AuthToken;
use AppBundle\Entity\User;
use AppBundle\Repository\AuthTokenRepository;
use AppBundle\Service\User\UserProvider;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class AuthHandler
 *
 * Checks username and password. On success creates authToken
 *
 * @package AdminApiBundle\Service
 */
class AuthHandler
{

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @var AuthTokenRepository
     */
    private $authTokenRepository;

    /**
     * AuthHandler constructor.
     * @param UserProvider $userProvider
     * @param UserPasswordEncoder $passwordEncoder
     * @param AuthTokenRepository $authTokenRepository
     */
    public function __construct(
        UserProvider $userProvider,
        UserPasswordEncoder $passwordEncoder,
        AuthTokenRepository $authTokenRepository
    ) {
        $this->userProvider = $userProvider;
        $this->passwordEncoder = $passwordEncoder;
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param string $username
     * @param string $password
     * @return AuthToken
     * @throws InvalidCredentialsException
     */
    public function getAuthToken($username, $password)
    {
        try {
            /** @var User $user */
            $user = $this->userProvider->loadUserByUsername($username);

            if ($user instanceof User && $this->passwordEncoder->isPasswordValid($user, $password)) {

                $authToken = $this->authTokenRepository->createAuthToken($user);

                return $authToken;
            }
        } catch (UsernameNotFoundException $e) {

            throw new InvalidCredentialsException('Invalid user credentials');
        }

        throw new InvalidCredentialsException('Invalid user credentials');
    }

}
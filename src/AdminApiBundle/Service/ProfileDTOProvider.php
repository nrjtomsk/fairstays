<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 18.11.2015
 * Time: 12:04
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\DTO\ProfileDTO;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class ProfileDTOProvider
 * @package AdminApiBundle\Service
 */
class ProfileDTOProvider
{

    /**
     * @var Router
     */
    private $router;

    /**
     * ProfileDTOProvider constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Gets ProfileDTO
     *
     * @param User $user
     * @return static
     */
    public function getProfileDTOFromUser(User $user)
    {
        $profile = ProfileDTO::fromUser($user);

        if ($profile->avatarUrl) {

            $host = $this->router->getContext()->getHost();
            $profile->avatarUrl = '//'.$host.'/'.$profile->avatarUrl;
        }

        return $profile;
    }

}
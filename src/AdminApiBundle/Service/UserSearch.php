<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 11.11.2015
 * Time: 9:55
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\Model\UserFilterParams;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Pagination and searching Users
 *
 * Class UserSearch
 * @package AdminApiBundle\Service
 */
class UserSearch
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @var array
     */
    private $queryParameters = [];

    /**
     * UserHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Returns Paginator for correctly display Users
     *
     * @param UserFilterParams $params
     * @return Paginator
     */
    public function getUsersPaginatorByParams(UserFilterParams $params)
    {
        $paginator = new Paginator($this->getQueryUsersByParams($params), $fetchJoinCollection = true);

        return $paginator;
    }

    /**
     * Returns SQL query for SELECT Users and their bookings
     *
     * @param UserFilterParams $params
     * @return \Doctrine\ORM\Query
     */
    private function getQueryUsersByParams(UserFilterParams $params)
    {
        $this->queryBuilder = $this->userRepository->createQueryBuilder('u');
        $this->queryBuilder->select('u as user');
        $this->queryBuilder->addSelect('COUNT(b.id) as bookingsCount');

        $this->queryBuilder->leftJoin('u.bookingRequests', 'b');

        $this->applyFilter($params);

        $this->queryBuilder->groupBy('u.id');

        $this->queryBuilder->setParameters($this->queryParameters);

        return $this->queryBuilder->getQuery();
    }

    /**
     *  Filtering incoming params
     *
     * @param UserFilterParams $params
     */
    private function applyFilter(UserFilterParams $params)
    {
        if (is_int($params->getLimit())) {
            $this->queryBuilder->setMaxResults($params->getLimit());
        }
        if (is_int($params->getLimit())) {
            $this->queryBuilder->setFirstResult($params->getOffset());
        }
        if ($params->getFullName()) {
            $this->applyFullName($params->getFullName());
        }
    }

    /**
     * Adds a name search in the Query
     *
     * @param string $fullName
     */
    private function applyFullName($fullName)
    {
        $this->queryBuilder->andWhere('u.fullName LIKE :fullName');
        $this->queryParameters['fullName'] = '%'.$fullName.'%';
    }

}
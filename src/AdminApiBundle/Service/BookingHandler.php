<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.11.2015
 * Time: 18:15
 */

namespace AdminApiBundle\Service;


use AdminApiBundle\Model\BookingsFilterParams;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class BookingHandler
 * @package AdminApiBundle\Service
 */
class BookingHandler
{

    /**
     * @var BookingSearch
     */
    private $bookingSearch;

    /**
     * @var BookingToJsonFormatter
     */
    private $bookingToJsonFormatter;

    /**
     * BookingHandler constructor.
     * @param BookingSearch $bookingSearch
     * @param BookingToJsonFormatter $bookingToJsonFormatter
     */
    public function __construct(BookingSearch $bookingSearch, BookingToJsonFormatter $bookingToJsonFormatter)
    {
        $this->bookingSearch = $bookingSearch;
        $this->bookingToJsonFormatter = $bookingToJsonFormatter;
    }

    /**
     * Returns bookings list
     * @param ParamFetcherInterface $fetcher
     * @return array
     */
    public function getBookings(ParamFetcherInterface $fetcher)
    {
        $params = BookingsFilterParams::fromFetcher($fetcher);
        $bookingsPaginator = $this->bookingSearch->getBookingsPaginatorByParams($params);
        $bookingsData = $this->bookingToJsonFormatter->getDataFromBookingsPaginator($bookingsPaginator);

        return $bookingsData;
    }

}
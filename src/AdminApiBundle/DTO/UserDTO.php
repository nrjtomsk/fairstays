<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.11.2015
 * Time: 17:38
 */

namespace AdminApiBundle\DTO;


use AppBundle\Entity\File;
use AppBundle\Entity\User;

/**
 * Class UserDTO
 *
 * Creates a UserDTO for next serialization
 *
 * @package AdminApiBundle\DTO
 */
class UserDTO
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $fullName = '';

    /**
     * @var string
     */
    public $firstName = '';

    /**
     * @var string
     */
    public $lastName = '';
    /**
     * @var string
     */
    public $email = '';

    /**
     * @var string
     */
    public $phone = '';

    /**
     * @var string
     */
    public $avatarUrl = '';

    /**
     * @var int
     */
    public $bookingsCount;

    /**
     * @param User $user
     * @return static
     */
    public static function fromUser(User $user)
    {
        $userDTO = new static;
        $userDTO->id = $user->getId();
        $userDTO->fullName = $user->getFullName();
        $userDTO->firstName = $user->getFirstName();
        $userDTO->lastName = $user->getLastName();
        $userDTO->email = $user->getEmail();
        $userDTO->phone = $user->getPhoneNumber();

        $avatar = $user->getAvatar();
        if ($avatar instanceof File) {

            $userDTO->avatarUrl = $avatar->getPath();
        }

        return $userDTO;
    }

}
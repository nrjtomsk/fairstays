<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.11.2015
 * Time: 17:27
 */

namespace AdminApiBundle\DTO;


use AppBundle\Entity\BookingRequest;

/**
 * Class BookingDTO
 *
 * Creates a BookingDTO for next serialization
 *
 * @package AdminApiBundle\DTO
 */
class BookingDTO
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $createdAt = '';

    /**
     * @var float
     */
    public $amount = 0;

    /**
     * @var string
     */
    public $status = '';

    /**
     * @var string
     */
    public $dateCheckIn;

    /**
     * @var string
     */
    public $dateCheckOut;

    /**
     * @var int
     */
    public $user;

    /**
     * @var int
     */
    public $hotel;

    /**
     * @var string
     */
    public $publicId;

    /**
     * @var array
     */
    public static $statusLabels = [
        BookingRequest::STATUS_PROVISIONAL_PRE => 'Created',
        BookingRequest::STATUS_PROVISIONAL_COMPLETE => 'Provisional',
        BookingRequest::STATUS_PAYMENT_COMPLETE => 'Paid',
        BookingRequest::STATUS_CANCELLED => 'Canceled',
        BookingRequest::STATUS_CANCELLING => 'Cancelling',
        BookingRequest::STATUS_REFUNDED => 'Refunded',
        BookingRequest::STATUS_REFUNDING => 'Refunding',
        BookingRequest::STATUS_CONFIRMED => 'Confirmed',
        BookingRequest::STATUS_CONFIRMING => 'Confirming',
    ];

    /**
     * @param BookingRequest $bookingRequest
     * @return BookingDTO
     */
    public static function fromBookingRequest(BookingRequest $bookingRequest)
    {
        $bookingDTO = new static;
        $bookingDTO->id = $bookingRequest->getId();
        $bookingDTO->createdAt = $bookingRequest->getCreatedAt()->format('d M Y');
        $bookingDTO->amount = $bookingRequest->getPayuAmount();
        if (isset(static::$statusLabels[$bookingRequest->getStatus()])) {
            $bookingDTO->status = static::$statusLabels[$bookingRequest->getStatus()];
        }
        $bookingDTO->dateCheckIn = $bookingRequest->getTimeSpanStartDate()->format('Y-m-d');
        $bookingDTO->dateCheckOut = $bookingRequest->getTimeSpanEndDate()->format('Y-m-d');
        $bookingDTO->user = $bookingRequest->getUser()->getId();
        $bookingDTO->hotel = (string)$bookingRequest->getHotelCode();
        $bookingDTO->publicId = $bookingRequest->getPublicBookingId();

        return $bookingDTO;
    }

}
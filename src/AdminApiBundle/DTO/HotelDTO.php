<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 23.11.2015
 * Time: 17:42
 */

namespace AdminApiBundle\DTO;


use AppBundle\Entity\Hotel;
use AppBundle\Entity\HotelPro;

/**
 * Class HotelDTO
 *
 * Creates a HotelDTO for next serialization
 *
 * @package AdminApiBundle\DTO
 */
class HotelDTO
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $address = '';

    /**
     * @var string
     */
    public $imageUrl = '';

    /**
     * @param Hotel $hotel
     * @return static
     */
    public static function fromTravelGuru(Hotel $hotel)
    {
        $hotelDTO = new static;
        $hotelDTO->id = (string)$hotel->getId();
        $hotelDTO->name = $hotel->getName();
        $hotelDTO->address = $hotel->getAddressLine1();
        $hotelDTO->imageUrl = $hotel->getImagePath();

        return $hotelDTO;
    }

    public static function fromHotelsPro(HotelPro $hotel)
    {
        $hotelDTO = new static;
        $hotelDTO->id = (string)$hotel->getHotelCode();
        $hotelDTO->name = $hotel->getHotelName();
        $hotelDTO->address = $hotel->getHotelAddress();
        if (is_array($hotel->getImagesUrls()) && count($hotel->getImagesUrls())) {
            $hotelDTO->imageUrl = $hotel->getImagesUrls()[0];
        } else {
            $hotelDTO->imageUrl = ''; //todo add Image file
        }


        return $hotelDTO;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 18.11.2015
 * Time: 11:56
 */

namespace AdminApiBundle\DTO;


use AppBundle\Entity\File;
use AppBundle\Entity\User;

/**
 * Class ProfileDTO
 *
 * Creates a ProfileDTO for next serialization
 *
 * @package AdminApiBundle\DTO
 */
class ProfileDTO
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $avatarUrl;

    /**
     * @param User $user
     * @return static
     */
    public static function fromUser(User $user)
    {
        $profile = new static;
        $profile->id = $user->getId();
        $profile->firstName = $user->getFirstName();
        $profile->lastName = $user->getLastName();
        $profile->email = $user->getEmail();

        $avatar = $user->getAvatar();
        if ($avatar instanceof File) {

            $profile->avatarUrl = $avatar->getPath();
        }

        return $profile;
    }

}
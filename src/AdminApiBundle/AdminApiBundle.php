<?php

namespace AdminApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AdminApiBundle
 * @package AdminApiBundle
 */
class AdminApiBundle extends Bundle
{
}

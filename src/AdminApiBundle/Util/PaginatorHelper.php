<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 26.11.2015
 * Time: 10:25
 */

namespace AdminApiBundle\Util;

/**
 * Class PaginatorHelper
 *
 * Takes params: rows and perPage, returns total number of pages
 *
 * @package AdminApiBundle\Util
 */
class PaginatorHelper
{

    /**
     * @param int $rows
     * @param int $perPage
     * @return int
     */
    public static function calculatePages($rows, $perPage)
    {
        $totalPages = $rows / $perPage;

        if ((float)$totalPages === (float)floor($totalPages)) {

            $totalPages = floor($totalPages);

        } else {

            $totalPages = floor($totalPages) + 1;
        }

        return (int)$totalPages;
    }

}
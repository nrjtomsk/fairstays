<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 11.11.2015
 * Time: 17:29
 */

namespace AdminApiBundle\Model;


use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class UserFilterParams
 * @package AdminApiBundle\Model
 */
class UserFilterParams
{

    /**
     * Users limit
     */
    const LIMIT = 10;

    /**
     * Users offset
     */
    const OFFSET = 0;

    /**
     * @var string
     */
    private $fullName;

    /**
     * @var int
     */
    private $limit = self::LIMIT;

    /**
     * @var int
     */
    private $offset = self::OFFSET;

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @return static
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        $strict = true;
        $filter = new static;

        $limit = (int)$fetcher->get('per_page', $strict);
        $filter->setLimit($limit);

        $page = (int)$fetcher->get('page', $strict);
        $offset = ($page - 1) * $limit;
        $filter->setOffset($offset);

        $filter->setFullName($fetcher->get('fullName', $strict));

        return $filter;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 11.11.2015
 * Time: 17:29
 */

namespace AdminApiBundle\Model;


use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * Class BookingsFilterParams
 * @package AdminApiBundle\Model
 */
class BookingsFilterParams
{

    /**
     * Bookings limit
     */
    const LIMIT = 10;

    /**
     * Bookings offset
     */
    const OFFSET = 0;

    /**
     * Bookings with all statuses
     */
    const TYPE_ALL = 0;

    /**
     * Booking with active statuses
     */
    const TYPE_ACTIVE = 1;

    /**
     * Bookings with canceled status
     */
    const TYPE_CANCELED = 2;

    /**
     * @var int
     */
    private $limit = self::LIMIT;

    /**
     * @var int
     */
    private $offset = self::OFFSET;

    /**
     * @var int
     */
    private $type = self::TYPE_ALL;

    /**
     * @var string
     */
    private $searchQuery = '';

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_ALL,
        self::TYPE_ACTIVE,
        self::TYPE_CANCELED,
    ];

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSearchQuery()
    {
        return $this->searchQuery;
    }

    /**
     * @param string $searchQuery
     */
    public function setSearchQuery($searchQuery)
    {
        $this->searchQuery = $searchQuery;
    }

    /**
     * @param ParamFetcherInterface $fetcher
     * @return static
     */
    public static function fromFetcher(ParamFetcherInterface $fetcher)
    {
        $strict = true;
        $filter = new static;

        $limit = (int)$fetcher->get('per_page', $strict);
        $filter->setLimit($limit);

        $page = (int)$fetcher->get('page', $strict);
        $offset = ($page - 1) * $limit;
        $filter->setOffset($offset);

        $strict = false;
        $type = (int)$fetcher->get('type', $strict);
        if ($type && in_array($type, self::$types, true)) {
            $filter->setType($type);
        }

        $searchQuery = $fetcher->get('searchQuery', $strict);
        if ($searchQuery) {
            $filter->setSearchQuery($searchQuery);
        }

        return $filter;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: YarovikovYO
 * Date: 19.11.2015
 * Time: 12:58
 */

namespace AdminApiBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfileFormType
 *
 * Form for managing a profile Admin
 *
 * @package AdminApiBundle\Form
 */
class ProfileFormType extends AbstractType
{
    /**
     * @inheritdoc
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'firstName',
            'text',
            [
                'required' => false,
            ]
        );
        $builder->add(
            'lastName',
            'text',
            [
                'required' => false,
            ]
        );
        $builder->add(
            'email',
            'email',
            [
                'required' => false,
            ]
        );
    }

    /**
     * @inheritdoc
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\User',
            ]
        );
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getName()
    {
        return 'admin_bundle_profile_form_type';
    }

}